import { LightningElement, wire, track, api } from 'lwc';
import HV_ROLES from '@salesforce/schema/Contact.HV_Role__c';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
export default class HvContactRoles extends LightningElement {
    @api conRecordTypeId;
    rolesOptions;

    @wire(getPicklistValues,{ recordTypeId: '$conRecordTypeId', fieldApiName: HV_ROLES })
    picklistData({data, error}){
        if(data){
            this.rolesOptions = [];
            console.log('data pic',data)
            data.values.forEach(pickVal => {
                this.rolesOptions.push({label: pickVal.label, value: pickVal.value });
            });
        }
        if(error){
            
            console.log(error);

        }
    }

    changeRole(e){
        const evt = new CustomEvent('handlerole', {
            detail: {
                role : e.target.value
            }
        });
        this.dispatchEvent(evt);
    }
    expandBody(e){
        console.log("sfefse");
        const evt = new CustomEvent('handlerole', {
            detail: {
                role : 'expand'
            }
        });
        this.dispatchEvent(evt);
    }
}