import { LightningElement, wire, track } from 'lwc';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { createRecord } from 'lightning/uiRecordApi';
import { updateRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import currentUserId from '@salesforce/user/Id';
import CURRENT_USER_CLINIC from '@salesforce/schema/User.Contact.AccountId';
import CURRENT_USER_CLINIC_NAME from '@salesforce/schema/User.Contact.Account.Name';
import CURRENT_USER_Name from '@salesforce/schema/User.Contact.Name';
import CURRENT_USER_EMAIL from '@salesforce/schema/User.Contact.Email';
import CURRENT_USER_ROLE from '@salesforce/schema/User.Contact.HV_Role__c';
import ACCOUNT_OBJECT from '@salesforce/schema/Account';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import GENDER_FIELD from '@salesforce/schema/Contact.HealthCloudGA__Gender__c';
import PREFERRED_CONTACT from '@salesforce/schema/Contact.Preferred_Contact_Method__c';
import getClinicFieldsMapping from '@salesforce/apex/HVPatientRegistrationController.getClinicFieldsMapping';
import generatePatientIds from '@salesforce/apex/HVPatientRegistrationController.generatePatientIds';
import createUser from '@salesforce/apex/HVPatientRegistrationController.createUser';
import updateExistingPatient from '@salesforce/apex/HVPatientRegistrationController.updateExistingPatient';
import sendExistingUserNotiftn from '@salesforce/apex/HVPatientRegistrationController.sendExistingUserNotiftn';
import bovIdNotAvailableNotifyTeam from '@salesforce/apex/HVPatientRegistrationController.bovIdNotAvailableNotifyTeam';
import getSobjectRecord from '@salesforce/apex/HVQueryUtility.getSingleRecordImprative';
import getSobjectRecords from '@salesforce/apex/HVQueryUtility.getMultipleRecordImprative';
import validateUser from '@salesforce/apex/HVExistingUserValidationController.validateUser';
import HV_Same_Facility_Active_User from '@salesforce/label/c.HV_Same_Facility_Active_User';
import HV_Different_Facility_and_Active_User from '@salesforce/label/c.HV_Different_Facility_and_Active_User';
import HV_Same_Facility_Inactive_User from '@salesforce/label/c.HV_Same_Facility_Inactive_User';
import HV_Same_Facility_Inactive_User_Reset_Password from '@salesforce/label/c.HV_Same_Facility_Inactive_User_Reset_Password';
import HV_Different_Facility_Inactive_User from '@salesforce/label/c.HV_Different_Facility_Inactive_User';
import HV_Ask_for_reset_password from '@salesforce/label/c.HV_Ask_for_reset_password';
import deprovisionBox from  '@salesforce/apex/HVInventoryManagementController.deprovisionBox'

const USERFIELDS = [CURRENT_USER_CLINIC, CURRENT_USER_Name, CURRENT_USER_EMAIL, CURRENT_USER_CLINIC_NAME, CURRENT_USER_ROLE]; //fields to query form user object
const ROLE = 'Patient'; //default role set for this component

export default class HvPatientRegistrationDynamic extends LightningElement {
    @track contactEmailsM = [];
    @track personAccountObject = {}; //store all the inputfields values
    @track existingPatientData = {};
    @track mapFieldnamesAndValues = new Map(); //map of clinic metadata field names
    @track mapClinicAndExistingUser = new Map(); //map of clinic and existing user
    @track mapRequiredFieldnamesAndValues = new Map(); //map of required clinic metadata field names
    @track generalSectionFieldsToDisplay; //list of general fields from metadata that needs to be displayed
    @track contactInfoSectionFieldsToDisplay; //list of contact section fields from metadata that needs to be displayed[This can be Used in future if sectionwise fields needs to be shown ]
    @track accountCreationSectionFieldsToDisplay; // list of account creation section fields from metadata that needs to be displayed[This can be Used in future if sectionwise fields needs to be shown ]
    @track requiredFields; //list of required fields from metadata
    @track departmentList; //list of departments for the clinic
    @track boxObject = {};
    @track prescribedByOptions;
    genderOptions; //picklist options for Gender field
    preferredcontactoptions; //picklist options for Preferred Contact field
    patientIdsOptions; //picklist options for PatientId/Username field
    selectedOptInValue = 'Yes'; //Is user opt in for two factor authentaction - default to Yes
    currentStep = "1"; //control the current Path Step
    currentTab = 1; //control the current tab
    isBoxExist = 'Yes'; //do patient have halo box? - store default value 'Yes'
    isHavBox = true; //boolean var to check box exists
    isBoxModalOpen = false; //if box exists open modal to show all its related pheripheral devices
    clinicFieldsData; //store all the clinics and there fields metadata values 
    errormessage; //show errormessage if any
    currentUserClinic; //store logged in user's Clinic Id  
    clinicName; //store current user clinic name and assign it to patient  
    maxdate; //boolean to check validation for date of birth field 
    notes; //store all the patient notes in notes object [store value for Notes field]
    cellphoneRequired; //boolean to validate if cellphone field is required
    homephoneRequired;//boolean to validate if homephone field is required
    emailRequired; //boolean to validate if email field is required
    activeSectionMessage; //message to show in accordian as per section
    confirmPassword; //store temporary confirm password field value to match it with actual password field
    prevDisable = true; //boolean to validate if Previos button in footer needs to be displayed or not
    nextDisable = false; //boolean to validate if Next button in footer needs to be displayed or not
    nextLabel = "Next"; //lable for the Next button in footer [this gets changed as per the Steps]
    isPatientToBeCreated = false; //is patient created successfully
    personAccountPatientRecId; //store value of patient Id(person account record Id) if patient created successfully
    boxmsg;
    boxWithNodevice;
    departmentOptions; //picklist options for department field
    showSpinner = false; //show or hide spinner
    selectedDepartment; //store selected department values
    isExistingPatientToBeUpdated; //if patient already exists check if it needs tobe updated 
    sendExistingUserNotftnToAdmin; //if patient alreadyexists in different clinic check if notification to be sent to livmor admin team
    activeSections = ['A'];
    showphonenumber = true;
    existinguserheader;
    existinguserbody;
    existingusercloselabel;
    existinguseroklabel;
    showExistingUserModal = false;
    patientOldClinic = '';
    patientnewClinic = '';
    rolename; //logged in user role;
    showPatientProfile = false;
    patientUserData;
    showfacilityselect = false; //if livmor admin registers the patient, ask to select facility to create patient in
    showPrescribedByList;
    selectedPrescribedBy; //if livmor admin registers the patient, ask to select prescribed by and save the selected value

    get boxExistsOption() { //return box exists field picklist options
        return [
            { label: 'Yes', value: 'Yes' },
            { label: 'No', value: 'No' },
        ];
    }

    get optionsforOptInTwoFactor() { //return two factor authentication field options
        return [
            { label: 'Yes', value: 'Yes' },
            { label: 'No', value: 'No' },
        ];
    }

    get stepCondition1() { //return if Step 1 of Patient Registration needs to be displayed
        return this.currentTab == 1 ? true : false;
    }
    get stepCondition2() { //return if Step 2 of Patient Registration needs to be displayed
        return this.currentTab == 2 ? true : false;
    }
    get stepCondition3() { //return if Step 3 of Patient Registration needs to be displayed
        return this.currentTab == 3 ? true : false;
    }

    connectedCallback() { //on load of component 
        let maxBirthdayDate = new Date();
        maxBirthdayDate.setFullYear(maxBirthdayDate.getFullYear() - 18);
        this.maxdate = this.formatDate(maxBirthdayDate); //set the validation for Birthdate field that needs to 18 years and above
    }

    renderedCallback() { //handle dynamic form [show dynamic fields as per clinic]
        if (this.mapFieldnamesAndValues.size > 0) { //check if clinic fields metadata map has some values i.e. clinic field mapping exists
            this.template.querySelectorAll('div').forEach(field => { //iterate on each template div component to check if they are present in metadata
                if (field.dataset.id && field.dataset.id !== 'printblock') { //to validate the div and metadata fields use - div's 'data-id' attribute and metadata map's key 
                    if (!this.mapFieldnamesAndValues.has(field.dataset.id)) { //if metadata map does not contains the div present in template [data-id should be same as metadata field name]
                        let fieldname = "[data-id='" + field.dataset.id + "']";
                        this.template.querySelectorAll(fieldname).forEach(fieldToHide => {
                            fieldToHide.className = 'HideField'; //hide the div from UI
                        })
                    } //else fields will be visible on form                  
                }
            });
            this.template.querySelectorAll('lightning-layout-item').forEach(field => { //iterate on each template div component to check if they are present in metadata
                if (field.dataset.id && field.dataset.id !== 'printblock') { //to validate the div and metadata fields use - div's 'data-id' attribute and metadata map's key 
                    if (!this.mapFieldnamesAndValues.has(field.dataset.id)) { //if metadata map does not contains the div present in template [data-id should be same as metadata field name]
                        let fieldname = "[data-id='" + field.dataset.id + "']";
                        this.template.querySelectorAll(fieldname).forEach(fieldToHide => {
                            fieldToHide.className = 'HideField'; //hide the div from UI
                        })
                    } //else fields will be visible on form                  
                }
            });

            if (this.isExistingPatientToBeUpdated) { //if patient already exists in same clinic make all the step 1 fields disabled to edit
                this.template.querySelectorAll('lightning-layout-item').forEach(field => {
                    if (field.dataset.id && field.dataset.id !== 'printblock') {
                        let fieldname = "[data-id='" + field.dataset.id + "']";
                        this.template.querySelectorAll(fieldname).forEach(fieldToHide => {
                            fieldToHide.disabled = true; //make field disabled in layout
                        });
                    }
                });
                this.template.querySelectorAll('lightning-input').forEach(field => {
                    if (field.name == 'FirstName' || field.name == 'LastName' || field.name == 'HV_Password__pc' || field.name == 'confirmPassword' || field.name == 'showpassword' || field.name == 'HV_Patient_ID__pc') { //if input fields name in required map key
                        field.disabled = true; //make field disabled in layout
                    }
                });
                this.template.querySelectorAll('lightning-combobox').forEach(field => {
                    if (field.name == 'HV_Patient_ID__pc') { //if input fields name in required map key
                        field.disabled = true; //make field disabled in layout
                    }
                });
                let notesfname = 'notes';
                let fieldname = "[data-id='" + notesfname + "']";
                this.template.querySelectorAll(fieldname).forEach(field => { //iterate on each template div component to check if they are present in metadata
                    field.className = 'HideField'; //hide the div from UI
                });
            }
        }
        if (this.mapRequiredFieldnamesAndValues.size > 0) { //check required fields map and iterate through all input fields
            this.template.querySelectorAll('lightning-input').forEach(field => {
                if (this.mapRequiredFieldnamesAndValues.has(field.name)) { //if input fields name in required map key
                    field.required = true; //make field required in layout
                }
            });
            this.template.querySelectorAll('lightning-combobox').forEach(field => {
                if (this.mapRequiredFieldnamesAndValues.has(field.name)) {
                    field.required = true;
                }
            });
            this.template.querySelectorAll('lightning-textarea').forEach(field => {
                if (this.mapRequiredFieldnamesAndValues.has(field.name)) {
                    field.required = true;
                }
            });
            this.template.querySelectorAll('lightning-radio-group').forEach(field => {
                if (this.mapRequiredFieldnamesAndValues.has(field.name)) {
                    field.required = true;
                }
            });
            this.template.querySelectorAll('lightning-dual-listbox').forEach(field => {
                if (this.mapRequiredFieldnamesAndValues.has(field.name)) {
                    field.required = true;
                }
            });
            this.template.querySelectorAll('lightning-input-address').forEach(field => {
                if (this.mapRequiredFieldnamesAndValues.has(field.name)) {
                    field.required = true;
                }
            });
        }

    }

    formatDate(date) { //funation to format the date for maxdate validation
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }

    @wire(getObjectInfo, { objectApiName: CONTACT_OBJECT })
    contactMetadata; //lds method to get schema and details of Contact object

    @wire(getObjectInfo, { objectApiName: ACCOUNT_OBJECT })
    accountMetadata; //lds method to get schema and details of Contact object

    @wire(getPicklistValues, { //lds method to get picklist values for gender field
        recordTypeId: '$contactMetadata.data.defaultRecordTypeId', //use schema method
        fieldApiName: GENDER_FIELD
    }) genderOptionslist({ error, data }) {
        if (data) {
            const options = [];
            data.values.map(row => {
                options.push({ label: row.label, value: row.value });
            });
            this.genderOptions = options; //set values in genderOptions variable
            this.errormessage = '';
        }
        if (error) {
            this.errormessage = error.body.message; //set error if any
            this.genderOptions = [];
        }
    }

    @wire(getPicklistValues, { //lds method to get picklist values for preferred contact method field
        recordTypeId: '$contactMetadata.data.defaultRecordTypeId', //use schema method
        fieldApiName: PREFERRED_CONTACT
    }) preferredcontactoptionslist({ error, data }) {
        if (data) {
            const options = [];
            data.values.map(row => {
                options.push({ label: row.label, value: row.value });
            });
            this.preferredcontactoptions = options; //set values in preferredcontactoptions variable
            this.errormessage = '';
        }
        if (error) {
            this.errormessage = error.body.message; //set error if any
            this.preferredcontactoptions = [];
        }
    }

    @wire(getRecord, { recordId: currentUserId, fields: USERFIELDS })
    loggedInUserData({ error, data }) { //lds method to get current logged in user details usinf schema
        if (data) {
            this.currentUserClinic = data.fields.Contact.value.fields.AccountId.value; //logged in user clinic/account Id
            this.clinicName = data.fields.Contact.value.fields.Account.value.fields.Name.value; //set default user clinic name
            this.personAccountObject.HV_Prescribed_By__pc = data.fields.Contact.value.fields.Name.value; //logged in user name 
            this.personAccountObject.HV_Prescribed_By_Email__pc = data.fields.Contact.value.fields.Email.value; //logged in user email 
            this.rolename = data.fields.Contact.value.fields.HV_Role__c.value;
            if (this.rolename == 'Livmor Admin') {
                this.showfacilityselect = true;
                this.showPrescribedByList = true;
                getSobjectRecords({ objectname: 'Contact', fieldnames: 'id,name,email,HV_Role__c,(select contactid from users)', conditions: ' Where HV_Role__c = \'' + 'Physician' + '\'and id in (select contactid from user where isactive=true)' })
                    .then(res => {
                        if (res.length > 0) { //if email already in use show error to use another email address
                            let options = [];
                            res.forEach((ele) => {
                                options.push({ label: ele.Name, value: ele.Name });
                                this.contactEmailsM.push({key:ele.Name, value:ele.Email});
                            });
                            this.prescribedByOptions = options;
                            console.log('######### this.contactEmailsM: ' + JSON.stringify(this.contactEmailsM));
                        }
                    })
                    .catch(error => {
                        this.errormessage = error.body.message;
                    })

            }
            else {
                this.callMetaData(); //call method to get clinic and fields mapping from metadata
            }
        }
        if (error) {
            this.errormessage = error.body.message; //set error if any
        }
    }

    callMetaData() { //method to get clinic and fields mapping from metadata viz. ClinicFieldsMapping
        getClinicFieldsMapping({ clinicId: this.currentUserClinic }) //call apex method as imperative
            .then(data => {
                if (data) {
                    console.log('data',data);
                    this.clinicName = data.MasterLabel; //get mapping only for current logged in user's clinic
                    this.personAccountObject.HV_Patient_Clinic__c = data.HV_Clinic_Id__c; //store clinic name as per metadata
                    if (data.HV_GeneralSectionFields__c) {
                        console.log('availablefields',data.HV_GeneralSectionFields__c);
                        data.HV_GeneralSectionFields__c.split(",").map(field => { 
                            field.trim();
                            this.mapFieldnamesAndValues.set(field.trim(), field.trim());
                        }
                        );//store list of general fields from metadata that needs to be displayed 
                    }
                    /*this.contactInfoSectionFieldsToDisplay = data.HV_ContactInformationSectionFields__c.split(",").map(field =>
                        field.trim()
                    ); //store list of contact information section fields from metadata that needs to be displayed [USE IN FUTURE IF NEEDED] 
                    this.accountCreationSectionFieldsToDisplay = data.HV_AccountInformationSectionFields__c.split(",").map(field =>
                        field.trim()
                    ); //store list of account creation section fields from metadata that needs to be displayed [USE IN FUTURE IF NEEDED] */
                    if (data.HV_RequiredFields__c) {
                        data.HV_RequiredFields__c.split(",").map(field => {
                            field.trim();
                            this.mapRequiredFieldnamesAndValues.set(field.trim(), field.trim());
                        }
                        ); //store list of required fields from metadata
                    }
                    const optionsDepart = [];
                    data.HV_Department_Values__c.split(",").map(field => {
                        field.trim();
                        optionsDepart.push({ label: field.trim(), value: field.trim() });
                    });
                    this.departmentOptions = optionsDepart;
                    this.errormessage = '';
                    return data;                    
                }
            })
            .catch(error => {
                console.log('error',error);
                this.errormessage = error.body.message; //set error message if any
            });
    }

    inputFieldOnchange(event) {
        this.boxmsg = ''; 
        //generic onchange function for all input fields present on template
        if (event.target.name === 'notes') { //onchange specific for notes field
            this.notes = event.target.value;
            return;
        }
        if (event.target.name == 'HV_Password__pc') { //onchange specific for confirmpassowrd field
            this.personAccountObject[event.target.name] = (event.target.value);
            if (this.confirmPassword) {
                this.confirmpasswordMatch(this.personAccountObject.HV_Password__pc, this.confirmPassword, 'confirmPassword'); //check passoword and confirm passowrd matches
            }
            return;
        }
        if (event.target.name == 'confirmPassword') { //onchange specific for confirmpassowrd field
            this.confirmPassword = event.target.value;
            this.confirmpasswordMatch(this.personAccountObject.HV_Password__pc, this.confirmPassword, event.target.name); //check passoword and confirm passowrd matches
            return;
        }
        if (event.target.name == 'FirstName' || event.target.name == 'LastName') {
            this.personAccountObject[event.target.name] = (event.target.value).toUpperCase();
            return;
        }
        if (event.target.name == 'Phone' || event.target.name == 'PersonHomePhone') { //onchange specific for phone fields to add '-'
            this.showphonenumber = false;
            this.personAccountObject[event.target.name] = (event.target.value).replace(/-/g, '');
            this.personAccountObject[event.target.name] = this.validatePhone(this.personAccountObject[event.target.name]);
            this.showphonenumber = true;
            return;
        }
        if (event.target.name == 'HV_isTwoFactorAuthentactionEnabled__pc') { //onchange specific for two factor authentaction field to store value
            this.selectedOptInValue = event.target.value;
            this.personAccountObject.HV_isTwoFactorAuthentactionEnabled__pc = event.target.value == 'Yes' ? true : false;
            return;
        }
        if (event.target.name === 'Preferred_Contact_Method__pc') { //onchange specific for preferred contact method field
            this.personAccountObject[event.target.name] = event.target.value;
            if (event.target.value == 'Cell Phone' || event.target.value == 'Cell Phone/Email') { //add required validation for email and phone as per perferred contact value
                this.cellphoneRequired = true;
            } else {
                this.cellphoneRequired = false;
            }
            if (event.target.value == 'Email' || event.target.value == 'Cell Phone/Email') { //add required validation for email and phone as per perferred contact value
                this.emailRequired = true;
            } else {
                this.emailRequired = false;
            }

            if(event.target.value == 'Home Phone'){
               this.homephoneRequired = true
            }else{
                this.homephoneRequired = false
            }
            return;
        }
        if (event.target.name === 'ShippingAddress') { //onchange specific for shipping address field
            this.personAccountObject.ShippingStreet = event.detail.street;
            this.personAccountObject.ShippingCity = event.detail.city;
            this.personAccountObject.ShippingCountry = event.detail.country;
            this.personAccountObject.ShippingState = event.detail.province;
            this.personAccountObject.ShippingPostalCode = event.detail.postalCode;
        }
        if (event.target.name === 'havBoxId') { //onchange specific for havBoxId field
            this.isBoxExist = event.target.value;
            if (this.isBoxExist == 'No') { //toggle for box exists or not
                this.isHavBox = false;
                this.boxObject.Name = '';
                this.personAccountObject.HV_Box_ID__pc = '';
                this.boxmsg = '';
                this.nextDisable = false;
            } else {
                this.isHavBox = true;
                this.personAccountObject.ShippingStreet = '';
                this.personAccountObject.ShippingCity = '';
                this.personAccountObject.ShippingCountry = '';
                this.personAccountObject.ShippingState = '';
                this.personAccountObject.ShippingPostalCode = '';
            }
            return;
        }
        if (event.target.name === 'HV_Department__pc') { //onchange specific for department field to store selected values as comaa seperated string
            this.selectedDepartment = event.detail.value;
            let valuesSelected = (event.detail.value).join(",");
            this.personAccountObject.HV_Department__pc = valuesSelected;
            return;
        }
        this.personAccountObject[event.target.name] = event.target.value; //generic onchange set target value for all remaining fields
    }

    validatePhone(phoneNumber) { //phone validation add hypen automatically
        var len = phoneNumber.length;
        if ((len > 3) && (phoneNumber[3] != '-')) {
            phoneNumber = [phoneNumber.slice(0, 3), '-', phoneNumber.slice(3)].join('');
        }
        if ((len > 7) && (phoneNumber[7] != '-')) {
            phoneNumber = [phoneNumber.slice(0, 7), '-', phoneNumber.slice(7)].join('');
        }
        return phoneNumber;
    }

    //call this function on onblur of email address - to check if email entered by user is already in use or not
    validateEmailAddress(event) {
        this.template.querySelectorAll('lightning-input').forEach((ele) => {
            if (ele.name == 'PersonEmail') {
                ele.setCustomValidity("");
                ele.reportValidity();
            }
        })
        getSobjectRecords({ objectname: 'User', fieldnames: ' id,Name,Email', conditions: ' Where email = \'' + event.target.value + '\'' })
            .then(res => {
                if (res.length > 0) { //if email already in use show error to use another email address
                    this.template.querySelectorAll('lightning-input').forEach((ele) => {
                        if (ele.name == 'PersonEmail') {
                            ele.setCustomValidity("Please use different Email as this is already used by other Patient");
                            ele.reportValidity();
                        }
                    })
                }
            })
            .catch(error => {
                this.errormessage = error.body.message;
            })
    }

    confirmpasswordMatch(pswrd, cnfrmPswrd, fieldName) { // Change password validation - Confirm password and Password should match
        if (pswrd != '' && cnfrmPswrd != '') {
            this.template.querySelectorAll('lightning-input').forEach((ele) => {
                if (ele.name == fieldName) {
                    if (cnfrmPswrd === pswrd) {
                        ele.setCustomValidity("");
                    } else {
                        ele.setCustomValidity("Entered password is not matching, please reenter the new password");
                    }
                    ele.reportValidity();
                }
            });
        }
    }

    togglePaswrd() { //toggle to show password/confirm password as plain text or masked 
        this.template.querySelectorAll('.inputTypesPass').forEach(input => {
            if (input.type == "password") {
                input.type = "text";
            } else if (input.type == "text") {
                input.type = "password";
            }
        });
    }

    handleToggleSection(event) { //method to handle accordian toggle sections and set the open section name
        // this.activeSectionMessage = 'Open section name:  ' + event.detail.openSections;
        const openSections = event.detail.openSections;

        if (openSections.length === 0) {
            this.activeSectionsMessage = 'All sections are closed';
        } else {
            this.activeSectionsMessage =
                'Open sections: ' + openSections.join(', ');
        }
    }

    callPatientId(event) { //imperative apex call to get all the possible unique patient Ids as an options for user to select
        if (this.personAccountObject.FirstName && this.personAccountObject.LastName) { //patientId calculated based on input fiest name and last name
            generatePatientIds({ firstname: this.personAccountObject.FirstName, lastname: this.personAccountObject.LastName })
                .then(result => {
                    const options = [];
                    result.map(row => {
                        options.push({ label: row, value: row });
                    });
                    this.patientIdsOptions = options; //set returned options
                    this.errormessage = '';
                })
                .catch(error => {
                    this.errormessage = error.body.message; //set error if any
                })
        }
        else {
            this.patientIdsOptions = [];
        }
    }

    changeTab(event) { //handle Step changes from path (Step1, Step2, Step3)
        this.boxmsg = ''
        event.preventDefault();
        if (this.validateFields()) {
            this.currentTab = event.target.value; //set selected tab value
            this.currentStep = '"' + this.currentTab + '"';
            if (this.currentTab == 1) { //set button visiblity and label as per selected tab                               
                this.prevDisable = true;
                this.nextDisable = false;
                this.nextLabel = "Next";
            } else if (this.currentTab == 2) { //set button visiblity and label as per selected tab
                this.prevDisable = false;
                this.nextDisable = false;
                this.nextLabel = "Next";
            } else if (this.currentTab == 3) { //set button visiblity and label as per selected tab
                this.nextDisable = false;
                this.prevDisable = false;
                this.nextLabel = "Verify and Activate";
            } else {
                this.nextLabel = "Next"; //set default button visiblity and label
                this.nextDisable = true;
                this.prevDisable = true;
            }
        }
    }

    closeModal() { //close the modal - opened to view the box and its related devices
        this.isBoxModalOpen = false;

    }

    boxChange(event) {
        this.boxmsg = '';
        this.boxObject.Name = event.target.value;
    }

    handleBoxData(event) {
        this.boxmsg = ''; 
        this.getBoxData().then(boxRes => {
            if (boxRes) {
                this.isBoxModalOpen = true;
            }
        })
    }

    getBoxData() {
        this.boxmsg  = ''; 
        if(this.boxObject.Name){

         console.log('this.boxObject.Name',this.boxObject.Name)
        return getSobjectRecord({
            objectname: 'HV_Box__c',
            fieldnames: ' id,Name,HV_Status__c, (SELECT Id From Contacts__r),(SELECT Id, HV_BDA__c FROM BPs__r where HV_User_Device_Status__c = \''+'Active'+'\'),' +
                '(SELECT Id, HV_BDA__c FROM Pulse_Oximeters__r where  HV_User_Device_Status__c = \''+'Active'+'\'),' +
                '(SELECT Id, HV_BDA__c FROM Spirometers__r where HV_User_Device_Status__c = \''+'Active'+'\'),' +
                '(SELECT Id, HV_IMEI__c FROM Tablets__r where HV_User_Device_Status__c = \''+'Active'+'\'),' +
                '(SELECT Id, HV_BDA__c FROM WSS__r where  HV_User_Device_Status__c = \''+'Active'+'\'),' +
                '(SELECT Id, Name, HV_Watch_ID__c FROM Watches__r where HV_User_Device_Status__c = \''+'Active'+'\'),' +
                '(SELECT Id, HV_BDA__c FROM Glucometers__r  where HV_User_Device_Status__c = \''+'Active'+'\'),' +
                '(Select Id,Name FROM QAs__r) ',
            conditions: ' Where Name = \'' + this.boxObject.Name + '\'' 
        })
            .then(res => {
                console.log('res',res)
                if (res) {
                   
                        if (res.Contacts__r) {
                            this.boxmsg = 'Box already associated';
                            return null;
                        } else { 
    
                            if (res['Watches__r'] || res['BPs__r'] || res['Pulse_Oximeters__r']
                                || res['Spirometers__r'] || res['Tablets__r'] || res['WSS__r']
                                || res['Glucometers__r'] || res['QAs__r']) {
                                this.boxObject.Id = res.Id;
                                this.boxObject.watchId = res['Watches__r']
                                this.boxObject.bp = res['BPs__r'];
                                this.boxObject.poMeter = res['Pulse_Oximeters__r'];
                                this.boxObject.spirometer = res['Spirometers__r'];
                                this.boxObject.tablet = res['Tablets__r'];
                                this.boxObject.wss = res['WSS__r'];
                                this.boxObject.glucometer = res['Glucometers__r'];
                                this.boxObject.qa = res['QAs__r'];
                                this.boxmsg = null;
                                this.boxWithNodevice = false;
                                return res;
                            } else { 
                                this.boxWithNodevice = true;
                                this.boxmsg = 'Box does not have any device';
                                return null;
                            }
                        }
                    
                   

                } else {

                    this.boxmsg = 'Box does not exist';
                    return null
                }

            })
            .catch(error => {
                const evt = new ShowToastEvent({ //show success toast
                    title: 'Error',
                    message: error.body.message,
                    variant: 'error',
                });
                this.dispatchEvent(evt);
                this.errormessage = error.body.message; //set error if any
                return null;

            })

        } else{
            this.boxmsg = '';
        }
    }
    havePeriphralDevice() {
        this.isBoxModalOpen = false;

        if (this.boxWithNodevice) {
            this.boxmsg = 'Please enter a box which has Devices';
        } else {
            this.boxmsg = '';
        }

    }
    cancleClick(event) { //cancel the registration process and go back
        this.dispatchEvent(new CustomEvent('backcall'));
    }

    previous(event) { //navigate to previous step
        if (this.currentTab > 1) {
            this.currentTab--;
            this.handleProgress(this.currentTab); //on step change set button visibility and label
        }
        if (this.currentTab == 2 && this.isExistingPatientToBeUpdated) {
            this.errorMessage = 'A Patient matching this registration already exists in this clinic and the user is inactive. Please add Box Id to the existing patient.';
        }
        else if (this.currentTab == 2 && this.sendExistingUserNotftnToAdmin) {
            this.errormessage = '';
        }
    }

    next(event) { //navigate to next step 
        this.boxmsg = ''; 
        if (this.rolename == 'Livmor Admin'){
            this.contactEmailsM.forEach(data => {
                if (data.key == this.personAccountObject.HV_Prescribed_By__pc) {
                    this.personAccountObject.HV_Prescribed_By_Email__pc = data.value;
                }
            })
        }

        if (this.validateFields()) {

            let patientRec;
            if (this.currentTab == 1) {
                this.showSpinner = true;
                //existing user validation - check if user already exists based on firstname,lastname,dob,phone and email fields
                validateUser({
                    accountRecord: this.personAccountObject,
                    contactRecord: null
                })
                    .then(result => {
                        console.log('result', result);
                        console.log('len', result.length);
                        if (result.length > 0) {
                            for (let i = 0; i < result.length; i++) {
                                if (this.mapClinicAndExistingUser.has(result[i].Contact.Account.HV_Patient_Clinic__c)) {
                                    this.mapClinicAndExistingUser.get(result[i].Contact.Account.HV_Patient_Clinic__c).push(result[i]);
                                }
                                else {
                                    let listOfExistingUsers = [];
                                    listOfExistingUsers.push(result[i]);
                                    this.mapClinicAndExistingUser.set(result[i].Contact.Account.HV_Patient_Clinic__c, listOfExistingUsers);
                                }
                            }
                            console.log('map', this.mapClinicAndExistingUser);
                        }
                        let existinguser = this.mapClinicAndExistingUser.has(this.personAccountObject.HV_Patient_Clinic__c) ? this.mapClinicAndExistingUser.get(this.personAccountObject.HV_Patient_Clinic__c) : result;
                        if (existinguser.length > 0) {
                            let i = 0;
                            if (existinguser.length > 1) {
                                i = (existinguser.length) - 1;
                            }
                            if ((existinguser[i].Contact.Account.HV_Patient_Clinic__c === this.personAccountObject.HV_Patient_Clinic__c) && existinguser[i].IsActive == true) {
                                this.showSpinner = false;
                                this.existinguserheader = 'Patient already active in this clinic';
                                this.existinguserbody = HV_Same_Facility_Active_User;
                                this.existingusercloselabel = '';
                                this.existinguseroklabel = 'OK';
                                this.showExistingUserModal = true;
                            } else if ((existinguser[i].Contact.Account.HV_Patient_Clinic__c === this.personAccountObject.HV_Patient_Clinic__c) && existinguser[i].IsActive == false) {
                                this.showSpinner = false;
                                this.existingPatientData = existinguser[i].Contact.Account;
                                this.existinguserheader = 'Patient already exists in this clinic and is inactive';
                                if (this.rolename == 'Clinical User' || this.rolename == 'Livmor Admin') {
                                    this.existinguserbody = HV_Same_Facility_Inactive_User_Reset_Password;
                                }
                                else {
                                    this.existinguserbody = HV_Same_Facility_Inactive_User;
                                }
                                this.existingusercloselabel = 'Cancel';
                                this.existinguseroklabel = 'Continue';
                                this.showExistingUserModal = true;
                            } else if ((existinguser[i].Contact.Account.HV_Patient_Clinic__c != this.personAccountObject.HV_Patient_Clinic__c) && existinguser[i].IsActive == true) {
                                this.showSpinner = false;
                                this.existinguserheader = 'Patient already active at another clinic';
                                this.existinguserbody = HV_Different_Facility_and_Active_User;
                                this.existingusercloselabel = 'Reject';
                                this.existinguseroklabel = 'Accept';
                                this.patientOldClinic = existinguser[i].Contact.Account.HV_Patient_Clinic__r.Name;
                                this.patientnewClinic = this.clinicName;
                                this.showExistingUserModal = true;
                            } else if (existinguser[i].Contact.Account.HV_Patient_Clinic__c != this.personAccountObject.HV_Patient_Clinic__c && existinguser[i].IsActive == false) {
                                this.showSpinner = false;
                                this.existinguserheader = 'Patient already exists at another clinic and is inactive';
                                this.existinguserbody = HV_Different_Facility_Inactive_User;
                                this.existingusercloselabel = 'Reject';
                                this.existinguseroklabel = 'Accept';
                                this.patientOldClinic = existinguser[i].Contact.Account.HV_Patient_Clinic__r.Name;
                                this.patientnewClinic = this.clinicName;
                                this.showExistingUserModal = true;
                            }
                        } else {
                            this.isPatientToBeCreated = true;
                            this.showSpinner = false;
                            this.currentTab++;
                            this.handleProgress(this.currentTab);
                        }
                    })
                    .catch(error => {
                        const evt = new ShowToastEvent({
                            title: 'Error',
                            message: 'Error' + error.body.message,
                            variant: 'error',
                        });
                        this.dispatchEvent(evt);
                        this.errormessage = 'Error';
                        this.showSpinner = false;
                    })
            }
            else if (this.currentTab == 2) {
                if (this.isExistingPatientToBeUpdated) {                    
                    this.errorMessage = 'A patient matching this registration already exists in this clinic, but the user is inactive.  Click Verify and Activate to activate this user.';
                }
                if (this.sendExistingUserNotftnToAdmin) {
                    this.errormessage = 'A Patient matching this registration already exists at another clinic. Click Verify and Activate If you’d like to transfer the data to this clinic.';
                }
                if (this.isBoxExist == 'Yes') {
                    this.boxmsg = '';
                    // update the patient record and assign box id                    
                    this.getBoxData().then(boxRes => {
                        if (boxRes) {
                            this.personAccountObject.HV_Box_ID__pc = boxRes.Id;
                            this.boxObject.Name = boxRes.Name;
                            this.currentTab++;
                            this.handleProgress(this.currentTab);
                        }
                    })
                }
                else {
                    this.boxmsg = '';
                    this.currentTab++;
                    this.handleProgress(this.currentTab); //on step change set button visibility and label
                }
            }
            else if (this.currentTab == 3) {
                if (this.isPatientToBeCreated) {
                    this.showSpinner = true;
                    this.createPatient();
                }
                if (this.isExistingPatientToBeUpdated) {
                    this.showSpinner = true;
                    //update existing user details with new user details by assigning boxid or updating shipping address
                    updateExistingPatient({ accountToUpdate: this.personAccountObject })
                        .then(result => {
                            if (this.isBoxExist == 'No') {
                                this.boxNotAvailable();
                            }
                            const evt = new ShowToastEvent({ //show success toast
                                title: 'Success',
                                message: 'Patient Details Updated Successfully!!',
                                variant: 'success',
                            });
                            this.dispatchEvent(evt);
                            this.showSpinner = false;
                            if (this.rolename == 'Clinical User' || this.rolename == 'Livmor Admin') {
                                this.existinguserheader = 'Patient Reset Password';
                                this.existinguserbody = HV_Ask_for_reset_password;
                                this.existingusercloselabel = 'NO';
                                this.existinguseroklabel = 'YES';
                                this.showExistingUserModal = true;
                            }
                            else {
                                this.dispatchEvent(new CustomEvent('backcall'));
                            }
                        })
                        .catch(error => {
                            this.showSpinner = false;
                            const evt = new ShowToastEvent({
                                title: 'Error',
                                message: 'Patient Details not Updated',
                                variant: 'error',
                            });
                            this.dispatchEvent(evt);
                            this.errormessage = error.body.message; //set error if any
                        })
                }
            }
        }
        else {
            this.activeSections = ['A', 'B', 'C'];
        }

    }

    createPatient() { //method to create person account record for patient
        //set default field values for patient (person account creation)
        //example - role, recordtype
        if (!this.personAccountObject.PersonEmail) { //if email is blank set default email as type - patientid@clinicname.com
            this.personAccountObject.PersonEmail = this.personAccountObject.HV_Patient_ID__pc + '@' + this.clinicName.split(' ').join('') + '.com';
        }
        this.personAccountObject.HV_Role__pc = ROLE;
        const rtis = this.accountMetadata.data.recordTypeInfos;
        this.personAccountObject.RecordTypeId = Object.keys(rtis).find(rti => rtis[rti].name === 'Person Account');
        const fields = this.personAccountObject; //set all input fields captured from from
        const recordInput = { apiName: 'Account', fields };
        console.log('@@@@@@@@@@@ before apex call');
        createRecord(recordInput) //use standard lds create person account Record Method
            .then(record => {
                if (record.id) { //if account/contact created successfully create user record for the personaccount
                    console.log('@@@@@@@@@@@ apex call triggered');
                    const notesTosend = this.notes;
                    this.personAccountPatientRecId = record.id;
                    this.personAccountObject.Id = record.id;
                    createUser({ conOrAccRecordId: record.id, notesForUser: notesTosend })
                        .then(result => {
                            const evt = new ShowToastEvent({ //show success toast
                                title: 'Success',
                                message: 'Patient Registered Successfully!!',
                                variant: 'success',
                            });
                            this.dispatchEvent(evt);
                            this.errormessage = '';
                            if (this.isBoxExist == 'No') { //check if patient has halo system 
                                this.boxNotAvailable(); //if not send notification to group o team with shipping address and patientid
                            }
                            if (this.sendExistingUserNotftnToAdmin) { //if user already exists in different clinic 
                                //send notification to livmoradmin team to transfer existing user details to new user details
                                sendExistingUserNotiftn({ patientRec: this.personAccountObject, oldclinic : this.patientOldClinic , newclinic : this.patientnewClinic})
                                    .then(result => {
                                        const evt = new ShowToastEvent({ //show success toast
                                            title: 'Success',
                                            message: 'Email sent to LivMor Admin team for transfer of Existing User details!!',
                                            variant: 'success',
                                        });
                                        this.dispatchEvent(evt);
                                    })
                                    .catch(error => {
                                        const evt = new ShowToastEvent({
                                            title: 'Error',
                                            message: 'Email not sent to LivMor Admin for transfer of Existing User details!!',
                                            variant: 'error',
                                        });
                                        this.dispatchEvent(evt);
                                        this.errormessage = error.body.message; //set error if any
                                    })
                            }
                            this.showSpinner = false;
                            this.dispatchEvent(new CustomEvent('backcall'));
                        })
                        .then(res => {
                            deprovisionBox({deprovisionVal:'In-Use',boxId:this.boxObject.Id})
                            .then(res => {
                                console.log(res);
                                
                                
                                
                                
                            })
                            .catch(e => {
                                console.log(e);
                                const evt = new ShowToastEvent({ //show success toast
                                    title: 'Error',
                                    message: e.body.message,
                                    variant: 'error',
                                });
                                this.dispatchEvent(evt);
                            })
                        })
                        .catch(error => {
                            console.log('error', error);
                            const evt = new ShowToastEvent({
                                title: 'Error',
                                message: 'Error in Patient Registeration ' + error.body.message,
                                variant: 'error',
                            });
                            this.dispatchEvent(evt);
                            this.errormessage = error.body.message; //set error if any
                            this.showSpinner = false;
                        })
                }
            })
            .catch(error => {
                console.log('error', error);
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: 'Error in Patient Registeration ' + error.body.message,
                    variant: 'error',
                });
                this.dispatchEvent(evt);
                this.errormessage = 'Error in Patient Registeration ' + error.body.message; //set error message if any
                this.showSpinner = false;
            });

    }

    boxNotAvailable() { //send email to Group O team - patient does not have box, ship box to shipping address
        bovIdNotAvailableNotifyTeam({ patientRec: this.personAccountObject })
            .then(result => {
                const evt = new ShowToastEvent({ //show success toast
                    title: 'Success',
                    message: 'Email sent to Group O Team!!',
                    variant: 'success',
                });
                this.dispatchEvent(evt);
                this.errormessage = '';
            })
            .catch(error => {
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: 'Not able to send email to Group O team!!',
                    variant: 'error',
                });
                this.dispatchEvent(evt);
                this.errormessage = error.body.message; //set error if any
            })
    }

    handleProgress(step) { //on step change set button visibility and label
        switch (step) {
            case 1:
                this.currentStep = "1";
                this.prevDisable = true;
                this.nextDisable = false;
                this.nextLabel = "Next";
                break;
            case 2:
                this.currentStep = "2";
                this.prevDisable = false;
                this.nextDisable = false;
                this.nextLabel = "Next";
                break;
            case 3:
                this.currentStep = "3";
                this.prevDisable = false;
                this.nextDisable = false;
                this.nextLabel = "Verify and Activate";
                break;
            default:
                this.currentStep = "1";
                this.prevDisable = true;
                this.nextDisable = false;
                this.nextLabel = "Next";
                break;
        }
    }

    validateFields() { 
       
        //validate all the field level validations set at component(template) level
        const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')] //validate all text input fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputsComboBoxCorrect = [...this.template.querySelectorAll('lightning-combobox')] //validate all combobox fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputsTextAreaCorrect = [...this.template.querySelectorAll('lightning-textarea')] //validate all teaxt area fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputsRadioGroupCorrect = [...this.template.querySelectorAll('lightning-radio-group')] //validate all radio group fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputAddressCorrect = [...this.template.querySelectorAll('lightning-input-address')] //validate all address fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const ismultipicklistCorrect = [...this.template.querySelectorAll('lightning-dual-listbox')] //validate all address fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        return (isInputsCorrect && isInputsComboBoxCorrect && isInputsTextAreaCorrect && isInputsRadioGroupCorrect && isInputAddressCorrect && ismultipicklistCorrect);
    }

    printDocument(element) { //review all the input details and download it as pdf   
        this.template.querySelector('[data-id="printblock"]').className = 'slds-col slds-size_12-of-12 printdiv';
        this.template.querySelector('[data-name="printBlockContainer"]').className = 'customPrint customPrintActive';
        window.print();
        this.template.querySelector('[data-name="printBlockContainer"]').className = 'customPrint';
        this.template.querySelector('[data-id="printblock"]').className = 'slds-col slds-size_12-of-12';
    }

    handleexistingmodalclose(event) {
        if (event.detail == 'Reject' || 'Cancel' || 'NO') {
            this.showExistingUserModal = false;
            this.showfacilityselect = false;
            this.errormessage = '';
            this.dispatchEvent(new CustomEvent('backcall'));
        }
    }

    handleexistingusersubmitmodal(event) {
        if (event.detail == 'OK') {
            this.showExistingUserModal = false;
            this.errormessage = '';
            this.isPatientToBeCreated = false;
            this.dispatchEvent(new CustomEvent('backcall'));
        }
        if (event.detail == 'Accept') {
            this.showExistingUserModal = false;
            this.isPatientToBeCreated = true;
            this.sendExistingUserNotftnToAdmin = true;
            this.currentTab++;
            this.handleProgress(this.currentTab);
        }
        if (event.detail == 'Continue') {
            this.errorMessage = 'A patient matching this registration already exists in this clinic, but the user is inactive.  Add the Box ID to the existing patient.';
            this.showExistingUserModal = false;
            this.isPatientToBeCreated = false;
            this.isExistingPatientToBeUpdated = true;
            //override existing patient data
            this.personAccountObject = this.existingPatientData;
            if (this.personAccountObject.HV_Department__pc) {
                this.selectedDepartment = this.personAccountObject.HV_Department__pc.split(',');
            }
            if (this.personAccountObject.HV_isTwoFactorAuthentactionEnabled__pc) {
                this.selectedOptInValue = 'Yes';
            }
            else {
                this.selectedOptInValue = 'No';
            }
            this.patientIdsOptions.push({ label: this.personAccountObject.HV_Patient_ID__pc, value: this.personAccountObject.HV_Patient_ID__pc });
            this.currentTab++;
            this.handleProgress(this.currentTab);
        }
        if (event.detail == 'YES') {
            this.showExistingUserModal = false;
            getSobjectRecord({ objectname: 'User', fieldnames: 'id,username,contactid,contact.HV_Role__c,SmallPhotoUrl,AccountId,Contact.Account.HV_Patient_Clinic__c,Contact.Account.HV_Patient_Clinic__r.Name,Contact.FirstName,Contact.LastName', conditions: 'where contact.accountid = \'' + this.personAccountObject.Id + '\'' }) //get selected user details to pass to user profile page
                .then(result => {
                    this.patientUserData = result;
                    this.showPatientProfile = true;
                })
                .catch(error => {
                    this.errormessage = error.body.message; //set error message if any
                })
        }
        if (event.detail.eventname == 'Next') {
            this.currentUserClinic = event.detail.clinicid;
            this.clinicName = event.detail.clinicname;
            this.callMetaData();
            this.showfacilityselect = false;
        }
    }
}