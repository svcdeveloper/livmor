import { LightningElement, track, wire } from 'lwc';
import loginUserId from '@salesforce/user/Id';
import { getRecord } from 'lightning/uiRecordApi';
import CURRENT_USER_CONTACTID from '@salesforce/schema/User.Contact.Id';
import CURRENT_USER_ROLE from '@salesforce/schema/User.Contact.HV_Role__c';
import getClinicLicense from '@salesforce/apex/HVAnalytics.getClinicLicense';

const USERFIELDS = [CURRENT_USER_CONTACTID, CURRENT_USER_ROLE]; //fields to query from user object
const columns = [
    { label: 'Clinic', fieldName: 'clinicName', type: 'text' },
    { label: 'Available Licenses', fieldName: 'license', type: 'text' }
];

export default class HvAnalytics extends LightningElement {
    @track displayFlag = false;
    @track dataFlag = false;
    @track data = [];
    @track columns = columns;

    @wire(getRecord, { recordId: loginUserId, fields: USERFIELDS })
    loggedInUserData({ error, data }) { //lds method to get current logged in user details using schema
        if (data) {
            console.log('HvAnalytics role: ' + data.fields.Contact.value.fields.HV_Role__c.value);
            if(data.fields.Contact.value.fields.HV_Role__c.value == 'Livmor Admin'){
                this.displayFlag = true;
            }
        } else if (error) {
            console.log('Error getting data from user: ' + error.body.message);
        }
    }

    connectedCallback() {
        getClinicLicense()
            .then(result => {
                this.data = result;
                /*result.forEach(val => {
                    this.data.push({
                        'clinicName' : val.clinicName,
                        'license' : val.license
                    })
                });*/
                this.dataFlag = true;
            })
            .catch(error => {
                console.log('HvAnalytics Error Occured: ' + error.body.message);
            })
    }
}