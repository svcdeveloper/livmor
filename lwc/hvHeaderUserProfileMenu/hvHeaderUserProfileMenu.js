import { LightningElement,track,wire } from 'lwc';
import { getRecord} from 'lightning/uiRecordApi';
import { NavigationMixin } from 'lightning/navigation';
import USER_ID from '@salesforce/user/Id';
import NAME_FIELD from '@salesforce/schema/User.Name';
import EMAIL_FIELD from '@salesforce/schema/User.Email';
import SMALL_PHOTOURL from '@salesforce/schema/User.SmallPhotoUrl';
import communityPath from '@salesforce/community/basePath';
import COMMUNITY_LOGOUT_URL from '@salesforce/label/c.HVCommunityLogoutURL';
import KNOWLEDGEBASEURL from '@salesforce/label/c.HVCommunityLogoutURL';

export default class HvHeaderUserProfileMenu extends NavigationMixin(LightningElement) {
    @track error ;
    @track email ; 
    @track name;
    url;
    showmenu = false;
    showprofilemenu = false;

    @wire(getRecord, {
        recordId: USER_ID,
        fields: [NAME_FIELD, EMAIL_FIELD,SMALL_PHOTOURL]
    }) wireuser({error,data}){
        if (error) {
           this.error = error ; 
        } else if (data) {
            this.email = data.fields.Email.value;
            this.name = data.fields.Name.value;
            this.url = data.fields.SmallPhotoUrl.value;
        }
    }

    toggleshowmenu(event){
        if(this.showmenu){
            this.showmenu = false;
        }
        else{
            this.showmenu = true;
        }
    }

    connectedCallback(){
        if(communityPath.includes('/login')){
            this.showprofilemenu = false;
        }
        else{
            this.showprofilemenu = true;
        }
    }
    revertbacktoUserProfile(){
        const event = new CustomEvent('child', {
            detail: {
                userProfile: true
            }
        });
        this.dispatchEvent(event);
    }

    dologout(event){
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__webPage',
            attributes: {
                url: ''
            }
        }).then(url => {
            window.open(COMMUNITY_LOGOUT_URL, "_self");
        });
    }

    gotoknowledgebase(event){
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: KNOWLEDGEBASEURL
            }
        },
        true
      );
    }
}