import { LightningElement,track } from 'lwc';
import getSobjectRecords from '@salesforce/apex/HVQueryUtility.getMultipleRecordImprative';
import getSobjectRecord from '@salesforce/apex/HVQueryUtility.getSingleRecordImprative'

export default class HvFooter extends LightningElement { 

    @track isContactOpen = false;
    @track isterm = false;
    @track isprivacy = false;  
    termofuse ;
    privacy ; 
    contactus= {}; 
    iframeurl = 'https://livmorinc--hvdev.my.salesforce.com/sfc/p/1100000CATL7/a/110000004w0F/rArawfGRy5OvatCMJNRLRYfezxure29a9GjzrYqyL.A'; 
    closeContactModal(){
        this.isContactOpen = false;   
    }
    openContactModal(){
        this.isContactOpen = true;   
    }

    
    handleTerms(){
        this.isterm = true;
          this.getEmailRecord('HV_Terms_and_Conditions')
        .then( result => {
            this.termofuse = result;
         });; 
    }
    handlePrivacy(){
        this.isprivacy = true; 
         this.getEmailRecord('HV_PRIVACY_NOTICE')
         .then( result => {
            this.privacy = result;
         });
        console.log('log modal footar')
    }

    closeModal(){
        this.isprivacy = false
        this.isterm = false; 
    }

    getEmailRecord(emailtemplatename){
        
       return getSobjectRecord({
			objectname: 'EmailTemplate',
			fieldnames: 'Id, DeveloperName , HtmlValue',
			conditions: 'where developername = \''+emailtemplatename+'\''
		}) 
        .then( result=> {
            console.log('result.HtmlValue',result.HtmlValue);
            return result.HtmlValue;
           
        })
        .catch(error => {
            console.log('error',error);
        })

        
    }
}