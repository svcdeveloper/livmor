import { LightningElement,track ,api } from 'lwc';

//import BP_OBJECT from "@salesforce/schema/BP__c";
//import BP_BOXID_FIELD from "@salesforce/schema/HV_BP__c.HV_Box__c";

export default class BpCmp extends LightningElement {
    
    @api objectApiName;
    @api boxid;
    @api recordId;
    @api objectFields;
    @api itemNumber;
    @track isSaved = false;
    @track column1fields;
    @track column2fields;
    spaceValidation = false;
    //fieldsJson = new Map();
    //fieldsJsonArray = [{apiName: 'Name', required: true, minValue: 8, minMassage: 'Please enter the maximum 8 charecter' }]
    
    connectedCallback(){
       // this.fieldsJson.set('Name',{apiName: 'Name', required: true, minValue: 8, minMassage: 'Please enter the maximum 8 charecter' })
        let fieldsArr = [];
        this.objectFields.forEach(element => {
            fieldsArr.push(element);
        });
        let fieldArrSize = Math.ceil(fieldsArr.length/2);
        this.column1fields = fieldsArr.slice(0,fieldArrSize);
        this.column2fields = fieldsArr.slice(fieldArrSize,fieldsArr.length+1);
        console.log('this cont',this);
        
    }
    handleSubmit(event){
        this.isLoading(true);
        event.preventDefault();     
        if(this.validateFields){
            // stop the form from submitting
            if(!this.spaceValidation){
                const fields = event.detail.fields;
                console.log('fields',fields);
                fields['HV_Box__c'] = this.boxid;
                console.log('box')
                console.log('box',fields)
                this.template.querySelector('lightning-record-edit-form').submit(fields);
            }
            

        }  
        
    } 

    validateFields() { 
       
        //validate all the field level validations set at component(template) level
        const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')] //validate all text input fields
            .reduce((validSoFar, inputField) => {
               console.log('inputField',inputField)
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputsComboBoxCorrect = [...this.template.querySelectorAll('lightning-combobox')] //validate all combobox fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputsTextAreaCorrect = [...this.template.querySelectorAll('lightning-textarea')] //validate all teaxt area fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputsRadioGroupCorrect = [...this.template.querySelectorAll('lightning-radio-group')] //validate all radio group fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputAddressCorrect = [...this.template.querySelectorAll('lightning-input-address')] //validate all address fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const ismultipicklistCorrect = [...this.template.querySelectorAll('lightning-dual-listbox')] //validate all address fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        return (isInputsCorrect && isInputsComboBoxCorrect && isInputsTextAreaCorrect && isInputsRadioGroupCorrect && isInputAddressCorrect && ismultipicklistCorrect);
    }
    handleError(event){
        console.log('error',event.detail.message);  
        console.log('error',event.detail); 
        console.log('error',event.detail.output); 
        console.log('error'+event.detail.output.fieldErrors);
        console.log('error',event.detail.output.fieldErrors.message); 

    }
    handleCreated(event){
        this.recordId = event.detail.id;
        this.isSaved = true;
        console.log('onsuccess: BP', this.recordId);
        this.isLoading(false);
    }

    watchScript(event){
        console.log('ok plus');
        let inputEle = this.template.querySelectorAll('lightning-input-field');

        event.target.value.split(',').forEach( (item,index) => {
            console.log('item',item);
            console.log('index',index);
            if(inputEle[(index+1)].disabled){
                inputEle[(index+1)].value = item;
            }
        })
       
        
    }

    tabletScript(event){
        console.log('ok plus');
        let inputEle = this.template.querySelectorAll('lightning-input-field');

        event.target.value.split(',').forEach( (item,index) => {
            console.log('item',item);
            console.log('index',index);
            if(inputEle[(index+1)].disabled){
                inputEle[(index+1)].value = item;
            }
            console.log('inputEle[(index+1)]',inputEle[(index+1)])
            
        })
       
        
    } 

   

    onchangeEvent(event){
        console.log('event',event);
        console.log('target',event.target);
        console.log('clas name',event.target.name);
        let tempObj = this.template.querySelector('p.'+event.target.name); 
        tempObj.textContent = '';
       
        console.log('is have lenght',event.target.value.length );
        if(event.target.value.length > 0){  
            console.log('is no tblcank' ) 
            if (!event.target.value.replace(/\s/g, '').length) {
                console.log('string only contains whitespace (ie. spaces, tabs or line breaks)');
                console.log('p ccccc',this.template.querySelector('p'));
                let tempObj = this.template.querySelector('p.'+event.target.name); 
                tempObj.textContent = 'Please enter a value only space is not allowed';
            }else{
                let tempObj = this.template.querySelector('p.'+event.target.name); 
                tempObj.textContent = '';
            }
        }
        

        if(event.target.dataset.onchange == 'watchScript'){ 
            this.watchScript(event);
        }

        if(event.target.dataset.onchange == 'tabletScript'){
            this.tabletScript(event);
        }
        
    } 

    keyUpEvent(event){

        if(event.target.value.includes(' ')){

            if (!event.target.value.replace(/\s/g, '').length) {
                console.log('string only contains whitespace (ie. spaces, tabs or line breaks)');
                console.log('p ccccc',this.template.querySelector('p'));
                let tempObj = this.template.querySelector('p.'+event.target.name); 
                tempObj.textContent = 'Please enter a value only space is not allowed';
                this.spaceValidation = true;
            }else{
                let tempObj = this.template.querySelector('p.'+event.target.name); 
                tempObj.textContent = '';
                this.spaceValidation = false;
            }
        }else{ 
            this.spaceValidation = false;
            let tempObj = this.template.querySelector('p.'+event.target.name); 
            tempObj.textContent = '';
        }
        
    }
    /*minLengthError(event){
        if(this.fieldsJson.has(event.target.dataset.field)){
            let errorField = this.template.querySelector('.'+event.target.dataset.field);
            if(this.fieldsJson.get(event.target.dataset.field).minValue){
                if(event.detail.value.length <= this.fieldsJson.get(event.target.dataset.field).minValue){
                    console.log('bye bye',errorField);
                    console.log('bye bye',this.fieldsJson.get(event.target.dataset.field).minMassage);
                    errorField.innerText = this.fieldsJson.get(event.target.dataset.field).minMassage;
                }else{
                    errorField.innerText = '';
                }
                event.reportValidity();
            }
        }
    }*/

    isLoading(val){
        console.log('vl',val);
        const evt = new CustomEvent('spinner',{
            detail:{
                isloading :val
            }
        })
        this.dispatchEvent(evt);

    }

    
}