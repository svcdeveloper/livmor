import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getReport from '@salesforce/apex/HVAFReportView.navigateToAFReport';

export default class HvGenerateReport extends NavigationMixin(LightningElement) {
    @track paramvals = {};
    @api startdate;
    @api enddate;
    @api server = 'BAYLOR';
    @api userid = 'bridge02n';
    @api s3bucket= 'baylorprod';
    generateReports(){
        /*utcStartDate = this.toUTC(this.startdate);
        utcEndDate = this.toUTC(this.enddate);*/
        console.log('Start :::: '+this.startdate);
        getReport({server: this.server, userid: this.userid, s3bucket: this.s3bucket})
        .then(result => {
            this.navigateToReport(result);
        })
        .catch(error => {
            this.error = error;
        });
    }

    navigateToReport(url) {
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                "url": url
            },
        });
    }

    toUTC(date){ 
        var now_utc =  Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
        date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
        console.log(now_utc);
        return now_utc;
    }

    setStartDate(event){
        console.log('============= '+event.target.value);
        this.startdate = this.toUTC(new Date(event.target.value));
    }

    setEndDate(event){
        this.enddate = this.toUTC(new Date(event.target.value));
    }
}