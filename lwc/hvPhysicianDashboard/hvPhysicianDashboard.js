import { LightningElement, api, wire, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { updateRecord } from 'lightning/uiRecordApi';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import uId from '@salesforce/user/Id';
import CURRENT_USER_CLINIC from '@salesforce/schema/User.Contact.AccountId';
import CURRENT_USER_CONTACTID from '@salesforce/schema/User.Contact.Id';
import CURRENT_USER_ROLE from '@salesforce/schema/User.Contact.HV_Role__c';
import CURRENT_USER_CLINIC_NAME from '@salesforce/schema/User.Contact.Account.Name';
import getSobjectRecord from '@salesforce/apex/HVQueryUtility.getSingleRecordImprative';
import getSobjectRecords from '@salesforce/apex/HVQueryUtility.getMultipleRecordImprative';
import getPatientData from '@salesforce/apex/HVPhysicianDashboard.getPatientData';
import getReport from '@salesforce/apex/HVAFReportView.navigateToAFReport';
import insertReviewData from '@salesforce/apex/HVPhysicianDashboard.insertReviewData';
import patientId from '@salesforce/apex/HVPatientDashboardController.getPatientId';
import { refreshApex } from '@salesforce/apex';
import updateSobjectRecord from '@salesforce/apex/HVDMLUtility.updateRecord';
let i = 0;

const USERFIELDS = [CURRENT_USER_CLINIC, CURRENT_USER_CONTACTID, CURRENT_USER_CLINIC_NAME, CURRENT_USER_ROLE]; //fields to query form user object

const DAYSRANGE = [
    { dayrange: "1D", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * 1, enddate: Math.floor(new Date().getTime() / 1000.0) },
    { dayrange: "1W", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * 7, enddate: Math.floor(new Date().getTime() / 1000.0) },
    { dayrange: "1M", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * 30, enddate: Math.floor(new Date().getTime() / 1000.0) },
    { dayrange: "3M", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * 90, enddate: Math.floor(new Date().getTime() / 1000.0) },
    { dayrange: "6M", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * 180, enddate: Math.floor(new Date().getTime() / 1000.0) },
    { dayrange: "YTD", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * ((Math.floor((new Date() - new Date(new Date().getFullYear(), 0, 0)) / 86400000))-1), enddate: Math.floor(new Date().getTime() / 1000.0) },
]

export default class HvPhysicianDashboard extends NavigationMixin(LightningElement) {
    userId = uId;
    @api datesend;
    isModalOpen = false;
    @track isGenerateReportOpen = false;
    @track page = 1; //this will initialize 1st page
    @track items = []; //it contains all the records.
    @track displaydata = []; //data to be displayed in the table
    @track selectedRows = []; //row selected in the table
    //@track columns; //holds column info.
    //@track startingRecord = 1; //start record positi on per page
    //@track endingRecord = 0; //end record position per page
    //@track pageSize = 20; //default value we are assigning
    //@track totalRecountCount = 0; //total record count received from all retrieved records
    @track totalPage = 0; //total number of page is needed to display all records
    @track clearReviewed = false;
    @track selectedpatientId = '';
    openPatientDetail = false;
    openOtherUserDetail = false;
    @track userData;
    contactId;
    isLoading = false;
    @api roleName;
    userContactId;
    currentUserClinic;
    currentUserDept = [];
    clinicName;
    metaDataRecords;
    totalcolumns = [];
    data;
    optionsCheckbox;
    @track otherUserData;
    server = 'BAYLOR';
    /*@api selectedpatientId = 'bridge02n';
    @api s3bucket = 'baylorprod';
    server; = 'BAYLOR'*/
    patientuserid /*= 'bridge02n'*/;
    s3bucket = 'baylorprod';
    @track selectedContactId;
    @track loggedinUserRolePhysician;
    slectedColumnCount = 0;
    isPatient;
    @api createUserAccess;
    @api columnApiName;
    @api currentUserRecord;
    showReportHistory = false;
    showdata = true;
    showspinner = true;
    totalNumberOfRows = 50;
    rowOffSet = 0;
    numberofrecordtoshow = 50;
    @track tabledata = [];
    showGenReport;
    dayrangeselected = "1M";
    dayrangestartdate;
    dayrangeenddate;
    searchCount = 0;
    @track searchAllData = [];
    searchvalue = '';
    defaultSortDirection = 'asc';
    sortDirection = 'asc';
    sortedBy;
    myInput;
    queryTerm;
    @track showFavFilter = true;
    loggedInUserRole;
    chooseByClinicoptions = [];
    chooseByDepartmentoptions = [];
    departmentMap = new Map();
    @track deptShowFlag = false;
    filterApplied = false;
    adminClinics = [];

    get showoffboardbutton() {
        if(this.currentUserRecord.fields.Contact.value.fields.HV_Role__c.value == 'Livmor Admin'){
            return (this.roleName == 'Onboarding Admin');
        }else{
            return (this.roleName == 'Physician' || this.roleName == 'Clinical User' || this.roleName == 'Onboarding Admin');
        }        
    }

    get showPatientFilter() {
        return (this.roleName == 'Patient');
    }

    get showdatable() {
        return (this.openOtherUserDetail || this.openPatientDetail);
    }

    get showtabledata() {
        return this.tabledata.length > 0 ? true : false;
    }

    /*get chooseByDepartmentoptions() {
        return this.currentUserDept;
    }*/

    get chooseByFavoriteoptions() {
        return [
            { label: 'Favorite', value: 'Favorite' },
            { label: 'Un Favorite', value: 'UnFavorite' },
        ];
    }

    get chooseByActivityoptions() {
        return [
            { label: 'Active', value: 'Active' },
            { label: 'InActive', value: 'InActive' },
        ];
    }

    /*get chooseByClinicoptions() {
        return [
            { label: this.clinicName, value: this.clinicName }
        ];
    }*/

    get optionsDaysReport() {
        return [
            { label: "1D", value: "1D" },
            { label: "1W", value: "1W" },
            { label: "1M", value: "1M" },
            { label: "3M", value: "3M" },
            { label: "6M", value: "6M" },
            { label: "MTD", value: "MTD" },
            { label: "YTD", value: "YTD" }
        ];
    }

    get optionsDevicesReport() {
        return [
            { label: 'Blood Pressure ', value: "Blood Pressure " },
            { label: 'Weigh Scale', value: "Weigh Scale" },
            { label: 'Heart Rate', value: "Heart Rate" },
            /*{ label: 'Spirometer', value: "Spirometer" },
            { label: 'Glucometer', value: "Glucometer" }*/
        ];
    }

    get userRoleLabel() {
        if (this.createUserAccess) {
            if (this.roleName === 'Patient') {
                this.isPatient = true;
            } else {
                this.isPatient = false;
            }
            return 'Create New ' + this.roleName;
        } else {
            return null;
        }
    }

    connectedCallback() {
        this.isLoading = true;
        DAYSRANGE.forEach(day => {
            if (day.dayrange == this.dayrangeselected) {
                this.dayrangestartdate = day.startdate;
                this.dayrangeenddate = day.enddate;
            }
        });
        this.setupEventListeners();
        if (this.currentUserRecord) {
            let columnJson;
            this.contactId = this.currentUserRecord.fields.Contact.value.fields.Id.value;
            if (this.currentUserRecord.fields.Contact.value.fields[this.columnApiName].value) {
                columnJson = this.currentUserRecord.fields.Contact.value.fields[this.columnApiName].value;
            }
            if (this.currentUserRecord.fields.Contact.value.fields.HV_Department__c.value) {
                var userDept = this.currentUserRecord.fields.Contact.value.fields.HV_Department__c.value.split(',');
                userDept.forEach(element => {
                    this.currentUserDept.push({ label: element, value: element });
                });
            }
            this.currentUserClinic = this.currentUserRecord.fields.Contact.value.fields.AccountId.value; //logged in user clinic/account Id
            this.clinicName = this.currentUserRecord.fields.Contact.value.fields.Account.value.fields.Name.value; //set default user clinic name
            var currentRoleName = this.currentUserRecord.fields.Contact.value.fields.HV_Role__c.value;
            this.loggedInUserRole = this.currentUserRecord.fields.Contact.value.fields.HV_Role__c.value;
            if (currentRoleName == 'Physician') {
                this.loggedinUserRolePhysician = true;
                this.showGenReport = true;
            } else if (currentRoleName == 'Clinical User') {
                this.showGenReport = true;
            } else if (currentRoleName == 'Onboarding Admin' || currentRoleName == 'Livmor Admin' || this.loggedInUserRole == 'Call Center Admin' || this.loggedInUserRole == 'Call Center User') {
                this.showFavFilter = false;
            } else {
                this.showGenReport = false;
            }
            this.handleMetadatRecord(columnJson);
        }

        if(this.loggedInUserRole == 'Livmor Admin' || this.loggedInUserRole == 'Call Center Admin' || this.loggedInUserRole == 'Call Center User'){
            this.allClinicDeptRec();
            this.chooseByDepartmentoptions = [];
        } else{
            this.chooseByClinicoptions = [{ label: this.clinicName, value: this.currentUserClinic /*this.clinicName*/ }];
            this.chooseByDepartmentoptions = this.currentUserDept;
        }
    }

    renderedCallback() {
        let dataTablecmp = this.template.querySelector('c-hv-custom-data-table')
        if (dataTablecmp) {
            dataTablecmp.getOnclick();
        }

        this.template.querySelectorAll('.generatereportbutton').forEach(btn => {
            if (this.dayrangeselected == btn.label) {
                btn.variant = "brand";
            }
            else {
                btn.variant = "neutral";
            }
        });

    }

    setupEventListeners() {
        this.messageHandler = ({ data, origin }) => {
            if (origin === window.location.origin) {
                if (data.datarow) {
                    this.selectedpatientId = data.datarow;
                    if (this.roleName == 'Patient') {
                        this.openPatientDetail = true;
                    }
                    else {
                        getSobjectRecord({ objectname: 'User', fieldnames: 'id,username,contactid,contact.HV_Role__c,SmallPhotoUrl,AccountId,Contact.Account.HV_Patient_Clinic__c,Contact.Account.HV_Patient_Clinic__r.Name,Contact.FirstName,Contact.LastName,Contact.AccountId,Contact.Account.Name,Contact.hv_patient_id__c', conditions: 'where contactid = \'' + this.selectedpatientId + '\'' }) //get selected user details to pass to user profile page
                            .then(result => {
                                this.otherUserData = result;
                                this.openOtherUserDetail = true;
                            })
                            .catch(error => {
                                this.errormessage = error.body.message; //set error message if any
                            })

                    }
                }
            }
        };
        window.addEventListener('message', this.messageHandler);
    }

    generateAFReports() {
        if(this.validateFields()){
            patientId({contactId : this.selectedContactId})  
            .then(data => {
                if (data) {
                    this.patientuserid = data.HV_Patient_ID__c;
                    //this.server =  data.Contact.Account.HV_Patient_Clinic__c;
                }
            })
            .then(data => {
                getReport({ server: this.server, userid: this.patientuserid, s3bucket: this.s3bucket, startdate: this.dayrangestartdate, enddate: this.dayrangeenddate })
                .then(result => {
                    this.navigateToReport(result);
                    this.dayrangeselected = '1M';
                    this.isGenerateReportOpen = false;
                })
                .catch(error => {
                    this.error = error;
                });
            })
            .catch(error => {
                this.error = error;  
            })
        }
    }

    navigateToReport(url) {
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                "url": url
            },
        });
    }


    getPagination() {

    }

    handleMetadatRecord(columnJson) {
        getSobjectRecords({
            objectname: 'HV_Roles_Data_Column__mdt',
            fieldnames: 'HV_Parent_Role__c,HV_Checked__c,HV_Disabled__c,HV_Sortable__c,HV_Editable__c,HV_Field_Name__c,HV_Label__c,HV_Roles__c,HV_Type__c,HV_Type_Attributes__c',
            conditions: ''
        })
            .then(data => {
                if (data) {
                    let selectedFields = [];
                    let allFields = [];
                    this.metaDataRecords = new Map();
                    let defultColumn = [];
                    if (columnJson) {
                        if (JSON.parse(columnJson)) {
                            selectedFields = JSON.parse(columnJson).selectedFields.split(';');
                            allFields = JSON.parse(columnJson).allFields.split(';');
                            this.totalcolumns = [selectedFields.length];
                            this.optionsCheckbox = [allFields.length];
                        }

                    } else {
                        this.totalcolumns = [];
                        this.optionsCheckbox = [];
                    }
                    data.forEach((record, index) => {
                        if (record.HV_Roles__c.includes(this.roleName)) {
                            if (record.HV_Parent_Role__c.includes(this.currentUserRecord.fields.Contact.value.fields.HV_Role__c.value)) {
                                this.metaDataRecords.set(record.HV_Field_Name__c, this.totalColumnJson(record));
                                if (selectedFields) {
                                    if (selectedFields.includes(record.HV_Field_Name__c)) {
                                        this.totalcolumns[selectedFields.indexOf(record.HV_Field_Name__c)] = this.totalColumnJson(record);
                                        record.HV_Checked__c = true;
                                    }
                                }
                                if (!selectedFields.length) {
                                    if (record.HV_Checked__c) {
                                        this.totalcolumns.push(this.totalColumnJson(record));
                                    } else {
                                        if (index <= 6) {
                                            defultColumn.push(this.totalColumnJson(record));
                                        }
                                    }
                                }
                                if (allFields) {
                                    if (allFields.includes(record.HV_Field_Name__c)) {
                                        this.optionsCheckbox[allFields.indexOf(record.HV_Field_Name__c)] = this.optionsCheckboxJson(record);
                                    }
                                }
                                if (!allFields.length) {
                                    this.optionsCheckbox.push(this.optionsCheckboxJson(record));
                                }
                            }
                        }
                    })
                    if (defultColumn.length) {
                        this.totalcolumns.push.apply(this.totalcolumns, defultColumn);
                    }
                }
            })
            .then(() => {
                this.getPatientsData();
            })
            .catch(error => {
                this.isLoading = false;
                console.log('error', error)
            })
    }

    getPatientsData() {
        getPatientData({
            currentUserClinic: this.currentUserClinic,
            roleName: this.roleName,
            fieldnames: 'CreatedDate,CreatedBy.Name,Email,Account.Name,AccountId,Account.HV_Patient_Clinic__c,HV_License_Number__c,HV_Qualification__c,HV_Specialization__c,LastName,FirstName,Phone,BirthDate,HV_Patient_ID__c,HV_Department__c, HV_Symptom_Status__c,HV_Prescribed_By__c,HV_Reviewed__c,HV_Reviewed_By__c,HV_Reviewed_By__r.Name,HV_Reviewed_Date__c,HV_Height__c,(select id, contactid, IsActive from users)'
        })
            .then(result => {
                if (result) {
                    this.data = [];
                    this.isLoading = true;
                    result.forEach(record => {

                        this.data.push({
                            Id: record.patientRec.Id,
                            isFav: record.isFavorite,
                            LastName: record.patientRec.LastName,
                            FirstName: record.patientRec.FirstName,
                            Phone: record.patientRec.Phone,
                            Birthdate: record.patientRec.Birthdate,
                            HV_Patient_ID__c: record.patientRec.HV_Patient_ID__c,
                            HV_Department__c: record.patientRec.HV_Department__c,
                            HV_Symptom_Status__c: record.patientRec.HV_Symptom_Status__c,
                            HV_Prescribed_By__c: record.patientRec.HV_Prescribed_By__c,
                            HV_Reviewed__c: /*record.patientRec.HV_Reviewed__c*/record.patientRec.HV_Reviewed_By__c != null ? true : false,
                            Email: record.patientRec.Email,
                            AccountId: record.patientRec.Account.Name,
                            ClinicId: record.patientRec.AccountId,
                            HV_License_Number__c: record.patientRec.HV_License_Number__c,
                            HV_Qualification__c: record.patientRec.HV_Qualification__c,
                            HV_Specialization__c: record.patientRec.HV_Specialization__c,
                            reviewedBy: record.patientRec.HV_Reviewed_By__c != null ? record.patientRec.HV_Reviewed_By__r.Name : '',
                            reviewedDate: record.patientRec.HV_Reviewed_Date__c != null ? (record.patientRec.HV_Reviewed_Date__c).split('T')[0] : '',
                            CreatedBy: record.patientRec.CreatedBy.Name,
                            CreatedDate: (record.patientRec.CreatedDate).split('T')[0],
                            HV_Patient_Clinic__c: record.patientRec.Account.HV_Patient_Clinic__c,
                            userActive: record.patientRec.Users[0].IsActive,
                            HV_Height__c : record.patientRec.HV_Height__c

                        })
                    })
                    this.displaydata = this.data;
                    //this.totalRecountCount = this.data.length; //here it is 23
                    //this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize); //here it is 5

                    //initial data to be displayed ----------->
                    //slice will take 0th element and ends with 5, but it doesn't include 5th element
                    //so 0 to 4th rows will be displayed in the table
                    //this.displaydata = this.items.slice(0, this.pageSize);
                    //this.endingRecord = this.totalRecountCount >= 20 ? this.pageSize : this.totalRecountCount;
                    // this.columns = columns;

                    this.error = undefined;
                    this.isLoading = false;
                    //this.manageandshowdata(this.displaydata);

                    this.tabledata = this.displaydata.slice(0, this.totalNumberOfRows);
                }
            })
            .catch(e => {
                console.log('error', e)
                this.isLoading = false;
                this.isLoading = false;
            })
    }

    totalColumnJson(record) {
        return {
            label: record.HV_Label__c,
            fieldName: record.HV_Field_Name__c,
            type: record.HV_Type__c,
            sortable: record.HV_Sortable__c,
            editable: record.HV_Editable__c,
            typeAttributes: record.HV_Type_Attributes__c ? JSON.parse(eval(JSON.stringify(record.HV_Type_Attributes__c))) : ''
        }
    }

    optionsCheckboxJson(record) {
        return {
            label: record.HV_Label__c,
            checked: record.HV_Checked__c,
            disabled: record.HV_Disabled__c,
            value: record.HV_Field_Name__c
        }
    }

    //clicking on previous button this method will be called
    // previousHandler() {
    //     console.log('Previous handler');
    //     if (this.page > 1) {
    //         this.page = this.page - 1; //decrease page by 1
    //         this.displayRecordPerPage(this.page);
    //     }
    // }

    // //clicking on next button this method will be called
    // nextHandler() {
    //     console.log('test');
    //     if ((this.page < this.totalPage) && this.page !== this.totalPage) {
    //         this.page = this.page + 1; //increase page by 1
    //         this.displayRecordPerPage(this.page);
    //     }
    // }

    // //this method displays records page by page
    // displayRecordPerPage(page) {

    //     /*let's say for 2nd page, it will be => "Displaying 6 to 10 of 23 records. Page 2 of 5"
    //     page = 2; pageSize = 5; startingRecord = 5, endingRecord = 10
    //     so, slice(5,10) will give 5th to 9th records.
    //     */
    //     this.startingRecord = ((page - 1) * this.pageSize);
    //     this.endingRecord = (this.pageSize * page);

    //     this.endingRecord = (this.endingRecord > this.totalRecountCount) ? this.totalRecountCount : this.endingRecord;

    //     this.displaydata = this.items.slice(this.startingRecord, this.endingRecord);

    //     //increment by 1 to display the startingRecord count, 
    //     //so for 2nd page, it will show "Displaying 6 to 10 of 23 records. Page 2 of 5"
    //     this.startingRecord = this.startingRecord + 1;
    // }

    //Sample hard-coded column data to demonstrate how to structure (generate) column data for custom columns


    handleSearch(evt) {
        this.totalNumberOfRows = 50;
        this.rowOffSet = 0;
        this.tabledata = [];
        this.searchvalue = evt.target.value;
        let searchString = evt.target.value.toUpperCase();
        let searchdata = [];
        if (searchString) {
            this.displaydata.forEach(record => {
                if (record) {
                    if (this.searchField(record, searchString)) {
                        searchdata.push(record);
                    }
                }
            });
            if (searchdata.length > 0) {
                this.tabledata = searchdata;
                this.searchAllData = searchdata;
            }
            else {
                this.searchAllData = [];
            }
        } else if (!searchString) {
            this.searchAllData = [];
            this.tabledata = this.displaydata.slice(0, this.numberofrecordtoshow);
        }
    }

    searchField(record, searchString) {
        let isseachable = false
        if (this.totalcolumns) {
            this.totalcolumns.forEach(columnsval => {
                if (columnsval) {
                    if ('boolean' !== typeof record[columnsval.fieldName]) {
                        if (record[columnsval.fieldName]) {
                            if (String(record[columnsval.fieldName]).toUpperCase().includes(searchString)) {
                                isseachable = true;
                            }
                        }
                    }
                }
            })
        }
        return isseachable;
    }

    sortBy(field, reverse, primer) {
        const key = primer ? function (x) {
            return primer(x[field]);
        } : function (x) {
            return x[field];
        };
        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

    onHandleSort(event) {
        this.searchvalue = '';
        const {
            fieldName: sortedBy,
            sortDirection
        } = event.detail;
        const cloneData = [...this.data];
        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.items = cloneData;
        //this.totalRecountCount = this.data.length; //here it is 23
        // this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize); //here it is 5

        //initial data to be displayed ----------->
        //slice will take 0th element and ends with 5, but it doesn't include 5th element
        //so 0 to 4th rows will be displayed in the table
        //this.displaydata = this.items.slice(0, this.pageSize);
        this.tabledata = this.items.slice(0, this.numberofrecordtoshow);
        //this.startingRecord = 1;
        //this.endingRecord = this.numberofrecordtoshow;
        this.page = 1;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }

    handelSelection(event) {
        this.selectedRows = event.detail.selectedRows;
        this.selectedContactId = this.selectedRows[0].Id;
    }


    navigateToRegisterPatient() {
        // console.log('vsdv');
        // this.dispatchEvent(true);

        // detail contains only primitives
        const event = new CustomEvent('child', {
            detail: {
                createpatient: true,
                role: this.roleName
            }
        });
        this.dispatchEvent(event);
        // this[NavigationMixin.Navigate]({
        //     type: 'standard__component',
        //     attributes: {
        //         componentName: 'c__patientRegistration'
        //     },
        // });

    }

    showPhysicianDashboard() {

        this.openPatientDetail = false;
        this.getPatientsData();
      //  eval("$A.get('e.force:refreshView').fire();"); 
    }

    openModal() {
        this.isModalOpen = true;
    }

    closeModal() {
        this.isModalOpen = false;
    }
    generateReportOpenModal() {
        if (this.selectedRows.length > 0) {
            if(this.selectedContactId){
                patientId({contactId : this.selectedContactId})  
                .then(data => {
                    if (data) {
                        console.log('server ',data);
                        this.patientuserid = data.HV_Patient_ID__c;
                    }
                }).catch(error => {
                    this.error = error;  
                })
            }
            this.isGenerateReportOpen = true;
            this.showReportHistory = false;
        } else {
            const evt = new ShowToastEvent({
                title: 'Error',
                message: 'Please select one row in the Table',
                variant: 'error',
            });
            this.dispatchEvent(evt);
        }
    }

    generateReportCloseModal() {
        this.isGenerateReportOpen = false;
    }

    submitDataColumns() {
        this.isLoading = true;
        let columns = [];
        //.totalcolumns = [];
        const elements = this.template.querySelectorAll('.Items');
        let colErrEle = this.template.querySelector('.columnError');
        let selectedFields;
        let allFields;
        let optionsCheckboxTemp = [];
        elements.forEach((ele, index) => {
            if (ele) {
                if (ele.checked) {
                    if (this.metaDataRecords.has(ele.value)) {
                        columns.push(this.metaDataRecords.get(ele.value));
                    }
                    if (this.slectedColumnCount >= 10) {
                        colErrEle.innerHTML = 'You can not select more than 10 column';
                        this.isLoading = false;
                        this.slectedColumnCount++;
                        throw new Error("ERROR");
                    }
                    selectedFields = selectedFields ? selectedFields + ';' + ele.value : ele.value;
                }
                optionsCheckboxTemp.push({
                    label: ele.label,
                    checked: ele.checked,
                    disabled: ele.disabled,
                    value: ele.value
                });
                allFields = allFields ? allFields + ';' + ele.value : ele.value;
            }
        })
        this.optionsCheckbox = optionsCheckboxTemp;
        const fields = {};
        fields['Id'] = this.contactId;
        fields[this.columnApiName] = JSON.stringify({ selectedFields: selectedFields, allFields: allFields });
        const recordInput = {
            fields
        };
        updateRecord(recordInput)
            .then(() => {
                this.totalcolumns = columns;
                this.isLoading = false;
            })
            .catch(error => {
                console.log('err', error)
            });
        this.isModalOpen = false;
    }

    navigateToRegisterPhysician(e) {
        const event = new CustomEvent('child', {
            detail: {
                createphysician: true,
                role: this.roleName
            }
        });
        this.dispatchEvent(event);

    }
    dragStart(event) {
        event.target.classList.add('drag')
        // event.target.disabled = true;
    }

    dragOver(event) {
        event.preventDefault()
        return false
    }

    drop(event) {
        event.stopPropagation()
        const elements = this.template.querySelectorAll('.Items')
        let dragVal = this.template.querySelector('.drag');
        let dropVal = event.target;
        let curentArr = [];

        elements.forEach(ele => {
            if (ele.name == dragVal.name) {
                curentArr.push({ checked: dropVal.checked, label: dropVal.label, disabled: dropVal.disabled, value: dropVal.value });
            } else if (ele.name == dropVal.name) {
                curentArr.push({ checked: dragVal.checked, label: dragVal.label, disabled: dragVal.disabled, value: dragVal.value });
            } else {
                curentArr.push({ checked: ele.checked, label: ele.label, disabled: ele.disabled, value: ele.value });
            }
        })
        this.optionsCheckbox = [];
        this.optionsCheckbox = curentArr;
        elements.forEach(element => {
            element.classList.remove('drag')
        })
        return this.optionsCheckbox;

    }

    handleOnselectFav(event) {
        this.filterApplied = true;
        //this.displaydata = [];
        this.tabledata = [];
        var selectedVal = event.detail.value;
        this.displaydata.forEach(record => {
            if (selectedVal === 'Favorite' && record.isFav) {
                //this.displaydata.push(record);
                this.tabledata.push(record);
            } else if (selectedVal === 'UnFavorite' && !record.isFav) {
                // this.displaydata.push(record);
                this.tabledata.push(record);
            }
        });

        /*this.chooseByFavoriteoptions.filter(res =>{
            if(res.value === event.detail.value){
                res.checked = true; 
            }else{
                res.checked = false; 
            }
        })*/
    }

    handleOnActivityStatus(event) {
        this.filterApplied = true;
        this.tabledata = [];
        var selectedVal = event.detail.value;
        this.displaydata.forEach(record => {
            if (selectedVal === 'Active' && record.userActive) {
                this.tabledata.push(record);
            } else if (selectedVal === 'InActive' && !record.userActive) {
                this.tabledata.push(record);
            }
        });

        /*this.chooseByActivityoptions.filter(res =>{
            if(res.value === event.detail.value){
                res.checked = true; 
            }else{
                res.checked = false; 
            }
        })*/
    }

    handleOnselectClinic(event) {
        //this.displaydata = [];
        /*this.chooseByClinicoptions.filter(res =>{
            if(res.value === event.detail.value){
                res.checked = true; 
            }else{
                res.checked = false; 
            }
        });*/
        this.filterApplied = true;
        this.tabledata = [];
        var selectedVal = event.detail.value;
        this.displaydata.forEach(record => {
            if(this.roleName == 'Physician' || this.roleName == 'Onboarding Admin'){
                if (record.ClinicId == selectedVal) {
                    this.tabledata.push(record);
                }
            } else{ //Patient tab
                if (record.HV_Patient_Clinic__c == selectedVal) {
                    this.tabledata.push(record);
                }
            }
        });

        if(this.loggedInUserRole == 'Livmor Admin' || this.loggedInUserRole == 'Call Center Admin' || this.loggedInUserRole == 'Call Center User'){
            this.adminClinics = [...this.tabledata];
            this.deptShowFlag = true;
            this.chooseByDepartmentoptions = [];
            this.chooseByDepartmentoptions = this.departmentMap.get(event.detail.value);
        }
    }

    handleOnselectDept(event) {
        //this.displaydata = [];
        /*this.chooseByDepartmentoptions.filter(res =>{
            if(res.value === event.detail.value){
                res.checked = true; 
            }else{
                res.checked = false;
            }
        });*/
        this.filterApplied = true;
        var selectedVal = event.detail.value;
        if(this.loggedInUserRole == 'Livmor Admin' || this.loggedInUserRole == 'Call Center Admin' || this.loggedInUserRole == 'Call Center User'){
            var deptList = [];
            this.adminClinics.forEach(record => {
                if (record.HV_Department__c != null && record.HV_Department__c.includes(selectedVal)) {
                    deptList.push(record);
                }
            });
            this.tabledata = [];
            this.tabledata = [...deptList];
        } else{
            this.tabledata = [];
            this.displaydata.forEach(record => {
                if (record.HV_Department__c != null && record.HV_Department__c.includes(selectedVal)) {
                    this.tabledata.push(record);
                }
            });
        }
    }

    handleReviewFilterClick() {
        insertReviewData({ selectedRowId: this.selectedRows[0].Id, selectedRowLastName: this.selectedRows[0].LastName, loginUser: this.userId })
            .then(result => {
                if (result) {
                    this.showToast('Success', 'The patient has been added for review', 'success');
                    this.getPatientsData();
                }
            })
            .catch(error => {
                this.error = error;
            });
    }

    showToast(msgTitle, msg, msgVarient) {
        const event = new ShowToastEvent({
            title: msgTitle,
            message: msg,
            variant: msgVarient
        });
        this.dispatchEvent(event);
    }

    viewReportHistory() {
        this.showReportHistory = true;
    }

    revertBack(event) {
        this.openOtherUserDetail = false;
    }

    loadMoreData(event) {
        var dataL = [];
        console.log('in loadmore');
        this.isLoading = true;
        if (event.target.isLoading) {
            return;
        }
        //const currentRecord = this.displaydata;
        let isload = event.target.isLoading;
        isload = true;
        this.rowOffSet = this.rowOffSet + this.numberofrecordtoshow;
        this.totalNumberOfRows = this.totalNumberOfRows + this.numberofrecordtoshow;

        // this.tabledata = [];
        if (this.searchAllData.length > 0 && this.totalNumberOfRows < (this.searchAllData.length + this.numberofrecordtoshow)) {
            this.tabledata = this.searchAllData.slice(0, this.totalNumberOfRows);
            isload = false;
        }
        else if (this.searchAllData.length > 0 && this.totalNumberOfRows > this.searchAllData.length) {
            //this.totalNumberOfRows = this.searchAllData.length;
            // this.tabledata = this.searchAllData.slice(0, this.totalNumberOfRows);
            isload = false;
        }
        else if (!this.filterApplied && (this.displaydata.length > 0 && this.totalNumberOfRows < (this.displaydata.length + this.numberofrecordtoshow))) {
            this.tabledata = this.displaydata.slice(0, this.totalNumberOfRows);
            isload = false;
        }
        else if (!this.filterApplied && (this.displaydata.length > 0 && this.totalNumberOfRows > this.displaydata.length)) {
            //this.totalNumberOfRows = this.displaydata.length;
            // this.tabledata = this.displaydata.slice(0, this.totalNumberOfRows);
            isload = false;
        }
        else if (this.filterApplied /*&& (this.tabledata.length > 0 && this.totalNumberOfRows < (this.tabledata.length + this.numberofrecordtoshow))*/) {
            //this.tabledata = this.tabledata.slice(0, this.totalNumberOfRows);
            isload = false;
        }
        /*else if (this.filterApplied && (this.tabledata.length > 0 && this.totalNumberOfRows > this.tabledata.length)) {
            //this.totalNumberOfRows = this.displaydata.length;
            // this.tabledata = this.displaydata.slice(0, this.totalNumberOfRows);
            isload = false;
        }*/
        else {
            this.tabledata = [];
            isload = false;
        }
        /*this.manageandshowdata(currentRecord)
            .then(() => {
                isload = false;
            });*/

        this.isLoading = false;
    }

    /* manageandshowdata(fetchedData) {
         let tabledata = [];
         for (let i = this.rowOffSet; i < this.totalNumberOfRows; i++) {
             if(fetchedData[i] != null){
                 tabledata.push(fetchedData[i]);
             }else{
                 break;
             }
         }
         this.tabledata = [...this.tabledata, ...tabledata];
         this.showspinner = false;
     }*/

     handleButtonGroupGenerateReport(event) {
        this.dayrangeselected = event.target.label;
        DAYSRANGE.forEach(day => {
            if (day.dayrange == this.dayrangeselected) {
                this.dayrangestartdate = day.startdate;
                this.dayrangeenddate = day.enddate;
            }
        });
        this.template.querySelectorAll('.generatereportbutton').forEach(btn => {
            if (this.dayrangeselected == btn.label) {
                btn.variant = "brand";
            }
            else {
                btn.variant = "neutral";
            }
        });
    }

    deptFilterClick(){
        if(!this.deptShowFlag && (this.loggedInUserRole == 'Livmor Admin' || this.loggedInUserRole == 'Call Center Admin' 
                || this.loggedInUserRole == 'Call Center User')){
            this.showToast('Warning', 'Please choose a clinic from the clinic filter first', 'warning');
        }
    }

    allClinicDeptRec(){
        let deptSet = new Set();
        this.chooseByClinicoptions = [];
        getSobjectRecords({ 
            objectname: 'HV_ClinicFieldsMapping__mdt', 
            fieldnames: 'HV_Clinic_Id__c,MasterLabel,HV_Department_Values__c',
            conditions: '' 
        })
        .then( result => {
            //this.chooseByClinicoptions = [{ label: 'All', value: 'All' }];
        
            result.forEach(element => {
                //let clinicDept = [{ label: 'All', value: 'All' }];
                let clinicDept = [];
                this.chooseByClinicoptions.push({ label: element.MasterLabel, value: element.HV_Clinic_Id__c });
                element.HV_Department_Values__c.split(',').forEach(ele => {
                    deptSet.add(ele);
                    clinicDept.push({ label: ele, value: ele });
                });
                if(clinicDept){
                    this.departmentMap.set(element.HV_Clinic_Id__c,clinicDept)
                }
            });
            
            return result;
        }) 
        .then( res => {
            this.chooseByDepartmentoptions = [];
            let allDept = [{ label: 'All', value: 'All' }];
            deptSet.forEach(element => {
                allDept.push({ label: element, value: element });
            });
            //this.chooseByDepartmentoptions =allDept;
            //this.departmentMap.set('All',allDept);
        })
        .catch( error => {
            console.log('Error: ',error);
        })
    }

    handleTableRefresh(event){
        this.getPatientsData();
    }

    validateFields() { 
        const ismultipicklistCorrect = [...this.template.querySelectorAll('lightning-dual-listbox')] //validate all address fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);
        return ismultipicklistCorrect;
    }
}