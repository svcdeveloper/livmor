import { LightningElement, api, track } from 'lwc';
import getSobjectRecord from '@salesforce/apex/HVQueryUtility.getSingleRecordImprative';
import updateSobjectRecord from '@salesforce/apex/HVDMLUtility.updateRecord';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import deprovisionBox from  '@salesforce/apex/HVInventoryManagementController.deprovisionBox'
import boxDeassociation from  '@salesforce/apex/HVInventoryManagementController.boxDeassociation'

export default class HvSwapBox extends LightningElement {

    @api patientId;
    @api boxName; 
    @track boxmsg;

    closeModal(){
        this.closeModalEvent();
    }

    boxChange(event) {
        this.boxmsg ='';
        this.boxName = event.target.value;
    }

    submit(){
        this.boxmsg ='';
        console.log('this.boxName',this.boxName)
        getSobjectRecord({
            objectname : 'HV_Box__c',
            fieldnames : 'Id, Name,HV_Status__c',
            conditions : 'Where Name = \''+this.boxName+'\' '
        })
        .then(result => {

            console.log('result',result);
            if(result){
                    console.log('res',result);
                    const fields = {}
                    fields.attributes = { type : "Contact"}
                    fields.Id = this.patientId;
                    fields.HV_Box_ID__c = result.Id;
                    boxDeassociation({boxId : result.Id})
                    .then(res => {
                        updateSobjectRecord({
                            uapdateRec : fields
                        })
                        .then(conResult => {
                            console.log('result pick',conResult);
                            this.dispatchEvent(
                                new ShowToastEvent({
                                    title: 'Success',
                                    message: 'Swap Box Successful',
                                    variant: 'success'
                                })
                            ); 
                            this.updateBoxId(result.Name)
                            this.closeModalEvent();
                            // Display fresh data in the form
                        // return refreshApex(this.contact); 
                        })
                        .then(res => {
                            deprovisionBox({deprovisionVal:'In-Use',boxId:result.Id})
                            .then(res => {
                                console.log(res);
                                
                                
                              
                                
                            })
                            .catch(e => {
                                console.log(e);
                                const evt = new ShowToastEvent({ //show success toast
                                    title: 'Error',
                                    message: e.body.message,
                                    variant: 'error',
                                });
                                this.dispatchEvent(evt);
                            })
                        })
                        .catch(error => {
                            this.dispatchEvent(
                                new ShowToastEvent({
                                    title: 'Error creating record',
                                    message: error.body.message,
                                    variant: 'error'
                                })
                            );
                        });
                    })
                    .catch(error => {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: 'Error creating record',
                                message: error.body.message,
                                variant: 'error'
                            })
                        );
                    });
                    
                
                
            }else{
                console.log('result',result);
               
                this.boxmsg = 'Box does not exist';
                console.log('this.boxmsg',this.boxmsg);
            }
        })
        .catch( error => {
            console.log('error',error);
            this.boxmsg = 'Please enter valid box id'
        })
    }

    closeModalEvent(){
        this.dispatchEvent(new CustomEvent('close', { 
            detail: {
                close : false
            } 
        }))
    }

    updateBoxId(boxName){
        this.dispatchEvent(new CustomEvent('updatedboxid', { 
            detail: {
                boxName : boxName
            } 
        }))
    }
}