import { LightningElement, wire, track, api } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import USER_PREFERENCE_OBJECT from '@salesforce/schema/HV_UserPreference__c';
import SECURITY_QUESTION_SET_ONE from '@salesforce/schema/HV_UserPreference__c.HV_Security_Question_One__c';
import SECURITY_QUESTION_SET_TWO from '@salesforce/schema/HV_UserPreference__c.HV_Security_Question_Two__c';
import SECURITY_QUESTION_SET_THREE from '@salesforce/schema/HV_UserPreference__c.HV_Security_Question_Three__c';
import CURRENT_USER_CONTACTID from '@salesforce/schema/User.Contact.Id';
import CURRENT_USER_ROLE from '@salesforce/schema/User.Contact.HV_Role__c';
import currentUserId from '@salesforce/user/Id';
import getUserSelectedQuestions from '@salesforce/apex/HVSecurityQuestionsController.getUserSelectedQuestions';
import setUserSelectedQuestions from '@salesforce/apex/HVSecurityQuestionsController.setUserSelectedQuestions';

const USERFIELDS = [CURRENT_USER_CONTACTID, CURRENT_USER_ROLE]; //fields to query form user object

export default class HvSecurityQuestions extends LightningElement {
    @track userpreferencedetails = {}; //get/set user preference details from HV_UserPreference__c record
    @track securityquestionOptions1; //picklist options for questionset 1
    @track securityquestionOptions2; //picklist options for questionset 2
    @track securityquestionOptions3; //picklist options for questionset 3       
    @api isFromUserProfile; //is this component called from user profile component
    @api currentuserdata; //user details passed by other user/component
    roleName; //store user role name
    userContactId; //store user contact id
    isdisabled = false; //store if fields should be readonly/editable
    errormessage; //store error message

    @wire(getObjectInfo, { objectApiName: USER_PREFERENCE_OBJECT })
    userpreferenceInfo; //lds method to get schema and details of HV_UserPreference__c object

    @wire(getPicklistValues, { recordTypeId: '$userpreferenceInfo.data.defaultRecordTypeId', fieldApiName: SECURITY_QUESTION_SET_ONE })
    getQuestionOneSetPicklistOptions({ error, data }) { //lds method to get picklist values for question set 1 field
        if (data) {
            let options = [];
            data.values.map(row => {
                options.push({ label: row.label, value: row.value });
            });
            this.securityquestionOptions1 = options;
        }
        if (error) {
            this.errormessage = error.body.message;
        }
    };

    @wire(getPicklistValues, { recordTypeId: '$userpreferenceInfo.data.defaultRecordTypeId', fieldApiName: SECURITY_QUESTION_SET_TWO })
    getQuestionTwoSetPicklistOptions({ error, data }) { //lds method to get picklist values for question set 2 field
        if (data) {
            let options = [];
            data.values.map(row => {
                options.push({ label: row.label, value: row.value });
            });
            this.securityquestionOptions2 = options;
        }
        if (error) {
            this.errormessage = error.body.message;
        }
    };

    @wire(getPicklistValues, { recordTypeId: '$userpreferenceInfo.data.defaultRecordTypeId', fieldApiName: SECURITY_QUESTION_SET_THREE })
    getQuestionThreeSetPicklistOptions({ error, data }) { //lds method to get picklist values for question set 3 field
        if (data) {
            let options = [];
            data.values.map(row => {
                options.push({ label: row.label, value: row.value });
            });
            this.securityquestionOptions3 = options;
        }
        if (error) {
            this.errormessage = error.body.message;
        }
    };

    @wire(getRecord, { recordId: currentUserId, fields: USERFIELDS })
    loggedInUserData({ error, data }) { //lds method to get current logged in user details usinf schema
        if (data) {
            let userRecId = '';
            if (this.currentuserdata) { //if user details passed from other component store these details to attributes
                let returndata = JSON.parse(JSON.stringify(this.currentuserdata));
                this.roleName = returndata.Contact.HV_Role__c;
                this.userContactId = returndata.ContactId;
                userRecId = returndata.Id;
            }
            else { //if user data not passed from other component, store logged in user details to attributes
                this.roleName = data.fields.Contact.value.fields.HV_Role__c.value;
                this.userContactId = data.fields.Contact.value.fields.Id.value;
                userRecId = currentUserId;
            }
            getUserSelectedQuestions({ userId: userRecId }) //call apex method to get selected user questions and answers 
                .then(result => {
                    if (result) {
                        this.userpreferencedetails = result; //store return result
                    }
                })
                .catch(error => {
                    this.errormessage = error.body.message; //set error if any
                })
        }
        if (error) {
            this.errormessage = error.body.message; //set error if any
        }
    }

    connectedCallback() {
        //if this component is part of user profile - display all fields as readonly
        if (this.isFromUserProfile == "true") {
            this.isdisabled = true;
        }
    }

    onSecurityQuestionChange(event) { //input field on change specific to questions field
        this.userpreferencedetails[event.target.name] = event.target.value;
    }

    onAnswerChange(event) { //input field on change specific to answers field
        this.userpreferencedetails[event.target.name] = event.target.value;
    }

    setSecuity() {
        if (this.userpreferencedetails.HV_Security_Question_One__c != null && this.userpreferencedetails.HV_Security_Answer_One__c != null &&
            this.userpreferencedetails.HV_Security_Question_Two__c != null && this.userpreferencedetails.HV_Security_Answer_Two__c != null &&
            this.userpreferencedetails.HV_Security_Question_Three__c != null && this.userpreferencedetails.HV_Security_Answer_Three__c != null &&
            this.userpreferencedetails.HV_Security_Answer_One__c.trim() != '' && this.userpreferencedetails.HV_Security_Answer_Two__c.trim() != '' &&
            this.userpreferencedetails.HV_Security_Answer_Three__c.trim() != '') {
            //apex call to insert data in object
            setUserSelectedQuestions({ userId: currentUserId, userpreferencedetails: this.userpreferencedetails })
                .then(result => {
                    if (result) {
                        //close popup
                        const evnt = new CustomEvent('child', {
                            detail: { setSecurity: true }
                        });
                        this.dispatchEvent(evnt);
                    }
                })
                .catch(error => {
                    this.errormessage = error.body.message;
                })
        } else {
            this.errormessage = 'Answer to all three questions is required';
        }

    }
}