import { LightningElement, wire, api, track } from 'lwc';
import currentGoal from '@salesforce/apex/HVPatientDashboardController.getLongTermGoal';
import patientId from '@salesforce/apex/HVPatientDashboardController.getUserId';

export default class HvLongTermGoals extends LightningElement {
    apiendpoint = 'https://salesforceapi.livmor.com/dashboard';
    inputpatientId;
    patientRecordId;
    inputStartDate= Math.floor(new Date().getTime()/1000.0) - 86400 * 1;
    inputenddate = Math.floor(new Date().getTime()/1000.0);
    apimethod = 'POST';
    devicetype = 'dashboardlatestdata';
    weight = 'N/A';
    heart_rate_data = 'N/A';
    bp = 'N/A';
    //@api selectedPatient;
    @track targetData = {};
    error;
    targetweight;
    targetheartrate;
    targetbp;
    targetDate;
    goalsetdate;
    apirequestbody = {};
    loadawsdata = false;
    weightDiff = 0;

    connectedCallback(){
        //this.apirequestbody = { "patientid":"test00000001","startdate":"1624795200","enddate":"1624881600" };
        patientId()  
        .then(data => {
            if (data) {
                this.patientRecordId = data.ContactId;
                this.inputpatientId = data.Contact.HV_Patient_ID__c;
                this.apirequestbody["patientid"] = this.inputpatientId;
                this.apirequestbody["startdate"] = this.inputStartDate;
                this.apirequestbody["enddate"] = this.inputenddate;
                this.loadawsdata = true;
            }
        })
        .catch(error => {
            this.error = error;  
        })
        currentGoal()  
        .then(data => {
            if (data) {
                let options = {  
                    weekday: "long", month: "short",
                    day: "numeric"
                }; 
                console.log('data ##### ',data);
                this.targetData = data;
                let targetD = ((new Date(this.targetData.Target_Date__c)).toLocaleTimeString("en-us", options).toString()).split(', ');
                this.targetDate = targetD[0]+', '+targetD[1];
                let date = new Date();
                let dateArr = (date.toLocaleTimeString("en-us", options).toString()).split(', ');
                this.goalsetdate = dateArr[0]+', '+dateArr[1];;
            }
        })
        .catch(error => {
            this.error = error;  
        })
    }

    handleresponse(event){
        var actualData = event.detail;
        if(actualData && actualData.awsresponse.data){
            this.weight = actualData.awsresponse.data.weight;
            this.heart_rate_data = actualData.awsresponse.data.heart_rate_data;
            this.bp = actualData.awsresponse.data.systolic+ '/' + actualData.awsresponse.data.diastolic+' mm Hg';
        }
        this.loadawsdata = false;
        if(this.weight != 'N/A'){
            let targetWeight = parseFloat((this.targetData.Weight__c).trim(0,-3));
            let actualWeight = parseFloat((this.weight).trim(0,-3));
            this.weightDiff = (targetWeight - actualWeight) > 0 ? Math.abs(targetWeight - actualWeight).toFixed(3)+' more' : Math.abs(targetWeight - actualWeight).toFixed(3)+' less';
        }else{
            this.weightDiff = 'N/A';
        }
        
        
    }

    /*@wire(currentGoal) 
    getSetGoal({ error, data }) {
        console.log('data ::: ',data);
        if (data) {
            this.targetData = data;
            // this.targetweight = data.Weight__c;
            // this.targetheartrate = data.Heart_Rate__c;
            // this.targetbp = data.Blood_Pressure__c;
            // this.goalsetdate = data.Target_Date__c;
        } else if (error) { 
            this.error = error;  
            console.log('error ::: ', error);
        }   
    }*/
}