import { LightningElement,api } from 'lwc';

export default class HvDatatableStatus extends LightningElement {

    // favimg = favimg;
    // notfavimg = notfavimg;
    @api status;
    get isGreen(){
        return this.status == 'Green'? true:false;
    }
    get isWarning(){
        return this.status == 'Yellow'? true:false; 
    }
    get isDanger(){
        return this.status == 'Red'? true:false;
    }
}