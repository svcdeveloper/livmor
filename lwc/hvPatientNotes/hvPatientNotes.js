import { LightningElement, wire, api, track } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import currentUserId from '@salesforce/user/Id';
import CURRENT_USER_CONTACTID from '@salesforce/schema/User.Contact.Id';
import CURRENT_USER_ROLE from '@salesforce/schema/User.Contact.HV_Role__c';
import getPatientNotes from '@salesforce/apex/HVUserProfileController.getPatientNotes';
import createnotes from '@salesforce/apex/HVUserProfileController.createnotes';

const USERFIELDS = [CURRENT_USER_CONTACTID, CURRENT_USER_ROLE]; //fields to query form user object

export default class HvPatientNotes extends LightningElement {

    @api currentuserdata; //user details passed by other user/component
    @track createNoteRecord = {}; //note record to be created
    roleName; //store user role name
    userContactId; //store user contact id
    patientAllNotes; //store patient's all related exisying notes
    errormessage; //store error message
    isPatientHasNotes = true; //check if patient has any notes
    notestoadd = false; //allow user to add notes
    showaddnoteButton = false; //show or hide add notes button as per role

    @wire(getRecord, { recordId: currentUserId, fields: USERFIELDS })
    loggedInUserData({ error, data }) { //lds method to get current logged in user details usinf schema
        if (data) {
            if (this.currentuserdata) { //if other user details passed from other component store in attributes
                let returndata = JSON.parse(JSON.stringify(this.currentuserdata));
                this.roleName = returndata.Contact.HV_Role__c;
                this.userContactId = returndata.ContactId;
                //on patient's profile, show add note button to - physician, clinical user 
                if (this.roleName == 'Patient' && (data.fields.Contact.value.fields.HV_Role__c.value == 'Physician' || data.fields.Contact.value.fields.HV_Role__c.value == 'Clinical User')) {
                    this.showaddnoteButton = true;
                }
            }
            else { //store logged in user details
                this.roleName = data.fields.Contact.value.fields.HV_Role__c.value;
                this.userContactId = data.fields.Contact.value.fields.Id.value;
            }
            if (this.roleName == 'Patient') {
                this.getExistingPatientNotes();
            }
        }
        if (error) {
            this.errormessage = error.body.message; //set error if any
        }
    }

    getExistingPatientNotes() { //function to get all the existing notes related to patient
        getPatientNotes({ patientId: this.userContactId })
            .then(result => {
                console.log('result', result);
                this.patientAllNotes = result;
                if (this.patientAllNotes.length > 0) {
                    this.isPatientHasNotes = true;
                }
                else {
                    this.isPatientHasNotes = false;
                }
            })
            .catch(error => {
                this.errormessage = error.body.message;
            })
    }

    addNote(event) { //allow user to add notes
        this.notestoadd = true;
    }

    oncancelnotes(event) { //on cancel do not save notes record
        this.notestoadd = false;
        this.createNoteRecord = {};
    }

    inputFieldOnchange(event) { //handle input fields on change event
        if (event.target.name == 'HV_Comment__c') {
            this.createNoteRecord.HV_Comment__c = event.target.value;
        }
    }

    saveNotes(event) { //save notes and relate it to patient in system
        if (this.validateFields()) { //check all input validations
            this.createNoteRecord.HV_Contact__c = this.userContactId;
            this.createNoteRecord.Name = this.currentuserdata.Contact.FirstName + ' ' + this.currentuserdata.Contact.LastName + ' Note';
            createnotes({ noteToCreate: this.createNoteRecord }) //call apex method to save notes and relate it to patient record
                .then(record => {
                    const evt = new ShowToastEvent({ //show success toast
                        title: 'Success',
                        message: 'Notes Added Successfully!!',
                        variant: 'success',
                    });
                    this.dispatchEvent(evt);
                    this.errormessage = '';
                    this.notestoadd = false;
                    this.createNoteRecord = {};
                    this.getExistingPatientNotes();
                })
                .catch(error => {
                    const evt = new ShowToastEvent({
                        title: 'Error',
                        message: 'Error in adding notes' + error.body.message,
                        variant: 'error',
                    });
                    this.dispatchEvent(evt);
                    this.errormessage = 'Error in adding notes'; //set error message if any
                    this.notestoadd = false;
                    this.createNoteRecord = {};
                });
        }
    }

    validateFields() { //validate all the field level validations set at component(template) level
        const isInputsTextAreaCorrect = [...this.template.querySelectorAll('lightning-textarea')] //validate all teaxt area fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);
        return isInputsTextAreaCorrect;
    }
}