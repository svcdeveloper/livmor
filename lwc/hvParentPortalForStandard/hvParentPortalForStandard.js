import { LightningElement,track, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import {getObjectInfo} from 'lightning/uiObjectInfoApi';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import getSobjectRecords from '@salesforce/apex/HVQueryUtility.getMultipleRecordImprative';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import CURRENTUSERID from '@salesforce/user/Id';
import CURRENT_USER_ROLE from '@salesforce/schema/User.Contact.HV_Role__c';
import getUserTypeAndLoginInfo from '@salesforce/apex/HVLoginUser.getUserTypeAndLoginInfo';
import CURRENT_USER_CLINIC from '@salesforce/schema/User.Contact.AccountId';
import CURRENT_USER_CONTACTID from '@salesforce/schema/User.Contact.Id';
import CURRENT_USER_CLINIC_NAME from '@salesforce/schema/User.Contact.Account.Name';
import CURRENT_USER_DEPARTMENT from '@salesforce/schema/User.Contact.HV_Department__c';
import CURRENT_USER_ONBOARD_DATA_COLUMN from '@salesforce/schema/User.Contact.HV_Onboarding_Admin_Data_Columns__c';
import CURRENT_USER_PHYSICIAN_DATA_COLUMN from '@salesforce/schema/User.Contact.HV_Physician_Data_Columns__c';
import CURRENT_USER_PATIENT_DATA_COLUMN from '@salesforce/schema/User.Contact.HV_Patient_Data_Columns__c';
import CURRENT_USER_ASSET_ADMIN from '@salesforce/schema/User.Contact.HV_Asset_Admin_Data_Columns__c';
import CURRENT_USER_ASSET_USER from '@salesforce/schema/User.Contact.HV_Asset_User_Data_Columns__c';
import CURRENT_USER_CALL_CENTER_ADMIN from '@salesforce/schema/User.Contact.HV_Call_Center_Admin_Data_Columns__c';
import CURRENT_USER_CALL_CENTER_USER from '@salesforce/schema/User.Contact.HV_Call_Center_User_Data_Columns__c';
import CURRENT_USER_CLINICAL_USER from '@salesforce/schema/User.Contact.HV_Clinical_User_Data_Columns__c';
import CURRENT_USER_DEVICE_ADMIN from '@salesforce/schema/User.Contact.HV_Device_Admin_Data_Columns__c';
import CURRENT_USER_LIVMOR_ADMIN from '@salesforce/schema/User.Contact.HV_Livmor_Admin_Data_Columns__c';
const USERFIELDS = [CURRENT_USER_ROLE,CURRENT_USER_CLINIC,CURRENT_USER_CONTACTID,CURRENT_USER_CLINIC_NAME,CURRENT_USER_DEPARTMENT,
    CURRENT_USER_ONBOARD_DATA_COLUMN,CURRENT_USER_PHYSICIAN_DATA_COLUMN,CURRENT_USER_PATIENT_DATA_COLUMN,CURRENT_USER_ASSET_ADMIN,
    CURRENT_USER_ASSET_USER,CURRENT_USER_CALL_CENTER_ADMIN,CURRENT_USER_CALL_CENTER_USER,CURRENT_USER_CLINICAL_USER,
    CURRENT_USER_DEVICE_ADMIN,CURRENT_USER_LIVMOR_ADMIN]; //fields to query form user object

export default class HvParentPortalForStandard extends LightningElement {
    @track patientRegister = false;
    @track physicianRegister = false;
    @track userProfile = false;
    @track showTabs=true;
    @track isPatientAndFirstLogin;
    role;
    roleName;
    isModalOpen = false;
    conRecordTypeId;
    showOnboardingTable = false;
    showPhysicianTable = false;
    showPatientTable = false;
    showClinicalUserTable = false;
    rolesArray;
    isActiveTab = false;
    isassetusers = false;
    currentUserRecord;
    
    @wire(getUserTypeAndLoginInfo, {userId : CURRENTUSERID})
    userCosentFormInfo({data, error}){
        if(data){
            this.isPatientAndFirstLogin = data;
        }else if (error) {
            console.log('error', error);
        }
    };

    
    @wire(getRecord, { recordId: CURRENTUSERID, fields: USERFIELDS })
    userData({data,error}){
        if(data){
            this.currentUserRecord = data;
            let roleVal = data.fields.Contact.value.fields.HV_Role__c.value;
            if(roleVal == 'Asset Admin' || roleVal == 'Asset User'){
                this.isassetusers = true;
            }
            if(roleVal){
                getSobjectRecords({ 
                    objectname: 'Roles_Data_Table_hierarchy__mdt', 
                    fieldnames: ' Id,DeveloperName,MasterLabel,HV_Column_Api_Name__c,HV_Create_User_Access__c,HV_Role__c,HV_Tab_Access__c ',
                    conditions: ' where MasterLabel = \'' + roleVal + '\''
        
                 }) //get selected user details to pass to user profile page
                .then(result => {                     
                    if(result){                         
                        this.rolesArray = [];                        
                        result.forEach( record => {                            
                            this.rolesArray.push({
                                roleName : record.HV_Role__c,
                                columnApiName:record.HV_Column_Api_Name__c,
                                tabAccess:record.HV_Tab_Access__c,
                                createUserAccess:record.HV_Create_User_Access__c
                            });
                        })                      
                        
                        /*let rolesMap = new Map();
                        if(result.HV_Tabs_Role__c){
                            result.HV_Tabs_Role__c.split(',').forEach( tabrole =>{
                                rolesMap.set(tabrole,'')
                            })

                        }
                        if(result.HV_Create_Users_Role__c){
                            result.HV_Create_Users_Role__c.split(',').forEach( createuserrole =>{
                                if(rolesMap.has(createuserrole)){
                                    rolesMap.set(createuserrole,createuserrole);
                                }
                                
                            }) 
                        }
                        rolesMap.forEach((key,value) => {
                            console.log('key',key); 
                            console.log('value',value);
                            this.rolesArray.push({tabrole:value,createuserrole:key,isActive:true})
                        })*/                        
                    }
                })
                .catch(error => {        
                    console.log('error',error);
                })
            }
        }
        if(error){
            console.log('error',error); 
        }
    }

    @wire(getObjectInfo, {
		objectApiName: CONTACT_OBJECT
	})
	contactMetadata({data, error}){
        if(data){
            const rtis = data.recordTypeInfos;
            this.conRecordTypeId = Object.keys(rtis).find(rti => rtis[rti].name === 'Business');
        }
        if(error){
            console.log('error',error);
        }
    }; 

    hanldleChild(event){        
        if(event.detail.createpatient){
            this.patientRegister = event.detail.createpatient;
            this.showTabs=false;
        } if( event.detail.createphysician){
            this.physicianRegister = event.detail.createphysician;
            this.roleName = event.detail.role;
            this.showTabs=false;
            this.isModalOpen = true;
        }        
    }
    hanldleChild2(event){       
            this.userProfile = event.detail.userProfile;
            this.showTabs=false;
            this.patientRegister=false;
            this.physicianRegister=false;
            this.isActiveTab = true; 
    }
    
    revertBackDashboard(event){
        this.patientRegister = false;
        this.physicianRegister=false;
        this.userProfile=false;
        this.showTabs=true;
    }

    closeModal(){
        this.isModalOpen = false;
    } 

    submitRole(){
        this.isModalOpen = false;
    }

    handleActive(event){  
        let tempRolesArray = []
        this.rolesArray.forEach( result => {
            if(result.roleName === event.target.value){
                tempRolesArray.push({
                    roleName: result.roleName,
                    columnApiName: result.columnApiName,
                    tabAccess: true,
                    createUserAccess: result.createUserAccess, 
                   
                });
            }else{ 
                tempRolesArray.push({
                    roleName: result.roleName,
                    columnApiName: result.columnApiName,
                    tabAccess: false,
                    createUserAccess: result.createUserAccess,
                    
                });
            }  
        }) 
        this.rolesArray = []; 
        this.rolesArray = tempRolesArray;
    } 
}