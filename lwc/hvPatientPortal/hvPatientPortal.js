import { LightningElement,track, wire } from 'lwc';
import CURRENTUSERID from '@salesforce/user/Id';
import patientId from '@salesforce/apex/HVPatientDashboardController.getUserId';
import getUserTypeAndLoginInfo from '@salesforce/apex/HVLoginUser.getUserTypeAndLoginInfo';

export default class HvPatientPortal extends LightningElement {
    @track isPatientAndFirstLogin;
    @track showhvdiv = false;
    @track showprofile = false;
    @track isModalOpen = false;
    frompatientportal = true;
    currentpatientid;

    @wire(getUserTypeAndLoginInfo, {userId : CURRENTUSERID})
    userCosentFormInfo({data, error}){
        if(data){
            this.isPatientAndFirstLogin = data;
        }else if (error) {
            console.log('error', error);
        }
    };

    connectedCallback(){
    patientId()  
        .then(data => {
            if (data) {
                this.currentpatientid = data.ContactId;
            }
        })
        .catch(error => {
            this.error = error;  
        })
    }
    
    get showdatable() {
        return (!this.showhvdiv && !this.showprofile);
    }

    showHeartView(){
        this.showhvdiv = true;
    }

    showPatientDashboard(){
        this.showhvdiv = false;
    }

    showMessages(){
        this.isModalOpen = true;
    }

    closeModal() {
        this.isModalOpen = false;
    }

    backtopatientdashboard(){
        this.showprofile = false;
    }

    handleProfile(){
        this.showprofile = true;
    }


    refreshViewAfterConsent(event){
        window.location.reload();
        //refreshApex(this.userCosentFormInfo);
    }
}