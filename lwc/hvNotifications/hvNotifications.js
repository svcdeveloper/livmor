import { LightningElement, wire, track, api } from 'lwc';
import getLogedInUserNotifications from '@salesforce/apex/HVBroadcastAlertController.getLogedInUserNotifications';

const columns = [
                { label: 'Message Modal', fieldName: 'messagemodal', type: 'showNotification',
                typeAttributes: { 
                    rowmessage: { fieldName: 'Message__c' }
                }
              },
              { label: 'Time Stamp', fieldName: 'Time_Stamp__c' },
              { label: 'Sent By', fieldName: 'Sent_By__c' },
              ];

export default class HvNotifications extends LightningElement {
    error;
    columns = columns;
    totalNumberOfRows = 20;
    rowOffSet = 0;
    numberofrecordtoshow = 20;
    @track displaydata = [];
    @track tabledata;
    isLoading = false;
    @api conid;

    connectedCallback(){
        getLogedInUserNotifications({contactId : this.conid})  
        .then(data => {
            if (data) {
                this.isLoading = true;
                data.forEach(element => {
                    let eachRec = {};
                    eachRec['Id'] = element.Id;
                    eachRec['Message__c'] = element.Message__c;
                    eachRec['Sent_By__c'] = element.Sent_By__c;
                    let utcSeconds = element.Time_Stamp__c;
                    let d = new Date(0);
                    if(element.Time_Stamp__c){
                        if(element.Time_Stamp__c.length == 10){
                            eachRec['Time_Stamp__c'] = new Date(parseInt(element.Time_Stamp__c)).toLocaleString();
                            /*let measuredate = new Date(element.Time_Stamp__c);
                            const timeZoneAbbreviated = () => { 
                                //element.Time_Stamp__c = 1629636501;                     
                                const { 1: tz } =measuredate.toString().match(/\((.+)\)/);                        
                                    if (tz.includes(" ")) {                          
                                        return tz.split(" ").map(([first]) => first).join("");                        
                                    } else {                          
                                        return tz;                        
                                    }                    
                                };                                        
                                    eachRec['Time_Stamp__c'] = measuredate.toLocaleString(undefined, {                        
                                    weekday: 'short', // long, short, narrow                        
                                    day: 'numeric', // numeric, 2-digit                        
                                    year: 'numeric', // numeric, 2-digit                        
                                    month: 'short', // numeric, 2-digit, long, short, narrow                        
                                    hour: 'numeric', // numeric, 2-digit                        
                                    minute: 'numeric', // numeric, 2-digit                    
                                })+' ('+timeZoneAbbreviated()+')';*/
                                
                            }else{
                                eachRec['Time_Stamp__c'] = 'Invalid date';
                            }
                    }else{
                        eachRec['Time_Stamp__c'] = 'Invalid date';
                    }
                    this.displaydata.push(eachRec);
                });
                //this.displaydata = data;
                if(this.displaydata.length > 0){
                    this.tabledata = this.displaydata.slice(0, this.totalNumberOfRows);
                }
                this.error = undefined;
                this.isLoading = false;
            }
        })
        .catch(error => {
            this.isLoading = false;
            this.error = error;  
        })
    }

    loadMoreData(event) {
        this.isLoading = true;
        if (event.target.isLoading) {
            return;
        }
        let isload = event.target.isLoading;
        isload = true;
        this.rowOffSet = this.rowOffSet + this.numberofrecordtoshow;
        this.totalNumberOfRows = this.totalNumberOfRows + this.numberofrecordtoshow;

        if (this.displaydata.length > 0 && this.totalNumberOfRows < (this.displaydata.length + this.numberofrecordtoshow)) {
            this.tabledata = this.displaydata.slice(0, this.totalNumberOfRows);
            isload = false;
        }
        else if (this.displaydata.length > 0 && this.totalNumberOfRows > this.displaydata.length) {
            isload = false;
        }
        else {
            this.tabledata = [];
            isload = false;
        }
        this.isLoading = false;
    }
}