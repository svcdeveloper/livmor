import { LightningElement,track } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import { NavigationMixin } from 'lightning/navigation';
import HVFullCalendarJS from '@salesforce/resourceUrl/HVFullCalendarJS';
import HVChartJS from '@salesforce/resourceUrl/HVChartJS';
import patientId from '@salesforce/apex/HVPatientDashboardController.getUserId';
//import getAllMyTasks from '@salesforce/apex/HVCalenderController.getAllMyTasks';
import USER_ID from "@salesforce/user/Id";

export default class HvCalendar extends NavigationMixin(LightningElement) {
    fullcalanderjsInitialized = false; // to initialize the third party scripts and styles
    @track allmystasks =[]; // all the user tasks
    @track selectedTask = undefined; // value of selected task
    apiendpoint = 'https://salesforceapi.livmor.com/calendartask';
    //apirequestbody = { 'patientid':'test00000001','startdate':'1622505600','enddate':'1632182400' };
    apirequestbody = {};
    inputStartDate= Math.floor(new Date().getTime() / 1000.0) - 86400 * 365;
    inputenddate = Math.floor(new Date().getTime() / 1000.0);
    inputpatientId;
    apimethod = 'POST';
    devicetype = 'calendarTask';
    @track responsedata = [];
    awsresponseloaded = false;

    connectedCallback(){
        patientId()  
        .then(data => {
            if (data) {
                this.inputpatientId = data.Contact.HV_Patient_ID__c;
                this.apirequestbody["patientid"] = this.inputpatientId;
                this.apirequestbody["startdate"] = this.inputStartDate;
                this.apirequestbody["enddate"] = this.inputenddate;
                this.awsresponseloaded = true;
            }
        })
        .catch(error => {
            this.error = error;  
        })
    }

    handleresponse(event){
        let actualData = event.detail;
        this.responsedata = (event.detail.awsresponse.data);
        this.awsresponseloaded = false;
        this.initializeFullCalenderJS();
    }
    
    renderedCallback() { // load all the thirdparty scripts and styles [FullCalenderJS, ChartJS]

        if(this.fullcalanderjsInitialized){
            return;
        }
        this.fullcalanderjsInitialized = true;

        Promise.all([
            loadScript(this, HVFullCalendarJS + '/HVFullCalendarJS/jquery.min.js'),
            loadScript(this, HVFullCalendarJS + '/HVFullCalendarJS/moment.min.js'),
            loadScript(this, HVFullCalendarJS + '/HVFullCalendarJS/theme.js'),
            loadScript(this, HVFullCalendarJS + '/HVFullCalendarJS/fullcalendar.min.js'),
            loadStyle(this, HVFullCalendarJS + '/HVFullCalendarJS/fullcalendar.min.css'),
            loadScript(this, HVChartJS)
        ])
        .then(() => {
            console.log('load success');
        })
        .catch(error => {       
            console.error({message: 'Error occured on HVFullCalendarJS',error});
        })

        /*Promise.all([loadScript(this, HVChartJS)])
        .then(() => {
            console.log('load success');
        })
        .catch(error => {
            console.log('error in chart loading');
        });*/
       
    }

    // getMyTasks(){ 
    //     getAllMyTasks({userId:USER_ID}) // call apex to get all the task related to loggedin user
    //     .then(result=>{
    //         this.allmystasks = result.map(item => { 
    //             return {
    //                 id: item.Id,
    //                 start : item.CreatedDate,
    //                 title : item.Subject,
    //                 status : item.Status,
    //                 end : item.ActivityDate
    //             };
    //         });
    //         this.initializeFullCalenderJS();
    //     })
    //     .catch(error=>{
    //         console.log('Error'+error);
    //     })
    // }

    initializeFullCalenderJS(){ // initialize and set calender events,days
        const ele = this.template.querySelector('.fullcalendar'); 
        $(ele).fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            defaultDate: new Date(),
            navLinks: true,
            editable: true,
            eventLimit: true,
            /*events: this.allmystasks,*/
            themeSystem: 'standard',
            dragScroll: true,
            /*eventClick: this.eventClickHandler.bind(this),*/
            dayClick : this.dayClickHandler.bind(this),
            eventMouseover : this.eventMouseoverHandler.bind(this),
            /*eventRender: this.eventRenderHandler.bind(this),*/
            dayRender: this.dayRenderHandler.bind(this),
        });
    }

    /*eventClickHandler = (event, jsEvent, view) => { //on click of task navigate to task detail
        this.selectedTask =  event;
        this[NavigationMixin.GenerateUrl]({
            type:'standard__recordPage',
            attributes:{
                "recordId":this.selectedTask.id,
                "objectApiName":"Task",
                "actionName": "view"
            },
        })
        .then(url => {
            window.open(url);
        });
    }*/

    dayClickHandler = (date, jsEvent, view)=>{ // prevent any event on day click in calender
        const ele = this.template.querySelector('div.fullcalendar');         
        jsEvent.preventDefault();
    }

    dayRenderHandler = (date,el,view) => { // on each day render find all the tasks, its status and create donught chart
        //let todayDate = new Date().setHours(0, 0, 0, 0);
        let dateRender = new Date(date).toLocaleDateString();
        let totaltask = 0;
        let taskPending = 0;
        let taskcompleted = 0;
        let noTask = 0;
        //let overduetask = 0;
        // let inprogresstask = 0;
        // let completedtask = 0;
        let showchart = false;
        // if(dateRender <= todayDate){
        //     const ele = this.template.querySelector('div.fullcalendar'); 
        //     var allEvents = [];
        //     allEvents = $(ele).fullCalendar('clientEvents');
        //     var event = $.grep(allEvents, function (v) {                
        //         if(new Date(v.start).setHours(0, 0, 0, 0) === dateRender){                    
        //             showchart = true;
        //             if(v.status === 'Completed'){
        //                 completedtask = completedtask +1;
        //             }
        //             if(v.status !== 'Completed' && new Date(v.end).setHours(0, 0, 0, 0) <  todayDate){
        //                 overduetask = overduetask +1;
        //             }
        //             if(v.status !== 'Completed' && new Date(v.end).setHours(0, 0, 0, 0) >=  todayDate){
        //                 inprogresstask = inprogresstask +1;
        //             }
        //         }                
        //     });
            this.responsedata.forEach(element => {
                if(element.date){
                    if(new Date(element.date).toLocaleDateString() == dateRender){
                        totaltask = element.totaltask;
                        taskcompleted = element.taskcompleted;
                        taskPending = totaltask - taskcompleted;
                        showchart = true;
                    }else{
                        showchart = false;
                    }
                }
            });
            let can = document.createElement('canvas');
            can.className = 'myPieChart';
            can.id = 'myPieChart';
            el.append(can);
            var ctx = can.getContext('2d');
            // if(showchart == true){
            //     var myChart = new Chart(ctx, {
            //         type: 'doughnut',
            //         data: {
            //             labels: ['OverDue','InProgress','Completed'],
            //             datasets: [{
            //                 label: 'My Tasks',
            //                 data: [overduetask, inprogresstask, completedtask],
            //                 backgroundColor: [
            //                     'rgba(234, 0, 30, 1)',
            //                     'rgba(254, 246, 0, 1)',
            //                     'rgba(46, 132, 74, 1)'
            //                 ],
            //                 borderColor: [
            //                     'rgba(234, 0, 30, 1)',
            //                     'rgba(254, 246, 0, 1)',
            //                     'rgba(46, 132, 74, 1)'
            //                 ],
            //                 borderWidth: 0
            //             }]
            //         },
            //         options: {
            //             legend: {
            //                 display: false,
            //             },
            //             scales: {
            //                 y: {
            //                     beginAtZero: true
            //                 }
            //             },
            //             cutoutPercentage: 85
            //         }
            //     });
            // }
            // else{
                if(!el.hasClass('fc-other-month')) {
                    var myChart = new Chart(ctx, {
                        type: 'doughnut',
                        data: {
                            labels: ['Completed','Pending'],
                            datasets: [{
                                label: 'My Tasks',
                                data: [taskcompleted, taskPending],
                                /*data: (showchart ? [taskcompleted, taskPending] : [0, 0]),*/
                                backgroundColor: [
                                    'rgba(1, 118, 211, 1)',
                                    'rgb(255,99,71)'
                                ],
                                borderColor: [
                                    'rgba(1, 118, 211, 1)',
                                    'rgb(255,99,71)'
                                ],
                                borderWidth: 0
                            }]
                        },
                        options: {
                            legend: {
                                display: false,
                            },
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            },
                            cutoutPercentage: 85
                        }
                    });
                }
            //}
    }

    eventRenderHandler = (event,element,view) => { // do not render any event on calender UI
        return false;
    }
    
    eventMouseoverHandler = (event, jsEvent, view) => { // do task on mouseover event

    }
   
}