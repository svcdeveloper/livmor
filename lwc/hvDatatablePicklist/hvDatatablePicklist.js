import { LightningElement,api,wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import currentUserId from '@salesforce/user/Id';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord } from 'lightning/uiRecordApi';
import getClinicFieldsMapping from '@salesforce/apex/HVPatientRegistrationController.getClinicFieldsMapping';
import updateSobjectRecord from '@salesforce/apex/HVDMLUtility.updateRecord';
import getSobjectRecord from '@salesforce/apex/HVQueryUtility.getSingleRecordImprative';

export default class HvDatatablePicklist extends LightningElement {
    @api value;
    @api rowId;
    isModalOpen = false; 
    departmentValue;
    departmentOptions;
    selectedDepartment;
    
    connectedCallback(){        
        if(this.value){
            if(this.value.length > 10){
                this.departmentValue = this.value.slice(0,16) +' ...';
               // console.log('this.value.slice(0,30)',this.value.slice(0,30))
            }
            this.selectedDepartment = this.value.split(',');
        }       
        getSobjectRecord({ 
            objectname: 'Contact', 
            fieldnames: 'Id,AccountId, HV_Role__c, Account.HV_Patient_Clinic__c', 
            conditions: 'where Id = \'' + this.rowId + '\'' }) 
        .then(data => {
            if(data.HV_Role__c){
                if(data.HV_Role__c === 'Patient'){
                    if(data.Account.HV_Patient_Clinic__c){    
                            this.callMetaData(data.Account.HV_Patient_Clinic__c);                         
                    }
                }else{
                    if(data.AccountId){ 
                            this.callMetaData(data.AccountId);                         
                    }
                }
            }
        })
        .catch(error => {
            console.log('err',error);
        })    
        //console.log('dep row i', this.rowId);        
    } 
 
    callMetaData(clinicId) { 
        //method to get clinic and fields mapping from metadata viz. ClinicFieldsMapping
        getClinicFieldsMapping({ clinicId: clinicId }) //call apex method as imperative
        .then(data => {
            if (data) {                
                this.departmentOptions = [];
                data.HV_Department_Values__c.split(",").map(field => {
                    field.trim();
                    this.departmentOptions.push({ label: field.trim(), value: field.trim() });
                });                
            }
        })
        .catch(error => {
            console.log('error',error);
        });
    }

    onselectValue(e){
        this.selectedDepartment = e.detail.value;
    }

    handleChange(event) {
        this.value = event.detail.value;
    }

     openModal(){
        this.isModalOpen = true; 
    }
 
    closeModal(){
        this.isModalOpen = false;
    }

    submitData(){        
        if(this.selectedDepartment){     
            this.departmentValue = this.selectedDepartment.join(',').slice(0,16) +' ...';           
            const fields = {}
            fields.attributes = { type : "Contact"}
            fields.Id = this.rowId;
            fields.HV_Department__c = this.selectedDepartment.join(',');
            updateSobjectRecord({
                uapdateRec : fields
            })
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Department updated',
                        variant: 'success'
                    })
                );
                // Display fresh data in the form
               // return refreshApex(this.contact);
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error creating record',
                        message: error.body.message,
                        variant: 'error'
                    })
                );
            });

            this.isModalOpen =false;
         }else{
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error',
                    message: 'Please select a value',
                    variant: 'error'
                })
            );
         }
        
    }
}