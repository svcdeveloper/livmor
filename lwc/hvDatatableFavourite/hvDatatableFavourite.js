import { LightningElement,api } from 'lwc';
import uId from '@salesforce/user/Id';
import favimg from '@salesforce/resourceUrl/pushPinColor';
import notfavimg from '@salesforce/resourceUrl/pushPinPlain';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import setUnsetFavourite from '@salesforce/apex/HVPhysicianDashboard.setUnsetFavourite';

export default class HvDatatableFavourite extends LightningElement {
    favimg = favimg;
    notfavimg = notfavimg;
    @api isFav;
    @api rowId;

    selectFav(){
        this.isFav = true;
        // apex call to insert contact-contact relationship obj record
        setUnsetFavourite({patientId : this.rowId, loginUser : uId, action : 'set'})
            .then(result => {
                if(result){
                    this.showToast('Success', 'Patient marked favorite.','success');
                    const event = new CustomEvent('favclick', {
                        composed: true,
                        bubbles: true
                    });
                    this.dispatchEvent(event);
                }
            })
            .catch(error => {
                this.showToast('Error', 'Some error occurred.', 'error');
            })
    }

    selectNotFav(){
        this.isFav = false;
        // apex call to delete contact-contact relationship obj record
        setUnsetFavourite({patientId : this.rowId, loginUser : uId, action : 'unset'})
            .then(result => {
                if(result){
                    this.showToast('Success', 'Patient marked unfavorite.','success');
                    const event = new CustomEvent('favclick', {
                        composed: true,
                        bubbles: true
                    });
                    this.dispatchEvent(event);
                }
            })
            .catch(error => {
                this.showToast('Error', 'Some error occurred.', 'error');
            })
    }

    showToast(msgTitle, msg, msgVarient) {
        const event = new ShowToastEvent({
            title: msgTitle,
            message: msg,
            variant: msgVarient
        });
        this.dispatchEvent(event);
    }
}