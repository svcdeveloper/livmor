import { LightningElement,api } from 'lwc';

export default class HvDatatableToggleButton extends LightningElement {
    @api checked;
    @api buttonDisabled;
    @api rowId;

    handleToggle(event) {
        const cevent = CustomEvent('selectedrec', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                value: { rowId: this.rowId, state: event.target.checked }
            },
        });
        this.dispatchEvent(cevent);
    }

    get getInactiveMsg(){
        return this.buttonDisabled?'Disabled':'Not Selected';
    }
}