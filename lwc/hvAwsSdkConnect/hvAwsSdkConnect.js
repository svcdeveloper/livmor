import { LightningElement, api } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import LivmorAWSAPIs from '@salesforce/resourceUrl/LivmorAWSAPIs';
import getDeviceAWSIntegrationDetails from '@salesforce/apex/HVPatientMeasurementController.getDeviceAWSIntegrationDetails';

export default class HvAwsSdkConnect extends LightningElement {
    awsSdkInitialized = false;
    @api reqendpoint = '';
    @api reqmethod = '';
    @api reqbody = '';
    @api reqidentifier = '';
    accessKeyId;
    secretAccessKey;
    region;
    errormessage;
   
    connectedCallback() {
        console.log('reqendpoint: ' + this.reqendpoint);
        console.log('reqmethod: ' + this.reqmethod);
        console.log('reqbody: ' + JSON.stringify(this.reqbody));   

        getDeviceAWSIntegrationDetails()
        .then(result =>{
            this.accessKeyId = result.HV_AWSAccessKey__c;
            this.secretAccessKey = result.HV_AWSSecretKey__c;
            this.region = result.HV_AWSRegion__c;
            if (this.awsSdkInitialized) {
                return;
            }
            this.awsSdkInitialized = true;
            Promise.all([
                loadScript(this, LivmorAWSAPIs + '/apiGateway-js-sdk/lib/axios/dist/axios.standalone.js'),
                loadScript(this, LivmorAWSAPIs + '/apiGateway-js-sdk/lib/CryptoJS/rollups/hmac-sha256.js'),
                loadScript(this, LivmorAWSAPIs + '/apiGateway-js-sdk/lib/CryptoJS/rollups/sha256.js'),
                loadScript(this, LivmorAWSAPIs + '/apiGateway-js-sdk/lib/CryptoJS/components/hmac.js'),
                loadScript(this, LivmorAWSAPIs + '/apiGateway-js-sdk/lib/CryptoJS/components/enc-base64.js'),
                loadScript(this, LivmorAWSAPIs + '/apiGateway-js-sdk/lib/url-template/url-template.js'),
                loadScript(this, LivmorAWSAPIs + '/apiGateway-js-sdk/lib/apiGatewayCore/sigV4Client.js'),
                loadScript(this, LivmorAWSAPIs + '/apiGateway-js-sdk/lib/apiGatewayCore/apiGatewayClient.js'),
                loadScript(this, LivmorAWSAPIs + '/apiGateway-js-sdk/lib/apiGatewayCore/simpleHttpClient.js'),
                loadScript(this, LivmorAWSAPIs + '/apiGateway-js-sdk/lib/apiGatewayCore/utils.js'),
                loadScript(this, LivmorAWSAPIs + '/apiGateway-js-sdk/apigClient.js'),
            ]).then(result => {
                var apigClient = apigClientFactory.newClient({
                    invokeUrl: this.reqendpoint,
                    region: this.region,
                    accessKey: this.accessKeyId,
                    secretKey: this.secretAccessKey 
                });
                var method = this.reqmethod;
                var params = {};
                var additionalParams = {
                    headers: {},
                    queryParams: {}
                };
                var body = this.reqbody;
                if(this.reqidentifier == 'watchsessions'){
                    apigClient.watchsessionsPost(params, body, additionalParams)
                        .then(result => {
                            this.successAction(result);
                    }).catch(error => {
                        this.errorAction();
                        console.log('Error in callout: ', error);
                    });
                }
                else if (this.reqidentifier == 'reportsessions'){
                    apigClient.reportdetailsPost(params, body, additionalParams)
                    .then(result => {
                        this.successAction(result);
                    }).catch(error => {
                        this.errorAction();
                        console.log('Error in callout: ', error);
                    });
                }
                else if (this.reqidentifier == 'dashboardlatestdata'){
                    apigClient.dashboardPost(params, body, additionalParams)
                    .then(result => {
                        this.successAction(result);
                    }).catch(error => {
                        this.errorAction();
                        console.log('Error in callout: ', error);
                    });
                }
                else if (this.reqidentifier == 'calendarTask'){
                    apigClient.calendartaskPost(params, body, additionalParams)
                    .then(result => {
                        this.successAction(result);
                    }).catch(error => {
                        this.errorAction();
                        console.log('Error in callout: ', error);
                    });
                }
                /* other else ifs to be added as per the other endpoints */ 
                else{
                    apigClient.peripheralPost(params, body, additionalParams)
                        .then(result => {
                            this.successAction(result);
                    }).catch(error => {
                        this.errorAction();
                        console.log('Error in callout: ', error);
                    });
                }
            }).catch(error => {
                this.errorAction();
                console.log('Error in file load: ', error);
            });
        })
        .catch(error=>{
            this.errorAction();
            this.errormessage = error.body.message;
        })     
    }

    successAction(result){
        console.log('AWS callout response: ', result);
        if(result.status == 200){
            const event = new CustomEvent('awsresponsesuccess', {
                detail: { awsresponse: result, awscallidentifier: this.reqidentifier }
            });
            this.dispatchEvent(event);
        }
        else{            
            const event = new CustomEvent('awsresponseerror', {
                detail: { awsresponse: result, awscallidentifier: this.reqidentifier }
            });
            this.dispatchEvent(event);
        }
    }

    errorAction(){
        const event = new CustomEvent('awsresponseerror', {
            detail: { awsresponse: 'Error', awscallidentifier: this.reqidentifier }
        });
        this.dispatchEvent(event);
    }
}