import { LightningElement, track, api } from 'lwc';
import verifyPhoneOTP from '@salesforce/apex/HVForgotPassword.verifyPhoneOTP';
import sendOTPtoUser from '@salesforce/apex/HVForgotPassword.sendOTPtoUser';
export default class HvOtpVerification extends LightningElement {
    emailString;
    phoneString;
    homePhoneString;

    @track message = false;
    emailSelection = false;
    phoneSelection = false;
    homePhoneSelection = false;
    errorMessageDisplay=false;
    enterOtp;
    errorMessage;
    @api email;
    @api phone;
    @api homePhone;
    @api otp;
    @api selectedValue;

    @track seconds = 0;
    @track totalSeconds = 0;
    hour = 0;
    minute = 0;
    intervalId = null;
    duration = 60*2;

    changeOtp(event) {
        this.enterOtp = event.target.value;
    }
    

    connectedCallback() {    
        // this.startTimer(this.duration);
        if (this.selectedValue == 'email') {
            this.emailString = this.emailHide(this.email);
            this.emailSelection = true;
        }
        if (this.selectedValue == 'phone') {
            this.phoneString = this.phoneHide(this.phone);
            this.phoneSelection = true;
        }

        if (this.selectedValue == 'homePhone') {
            this.homePhoneString = this.phoneHide(this.homePhone);
            this.homePhoneSelection = true;
        }

    }
    startTimer(duration) {
        var timer = duration, minutes, seconds;
        setInterval(function () {

            ++this.totalSeconds;

            this.hour = Math.floor(this.totalSeconds /3600);
            this.minute = Math.floor((this.totalSeconds - this.hour*3600)/60);
            this.seconds = this.totalSeconds - (this.hour*3600 + this.minute*60);
            console.log(this.totalSeconds);
            console.log(this.seconds);
            
            // minutes = parseInt(timer / 60, 10);
            // seconds = parseInt(timer % 60, 10);
            
            // minutes = minutes < 10 ? "0" + minutes : minutes;
            // seconds = seconds < 10 ? "0" + seconds : seconds;
            // this.totalSeconds++;
            // console.log(this.totalSeconds);

            // this.querySelector('[data-id="hourblock"]').innerHTML =hour;
            // this.querySelector('[data-id="minuteblock"]').innerHTML =minute;
            // this.template.querySelector('[data-id="secondsblock"]').innerHTML = minutes + ":" + totalSeconds;
            console.log(this.template.querySelector('[data-id="secondsblock"]'));
            this.template.querySelector('[data-id="secondsblock"]').textContent = this.totalSeconds;

            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
      }

    verify(event) {       
        console.log('verify otp');
        if(this.selectedValue == 'homePhone'){
            verifyPhoneOTP({ otp: this.enterOtp, phone: this.homePhone })
                .then(result => {                    
                    let response = JSON.parse(result);
                    if (response.status == 'approved') {
                        const event = new CustomEvent('child', {                            
                            detail: { step: 4 }
                        });
                        this.dispatchEvent(event);
                    } else {    
                         this.errorMessageDisplay = true;  
                         this.message = false;                  
                        this.errorMessage = 'OTP Mismatch';
                    }
                }).catch(error => {                    
                    if (error.body.message) {
                        this.errorMessage = error.body.message;
                    }
                });
        }else if (this.selectedValue == 'phone') {            
            verifyPhoneOTP({ otp: this.enterOtp, phone: this.phone })
                .then(result => {                    
                    let response = JSON.parse(result);
                    if (response.status == 'approved') {
                        const event = new CustomEvent('child', {                            
                            detail: { step: 4 }
                        });
                        this.dispatchEvent(event);
                    } else {    
                         this.errorMessageDisplay = true;  
                         this.message = false;                  
                        this.errorMessage = 'OTP Mismatch';
                    }
                }).catch(error => {                    
                    if (error.body.message) {
                        this.errorMessage = error.body.message;
                    }
                });
        } else if (this.selectedValue == 'email') {
            if (this.otp == this.enterOtp) {                
                const event = new CustomEvent('child', {                    
                    detail: { step: 4 }
                });
                this.dispatchEvent(event);
            } else {                
                this.errorMessageDisplay = true;
                this.message = false;
                this.errorMessage = 'OTP Mismatch';
            }
        }
    }

    emailHide(email) {
        var emi = email.split('@');
        var emiLength1 = emi[0].length;
        var em2 = emi[1].split('.');
        var emiLength2 = em2[0].length;
        var emailString = emi[0].charAt(0) + emi[0].charAt(1) + emi[0].charAt(2);
        for (var i = 3; i < emiLength1; i++) {
            emailString += '.';
        }
        emailString += '@';
        emailString += em2[0].charAt(0) + em2[0].charAt(1);
        for (var j = 2; j < emiLength2; j++) {

            emailString += '.';
        }
        emailString += '.' + em2[1];
        return emailString;
    }

    phoneHide(phone) {
        var phonelength = phone.length;
        var phoneString = '';
        for (var i = 0; i < (phonelength - 2); i++) {
            phoneString += '.';
        }
        phoneString += phone.charAt(phonelength - 2) + phone.charAt(phonelength - 1);
        return phoneString;
    }
    ResendOTP() {
        this.enterOtp = '';
            this.errorMessageDisplay = false;
            if(this.homePhoneSelection){
                console.log('phone', this.homePhone);
                this.selectedValue = 'homePhone';
                sendOTPtoUser({
                    email: null,
                    phone: this.homePhone,
                    channel : 'call'
                })
                    .then(result => {
                        console.log(JSON.stringify(result));
        
                        const event = new CustomEvent('child', {
                            detail: { step: 3, selectedValue: this.selectedValue }
                        });
                        this.dispatchEvent(event);
                    })
                    .catch(error => {
                        console.log('error', error);
                        console.log(JSON.stringify(error));
                        if (error.body.message) {
                            this.errorMessage = error.body.message;
                        }
                    });
               }else if (this.phoneSelection) {
                    console.log('phone', this.phone);
                    this.selectedValue = 'phone';
                    sendOTPtoUser({
                        email: null,
                        phone: this.phone,
                        channel : 'sms'
                    })
                        .then(result => {
                            console.log(JSON.stringify(result));
        
                            const event = new CustomEvent('child', {
                                detail: { step: 3, selectedValue: this.selectedValue }
                            });
                            this.dispatchEvent(event);
                        })
                        .catch(error => {
                            console.log('error', error);
                            console.log(JSON.stringify(error));
                            if (error.body.message) {
                                this.errorMessage = error.body.message;
                            }
                        });
                } else 
                    if(this.emailSelection){
                        
                    
                    console.log('email,', this.email);
                    this.selectedValue = 'email';
                        sendOTPtoUser({
                            email: this.email,
                            phone: null,
                            channel: 'sms'
                        })
                        .then(result => {
                            console.log(JSON.stringify(result));
                            this.otp = result;
                            console.log("Prefered Contact OTP " + this.otp);
                            const event = new CustomEvent('child', {
                                // detail contains only primitives
                                detail: { step: 3, otp: this.otp, selectedValue: this.selectedValue }
                            });
                            this.dispatchEvent(event);
                        })
                        .catch(error => {
                            console.log('error', error);
                            console.log(JSON.stringify(error));
                            if (error.body.message) {
                                this.errorMessage = error.body.message;
                            }
                        });
                       }
    }
}