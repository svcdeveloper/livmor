import { LightningElement, track, wire, api } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import currentUserId from '@salesforce/user/Id';
import CURRENT_USER_CLINIC from '@salesforce/schema/User.Contact.AccountId';
import CURRENT_USER_CONTACTID from '@salesforce/schema/User.Contact.Id';
import CURRENT_USER_ROLE from '@salesforce/schema/User.Contact.HV_Role__c';
import SMALL_PHOTOURL from '@salesforce/schema/User.SmallPhotoUrl';
import CURRENT_USER_CLINIC_NAME from '@salesforce/schema/User.Contact.Account.Name';
import PATIENT_USER_CLINIC from '@salesforce/schema/User.Contact.Account.HV_Patient_Clinic__c';
import PATIENT_USER_CLINIC_NAME from '@salesforce/schema/User.Contact.Account.HV_Patient_Clinic__r.Name';
import GENDER_FIELD from '@salesforce/schema/Contact.HealthCloudGA__Gender__c';
import PREFERRED_CONTACT from '@salesforce/schema/Contact.Preferred_Contact_Method__c';
import getRoleFieldsMapping from '@salesforce/apex/HVUserProfileController.getRoleFieldsMapping';
import getPatientClinicFieldsMapping from '@salesforce/apex/HVUserProfileController.getPatientClinicFieldsMapping';
import getOtherUserData from '@salesforce/apex/HVUserProfileController.getOtherUserData';
import getPatientUserData from '@salesforce/apex/HVUserProfileController.getPatientUserData';
import saveUserData from '@salesforce/apex/HVUserProfileController.saveUserData';
import patientOffboard from '@salesforce/apex/HVUserProfileController.patientOffboard';

const USERFIELDS = [CURRENT_USER_CLINIC, CURRENT_USER_CONTACTID, CURRENT_USER_CLINIC_NAME, CURRENT_USER_ROLE, PATIENT_USER_CLINIC, PATIENT_USER_CLINIC_NAME, SMALL_PHOTOURL]; //fields to query form user object

export default class HvViewEditUserProfile extends LightningElement {

    @api currentuserdata; //user details passed by other user/component
    @track otherUserData = {}; //store all data related to other users like- physician/clinical
    @track patientUserData = {}; //store all data of patient
    @track mapFieldnamesAndValues = new Map(); //map of clinic/role metadata field names
    @track mapRequiredFieldnamesAndValues = new Map(); //map of required clinic/role metadata field names
    currentUserClinic; //logged in user clinic id
    clinicName; // logged in user clinic name
    personAccountId; //if pateint store its person account ID
    userContactId; // store user conatctId
    fieldsToDisplay; // final list of fields to be displayed on form
    requiredFields; //final list of fields to be required on form
    roleName; //logged in user role name
    maxdate; //dob field validation store max date
    boxIdName; //store box name from box id
    url; //profile pic url
    departmentOptions; //department dropdown options
    selectedDepartment; //selected department values
    showSpinner = false; //ruse to show/hide spinner
    preferredcontactoptions; //preferred contact method options
    selectedOptInValue; //Is user opt in for two factor authentaction - default to Yes
    selectedReviewed; //selected value for reviewed field
    cellphoneRequired; //boolean to validate if cellphone field is required
    emailRequired = true; //boolean to validate if email field is required
    editable = false; //is form in editable mode
    showEditProfile = true; //show/hide edit profile button
    @api showdeprovision; //show/hide deprovision button
    isSwapBox = false;//show/hide swap box modal
    isEditDevices = false;//show/hide devices in edit mode
    isClinicalUser;
    get optionsforOptInTwoFactor() { //return two factor authentication field options
        return [
            { label: 'Yes', value: 'Yes' },
            { label: 'No', value: 'No' },
        ];
    }

    get optionsforReviewed() { //return Reviewed field options
        return [
            { label: 'Yes', value: 'Yes' },
            { label: 'No', value: 'No' },
        ];
    }

   

    @wire(getObjectInfo, { objectApiName: CONTACT_OBJECT })
    contactMetadata; //lds method to get schema and details of Contact object

    @wire(getPicklistValues, { //lds method to get picklist values for gender field
        recordTypeId: '$contactMetadata.data.defaultRecordTypeId', //use schema method
        fieldApiName: GENDER_FIELD
    }) genderOptionslist({ error, data }) {
        if (data) {
            const options = [];
            data.values.map(row => {
                options.push({ label: row.label, value: row.value });
            });
            this.genderOptions = options; //set values in genderOptions variable
            this.errormessage = '';
        }
        if (error) {
            this.errormessage = error.body.message; //set error if any
            this.genderOptions = [];
        }
    }

    @wire(getPicklistValues, { //lds method to get picklist values for preferred contact method field
        recordTypeId: '$contactMetadata.data.defaultRecordTypeId', //use schema method
        fieldApiName: PREFERRED_CONTACT
    }) preferredcontactoptionslist({ error, data }) {
        if (data) {
            const options = [];
            data.values.map(row => {
                options.push({ label: row.label, value: row.value });
            });
            this.preferredcontactoptions = options; //set values in preferredcontactoptions variable
            this.errormessage = '';
        }
        if (error) {
            this.errormessage = error.body.message; //set error if any
            this.preferredcontactoptions = [];
        }
    }

    connectedCallback() { //on load of component 
        let maxBirthdayDate = new Date();
        maxBirthdayDate.setFullYear(maxBirthdayDate.getFullYear() - 18);
        this.maxdate = this.formatDate(maxBirthdayDate); //set the validation for Birthdate field that needs to 18 years and above
    }

    formatDate(date) { //funation to format the date for maxdate validation
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }

    renderedCallback() {
        if (!this.editable) { // on load by default set all fields disabled
            this.editProfileReadOnly();
        }
        if (this.mapFieldnamesAndValues.size > 0) { // show or hide fields on form based on clinic/role
            this.template.querySelectorAll('lightning-layout-item').forEach(field => {
                let itemclass = field.className.split(' ');
                if (!this.mapFieldnamesAndValues.has(itemclass[0]) && (itemclass[0] != 'FirstName' && itemclass[0] != 'LastName' && itemclass[0] != 'HV_Box_ID__pc' && itemclass[0] != 'HV_Reviewed__pc' && itemclass[0] != 'HV_Patient_ID__pc' && itemclass[0] != 'HV_Symptom_Check_In__pc')) {
                    this.template.querySelector('.' + itemclass[0]).className = 'HideField';
                }

            }); 
        }
        if (this.mapRequiredFieldnamesAndValues.size > 0) { // make fields required on form based on clinic/role
            this.template.querySelectorAll('lightning-layout-item').forEach(field => {
                let itemclass = field.className.split(' ');
                if (this.mapRequiredFieldnamesAndValues.has(itemclass[0])) {
                    field.childNodes[0].required = true;
                }
            });
        }
    }

    editProfileReadOnly() { //set all fields disabled
        this.editable = false;
        this.template.querySelectorAll('lightning-layout-item').forEach((ele) => {
            ele.childNodes[0].disabled = true;
        });
    }

    editProfileReadOnlyAfterSaveCancel() { //set all fields disabled and remove all validation error message if clicked on cancel button
        this.callMetaData();
        this.editable = false;
        this.template.querySelectorAll('lightning-layout-item').forEach((ele) => {
            ele.childNodes[0].disabled = true;
        });
        this.removeValidateFieldsOnCancel();
    }

    editProfile() { //show form in edit mode on click of edit profile button
        this.editable = true;
        this.template.querySelectorAll('lightning-layout-item').forEach((elemts) => {
            elemts.childNodes[0].disabled = false;
        });
    }

    @wire(getRecord, { recordId: currentUserId, fields: USERFIELDS })
    loggedInUserData({ error, data }) { //lds method to get current logged in user details usinf schema
        if (data) {
            if(data.fields.Contact.value.fields.HV_Role__c.value === 'Clinical User'){
                this.isClinicalUser = true;
            }else{ 
                this.isClinicalUser = false;
            }
            if (this.currentuserdata) { //if user details passed from other component store these details to attributes
                let returndata = JSON.parse(JSON.stringify(this.currentuserdata));
                this.url = returndata.SmallPhotoUrl;
                this.roleName = returndata.Contact.HV_Role__c;
                this.userContactId = returndata.ContactId;
                if (this.roleName == 'Patient') {
                    this.personAccountId = returndata.AccountId;
                    this.currentUserClinic = returndata.Contact.Account.HV_Patient_Clinic__c;
                    this.clinicName = returndata.Contact.Account.HV_Patient_Clinic__r.Name;
                }
                else {
                    this.currentUserClinic = returndata.Contact.AccountId; //logged in user clinic/account Id
                    this.clinicName = returndata.Contact.Account.Name; //set default user clinic name
                }
                //show patient profile as a readonly to clinical user, call cener admin, call center user
                if (this.roleName == 'Patient' && (data.fields.Contact.value.fields.HV_Role__c.value == 'Clinical User' || data.fields.Contact.value.fields.HV_Role__c.value == 'Call Center Admin' || data.fields.Contact.value.fields.HV_Role__c.value == 'Call Center User')) {
                    this.showEditProfile = false;
                }
                //show physician/clinical user profile as read only to onboarding user
                if (data.fields.Contact.value.fields.HV_Role__c.value == 'Onboarding Admin') {
                    this.showEditProfile = false;
                }
            }
            else { //if user data not passed from other component, store logged in user details to attributes
                this.url = data.fields.SmallPhotoUrl.value;
                this.roleName = data.fields.Contact.value.fields.HV_Role__c.value;
                this.userContactId = data.fields.Contact.value.fields.Id.value;

                if (this.roleName == 'Patient') {
                    this.personAccountId = data.fields.Contact.value.fields.AccountId.value;
                    this.currentUserClinic = data.fields.Contact.value.fields.Account.value.fields.HV_Patient_Clinic__c.value;
                    this.clinicName = data.fields.Contact.value.fields.Account.value.fields.HV_Patient_Clinic__r.value.fields.Name.value;
                }
                else {
                    this.currentUserClinic = data.fields.Contact.value.fields.AccountId.value; //logged in user clinic/account Id
                    this.clinicName = data.fields.Contact.value.fields.Account.value.fields.Name.value; //set default user clinic name
                }
            }
            //show profile as a readonly to call cener admin, call center user,livmor admin, asset admin, asset user
            if (this.roleName == 'Livmor Admin' || this.roleName == 'Call Center Admin' || this.roleName == 'Call Center User' || this.roleName == 'Asset Admin' || this.roleName == 'Asset User') {
                this.showEditProfile = false;
            }
            this.callMetaData(); //call method to get clinic/role and fields mapping from metadata
        }
        if (error) {
            this.errormessage = error.body.message; //set error if any
        }
    }

    callMetaData() {
        this.showSpinner = true;
        if (this.roleName != 'Patient') { //if logged in user not patient get fields mapping based on role
            getRoleFieldsMapping({ roleFieldsName: this.roleName })
                .then(data => {
                    if (data) {
                        this.fieldsToDisplay = data.HV_Avaliable_Fields__c.split(",").map(field => {
                            this.mapFieldnamesAndValues.set(field.trim(), field.trim());
                            return field.trim();
                        });
                        this.requiredFields = data.HV_Required_Fields__c.split(",").map(field => {
                            this.mapRequiredFieldnamesAndValues.set(field.trim(), field.trim());
                            return field.trim();
                        });
                        this.getOtherUserFieldsData();
                    }
                })
                .catch(error => {
                    this.errormessage = error.body.message; //set error message if any
                })
        }
        getPatientClinicFieldsMapping({ clinicId: this.currentUserClinic })
            .then(data => {
                if (data) {
                    if (this.roleName == 'Patient') { //if logged in user is patient get fields mapping based on clinic
                        this.fieldsToDisplay = data.HV_GeneralSectionFields__c.split(",").map(field => {
                            this.mapFieldnamesAndValues.set(field.trim(), field.trim());
                            return field.trim();
                        });
                        this.requiredFields = data.HV_RequiredFields__c.split(",").map(field => {
                            this.mapRequiredFieldnamesAndValues.set(field.trim(), field.trim());
                            return field.trim();
                        });
                        this.getPatientUserFieldsData();
                    }
                    const optionsDepart = [];
                    data.HV_Department_Values__c.split(",").map(field => { //get department options from clinic and department mapping
                        field.trim();
                        optionsDepart.push({ label: field.trim(), value: field.trim() });
                    });
                    this.departmentOptions = optionsDepart;
                }

            })
            .catch(error => {
                this.errormessage = error.body.message; //set error message if any
            })
    }

    getOtherUserFieldsData() { //on load get all existing field values of logged in user (other users- physician/clinical)
        let tempFields = this.fieldsToDisplay;
        tempFields.push('RecordTypeId');
        tempFields.push('Id');
        getOtherUserData({ userContactRecordId: this.userContactId, fields: tempFields })
            .then(data => {
                this.showSpinner = false;
                this.otherUserData = data;
                this.selectedDepartment = this.otherUserData.HV_Department__c.split(',');
            })
            .catch(error => {
                this.showSpinner = false;
                this.errormessage = error.body.message; //set error message if any
            })
    }

    getPatientUserFieldsData() { //on load get all existing field values of logged in user patient
        let tempFields = this.fieldsToDisplay;
        var index = tempFields.indexOf('notes');    // <-- Not supported in <IE9
        if (index !== -1) {
            tempFields.splice(index, 1);
        }
        tempFields.push('FirstName');
        tempFields.push('LastName');
        tempFields.push('RecordTypeId');
        //tempFields.push('ShippingAddress');
        tempFields.push('ShippingStreet');
        tempFields.push('ShippingCity');
        tempFields.push('ShippingCountry');
        tempFields.push('ShippingState');
        tempFields.push('ShippingPostalCode');
        tempFields.push('HV_Box_ID__pc');
        tempFields.push('HV_Box_ID__pr.Name');
        tempFields.push('HV_Reviewed__pc');

        tempFields.push('HV_Patient_ID__pc');
        tempFields.push('PersonContactId');
        tempFields.push('HV_Symptom_Check_In__pc'); 

        getPatientUserData({ userPersonAcountId: this.personAccountId, fields: tempFields })
            .then(data => {
                console.log('data user',data)
                this.patientUserData = data;
                if (this.patientUserData.HV_Department__pc) {
                    this.selectedDepartment = this.patientUserData.HV_Department__pc.split(',');
                }
                else {
                    this.selectedDepartment = '';
                }
                if (this.patientUserData.HV_Box_ID__pc) {
                    this.boxIdName = this.patientUserData.HV_Box_ID__pr.Name;
                } 
                else {
                    this.boxIdName = '';
                }
                if (this.patientUserData.HV_isTwoFactorAuthentactionEnabled__pc) {
                    this.selectedOptInValue = 'Yes';
                }
                else {
                    this.selectedOptInValue = 'No';
                }
                if (this.patientUserData.HV_Reviewed__pc) {
                    this.selectedReviewed = 'Yes';
                }
                else {
                    this.selectedReviewed = 'No';
                }
                if (this.patientUserData.Preferred_Contact_Method__pc == 'Cell Phone' || this.patientUserData.Preferred_Contact_Method__pc == 'Cell Phone/Email') { //add required validation for email and phone as per perferred contact value
                    this.cellphoneRequired = true;
                } else {
                    this.cellphoneRequired = false;
                }
                if (this.patientUserData.Preferred_Contact_Method__pc == 'Email' || this.patientUserData.Preferred_Contact_Method__pc == 'Cell Phone/Email') { //add required validation for email and phone as per perferred contact value
                    this.emailRequired = true;
                } else {
                    this.emailRequired = false;
                }
                if (!this.patientUserData.Preferred_Contact_Method__pc) {
                    this.emailRequired = true;
                    this.cellphoneRequired = true;
                }
                this.showSpinner = false;
            })
            .catch(error => {
                this.showSpinner = false;
                this.errormessage = error.body.message;
            })
    }

    saveData(event) { //save the modifed changes of user profile fields to system
        if (this.validateFields()) { //check validation for all input fields
            this.showSpinner = true;
            if (this.patientUserData.Id) {
                if (!this.patientUserData.PersonEmail) {
                    this.patientUserData.PersonEmail = this.patientUserData.HV_Patient_ID__pc + '@' + this.clinicName.split(' ').join('') + '.com';
                }
                console.log('patientUserData',this.patientUserData);
                saveUserData({ objectToSave: this.patientUserData, roleName: this.roleName })
                    .then(result => {
                        this.patientUserData = result;
                        this.editProfileReadOnlyAfterSaveCancel();
                        const evt = new ShowToastEvent({ //show success toast
                            title: 'Success',
                            message: 'Changes to User Profile Saved !!',
                            variant: 'success',
                        });
                        this.dispatchEvent(evt);
                        this.showSpinner = false;
                    })
                    .catch(error => {
                        this.showSpinner = false;
                        const evt = new ShowToastEvent({ //show success toast
                            title: 'Error',
                            message: 'Error in User Profile Save: ' + error.body.message,
                            variant: 'error',
                        });
                        this.dispatchEvent(evt);
                        this.errormessage = error.body.message; //set error message if any
                    })
            }
            if (this.otherUserData.Id) {
                saveUserData({ objectToSave: this.otherUserData, roleName: this.roleName })
                    .then(result => {
                        this.otherUserData = result;
                        this.editProfileReadOnly();
                        const evt = new ShowToastEvent({ //show success toast
                            title: 'Success',
                            message: 'Changes to User Profile Saved !!',
                            variant: 'success',
                        });
                        this.dispatchEvent(evt);
                        this.showSpinner = false;
                    })
                    .catch(error => {
                        this.showSpinner = false;
                        const evt = new ShowToastEvent({ //show success toast
                            title: 'Error',
                            message: 'Error in User Profile Save: ' + error.body.message,
                            variant: 'error',
                        });
                        this.dispatchEvent(evt);
                        this.errormessage = error.body.message; //set error message if any
                    })
            }
        }
    }

    otherUserDataChange(event) { //handle on change for fields specific to other users
        if (event.target.name === 'AccountId') {
            this.otherUserData.AccountId = thhis.currentUserClinic;
            return;
        }
        if (event.target.name === 'HV_Department__c') { //onchange specific for department field
            this.selectedDepartment = event.detail.value;
            let valuesSelected = (event.detail.value).join(",");
            this.otherUserData.HV_Department__c = valuesSelected; //store selected value as comma seperated in system
            return;
        }
        if (event.target.name == 'Phone') //onchange specific for phone fields to add '-'
        {
            this.otherUserData.Phone = (event.target.value).replace(/-/g, '');
            this.otherUserData.Phone = this.validatePhone(this.otherUserData.Phone);
            return;
        }
        this.otherUserData[event.target.name] = event.target.value;
    }

    patientUserDataChange(event) { //handle on change for fields specific patients
        if (event.target.name === 'HV_Department__pc') { //onchange specific for department field
            this.selectedDepartment = event.detail.value;
            let valuesSelected = (event.detail.value).join(",");
            this.patientUserData.HV_Department__pc = valuesSelected; //store selected value as comma seperated in system
            return;
        }
        if (event.target.name === 'Preferred_Contact_Method__pc') { //onchange specific for preferred contact method field
            this.patientUserData[event.target.name] = event.target.value;
            if (event.target.value == 'Cell Phone' || event.target.value == 'Cell Phone/Email') { //add required validation for email and phone as per perferred contact value
                this.cellphoneRequired = true;
            } else {
                this.cellphoneRequired = false;
            }
            if (event.target.value == 'Email' || event.target.value == 'Cell Phone/Email') { //add required validation for email and phone as per perferred contact value
                this.emailRequired = true;
            } else {
                this.emailRequired = false;
            }
            return;
        }
        if (event.target.name === 'ShippingAddress') { //onchange specific for shipping address field
            this.patientUserData.ShippingStreet = event.detail.street;
            this.patientUserData.ShippingCity = event.detail.city;
            this.patientUserData.ShippingCountry = event.detail.country;
            this.patientUserData.ShippingState = event.detail.province;
            this.patientUserData.ShippingPostalCode = event.detail.postalCode;
        }
        if (event.target.name == 'HV_isTwoFactorAuthentactionEnabled__pc') { //onchange specific for two factor authentaction field to store value
            this.selectedOptInValue = event.target.value;
            this.patientUserData.HV_isTwoFactorAuthentactionEnabled__pc = event.target.value == 'Yes' ? true : false;
            return;
        }
        if (event.target.name == 'HV_Reviewed__pc') { //onchange specific for two factor authentaction field to store value
            this.selectedReviewed = event.target.value;
            this.patientUserData.HV_Reviewed__pc = event.target.value == 'Yes' ? true : false;
            return;
        }
        if (event.target.name == 'Phone' || event.target.name == 'PersonHomePhone') { //onchange specific for phone fields to add '-'
            this.patientUserData[event.target.name] = (event.target.value).replace(/-/g, '');
            this.patientUserData[event.target.name] = this.validatePhone(this.patientUserData[event.target.name]);
            return;
        }
        if(event.target.name == 'HV_Symptom_Check_In__pc'){
            this.patientUserData[event.target.name] = event.target.checked;
            return;
        }
        this.patientUserData[event.target.name] = event.target.value;
    }

    validatePhone(phoneNumber) { //phone validation add hypen automatically
        var len = phoneNumber.length;
        if ((len > 3) && (phoneNumber[3] != '-')) {
            phoneNumber = [phoneNumber.slice(0, 3), '-', phoneNumber.slice(3)].join('');
        }
        if ((len > 7) && (phoneNumber[7] != '-')) {
            phoneNumber = [phoneNumber.slice(0, 7), '-', phoneNumber.slice(7)].join('');
        }
        return phoneNumber;
    }

    validateFields() { //validate all the field level validations set at component(template) level
        const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')] //validate all text input fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputsComboBoxCorrect = [...this.template.querySelectorAll('lightning-combobox')] //validate all combobox fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputsTextAreaCorrect = [...this.template.querySelectorAll('lightning-textarea')] //validate all teaxt area fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputsRadioGroupCorrect = [...this.template.querySelectorAll('lightning-radio-group')] //validate all radio group fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputAddressCorrect = [...this.template.querySelectorAll('lightning-input-address')] //validate all address fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const ismultipicklistCorrect = [...this.template.querySelectorAll('lightning-dual-listbox')] //validate all address fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        return (isInputsCorrect && isInputsComboBoxCorrect && isInputsTextAreaCorrect && isInputsRadioGroupCorrect && isInputAddressCorrect && ismultipicklistCorrect);
    }

    removeValidateFieldsOnCancel() { //remove all the field level validations set at component(template) level
        const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')].forEach(field => {
            field.setCustomValidity("");
            field.reportValidity();
        });
        const isInputsComboBoxCorrect = [...this.template.querySelectorAll('lightning-combobox')].forEach(field => {
            field.setCustomValidity("");
            field.reportValidity();
        });
        const isInputsTextAreaCorrect = [...this.template.querySelectorAll('lightning-textarea')].forEach(field => {
            field.setCustomValidity("");
            field.reportValidity();
        });
        const isInputsRadioGroupCorrect = [...this.template.querySelectorAll('lightning-radio-group')].forEach(field => {
            field.setCustomValidity("");
            field.reportValidity();
        });
        const isInputAddressCorrect = [...this.template.querySelectorAll('lightning-input-address')].forEach(field => {
            field.setCustomValidity("");
            field.reportValidity();
        });
        const ismultipicklistCorrect = [...this.template.querySelectorAll('lightning-dual-listbox')].forEach(field => {
            field.setCustomValidity("");
            field.reportValidity();
        });

    }

    offBoardPatient() {
        // apex call to insert contact-contact relationship obj record
        patientOffboard({ patientContactId: this.patientUserData.PersonContactId })
            .then(result => {
                if (result) {
                    this.boxIdName = '';
                    this.showToast('Success', 'The patient has been deprovisioned.', 'success');
                }
            })
            .catch(error => {
                this.showToast('Error', 'Some error occurred while deprovisioning patient.', 'error');
            })
    }

    showToast(msgTitle, msg, msgVarient) {
        const event = new ShowToastEvent({
            title: msgTitle,
            message: msg,
            variant: msgVarient
        });
        this.dispatchEvent(event);
    }

    openSwapBox() {
        this.isSwapBox = true;
    }

    closeSwapBox(event) {
        this.isSwapBox = event.detail.close;
    }

    updatedBoxId(event) {
        this.boxIdName = event.detail.boxName;
        console.log('event.detail.boxIdName', event.detail.boxName)
    }

    closeEditDevices(event) {
        this.isEditDevices = event.detail.close;
    }

    openEditDevices() {
        this.isEditDevices = true;
    }
}