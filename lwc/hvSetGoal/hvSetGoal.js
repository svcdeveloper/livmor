import { LightningElement, api, wire, track } from 'lwc';
import currentGoal from '@salesforce/apex/HVPatientDashboardController.getSetGoal';
import updateGoal from '@salesforce/apex/HVPatientDashboardController.updateGoal';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class HvSetGoal extends LightningElement {
    @track goalData = {};
    @api currentuserdetails;
    objectApiName = 'HV_Goal__c';
    patientid;
    targetweight;
    targetheartrate;
    targetbp;
    goalId;
    targetDate;
    error;
    
    connectedCallback(){
        console.log('patientid :::::: '+this.currentuserdetails.Contact.Id);
        this.patientid = this.currentuserdetails.Contact.Id;
        currentGoal({patientId : this.patientid})  
        .then(data => {
            console.log('data ::: ', data);
            if (data) {
                this.goalData = data;
                // this.targetweight = data.Weight__c;
                // this.targetheartrate = data.Heart_Rate__c;
                // this.targetbp = data.Blood_Pressure__c;
                // this.goalsetdate = data.LastModifiedDate;
            }
        })
        .catch(error => {
            this.error = error;  
            console.log('error ::: ', error);
        })
    }

    saveGoal(){
        this.goalData['SobjectType'] = 'HV_Goal__c';
        this.goalData['Related_Patient__c'] = this.patientid;
        updateGoal({goal_rec : this.goalData})  
        .then(data => {
            console.log('this.patientid :::: '+this.patientid);
            console.log('goalData ::: ', this.goalData);
            if (data) {
                console.log('Goal updated successfully', data);
                if(this.goalData.Id){
                    this.showToast('Success', 'Goal updated successfully !!', 'success');
                }else{
                    this.showToast('Success', 'Goal inserted successfully !!', 'success')
                }
            }
        })
        .catch(error => {
            this.error = error;  
            this.showToast('Success', 'Goal failed to update', 'error')
            console.log('error ::: ', error);
        })
    }

    showToast(msgTitle, msg, msgVarient) {
        const event = new ShowToastEvent({
            title: msgTitle,
            message: msg,
            variant: msgVarient
        });
        this.dispatchEvent(event);
    }

    inputFieldOnchange(event) {
        if (event.target.name === 'Blood_Pressure__c') {
            this.goalData.Blood_Pressure__c = event.target.value;
            return;
        }
        if (event.target.name === 'Heart_Rate__c') {
            this.goalData.Heart_Rate__c = event.target.value;
            return;
        }
        if (event.target.name === 'Weight__c') {
            this.goalData.Weight__c = event.target.value;
            return;
        }
        if (event.target.name === 'Target_Date__c') {
            this.goalData.Target_Date__c = event.target.value;
            return;
        }
    }

}