import { LightningElement, api, wire, track } from 'lwc';
import getSingleRecordImprative from '@salesforce/apex/HVQueryUtility.getSingleRecordImprative';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import getDeviceTabsDetails from '@salesforce/apex/HVPatientMeasurementController.getDeviceTabsDetails';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import FIRSTNAME from '@salesforce/schema/Contact.FirstName';
import LASTNAME from '@salesforce/schema/Contact.LastName';
import NAME from '@salesforce/schema/Contact.Name';
import getReport from '@salesforce/apex/HVAFReportView.navigateToAFReport';
import PatientDashboardCalloutAPI from '@salesforce/label/c.PatientDashboardCalloutAPI';
import { NavigationMixin } from 'lightning/navigation';

const CONTACTFIELDS = [FIRSTNAME, LASTNAME, NAME]; //fields to query form user object
//const USERFIELDS = [CURRENT_USER_CLINIC, CURRENT_USER_CONTACTID, CURRENT_USER_CLINIC_NAME, CURRENT_USER_ROLE, PATIENT_USER_CLINIC, PATIENT_USER_CLINIC_NAME, SMALL_PHOTOURL]; //fields to query form user object
const DAYSRANGE = [
    { dayrange: "1D", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * 1, enddate: Math.floor(new Date().getTime() / 1000.0) },
    { dayrange: "1W", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * 7, enddate: Math.floor(new Date().getTime() / 1000.0) },
    { dayrange: "1M", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * 30, enddate: Math.floor(new Date().getTime() / 1000.0) },
    { dayrange: "3M", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * 90, enddate: Math.floor(new Date().getTime() / 1000.0) },
    { dayrange: "6M", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * 180, enddate: Math.floor(new Date().getTime() / 1000.0) },
    { dayrange: "YTD", startdate: Math.floor(new Date().getTime() / 1000.0) - 86400 * ((Math.floor((new Date() - new Date(new Date().getFullYear(), 0, 0)) / 86400000))-1), enddate: Math.floor(new Date().getTime() / 1000.0) },
]
export default class HvPatientMeasurmentsDetail extends NavigationMixin(LightningElement) {

    @api patientId;
    @api frompatientdashboard = false;
    @track patientUserData = {};
    @track requestParams;
    @track deviceData;
    @track selecteddevicedetail = {};
    @track awslatestdataresponse;
    hvpatientid;
    patientName = '';
    showUserProfile = false;
    dayrangeselected = "1M";
    dayrangeselectedforreport = "1M";
    deviceselected = "Heart";
    dayrangestartdate;
    dayrangeenddate;
    dayrangestartdatereport;
    dayrangeenddatereport;
    isPeripheralDeviceSelected = false;
    isheartselected = true;
    isdefaultselecetd = false;
    isinitializedheart = false;
    isSymptomChecker = false;
    loadlatestdatafromAWS = false;
    server = 'BAYLOR';
    userid;/* = 'bridge02n';*/
    s3bucket = 'baylorprod';
    isSymptomHistory = false;
    showspinner = false;
    isGenerateReportOpen = false;
    showReportHistory = false;

    get optionsDevicesReport() {
        return [
            { label: 'Blood Pressure ', value: "Blood Pressure " },
            { label: 'Weigh Scale', value: "Weigh Scale" },
            { label: 'Heart Rate', value: "Heart Rate" },
            /*{ label: 'Spirometer', value: "Spirometer" },
            { label: 'Glucometer', value: "Glucometer" }*/
        ];
    }

    handleButtonGroupGenerateReport(event){
        this.dayrangeselectedforreport = event.target.label;
        DAYSRANGE.forEach(day => {
            if (day.dayrange == this.dayrangeselectedforreport) {
                this.dayrangestartdatereport = day.startdate;
                this.dayrangeenddatereport = day.enddate;
            }
        });
        this.template.querySelectorAll('.generatereportbutton').forEach(btn => {
            if (this.dayrangeselectedforreport == btn.label) {
                btn.variant = "brand";
            }
            else {
                btn.variant = "neutral";
            }
        });
    }

    handleButtonGroup(event) {
        this.dayrangeselected = event.target.label;
        DAYSRANGE.forEach(day => {
            if (day.dayrange == this.dayrangeselected) {
                this.dayrangestartdate = day.startdate;
                this.dayrangeenddate = day.enddate;
            }
        });
        this.template.querySelectorAll('.activebuttonclass').forEach(btn => {
            if (this.dayrangeselected == btn.label) {
                btn.variant = "brand";
            }
            else {
                btn.variant = "neutral";
            }
        });
        this.isPeripheralDeviceSelected = false;
        this.isheartselected = false;
        this.isdefaultselecetd = false;        
        this.requestParams = {};
        let reqbody = { "patientId": this.hvpatientid, "startdate": this.dayrangestartdate, "enddate": this.dayrangeenddate }
        //let reqbody = { "patientid": "test00000001", "startdate": "1624795200", "enddate": "1624881600" }
        this.requestParams.requestbody = reqbody;
        this.requestParams.endpoint = PatientDashboardCalloutAPI;
        this.requestParams.awsmethod = 'POST';
        this.requestParams.dataType = 'dashboardlatestdata';
        this.loadlatestdatafromAWS = true;
        this.showspinner = true;
    }

    renderedCallback() {
        this.template.querySelectorAll('.activebuttonclass').forEach(btn => {
            if (this.dayrangeselected == btn.label) {
                btn.variant = "brand";
            }
            else {
                btn.variant = "neutral";
            }
        });
        this.template.querySelectorAll('.generatereportbutton').forEach(btn => {
            if (this.dayrangeselectedforreport == btn.label) {
                btn.variant = "brand";
            }
            else {
                btn.variant = "neutral";
            }
        });
        if (this.deviceData && !this.isinitializedheart) {
            this.deviceData.forEach(device => {
                if (device.isdefault) {
                    this.activateTabs(device.Id);
                    this.activateTabsData(device.Id);
                }
            })
            this.isinitializedheart = true;
        }
        if (!this.loadlatestdatafromAWS) {
            if (this.deviceselected == 'weightscale' || this.deviceselected == 'bloodpressure') {
                this.isPeripheralDeviceSelected = true;
                this.isheartselected = false;
                this.isdefaultselecetd = false;
                let reqbody = { "patientId": this.hvpatientid, "dataType": this.deviceselected, "startdate": this.dayrangestartdate, "enddate": this.dayrangeenddate }
                //let reqbody = { "patientID": "test00000002", "dataType": this.deviceselected, "startdate": "1598918400", "enddate": "1600560000" }
                this.requestParams.requestbody = reqbody;
                this.requestParams.endpoint = this.selecteddevicedetail.awsendpoint;
                this.requestParams.awsmethod = 'POST';
                this.requestParams.dataType = this.deviceselected;
            }
            else if (this.deviceselected == 'Heart') {
                this.isheartselected = true;
                this.isPeripheralDeviceSelected = false;
                this.isdefaultselecetd = false;
            }
            else {
                this.isheartselected = false;
                this.isPeripheralDeviceSelected = false;
                this.isdefaultselecetd = true;
            }
            this.activateTabs(this.deviceselected);
            this.activateTabsData(this.deviceselected);
            this.showSymptomHistory(this.deviceselected);
        }
    }

    connectedCallback() {
        this.showspinner = true;
        DAYSRANGE.forEach(day => {
            if (day.dayrange == this.dayrangeselected) {
                this.dayrangestartdate = day.startdate;
                this.dayrangeenddate = day.enddate;
            }
        });
        if (this.patientId) {
            this.getPatientData();
        }
    }

    changeTabDeices(event) {
        this.isPeripheralDeviceSelected = false;
        this.isheartselected = false;
        this.isdefaultselecetd = false;
        this.requestParams = {};
        var dataId = event.currentTarget.getAttribute('data-id');
        this.deviceselected = dataId;
        this.deviceData.forEach(device => {
            if (device.Id == dataId) {
                this.selecteddevicedetail = device;
            }
        });
        this.activateTabs(dataId);
        this.activateTabsData(dataId);
        this.showSymptomHistory(dataId);

    }

    showSymptomHistory(deviceName) {
        if (deviceName === 'LatestSymptomStatus') {
            this.isSymptomHistory = true;
        } else {
            this.isSymptomHistory = false;
        }
    }

    closeSymptomChecker(event) {
        this.isSymptomChecker = event.detail;
    }
    handleTabInfo(event) {
        this.isPeripheralDeviceSelected = false;
        this.isheartselected = false;
        this.isdefaultselecetd = false;
        this.requestParams = {};
        var dataId = event.currentTarget.getAttribute('data-id');
        this.deviceselected = dataId;
        this.deviceData.forEach(device => {
            if (device.Id == dataId) {
                this.selecteddevicedetail = device;
                this.showSymptomChecker(dataId);
            }
        });
    }
    showSymptomChecker(deviceName) {
        if (deviceName === 'LatestSymptomStatus') {
            this.isSymptomChecker = true;
        }
    }
    // Above Tabs display logic
    activateTabs(dataId) {
        this.template.querySelectorAll('.boderAllHide').forEach(element => {
            if (element.classList.contains('activeTab')) {
                element.classList.remove('activeTab')
            }
        });
        if (this.template.querySelector('[data-id="' + dataId + '"]') && !(this.template.querySelector('[data-id="' + dataId + '"]').classList.contains('activeTab'))) {
            this.template.querySelector('[data-id="' + dataId + '"]').classList.add('activeTab');
        }
    }

    // Below data display logic
    activateTabsData(dataId) {
        this.template.querySelectorAll('.hideAll').forEach(element => {
            if (!(element.classList.contains('slds-hide'))) {
                element.classList.add('slds-hide')
            }
        });
        if (this.template.querySelector('[data-name="' + dataId + '"]') && this.template.querySelector('[data-name="' + dataId + '"]').classList.contains('slds-hide')) {
            this.template.querySelector('[data-name="' + dataId + '"]').classList.remove('slds-hide');
        }
    }

    //navigate to user profile page of patient
    gotouserprofile(event) {
        this.showUserProfile = true;
    }

    revertBack(event) {
        this.showUserProfile = false;
        this.getPatientData();
    }

    goBackToPhysicianDashboard() {
        const event = new CustomEvent('goback');
        this.dispatchEvent(event);
    }

    goBackToPatientDashboard() {
        const event = new CustomEvent('back');
        this.dispatchEvent(event);
    }

    generateReportOpenModal() {
        this.isGenerateReportOpen = true;
        this.showReportHistory = false;
    }

    generateReportCloseModal() {
        this.isGenerateReportOpen = false;
    }

    generateAFReports() {
        if(this.validateFields()){
            getReport({ server: this.server, userid: this.hvpatientid, s3bucket: this.s3bucket, startdate: this.dayrangestartdatereport, enddate: this.dayrangeenddatereport })
                .then(result => {
                    this.navigateToReport(result);
                    this.dayrangeselected = null;
                    this.isGenerateReportOpen = false;
                })
                .catch(error => {
                    this.error = error;
                });
            }
    }

    navigateToReport(url) {
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                "url": url
            },
        });
    }

    viewReportHistory() {
        this.showReportHistory = true;
    }

    handleresponse(event) {
        console.log('Response', event.detail);
        if (this.requestParams.dataType == event.detail.awscallidentifier) {
            if ((event.detail.awsresponse).data) {
                this.awslatestdataresponse = (event.detail.awsresponse).data;
                this.deviceData.forEach(device => {
                    if (device.Id == 'weightscale') {
                        if (this.awslatestdataresponse.weight) {
                            device.data = this.awslatestdataresponse.weight;
                        }
                    }
                    if (device.Id == 'bloodpressure') {
                        if (this.awslatestdataresponse.systolic && this.awslatestdataresponse.diastolic) {
                            device.data = this.awslatestdataresponse.systolic + ' / ' + this.awslatestdataresponse.diastolic;
                        }
                    }
                    if (device.Id == 'TotalRecordings') {
                        if (this.awslatestdataresponse.numRecordingSessions) {
                            device.data = this.awslatestdataresponse.numRecordingSessions;
                        }
                    }
                    if (device.Id == 'DayswithRecordingSession') {
                        if (this.awslatestdataresponse.daysWithRecording && this.awslatestdataresponse.numDaysTotal) {
                            device.data = this.awslatestdataresponse.daysWithRecording + '/' + this.awslatestdataresponse.numDaysTotal;
                        }
                    }
                })
            }
        }
        this.loadlatestdatafromAWS = false;
        this.showspinner = false;
    }

    handleresponseerror(event) {
        this.loadlatestdatafromAWS = false;
        this.showspinner = false;
    }

    validateFields() { 
        const ismultipicklistCorrect = [...this.template.querySelectorAll('lightning-dual-listbox')] //validate all address fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);
        return ismultipicklistCorrect;
    }

    getPatientData(){
        
        getDeviceTabsDetails()
        .then(result => {
            let tabdata = [];
            result.forEach(device => {
                tabdata.push({
                    Id: device.HV_DeviceUniqueIdentifier__c,
                    heading: device.MasterLabel,
                    data: device.HV_DefaultData__c,
                    subText: device.HV_DeviceSubText__c,
                    helpText: device.HV_DeviceHelpText__c,
                    isdefault: device.IsDefaultShow__c,
                    order: device.HV_Oder__c,
                    awsendpoint: device.HV_AWSEndpointAPI__c
                })
            });
            tabdata = tabdata.sort((a, b) => a.order - b.order);
            this.deviceData = tabdata;
            this.deviceData.forEach(device => {
                if (device.Id == this.deviceselected) {
                    this.selecteddevicedetail = device;
                }
            })
        })
        .then(() => {
            getSingleRecordImprative({ objectname: 'User', fieldnames: 'id,username,ContactId,contact.HV_Role__c,SmallPhotoUrl,AccountId,Contact.Account.HV_Patient_Clinic__c,Contact.Account.HV_Patient_Clinic__r.Name,Contact.FirstName,Contact.LastName,Contact.HV_Patient_ID__c,Contact.HV_Symptom_Status__c,Contact.HV_Symptom_Check_In__c', conditions: 'where contactid = \'' + this.patientId + '\'' }) //get selected user details to pass to user profile page
                .then(result => {
                    let newdata = [];
                    this.patientUserData = result;
                    this.patientName = this.patientUserData.Contact.FirstName + ' ' + this.patientUserData.Contact.LastName;
                    this.hvpatientid = this.patientUserData.Contact.HV_Patient_ID__c;
                    if (!result.Contact.HV_Symptom_Check_In__c) {
                        newdata = this.deviceData.filter(item => item.Id != 'LatestSymptomStatus');
                    }
                    if (newdata.length > 0) {
                        this.deviceData = newdata;
                    }
                })
                .then(result => {
                    this.requestParams = {}
                    let reqbody = { "patientId": this.hvpatientid, "startdate": this.dayrangestartdate, "enddate": this.dayrangeenddate }
                    //let reqbody = { "patientid": "test00000001", "startdate": "1624795200", "enddate": "1624881600" }
                    this.requestParams.requestbody = reqbody;
                    this.requestParams.endpoint = PatientDashboardCalloutAPI;
                    this.requestParams.awsmethod = 'POST';
                    this.requestParams.dataType = 'dashboardlatestdata';
                    this.loadlatestdatafromAWS = true;
                    this.showspinner = true;
                })
                .catch(error => {
                    this.errormessage = error.body.message; //set error message if any
                })
        })
        .catch(error => {
            this.errormessage = error.body.message; //set error message if any
            this.showspinner = false;
            this.loadlatestdatafromAWS = false;
        });

    }
}