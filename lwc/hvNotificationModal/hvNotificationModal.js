import { LightningElement, api } from 'lwc';

export default class HvNotificationModal extends LightningElement {
    @api value;
    isModalOpen = false; 
    showdiv = false;

    renderedCallback(){
        if(this.value.length > 40){
            this.showdiv = true;
        }else{
            this.showdiv = false;
        }
    }
    

    openModal(){
        this.isModalOpen = true; 
    }
 
    closeModal(){
        this.isModalOpen = false;
    }
}