import { LightningElement, api, track } from 'lwc';

export default class HvPatientPeripheralDeviceData extends LightningElement {
    @api awsrequestdetails;
    @track responsedata;
    @track tablecolumns = [];
    @track tabledata = [];
    showdata = true;
    showspinner = true;
    totalNumberOfRows = 50;
    rowOffSet = 0;
    numberofrecordtoshow = 50;

    handleresponse(event) {
        console.log('Response', event.detail);
        if (this.awsrequestdetails.dataType == event.detail.awscallidentifier) {
            if ((event.detail.awsresponse).data.data) {
                this.responsedata = (event.detail.awsresponse).data.data;
            }
            if (this.responsedata) {
                console.log('response length' + this.responsedata.length);
                if (this.responsedata.length == 0) {
                    this.showdata = false;
                    this.showspinner = false;
                }
                else{
                    let tableheaders = [];
                    Object.getOwnPropertyNames(this.responsedata[0]).forEach(row => {
                        tableheaders.push({ label: row.toUpperCase().replace(/_/g, ' '), fieldName: row, type: 'text' });
                    })
                    this.tablecolumns = tableheaders;
                    this.manageandshowdata(this.responsedata);
                }
            }
            else {
                this.showspinner = false;
                this.showdata = false;
            }
        }
    }

    manageandshowdata(responsedata) {
        let tabledata = [];
        let count = 0;
        for (let i = this.rowOffSet; i < this.totalNumberOfRows; i++) {
            let data = responsedata[i];
            let tempdata = [];
            let iddata = { id: count };
            if(data){
                Object.getOwnPropertyNames(data).forEach(attribute => {
                    if (attribute == 'measure_time') {
                        let utcSeconds = data[attribute];
                        let measuredate = new Date(0); // The 0 there is the key, which sets the date to the epoch
                        measuredate.setUTCSeconds(utcSeconds);
                        //let measuredate = new Date(data[attribute]);
                        const timeZoneAbbreviated = () => {
                            const { 1: tz } = measuredate.toString().match(/\((.+)\)/);
                            if (tz.includes(" ")) {
                            return tz
                                .split(" ")
                                .map(([first]) => first)
                                .join("");
                            } else {
                            return tz;
                            }
                        };                    
                        iddata[attribute] = measuredate.toLocaleString(undefined, {
                            weekday: 'short', // long, short, narrow
                            day: 'numeric', // numeric, 2-digit
                            year: 'numeric', // numeric, 2-digit
                            month: 'short', // numeric, 2-digit, long, short, narrow
                            hour: 'numeric', // numeric, 2-digit
                            minute: 'numeric', // numeric, 2-digit
                        })+' ('+timeZoneAbbreviated()+')';
                    }
                    else {
                        iddata[attribute] = data[attribute];
                    }

                })
                tabledata.push(iddata);
                count = count + 1;
            }
        }
        this.tabledata = [...this.tabledata, ...tabledata];
        this.showspinner = false;
    }

    loadMoreData(event) {
        console.log('in load more');
        if (event.target.isLoading) {
            return;
        }
        const currentRecord = this.responsedata;
        let isload = event.target.isLoading;
        isload = true;
        this.rowOffSet = this.rowOffSet + this.numberofrecordtoshow;
        this.totalNumberOfRows = this.totalNumberOfRows + this.numberofrecordtoshow;

        if (this.responsedata.length > 0 && this.totalNumberOfRows < (this.responsedata.length + this.numberofrecordtoshow)) {
            this.manageandshowdata(currentRecord)
            .then(() => {
                isload = false;
            });
        }
        else if (this.responsedata.length > 0 && this.totalNumberOfRows > this.responsedata.length) {
            isload = false;
        }        
    }

}