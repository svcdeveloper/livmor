import { LightningElement, api, track } from 'lwc';
import getBoxRecords from  '@salesforce/apex/HVQueryUtility.getMultipleRecordImprative'

export default class HvEditInventoryManagement extends LightningElement {

    @api boxName;
    @track boxid;
    @track watchrecid;
    @track bprecid;
    @track qarecid 
    @track pulseoximeterrecid
    @track spirometersrecid
    @track wssrecid
    @track glucometersrecid
    @track tabletrecid;
    @track trackingrecid;

    closeModal(){
        this.closeModalEvent();
    }

    connectedCallback(){

        console.log('devices re',this.boxName);
        getBoxRecords({
            objectname: 'HV_Box__c',
            fieldnames: ' id,Name,HV_Status__c,HV_Customer__c,(SELECT Id, Name,HV_Box__c, HV_BDA__c, HV_Model__c, HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c FROM BPs__r),'+
            '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_BDA__c, HV_Model__c, HV_Manufacturer__c FROM Pulse_Oximeters__r),'+
            '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_BDA__c, HV_Manufacturer__c, HV_Model__c FROM Spirometers__r),'+
            '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_SSID__c, HV_Password__c, HV_IMEI__c, HV_Serial_Number__c, HV_Android_ID__c,HV_Model__c FROM Tablets__r),'+
            '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_BDA__c, HV_Model__c, HV_Manufacturer__c FROM WSS__r),'+
            '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_Watch_Script_Output__c, HV_Watch_Serial_Number__c, HV_Watch_Model__c, HV_Watch_ID__c, HV_Watch_MAC_ID__c, HV_Watch_DUID__c, HV_Watch_MEID__c FROM Watches__r),'+
            '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_BDA__c, HV_Model__c, HV_Manufacturer__c FROM Glucometers__r),'+
            '(Select Id, HV_Status__c, Name, HV_Box__c, HV_Shipping_Address__c, HV_Shipped_Date__c, HV_QA_Date__c, QA_by__c, HV_Tracking_Number__c, HV_Batch_Number__c, HV_Manufactured_Date__c, HV_Manufactured_By__c FROM QAs__r),'+
            '(SELECT Id, HV_Box__c, HV_Shipped_Date__c,HV_Shipping_Address__c,HV_Tracking_number__c From Tracking_and_Shipping__r)',
            conditions: ' Where Name =  \''+this.boxName+'\''
        })
        .then( result => {
 
            if(result){
 
                console.log('devices re',result);
                result.forEach( record => {
                    this.boxid = record.Id;
                    this.watchrecid  =  record['Watches__r'];
                    this.bprecid = record['BPs__r'];
                    this.qarecid = record['QAs__r'];
                    this.pulseoximeterrecid = record['Pulse_Oximeters__r'];
                    this.spirometersrecid = record['Spirometers__r'];
                    this.glucometersrecid = record['Glucometers__r'];
                    this.tabletrecid = record['Tablets__r'];
                    this.trackingrecid = record['Tracking_and_Shipping__r']
                })
            }
           
        })
        .catch( error => {
            console.log('error',error);
        })
    }

    closeModalEvent(){
        this.dispatchEvent(new CustomEvent('close', { 
            detail: {
                close : false
            } 
        }))
    }

    submit(){
        this.closeModalEvent();
    }


}