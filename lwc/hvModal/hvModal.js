import { api, LightningElement, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getAllFacilities from '@salesforce/apex/HVModalController.getAllFacilities';
import getcontactusdetails from '@salesforce/apex/HVModalController.getcontactusdetails';
import sendEmailtoLivmorSupport from '@salesforce/apex/HVModalController.sendEmailtoLivmorSupport';

export default class HvModal extends LightningElement {

    @api modalheader;
    @api modalbody;
    @api closebuttonlabel;
    @api okbuttonlabel;    
    @track facilityOptions;
    @track contactusobj = {};   
    @track contactusdetails = {};
    selectedFacility;
    selectedFacilityName;
    errormessage;     

    get showfacilityselect() {
        return this.modalbody == 'Select Facility' ? true : false;
    }

    get showcontactus(){
        return this.modalbody =='Contact Us' ? true :false;
    }
 
    get showdefaultbody(){
        return this.modalbody != 'Contact Us' && this.modalbody != 'Select Facility' && this.modalheader != 'PRIVACY NOTICE' && this.modalheader != 'Terms and Conditions'? true : false;
    }

    get isPrivacy(){
        return this.modalheader? this.modalheader === 'PRIVACY NOTICE'?true: false  : false;
    } 

    get isTerm(){
        return this.modalheader? this.modalheader === 'Terms and Conditions'?true: false  : false;
    }

    @wire(getAllFacilities)
    allfacilities({ error, data }) {
        if (data) {
            let options = [];
            data.forEach(row => {
                options.push({ label: row.MasterLabel, value: row.HV_Clinic_Id__c });
            });
            this.facilityOptions = options;
        }
        if (error) {
            this.errormessage = error.modalbody.message;
        }
    }

    @wire(getcontactusdetails)
    contactus({ error, data }) {
        if (data) {
            this.contactusdetails = data;
        }
        if (error) {
            this.errormessage = error.modalbody.message;
        }
    }

    renderedCallback(){
        if(this.isPrivacy || this.isTerm){
            let deom = this.template.querySelector('.d3');             
            let li = document.createElement('div');
            li.innerHTML =this.modalbody; 
            deom.appendChild(li);
        }        
    }
   
    facilitychange(event) { 
        this.selectedFacility = event.target.value;
          this.facilityOptions.filter( option => {
            if(option.value === event.target.value){
                this.selectedFacilityName = option.label
            }
        });
    }

    inputFieldOnchange(event){
        if (event.target.name == 'Phone' || event.target.name == 'HomePhone') {
            this.contactusobj[event.target.name] = (event.target.value).replace(/-/g, '');
            this.contactusobj[event.target.name] = this.validatePhone(this.contactusobj[event.target.name]);
            return;
        }
        this.contactusobj[event.target.name] = event.target.value;
    }

    validatePhone(phoneNumber) { //phone validation add hypen automatically
        var len = phoneNumber.length;
        if ((len > 3) && (phoneNumber[3] != '-')) {
            phoneNumber = [phoneNumber.slice(0, 3), '-', phoneNumber.slice(3)].join('');
        }
        if ((len > 7) && (phoneNumber[7] != '-')) {
            phoneNumber = [phoneNumber.slice(0, 7), '-', phoneNumber.slice(7)].join('');
        }
        return phoneNumber;
    }

    closeModal(event) {
        this.dispatchEvent(new CustomEvent('closemodal', { detail: event.target.name }));
    }

    submitDetails(event) {
        if (event.target.name == 'Next') {
            this.dispatchEvent(new CustomEvent('submistmodal', { detail: { eventname: event.target.name, clinicname: this.selectedFacilityName, clinicid: this.selectedFacility } }));
        }
        else {
            this.dispatchEvent(new CustomEvent('submistmodal', { detail: event.target.name }));
        }
    }    

    submitmessage(event){        
        if(this.validateFields()){
            let emailtoaddress = this.contactusdetails.HV_Contact_Us_Email__c.split(','); 
            sendEmailtoLivmorSupport({toaddress: emailtoaddress, contactusrecorddata: JSON.stringify(this.contactusobj)})
            .then(result => {
                const evt = new ShowToastEvent({ //show success toast
                    title: 'Success',
                    message: 'Email sent to Support Team',
                    variant: 'success',
                });
                this.dispatchEvent(evt);
                this.dispatchEvent(new CustomEvent('closemodal', { detail: event.target.name }));
            })
            .catch(error =>{
                this.errormessage = error.body.message;
            })
        }
    }

    validateFields() { //validate all the field level validations set at component(template) level
        const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')] //validate all text input fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);       

        const isInputsTextAreaCorrect = [...this.template.querySelectorAll('lightning-textarea')] //validate all teaxt area fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        return (isInputsCorrect && isInputsTextAreaCorrect);
    }
}