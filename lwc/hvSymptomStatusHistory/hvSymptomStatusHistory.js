import { LightningElement,api } from 'lwc';
import getSobjectRecords from '@salesforce/apex/HVQueryUtility.getMultipleRecordImprative';
const columns = [
    { label: 'New Value', fieldName: 'NewValue' },
    { label: 'Old Value', fieldName: 'OldValue' },
    { label: 'Created Date', fieldName: 'CreatedDate',type: "date-local",
    typeAttributes:{
        month: "2-digit",
        day: "2-digit"
    } },
    
];
export default class HvSymptomStatusHistory extends LightningElement {

    @api patientId;
    columns = columns;
    data; 
    connectedCallback(){
        if(this.patientId){
            console.log('this.patientId',this.patientId)
            getSobjectRecords({ 
                objectname: 'AccountHistory', 
                fieldnames: 'OldValue,NewValue,CreatedDate', 
                conditions: ' where Field =\''+'HV_Symptom_Status__pc'+'\' AND AccountId = \''+this.patientId+'\' ORDER by CreatedDate DESC'
            }) //get selected user details to pass to user profile page
            .then(result => {
                if(result){ 
                    console.log('result cont his',result) 
                    this.data = result 
                } 
            }) 
            .catch(error => {
                console.error('error',error)
                //set error message if any
            })
        }
    }
}