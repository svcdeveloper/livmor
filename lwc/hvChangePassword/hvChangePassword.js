import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import changePassword from '@salesforce/apex/HVForgotPassword.changePassword';
import changePasswordStandard from '@salesforce/apex/HVForgotPassword.changePasswordStandard'; 
import sendpasswordchangeconfirmation from '@salesforce/apex/HVForgotPassword.sendpasswordchangeconfirmation'; 

export default class HvChangePassword extends LightningElement {

    @api isFromUserProfile = false; //is component loaded from User Profile Component
    @api otheruserchangepass; //user details passed by other user/component
    @api username; // username for which password needs to be changed
    oldPassword; //store old password details
    newPassword; // to get new password
    errorMessage; // to show error message if any
    confirmPassword; // to get confirm password
    showPassword = false; //show password in plain text or not

    get isUseStandardPassword(){
        return this.isFromUserProfile && !this.otheruserchangepass ? true : false;
    }

    setPassword(event) { //on password change set new value
        this.errorMessage = '';
        this.newPassword = (event.target.value).replace(/\s+/g, '');
        if (this.confirmPassword) {
            this.confirmpasswordMatch(this.newPassword, this.confirmPassword, 'confirmpassword');
        }
    }

    setConfirmPassword(event) { //on confirm password change set new value
        this.errorMessage = '';
        this.confirmPassword = (event.target.value).replace(/\s+/g, '');
        this.confirmpasswordMatch(this.newPassword, this.confirmPassword, event.target.name);
    }

    setOldPassword(event) {
        this.oldPassword = (event.target.value).replace(/\s+/g, '');
    }

    confirmpasswordMatch(pswrd, cnfrmPswrd, fieldName) { //check if password and confirm password field value matches
        if (pswrd != '' && cnfrmPswrd != '') {
            this.template.querySelectorAll("lightning-input").forEach((ele) => {
                if (ele.name == fieldName) {
                    if (cnfrmPswrd == pswrd) {
                        ele.setCustomValidity("");
                    } else {
                        ele.setCustomValidity("Entered password is not matching, please reenter the new password"); //if not match, set error
                    }
                    ele.reportValidity();
                }
            });
        }
    }

    changePass(event) { //change user password
        if (this.validateFields()) { //check all input field validation (password pattern)
            if (this.isUseStandardPassword) {
                console.log('in change password from user profile');
                changePasswordStandard({ newPassword: this.newPassword, verifyNewPassword: this.confirmPassword, oldpassword: this.oldPassword }) //call apex to change password of user
                    .then(result => {
                        if (result != null) {
                            this.errorMessage = '';
                            const evt = new ShowToastEvent({ //show success toast
                                title: 'Success',
                                message: 'Password Changed Successfully !!',
                                variant: 'success',
                            });
                            this.dispatchEvent(evt);
                            if (!this.otheruserchangepass) {
                                location.href = result; //if password change successfully, redirect to home page (login with new password)
                            }
                            else { //if password is changed for other user passed from other component - do not login and go back to home page
                                this.dispatchEvent(new CustomEvent('cancelpasswordchange'));
                            }
                            sendpasswordchangeconfirmation({username: this.username})
                            .then(result=>{
                                console.log('Password Change Confirmation sent ');
                            })
                            .catch(error=>{
                                this.errorMessage = error.body.message;
                            })
                        } else {
                            this.errorMessage = 'Incorrect Username and Password';
                        }
                    })
                    .catch(error => {
                        console.log(JSON.stringify(error));
                        if (error.body.message) {
                            if ((error.body.message).split(':')[1]) {
                                this.errorMessage = (error.body.message).split(':')[1].toUpperCase(); //show only relevant error message by removing unwanted text from actual message
                            }
                            else {
                                this.errorMessage = error.body.message;  //set error message if any
                            }
                        }
                    });
            }
            else {
                changePassword({ username: this.username, password: this.newPassword }) //call apex to change password of user
                    .then(result => {
                        if (result != null) {
                            this.errorMessage = '';
                            const evt = new ShowToastEvent({ //show success toast
                                title: 'Success',
                                message: 'Password Changed Successfully !!',
                                variant: 'success',
                            });
                            this.dispatchEvent(evt);
                            if (!this.otheruserchangepass) {
                                location.href = result; //if password change successfully, redirect to home page (login with new password)
                            }
                            else { //if password is changed for other user passed from other component - do not login and go back to home page
                                this.dispatchEvent(new CustomEvent('cancelpasswordchange'));
                            }
                            sendpasswordchangeconfirmation({username: this.username})
                            .then(result=>{
                                console.log('Password Change Confirmation sent ');
                            })
                            .catch(error=>{
                                this.errorMessage = error.body.message;
                            })
                        } else {
                            this.errorMessage = 'Incorrect Username and Password';
                        }

                    })
                    .catch(error => {
                        console.log(JSON.stringify(error));
                        if (error.body.message) {
                            if ((error.body.message).split(':')[1]) {
                                this.errorMessage = (error.body.message).split(':')[1].toUpperCase(); //show only relevant error message by removing unwanted text from actual message
                            }
                            else {
                                this.errorMessage = error.body.message;  //set error message if any
                            }
                        }
                    });
            }
        }
    }

    validateFields() { //check all the fields validations and report error if any
        const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);
        return isInputsCorrect;
    }

    togglePaswrd(event) { // toggle password field value to show in plain text or encrypted
        this.showPassword = event.target.value;
        this.template.querySelectorAll('.inputTypesPass').forEach(input => {
            if (input.type == "password") {
                input.type = "text";
            } else if (input.type == "text") {
                input.type = "password";
            }
        });
    }

    cancelPasswordChange() { //if component is called from user profile- on cancel go to profile tab by calling custom event to pass to parent component
        this.newPassword = '';
        this.confirmPassword = '';
        this.showPassword = false;
        this.template.querySelectorAll('lightning-input').forEach((ele) => {
            ele.setCustomValidity("");
            ele.reportValidity();
        });
        this.dispatchEvent(new CustomEvent('cancelpasswordchange'));
    }

}