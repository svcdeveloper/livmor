import { LightningElement, track, api } from 'lwc';
import LightningDatatable from 'lightning/datatable';
import toggleButtonColumnTemplate from './toggleButtonColumnTemplate.html';
import picklistColumnTemplate from './picklistColumnTemplate.html';
import isFavouriteTemplate from './isFavouriteTemplate.html';
import statusTemplate from './statusTemplate.html';
import notificationTemplate from './notificationTemplate.html';

export default class HvCustomDataTable extends LightningDatatable {

    hasRendered;

    //create custom types to be used in lightning data table as columns
    static customTypes = {
        toggleButton: { //add custom toggle button component
            template: toggleButtonColumnTemplate,
            standardCellLayout: true,
            typeAttributes: [''],
        },
        pickListColumn: { //add custom picklist component
            template: picklistColumnTemplate,
            standardCellLayout: true,
            typeAttributes: ['rowId'],
        },
        isFavourite: { // add custom favourite component
            template: isFavouriteTemplate,
            standardCellLayout: true,
            typeAttributes: ['rowId'],
        },
        symptomStatus: { // add custom favourite component
            template: statusTemplate,
            standardCellLayout: true,
            typeAttributes: [''],
        },
        showNotification : { // add custom notification component
            template : notificationTemplate,
            standardCellLayout: true,
            typeAttributes: ['rowmessage']
        }
    };

    @api
    getRows() {
        console.log('row event', this.template)
    }


    /*  renderedCallback() {
         if (this.hasRendered) {
              return;
          }
  
          const table = this.template.querySelector('tbody');
          console.log('table',table);
          console.log('table'+table);
          table.addEventListener(
              'click',
              (e) => { 
                  console.log('e',e);
                  const parentRow = findParentRow(e.target);
                  console.log('parentRow',parentRow);
                  if (parentRow) {
                      window.postMessage(
                          {
                              datarow: parentRow.getAttribute(
                                  'data-row-key-value'
                              )
                          },
                          window.location.origin
                      );
                  }
              },
              true
          );
  
          this.hasRendered = true;
  
          function findParentRow(element) {
              if (element.tagName === 'TR') return element;
              return findParentRow(element.parentElement);
          }
          
  
          
        
      }*/

    @api
    getOnclick() {
        const table = this.template.querySelector('tbody');
        table.addEventListener(
            'click',
            (e) => {
                if (e.target.getAttribute('data-label') != 'Department' && e.target.getAttribute('data-label') != 'Favorite' && e.target.localName != "lightning-primitive-cell-checkbox") {
                    const parentRow = findParentRow(e.target);
                    if (parentRow) {
                        window.postMessage(
                            {
                                datarow: parentRow.getAttribute(
                                    'data-row-key-value'
                                )
                            },
                            window.location.origin
                        );
                    }
                }
            },
            true
        );
        function findParentRow(element) {
            if (element.tagName === 'TR') return element;
            return findParentRow(element.parentElement);
        }

    }
}