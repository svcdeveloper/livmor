import { LightningElement, track, api } from 'lwc';
import sendOTPtoUser from '@salesforce/apex/HVForgotPassword.sendOTPtoUser';

export default class HvPreferredContactMethod extends LightningElement {
    emailString;
    phoneString;
    homePhoneString;
    otp;
    emailSelection = false;
    phoneSelection = false;
    homePhoneSelection = false;
    selectedValue;
    @api email;
    @api phone;
    @api homePhone; 
    @api preferedMethod;
    errorMessage;


    connectedCallback() {
        console.log('email' + this.email + 'phone ' + this.phone);
        if (this.email) {
            this.emailString = this.emailHide(this.email);
        }
        if (this.phone) {
            this.phoneString = this.phoneHide(this.phone);
        }

        if(this.homePhone){
            this.homePhoneString = this.phoneHide(this.homePhone);
           
        }

        if(this.preferedMethod == 'Email' || this.preferedMethod == 'Cell Phone/Email'){
            this.emailSelection = true;
        }

        if(this.preferedMethod == 'Cell Phone'){
            this.phoneSelection = true;
        }

        

    }    

    get onlyEmail() {
        
        return (this.email && ( this.preferedMethod == 'Email' || this.preferedMethod == 'Cell Phone/Email') )? true : false;
    }
    get onlyPhone() {
       
        return (this.phone && (this.preferedMethod == 'Cell Phone' || this.preferedMethod == 'Cell Phone/Email') )? true : false;
    }
   /* get bothEmailandPhone() {
        
        return (this.email && this.phone ) ? true : false;
    }*/

    get onlyHomePhone(){
        
        return (this.homePhone )
    }
    toogleselection(event) {
        var value = event.target.value;
        console.log('value',value);
        if (value == 'email' && event.target.checked) {
            this.emailSelection = true;
            this.phoneSelection = false;
            this.homePhoneSelection = false;
        } else if (value == 'phone' && event.target.checked) {
            this.phoneSelection = true;
            this.emailSelection = false;
            this.homePhoneSelection = false;
        }else if (value == 'homePhone' && event.target.checked) {
            this.phoneSelection = false;
            this.emailSelection = false;
            this.homePhoneSelection = true;
        }else{
            this.phoneSelection = false;
            this.emailSelection = false;
            this.homePhoneSelection = false;
        }



    }

    emailHide(email) {
        var emi = email.split('@');
        var emiLength1 = emi[0].length;
        var em2 = emi[1].split('.');
        var emiLength2 = em2[0].length;
        var emailString = emi[0].charAt(0) + emi[0].charAt(1) + emi[0].charAt(2);
        for (var i = 3; i < emiLength1; i++) {
            emailString += '.';
        }
        emailString += '@';
        emailString += em2[0].charAt(0) + em2[0].charAt(1);
        for (var j = 2; j < emiLength2; j++) {

            emailString += '.';
        }
        emailString += '.' + em2[1];
        return emailString;
    }

    phoneHide(phone) {
        var phonelength = phone.length;
        var phoneString = '';
        for (var i = 0; i < (phonelength - 2); i++) {
            phoneString += '.';
        }
        phoneString += phone.charAt(phonelength - 2) + phone.charAt(phonelength - 1);
        return phoneString;
    }

    sendOtp() {

        this.errorMessage = '';
        console.log('homePhoneSelection', this.homePhoneSelection);
        console.log('phoneSelection', this.phoneSelection);
        console.log('emailSelection', this.emailSelection);
       if(this.homePhoneSelection){
        console.log('phone', this.homePhone);
        this.selectedValue = 'homePhone';
        sendOTPtoUser({
            email: null,
            phone: this.homePhone,
            channel : 'call'
        })
            .then(result => {
                console.log(JSON.stringify(result));

                const event = new CustomEvent('child', {
                    detail: { step: 3, selectedValue: this.selectedValue }
                });
                this.dispatchEvent(event);
            })
            .catch(error => {
                console.log('error', error);
                console.log(JSON.stringify(error));
                if (error.body.message) {
                    this.errorMessage = error.body.message;
                }
            });
       }else if (this.phoneSelection) {
            console.log('phone', this.phone);
            this.selectedValue = 'phone';
            sendOTPtoUser({
                email: null,
                phone: this.phone,
                channel : 'sms'
            })
                .then(result => {
                    console.log(JSON.stringify(result));

                    const event = new CustomEvent('child', {
                        detail: { step: 3, selectedValue: this.selectedValue }
                    });
                    this.dispatchEvent(event);
                })
                .catch(error => {
                    console.log('error', error);
                    console.log(JSON.stringify(error));
                    if (error.body.message) {
                        this.errorMessage = error.body.message;
                    }
                });
        } else 
            if(this.emailSelection){
                
            
            console.log('email,', this.email);
            this.selectedValue = 'email';
                sendOTPtoUser({
                    email: this.email,
                    phone: null,
                    channel: 'sms'
                })
                .then(result => {
                    console.log(JSON.stringify(result));
                    this.otp = result;
                    console.log("Prefered Contact OTP " + this.otp);
                    const event = new CustomEvent('child', {
                        // detail contains only primitives
                        detail: { step: 3, otp: this.otp, selectedValue: this.selectedValue }
                    });
                    this.dispatchEvent(event);
                })
                .catch(error => {
                    console.log('error', error);
                    console.log(JSON.stringify(error));
                    if (error.body.message) {
                        this.errorMessage = error.body.message;
                    }
                });
        }else{
            console.log('rrrrrrrr')
            this.errorMessage = 'Please choose a method';
        }
    } 
}