import { LightningElement } from 'lwc';
import HVLogo from '@salesforce/resourceUrl/HVLogo';

export default class HvHeader extends LightningElement {
    url = HVLogo;
    hanldleChild2(){
        const event = new CustomEvent('child', {
            detail: {
                userProfile: true
            }
        });
        this.dispatchEvent(event);
    }
}