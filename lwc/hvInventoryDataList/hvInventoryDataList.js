import { LightningElement, track ,api, wire} from 'lwc';
import getBoxRecords from  '@salesforce/apex/HVQueryUtility.getMultipleRecordImprative'
import uId from '@salesforce/user/Id';
import getSobjectRecord from '@salesforce/apex/HVQueryUtility.getSingleRecordImprative';
import getSobjectRecords from '@salesforce/apex/HVQueryUtility.getMultipleRecordImprative';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import CURRENT_USER_CLINIC from '@salesforce/schema/User.Contact.AccountId';
const USERFIELDS = [CURRENT_USER_CLINIC ]


export default class HvInventoryDataList extends LightningElement {
    userId = uId;
    totalColumns = [{ label: '', fieldName: '', type: 'isEdit',editable: true,
    initialWidth : 80,
    typeAttributes: { 
            boxrecid: { 
                fieldName: 'boxrecid' 
            }, 
            watchrecid: {
                fieldName: 'watchrecid'
            },
            bprecid: { 
                fieldName: 'bprecid' 
            },
            qarecid: { 
                fieldName: 'qarecid' 
            }, 
            pulseoximeterrecid: {
                fieldName: 'pulseoximeterrecid'
            },
            spirometersrecid: { 
                fieldName: 'spirometersrecid' 
            },wssrecid: { 
                fieldName: 'wssrecid' 
            }, 
            glucometersrecid: {
                fieldName: 'glucometersrecid'
            },
            tabletrecid: { 
                fieldName: 'tabletrecid' 
            },
            trackingrecid:{
                fieldName: 'trackingrecid'
            }
        }
},

{label: 'Box Id', fieldName: 'boxid', sortable: true, type: 'text'},
{label: 'Status', fieldName: 'boxstatus', sortable: true, type: 'text'},
{label: 'Customer', fieldName: 'boxcustomer', sortable: true, type: 'text'},
{label: 'Tablet IMEI', fieldName: 'tableimei', sortable: true,  type: 'text'},
{label: 'BP BDA', fieldName: 'bpbda',   sortable: true, type: 'text'},
{label: 'WS BDA', fieldName: 'wsbda', sortable: true,type: 'text'},
{label: 'Watch Serial Number', fieldName: 'watchserialnumber',  sortable: true, type: 'text'},
{label: 'Model', fieldName: 'watchmodel', sortable: true,  type: 'text'},
{label: 'Watch Id', fieldName: 'watchid',sortable: true,type: 'text'},
{label: 'Watch MEID', fieldName: 'wathcmeid',  sortable: true, type: 'text'},
];
    data ;
    defaultSortDirection = 'asc';
    sortDirection = 'asc';
    sortedBy;
    startingRecord;
    endingRecord;
    totalRecountCount;
    page = 1; //this will initialize 1st page
    displaydata = []; //data to be displayed in the table
    startingRecord = 1; //start record position per page
    endingRecord = 0; //end record position per page
    pageSize = 50; //default value we are assigning
    totalRecountCount = 0; //total record count received from all retrieved records
    totalPage = 0
    searchInput;
    isModalOpen = false;
    contactId; 
    chooseByDepartmentoptions ;
    departmentVal;
    clinicVal;
    chooseByClinicoptions;
    clinicMap = new Map();
    boxObjectApiName = 'HV_Box__c';
    boxFields = ' id,Name,HV_Status__c,HV_Customer__c,(SELECT Id, Name,HV_Box__c, HV_BDA__c, HV_Model__c, HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c FROM BPs__r),'+
    '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_BDA__c, HV_Model__c, HV_Manufacturer__c FROM Pulse_Oximeters__r),'+
    '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_BDA__c, HV_Manufacturer__c, HV_Model__c FROM Spirometers__r),'+
    '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_SSID__c, HV_Password__c, HV_IMEI__c, HV_Serial_Number__c, HV_Android_ID__c,HV_Model__c FROM Tablets__r),'+
    '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_BDA__c, HV_Model__c, HV_Manufacturer__c FROM WSS__r),'+
    '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_Watch_Script_Output__c, HV_Watch_Serial_Number__c, HV_Watch_Model__c, HV_Watch_ID__c, HV_Watch_MAC_ID__c, HV_Watch_DUID__c, HV_Watch_MEID__c FROM Watches__r),'+
    '(SELECT Id, HV_User_Device_Status__c, HV_Status__c, Name, HV_Box__c, HV_BDA__c, HV_Model__c, HV_Manufacturer__c FROM Glucometers__r),'+
    '(Select Id, HV_Status__c, Name, HV_Box__c, HV_Shipping_Address__c, HV_Shipped_Date__c, HV_QA_Date__c, QA_by__c, HV_Tracking_Number__c, HV_Batch_Number__c, HV_Manufactured_Date__c, HV_Manufactured_By__c FROM QAs__r),'+
    '(SELECT Id, HV_Box__c, HV_Shipped_Date__c,HV_Shipping_Address__c,HV_Tracking_number__c From Tracking_and_Shipping__r),'+
    '(Select Id, HV_Department__c,Account.HV_Patient_Clinic__c From Contacts__r )' 
    optionsCheckbox = [{
        checked: true,
        label: 'Box Id',
        disabled: true,
        value: JSON.stringify({label: 'Box Id', fieldName: 'boxid', sortable: true, type: 'text'}),
    },{
        checked: true,
        label: 'Status',
        disabled: true,
        value: JSON.stringify({label: 'Status', fieldName: 'boxstatus', sortable: true, type: 'text'}),
    },{
        checked: true,
        label: 'Customer',
        disabled: true,
        value: JSON.stringify({label: 'Customer', fieldName: 'boxcustomer', sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'Watch Serial Number',
        disabled: false,
        value: JSON.stringify({label: 'Watch Serial Number', fieldName: 'watchserialnumber',  sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'Watch Model',
        disabled: false,
        value: JSON.stringify({label: 'Watch Model', fieldName: 'watchmodel', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Watch Id',
        disabled: false,
        value: JSON.stringify({label: 'Watch Id', fieldName: 'watchid',sortable: true,type: 'text'}),
    },{
        checked: false,
        label: 'Watch MEID',
        disabled: false,
        value: JSON.stringify({label: 'Watch MEID', fieldName: 'wathcmeid',  sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'Watch MACID',
        disabled: false,
        value: JSON.stringify({label: 'Watch MACID', fieldName: 'wathcmacid',  sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'Watch Status',
        disabled: false,
        value: JSON.stringify({label: 'Watch Status', fieldName: 'watchstatus',  sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'Watch Device Status',
        disabled: false,
        value: JSON.stringify({label: 'Watch Device Status', fieldName: 'watchdevicestatus',  sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'Watch DUID',
        disabled: false,
        value: JSON.stringify({label: 'Watch DUID', fieldName: 'watchduid',  sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'BP BDA',
        disabled: false,
        value: JSON.stringify({label: 'BP BDA', fieldName: 'bpbda',   sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'BP Manufacturer',
        disabled: false,
        value: JSON.stringify({label: 'BP Manufacturer', fieldName: 'bpmanufacturer',   sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'BP Model',
        disabled: false,
        value: JSON.stringify({label: 'BP Model', fieldName: 'bpmodel',   sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'BP Status',
        disabled: false,
        value: JSON.stringify({label: 'BP Status', fieldName: 'bpstatus',   sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'BP Device Status',
        disabled: false,
        value: JSON.stringify({label: 'BP Device Status', fieldName: 'bpdevicestatus',   sortable: true, type: 'text'}),
    },{
        checked: false,
        label: 'Tablet IMEI',
        disabled: false,
        value: JSON.stringify({label: 'Tablet IMEI', fieldName: 'tableimei', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Tablet Android Id',
        disabled: false,
        value: JSON.stringify({label: 'Tablet Android Id', fieldName: 'tableandroidid', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Tablet Model',
        disabled: false,
        value: JSON.stringify({label: 'Tablet Model', fieldName: 'tablemodel', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Tablet Serial Number',
        disabled: false,
        value: JSON.stringify({label: 'Tablet Serial Number', fieldName: 'tableserialnumber', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Tablet Status',
        disabled: false,
        value: JSON.stringify({label: 'Tablet SSID', fieldName: 'tablessid', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Tablet Status',
        disabled: false,
        value: JSON.stringify({label: 'Tablet Status', fieldName: 'tablestatus', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Tablet Device Status',
        disabled: false,
        value: JSON.stringify({label: 'Tablet Device Status', fieldName: 'tabledevicestatus', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Pulse Oximeter BDA',
        disabled: false,
        value: JSON.stringify({label: 'Pulse Oximeter BDA', fieldName: 'pobda', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Pulse Oximeter Manufacturer',
        disabled: false,
        value: JSON.stringify({label: 'Pulse Oximeter Manufacturer', fieldName: 'pomanufacturer', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Pulse Oximeter Model',
        disabled: false,
        value: JSON.stringify({label: 'Pulse Oximeter Model', fieldName: 'pomodel', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Pulse Oximeter Device Status',
        disabled: false,
        value: JSON.stringify({label: 'Pulse Oximeter Device Status', fieldName: 'podevicestatus', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Pulse Oximeter Status',
        disabled: false,
        value: JSON.stringify({label: 'Pulse Oximeter Status', fieldName: 'postatus', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Spirometer BDA',
        disabled: false,
        value: JSON.stringify({label: 'Spirometer BDA', fieldName: 'smbda', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Spirometer Manufacturer',
        disabled: false,
        value: JSON.stringify({label: 'Spirometer Manufacturer', fieldName: 'smmanufacturer', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Spirometer Model',
        disabled: false,
        value: JSON.stringify({label: 'Spirometer Model', fieldName: 'smmodel', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Spirometer Device Status',
        disabled: false,
        value: JSON.stringify({label: 'Spirometer Device Status', fieldName: 'smdevicestatus', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Spirometer  Status',
        disabled: false,
        value: JSON.stringify({label: 'Spirometer Status', fieldName: 'smstatus', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Glucometer BDA',
        disabled: false,
        value: JSON.stringify({label: 'Glucometer BDA', fieldName: 'gmbda', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Glucometer Manufacturer',
        disabled: false,
        value: JSON.stringify({label: 'Glucometer Manufacturer', fieldName: 'gmmanufacturer', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Glucometer Model',
        disabled: false,
        value: JSON.stringify({label: 'Glucometer Model', fieldName: 'gmmodel', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Glucometer Device Status',
        disabled: false,
        value: JSON.stringify({label: 'Glucometer Device Status', fieldName: 'gmdevicestatus', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Glucometer  Status',
        disabled: false,
        value: JSON.stringify({label: 'Glucometer Status', fieldName: 'gmstatus', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'QA Status',
        disabled: false,
        value: JSON.stringify({label: 'QA Status', fieldName: 'qastatus', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'QA Manufactured Date',
        disabled: false,
        value: JSON.stringify({label: 'QA Manufactured Date', fieldName: 'qamanufactureddate', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'QA Manufactured By',
        disabled: false,
        value: JSON.stringify({label: 'QA Manufactured By', fieldName: 'qamanufacturedby', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'QA Date',
        disabled: false,
        value: JSON.stringify({label: 'QA Date', fieldName: 'qadate', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'QA By',
        disabled: false,
        value: JSON.stringify({label: 'QA By', fieldName: 'qaby', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'QA Batch Number',
        disabled: false,
        value: JSON.stringify({label: 'QA Batch Number', fieldName: 'qabatchnumber', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Tracking Shipped Date',
        disabled: false,
        value: JSON.stringify({label: 'Tracking Shipped Date', fieldName: 'trackshippeddate', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Tracking Shipped Address',
        disabled: false,
        value: JSON.stringify({label: 'Tracking Shipped Address', fieldName: 'trackshippedaddress', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Tracking and Shipping Name',
        disabled: false,
        value: JSON.stringify({label: 'Tracking and Shipping Name', fieldName: 'trackshippingname', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'Tracking Number',
        disabled: false,
        value: JSON.stringify({label: 'Tracking Number', fieldName: 'tracknumber', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'WS BDA',
        disabled: false,
        value: JSON.stringify({label: 'WS BDA', fieldName: 'wsbda', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'WS Manufacturer',
        disabled: false,
        value: JSON.stringify({label: 'WS Manufacturer', fieldName: 'wsmanufacturer', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'WS Model',
        disabled: false,
        value: JSON.stringify({label: 'WS Model', fieldName: 'wsmodel', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'WS Device Status',
        disabled: false,
        value: JSON.stringify({label: 'WS Device Status', fieldName: 'wsdevicestatus', sortable: true,  type: 'text'}),
    },{
        checked: false,
        label: 'WS  Status',
        disabled: false,
        value: JSON.stringify({label: 'WS Status', fieldName: 'wsstatus', sortable: true,  type: 'text'}),
    },

];

departmentMap = new Map();


    connectedCallback(){

        console.log('optionsCheckbox',this.optionsCheckbox)
        this.isLoading(true);
        this.handleUserRecord();
        this.getBoxRecord(this.boxObjectApiName,this.boxFields,'');
    }

    

    

    @api
    handleSelectedRec(event) {
        
        const evt = new CustomEvent('isedit',{
            detail:{
                isedit: true,
                boxrecid: event.detail.boxrecid,
                watchrecid: event.detail.watchrecid,
                bprecid: event.detail.bprecid,
                qarecid: event.detail.qarecid,
                pulseoximeterrecid: event.detail.pulseoximeterrecid,
                spirometersrecid: event.detail.spirometersrecid,
                wssrecid: event.detail.wssrecid,
                glucometersrecid: event.detail.glucometersrecid,
                tabletrecid: event.detail.tabletrecid,
                trackingrecid: event.detail.trackingrecid
            }
        })
        this.dispatchEvent(evt);

    }
    

    selectDepartment(event){
        console.log('dept ev',event);
        console.log('dept ev',event.detail.value);
        
        if(event.detail.value === 'All'){
            this.departmentVal = '';
            this.getboxFilterRecord();
           
        }else{
            this.departmentVal = event.detail.value;
            this.getboxFilterRecord();
           
        }
        this.chooseByDepartmentoptions.filter( res =>{

            if(res.value === event.detail.value ){
                res.checked = true; 
            }else{
                res.checked = false;
            }
        }  )
        
        
    }

    selectClinic(event){
        if(event.detail.value === 'All'){
            this.clinicVal = '';
            this.getboxFilterRecord();
        }else{
            this.clinicVal = event.detail.value;
            this.getboxFilterRecord();
        }   
        this.departmentVal = '';
        this.chooseByClinicoptions.filter( res =>{

            if(res.value === event.detail.value ){
                res.checked = true; 
            }else{
                res.checked = false; 
            }
        }  )
        this.chooseByDepartmentoptions = [];
        
        this.departmentMap.get(event.detail.value).filter( res =>{

            
                res.checked = false; 
                this.chooseByDepartmentoptions.push(res);
           
        }  )
        
    }

    onHandleSort(event) {
        console.log('event',event) 
        const {  
            fieldName: sortedBy,
            sortDirection
        } = event.detail;
        const cloneData = [...this.data];
        cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));
        this.data = cloneData;
        console.log('cloneData',cloneData)
        this.displaydata = this.data.slice(0, this.pageSize);
        this.startingRecord = 1; 
        this.endingRecord = this.pageSize;
        this.page = 1;
        this.sortDirection = sortDirection;
        this.sortedBy = sortedBy;
    }

    sortBy(field, reverse, primer) {
        console.log('sort')
        const key = primer ? function (x) {
            return primer(x[field]);
        } : function (x) {
            return x[field];
        };

        return function (a, b) {
            a = key(a);
            b = key(b);
            return reverse * ((a > b) - (b > a));
        };
    }

     //clicking on previous button this method will be called
    previousHandler() {
        console.log('Previous handler');
        if (this.page > 1) {
            this.page = this.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.page);
        }
    }

    //clicking on next button this method will be called
    nextHandler() {
        console.log('test');
        if ((this.page < this.totalPage) && this.page !== this.totalPage) {
            this.page = this.page + 1; //increase page by 1
            this.displayRecordPerPage(this.page);
        }
    }

    //this method displays records page by page
    displayRecordPerPage(page) {

        /*let's say for 2nd page, it will be => "Displaying 6 to 10 of 23 records. Page 2 of 5"
        page = 2; pageSize = 5; startingRecord = 5, endingRecord = 10
        so, slice(5,10) will give 5th to 9th records.
        */
        this.startingRecord = ((page - 1) * this.pageSize);
        this.endingRecord = (this.pageSize * page);

        this.endingRecord = (this.endingRecord > this.totalRecountCount) ? this.totalRecountCount : this.endingRecord;

        this.displaydata = this.data.slice(this.startingRecord, this.endingRecord);

        //increment by 1 to display the startingRecord count, 
        //so for 2nd page, it will show "Displaying 6 to 10 of 23 records. Page 2 of 5"
        this.startingRecord = this.startingRecord + 1;
    }

    searchData(event){
        let searchString = evt.target.value.toUpperCase();
        let allRecords = this.data;
        var searchResults = [];
        var i;

        if (searchString) {
            for (i = 0; i < allRecords.length; i++) {
                if ((allRecords[i].lastname) && (allRecords[i].lastname.toUpperCase().includes(searchString)) ||
                    (allRecords[i].firstname) && (allRecords[i].firstname.toUpperCase().includes(searchString)) ||
                    (allRecords[i].phone) && (allRecords[i].phone.toUpperCase().includes(searchString))) {
                    searchResults.push(allRecords[i]);
                }
            }
            this.displaydata = searchResults;
        } else if (!searchString) {
            console.log('blank');
            this.data = allRecords;
        }
    }
    isLoading(val){
        const evt = new CustomEvent('spinner',{
            detail:{
                isloading :val
            }
        })
        this.dispatchEvent(evt);

    }

    spinload(e){
        this.isLoading(e.detail.isloading);
    }
    handleUserRecord() {
        getSobjectRecord({
             objectname : 'User', 
             fieldnames : 'Contact.HV_Inventory_Data_Column__c,Contact.Id,Contact.AccountId', 
             conditions: ' WHERE Id = \''+this.userId+'\''})
             .then(data => {
                 console.log('data', data);
                 
                 if (data) {
                     
                     this.contactId = data.Contact.Id; 
                     if(data.Contact.AccountId){
                        /*getSobjectRecord({ 
                            objectname: 'HV_ClinicFieldsMapping__mdt', 
                            fieldnames: 'HV_Clinic_Id__c,HV_Department_Values__c',
                            conditions: 'where HV_Clinic_Id__c = \'' + data.Contact.AccountId + '\'' 
                        })
                        .then( result => {
                            console.log('result',result);
                            this.chooseByDepartmentoptions = [];
                        
                            result.HV_Department_Values__c.split(',').forEach(element => {
                                this.chooseByDepartmentoptions.push({ label: element, value: element });
                            });
                        }) 
                        .catch( error => {
                            console.log('error',error);
                        })*/
                        let deptSet = new Set();
                        getSobjectRecords({ 
                            objectname: 'HV_ClinicFieldsMapping__mdt', 
                            fieldnames: 'HV_Clinic_Id__c,MasterLabel,HV_Department_Values__c',
                            conditions: '' 
                        })
                        .then( result => {
                            console.log('result acc',result);
                            this.chooseByClinicoptions = [{ label: 'All', value: 'All' }];
                        
                            result.forEach(element => {
                                let clinicDept = [{ label: 'All', value: 'All' }]
                                this.chooseByClinicoptions.push({ label: element.MasterLabel, value: element.HV_Clinic_Id__c });
                                this.clinicMap.set(element.HV_Clinic_Id__c,element.MasterLabel);
                                element.HV_Department_Values__c.split(',').forEach(ele => {
                                    console.log('ele',ele);
                                    deptSet.add(ele);
                                    clinicDept.push({ label: ele, value: ele });
                                });
                                if(clinicDept){
                                    this.departmentMap.set(element.HV_Clinic_Id__c,clinicDept)
                                }
                                
                            });
                            

                            console.log('this.departmentMap',this.departmentMap)
                            
                            
                            
                            return result;
                        }) 
                        .then( res => {
                            this.chooseByDepartmentoptions = [];
                            let allDept = [{ label: 'All', value: 'All' }];
                            deptSet.forEach(element => {
                                allDept.push({ label: element, value: element });
                            });
                            this.chooseByDepartmentoptions =allDept;
                            this.departmentMap.set('All',allDept);

                        })
                        .catch( error => {
                            console.log('error',error);
                        })

                        

                     }
                     if (data.Contact.HV_Inventory_Data_Column__c) {
                         this.totalColumns = [];
                         this.optionsCheckbox = [];
                         console.log('total column',this.totalColumns);
                         data.Contact.HV_Inventory_Data_Column__c.split(';').forEach(re => {
                             if (JSON.parse(re).checked) {
                                 this.totalColumns.push(JSON.parse(JSON.parse(re).value));
                             }
                             console.log('json', JSON.parse(re));
                             this.optionsCheckbox.push(JSON.parse(re));
                         })
                         console.log('total column',this.totalColumns);
                         this.totalColumns.unshift({ label: '', fieldName: '', type: 'isEdit',editable: true,
        initialWidth : 80,
        typeAttributes: { 
                boxrecid: { 
                    fieldName: 'boxrecid' 
                }, 
                watchrecid: {
                    fieldName: 'watchrecid'
                },
                bprecid: { 
                    fieldName: 'bprecid' 
                },
                qarecid: { 
                    fieldName: 'qarecid'
                }, 
                pulseoximeterrecid: {
                    fieldName: 'pulseoximeterrecid'
                },
                spirometersrecid: { 
                    fieldName: 'spirometersrecid' 
                },wssrecid: { 
                    fieldName: 'wssrecid' 
                }, 
                glucometersrecid: {
                    fieldName: 'glucometersrecid'
                },
                tabletrecid: { 
                    fieldName: 'tabletrecid' 
                },
                trackingrecid:{
                    fieldName: 'trackingrecid'
                }
            }
    })
 console.log('total column',this.totalColumns);
                     } 
                 }
             })
             .catch(error => {
                 if (error) {
                     console.log('err', error)
                 }
             })
     }
    submitDataColumns(event) {
        this.totalColumns = event.detail.totalColumns;
        this.optionsCheckbox = event.detail.optionColumns;

        this.totalColumns.unshift({ label: '', fieldName: '', type: 'isEdit',editable: true,
        initialWidth : 80,
        typeAttributes: { 
                boxrecid: { 
                    fieldName: 'boxrecid' 
                }, 
                watchrecid: {
                    fieldName: 'watchrecid'
                },
                bprecid: { 
                    fieldName: 'bprecid' 
                },
                qarecid: { 
                    fieldName: 'qarecid' 
                }, 
                pulseoximeterrecid: {
                    fieldName: 'pulseoximeterrecid'
                },
                spirometersrecid: { 
                    fieldName: 'spirometersrecid' 
                },wssrecid: { 
                    fieldName: 'wssrecid' 
                }, 
                glucometersrecid: {
                    fieldName: 'glucometersrecid'
                },
                tabletrecid: { 
                    fieldName: 'tabletrecid' 
                },
                trackingrecid:{
                    fieldName: 'trackingrecid'
                }
            }
    })
    }

    closeModal(event){

        this.isModalOpen = event.detail.closeModal;
    }
    
    openModal(event){
        this.isModalOpen = true;
    }

    handleSearch(evt) {
        let searchString = evt.target.value.toUpperCase();
        let allRecords = this.data;
        var searchResults = [];

        if (searchString) {

            allRecords.forEach(record => {
               let isSearchable = false;
                this.totalColumns.forEach( column => {
                    if(record[column.fieldName]){
                        if(record[column.fieldName].toUpperCase().includes(searchString)){
                            isSearchable = true;
                        }
                    }
                })
                if(isSearchable){
                    searchResults.push(record);
                }
            })
            this.displaydata = searchResults;
        } else if (!searchString) {
            console.log('blank');
            this.displaydata = this.data.slice(0, this.pageSize);
        }
    }

   
    getChildRecords(record,fields){
        let objectVal = {}; 
        if(record){
            record.forEach(val => {
                fields.split(',').forEach( field => {
                    if(objectVal[field]){
                        objectVal[field] = objectVal[field]+','+val[field];
                    }else{
                        objectVal[field] = val[field];
                    }
                })
            })
        }
        return objectVal;
    }


    getboxFilterRecord(){
        getBoxRecords({
            objectname: this.boxObjectApiName,
            fieldnames: this.boxFields,
            conditions: ''
        })
        .then( result => { 
            console.log('result',result);
            this.data = []
            if(result){
                result.forEach(record => {
                    if(this.departmentVal && this.clinicVal){
                        if(record['Contacts__r']){
                            record['Contacts__r'].forEach( contRecord => {
                                console.log('dept',this.departmentVal )
                                console.log('clinic',this.clinicVal )
                                if(contRecord.HV_Department__c && record.HV_Customer__c){
                                    if(contRecord.HV_Department__c === this.departmentVal && record.HV_Customer__c === this.clinicMap.get(this.clinicVal)){

                                        this.data.push(this.getBoxData(record));
                                    }
                                }
                            })
                        }
                    }else if(this.clinicVal){
                        
                        if(record['Contacts__r']){
                            record['Contacts__r'].forEach( contRecord => {
                                console.log(' only clinic',this.clinicVal )
                                console.log('contRecord',contRecord.Account.HV_Patient_Clinic__c)
                                if(record.HV_Customer__c){
                                    if(record.HV_Customer__c === this.clinicMap.get(this.clinicVal)){

                                        this.data.push(this.getBoxData(record));
                                    }
                                }
                            })
                        }
                    }else if(this.departmentVal){
                        if(record['Contacts__r']){
                            record['Contacts__r'].forEach( contRecord => {
                                if(contRecord.HV_Department__c){
                                    console.log('only dept',this.departmentVal )
                                    if(contRecord.HV_Department__c === this.departmentVal){

                                        this.data.push(this.getBoxData(record));
                                    }
                                }
                            })
                        }
                    }else{
                        this.data.push(this.getBoxData(record));
                    }
                })
                   
            }
            return this.data;
        })
        .then(data => {
            if(data){
              
                this.totalRecountCount = data.length; //here it is 23
                this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize); //here it is 5
    
                //initial data to be displayed ----------->
                //slice will take 0th element and ends with 5, but it doesn't include 5th element
                //so 0 to 4th rows will be displayed in the table
                this.displaydata = data.slice(0, this.pageSize);
                this.endingRecord = this.pageSize;
                this.isLoading(false);
            }
        })
        .catch( err => {
            console.log('err ',err)
            this.isLoading(false);
        })
      
    }
    getBoxRecord(objectname,fieldnames,conditions){
        getBoxRecords({
            objectname: objectname,
            fieldnames: fieldnames,
            conditions: conditions
        })
        .then( result => { 
            console.log('result',result);
            if(result){
                console.log('record',result);
                this.data = []
                result.forEach( record => {
                   

                    if( record.Id){
                        
                        this.data.push(this.getBoxData(record));
                        console.log(this.data)
                    }
                    
                }) 
                console.log('adadwfw',this.data)
            }
            return this.data;
        })
        .then(data => {
            if(data){
              
                this.totalRecountCount = data.length; //here it is 23
                this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize); //here it is 5
    
                //initial data to be displayed ----------->
                //slice will take 0th element and ends with 5, but it doesn't include 5th element
                //so 0 to 4th rows will be displayed in the table
                this.displaydata = data.slice(0, this.pageSize);
                this.endingRecord = this.pageSize;
                console.log('this.displaydata',this.displaydata)
                this.isLoading(false);
            }
        })
        .catch( err => {
            console.log('err ',err)
            this.isLoading(false);
        })
    }

    getBoxData(record){
        let watchObject = this.getChildRecords(record['Watches__r'],'HV_User_Device_Status__c,HV_Status__c,HV_Watch_Script_Output__c,HV_Watch_Serial_Number__c,HV_Watch_Model__c,HV_Watch_ID__c,HV_Watch_MAC_ID__c,HV_Watch_DUID__c,HV_Watch_MEID__c');
        let tabletObject = this.getChildRecords(record['Tablets__r'],'HV_User_Device_Status__c,HV_Status__c,HV_SSID__c,HV_IMEI__c,HV_Serial_Number__c,HV_Android_ID__c,HV_Model__c');
        let bpObject = this.getChildRecords(record['BPs__r'],'HV_BDA__c, HV_Model__c, HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c');
        let poObject = this.getChildRecords(record['Pulse_Oximeters__r'],'HV_User_Device_Status__c,HV_Status__c,HV_BDA__c,HV_Model__c,HV_Manufacturer__c');
        let gmObject = this.getChildRecords(record['Glucometers__r'],'HV_User_Device_Status__c,HV_Status__c,HV_BDA__c,HV_Model__c,HV_Manufacturer__c');
        let smObject = this.getChildRecords(record['Spirometers__r'],'HV_User_Device_Status__c,HV_Status__c,HV_BDA__c,HV_Manufacturer__c,HV_Model__c');
        let wsObject = this.getChildRecords(record['WSS__r'],'HV_User_Device_Status__c,HV_Status__c,HV_BDA__c,HV_Model__c,HV_Manufacturer__c');
        let qaObject = this.getChildRecords(record['QAs__r'],'HV_QA_Date__c, QA_by__c,HV_Batch_Number__c,HV_Status__c,HV_Manufactured_Date__c,HV_Manufactured_By__c');
        let trackObject = this.getChildRecords(record['Tracking_and_Shipping__r'],'HV_Shipped_Date__c,HV_Shipping_Address__c,HV_Tracking_number__c');

    console.log('tabletObject',tabletObject);
        
       return {
            boxid : record.Name ,
            boxstatus: record.HV_Status__c,
            boxcustomer: record.HV_Customer__c,
            boxrecid : record.Id, 
            watchserialnumber: watchObject.HV_Watch_Serial_Number__c,
            watchmodel:watchObject.HV_Watch_Model__c,
            watchid:watchObject.HV_Watch_ID__c,
            wathcmacid:watchObject.HV_Watch_MAC_ID__c,
            wathcmeid:watchObject.HV_Watch_MEID__c,
            watchstatus:watchObject.HV_Status__c,
            watchdevicestatus:watchObject.HV_User_Device_Status__c,
            watchduid:watchObject.HV_Watch_DUID__c,
            bpbda:bpObject.HV_BDA__c,
            bpmanufacturer:bpObject.HV_Manufacturer__c,
            bpmodel:bpObject.HV_Model__c,
            bpstatus:bpObject.HV_Status__c,
            bpdevicestatus:bpObject.HV_User_Device_Status__c,
            tableimei:tabletObject.HV_IMEI__c,
            tableandroidid:tabletObject.HV_Android_ID__c,
            tablemodel:tabletObject.HV_Model__c,
            tableserialnumber:tabletObject.HV_Serial_Number__c,
            tablessid:tabletObject.HV_SSID__c,
            tablestatus:tabletObject.HV_Status__c,
            tabledevicestatus:tabletObject.HV_User_Device_Status__c,
            pobda:poObject.HV_BDA__c,
            pomanufacturer:poObject.HV_Manufacturer__c,
            pomodel:poObject.HV_Model__c,
            podevicestatus:poObject.HV_User_Device_Status__c,
            postatus:poObject.HV_Status__c,
            smbda:smObject.HV_BDA__c,
            smmanufacturer:smObject.HV_Manufacturer__c,
            smmodel:smObject.HV_Model__c,
            smdevicestatus:smObject.HV_User_Device_Status__c,
            smstatus:smObject.HV_Status__c,
            gmbda:gmObject.HV_BDA__c,
            gmmanufacturer:gmObject.HV_Manufacturer__c,
            gmmodel:gmObject.HV_Model__c,
            gmdevicestatus:gmObject.HV_User_Device_Status__c,
            gmstatus:gmObject.HV_Status__c,
            qastatus:qaObject.HV_Status__c,
            qamanufactureddate:qaObject.HV_Manufactured_Date__c,
            qamanufacturedby:qaObject.HV_Manufactured_By__c,
            qadate:qaObject.HV_QA_Date__c,
            qaby:qaObject.QA_by__c,
            qabatchnumber:qaObject.HV_Batch_Number__c,
            trackshippeddate:trackObject.HV_Shipped_Date__c,
            trackshippedaddress:trackObject.HV_Shipping_Address__c,
            tracknumber:trackObject.HV_Tracking_number__c,
            wsbda:wsObject.HV_BDA__c,
            wsmanufacturer:wsObject.HV_Manufacturer__c,
            wsmodel:wsObject.HV_Model__c,
            wsdevicestatus:wsObject.HV_User_Device_Status__c,
            wsstatus:wsObject.HV_Status__c,
            watchrecid : record['Watches__r'],
            bprecid : record['BPs__r'],
            qarecid : record['QAs__r'],
            pulseoximeterrecid : record['Pulse_Oximeters__r'],
            spirometersrecid : record['Spirometers__r'],
            wssrecid : record['WSS__r'],
            glucometersrecid : record['Glucometers__r'],
            tabletrecid : record['Tablets__r'],
            trackingrecid :  record['Tracking_and_Shipping__r']
        }
    }
}