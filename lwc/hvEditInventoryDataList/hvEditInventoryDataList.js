import { LightningElement, api } from 'lwc';

export default class HvEditInventoryDataList extends LightningElement {

    @api boxrecid;
    @api watchrecid;
    @api bprecid;
    @api qarecid
    @api pulseoximeterrecid
    @api spirometersrecid
    @api wssrecid
    @api glucometersrecid
    @api tabletrecid;
    @api trackingrecid;

    handleBoxRecid(event){
        const custEvent = CustomEvent('selectedrec', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                boxrecid: this.boxrecid,
                watchrecid: this.watchrecid,
                bprecid: this.bprecid,
                qarecid: this.qarecid, 
                pulseoximeterrecid: this.pulseoximeterrecid,
                spirometersrecid: this.spirometersrecid,
                wssrecid: this.wssrecid,
                glucometersrecid: this.glucometersrecid,
                tabletrecid:this.tabletrecid,
                trackingrecid: this.trackingrecid

            }, 
        });
        this.dispatchEvent(custEvent);
        
    }
}