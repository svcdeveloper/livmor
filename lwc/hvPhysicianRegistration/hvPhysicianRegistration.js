import { LightningElement, track, wire, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi'
import { createRecord } from 'lightning/uiRecordApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import currentUserId from '@salesforce/user/Id';
import CURRENT_USER_CLINIC from '@salesforce/schema/User.Contact.AccountId';
import CURRENT_USER_Name from '@salesforce/schema/User.Contact.Name';
import CURRENT_USER_CLINIC_NAME from '@salesforce/schema/User.Contact.Account.Name';
import CURRENT_USER_ROLE from '@salesforce/schema/User.Contact.HV_Role__c';
import CONTACT_OBJECT from '@salesforce/schema/Contact';
import getRoleFieldsMapping from '@salesforce/apex/HVExistingUserValidationController.getRoleFieldsMapping';
import getSobjectRecord from '@salesforce/apex/HVQueryUtility.getSingleRecordImprative';
import createUser from '@salesforce/apex/HVPatientRegistrationController.createUser';
import validateUser from '@salesforce/apex/HVExistingUserValidationController.validateUser';
import sendlicenserequestemail from '@salesforce/apex/HVExistingUserValidationController.sendlicenserequestemail';
import updateOtherUsers from '@salesforce/apex/HVPatientRegistrationController.updateOtherUsers';
import getAllClinc from '@salesforce/apex/HVExistingUserValidationController.getAllClinc';
import getSobjectRecords from '@salesforce/apex/HVQueryUtility.getMultipleRecordImprative';
import HV_License_Validation_Error from '@salesforce/label/c.HV_License_Validation_Error';
import HV_Same_Facility_Active_User from '@salesforce/label/c.HV_Same_Facility_Active_User';
import HV_Different_Facility_and_Active_User from '@salesforce/label/c.HV_Different_Facility_and_Active_User';
import HV_Same_Facility_Inactive_User from '@salesforce/label/c.HV_Same_Facility_Inactive_User';
import HV_Different_Facility_Inactive_User from '@salesforce/label/c.HV_Different_Facility_Inactive_User';
import createSobjectRecord from '@salesforce/apex/HVDMLUtility.createRecord';

export default class HvPhysicianRegistration extends NavigationMixin(LightningElement) {

	@track contactObject = {};
	@track existingUserData = {};
	@track mapClinicAndExistingUser = new Map(); //map of clinic and existing user
	@api roleValue;
	@track errorMessage;
	departmentOptions;
	isLoading = false;
	currentStep = "1"; //control the current Path Step
	currentTab = 1; //control the current tab
	prevDisable = true; //boolean to validate if Previos button in footer needs to be displayed or not
	nextDisable = false; //boolean to validate if Next button in footer needs to be displayed or not
	nextLabel = "Next"; //lable for the Next button in footer [this gets changed as per th
	activeSections = ['A'];
	availableFieldMap;
	isexistinguserindiffclinic;
	selectedDepartment;
	showValidationModal;
	licensevalidation;
	existinguserheader;
	existinguserbody;
	existingusercloselabel;
	existinguseroklabel;
	facilityname;
	showfacilityselect = false;
	parentrolename;

	get facilityModalheader(){
		if(this.roleValue)
		return 'Select Facilility to Create '+this.roleValue;
	}

	get roleName() {
		return this.roleValue + ' Registration';
	}

	get stepCondition1() { //return if Step 1 of Patient Registration needs to be displayed
		return this.currentTab == 1 ? true : false;
	}

	get stepCondition2() { //return if Step 2 of Patient Registration needs to be displayed
		return this.currentTab == 2 ? true : false;
	}

	@wire(getObjectInfo, {
		objectApiName: CONTACT_OBJECT
	}) contactMetadata; //lds method to get schema and details of Contact object

	@wire(getRecord, {
		recordId: currentUserId,
		fields: [CURRENT_USER_CLINIC, CURRENT_USER_Name, CURRENT_USER_CLINIC_NAME,CURRENT_USER_ROLE]
	}) loggedInUserData({
		error,
		data
	}) {
		this.isLoading = true;
		if (data) {
			this.parentrolename = data.fields.Contact.value.fields.HV_Role__c.value;			
			if(this.parentrolename == 'Livmor Admin'){
				this.showfacilityselect = true;
				this.isLoading = false;
			}	
			else{
				this.showFields();
				this.contactObject['AccountId'] = data.fields.Contact.value.fields.AccountId.value;	
				this.facilityname = data.fields.Contact.value.fields.Account.value.fields.Name.value;
				this.isLoading = false;
			}
		}
		if (error) {
			this.isLoading = false;
		}
	}

	renderedCallback() {
		if (this.availableFieldMap) {
			this.availableFieldMap.forEach(fieldName => {
				let layoutEle = this.template.querySelectorAll('.' + fieldName);
				layoutEle.forEach(ele => {
					ele.hidden = false;
				})

				let layoutEleText = this.template.querySelectorAll('.Text-' + fieldName);
				layoutEleText.forEach(ele => {
					ele.hidden = false;
				});

			});
		}

		if (this.isexistinguserindiffclinic) { //if user already exists in same clinic make all the step 1 fields disabled to edit
			this.template.querySelectorAll('lightning-input').forEach(field => {
				field.disabled = true; //make field disabled in layout
			});
			this.template.querySelectorAll('lightning-combobox').forEach(field => {
				field.disabled = true; //make field disabled in layout				
			});
			this.template.querySelectorAll('lightning-dual-listbox').forEach(field => {
				field.disabled = true; //make field disabled in layout				 
			});
			this.template.querySelectorAll('lightning-textarea').forEach(field => {
				field.disabled = true; //make field disabled in layout				 
			});
		}
	}

	handleToggleSection(event) { //method to handle accordian toggle sections and set the open section name
		const openSections = event.detail.openSections;
		if (openSections.length === 0) {
			this.activeSectionsMessage = 'All sections are closed';
		} else {
			this.activeSectionsMessage =
				'Open sections: ' + openSections.join(', ');
		}
	}

	previous(event) { //navigate to previous step
		if (this.currentTab > 1) {
			this.currentTab--;
			this.handleProgress(this.currentTab); //on step change set button visibility and label
		}
	}

	next(event) {
		if (this.validateFields()) {
			if (this.currentTab === 1) {
				this.registerSubmit();
			} else if (this.currentTab === 2) {
				if (this.isexistinguserindiffclinic) {
					this.isLoading = true;
					updateOtherUsers({ contactRecId: this.contactObject.Id })
						.then(result => {
							const evt = new ShowToastEvent({ //show success toast
								title: 'Success',
								message: 'Existing User Activated Successfully!!',
								variant: 'success',
							});
							this.dispatchEvent(evt);
							this.isLoading = false;
							this.cancleClick();
						})
						.catch(error => {
							const evt = new ShowToastEvent({
								title: 'Error ' + error.body.message,
								message: error.body.message,
								variant: 'error',
							});
							this.dispatchEvent(evt);
							this.errormessage = error.body.message;
							this.isLoading = false;
						})
				}
				else {
					this.createUser();
				}
			}
		}
	}

	handleProgress(step) { //on step change set button visibility and label
		switch (step) {
			case 1:
				this.currentStep = "1";
				this.prevDisable = true;
				this.nextDisable = false;
				this.nextLabel = "Next";
				break;
			case 2:
				this.currentStep = "2";
				this.prevDisable = false;
				this.nextDisable = false;
				this.nextLabel = "Verify and Activate";
				break;
			default:
				this.currentStep = "1";
				this.prevDisable = true;
				this.nextDisable = false;
				this.nextLabel = "Next";
				break;
		}
	}

	nextChild(child, reqFieldMap) {
		//console.log('child.childNodes',child.childNodes)
		if (child.childNodes) {
			 
			child.childNodes.forEach(child => {
				console.log('child.name',child.name);
				if (reqFieldMap.has(child.name)) {
					
					child.required = true;
					console.log('child',child);
				} else {
					this.nextChild(child, reqFieldMap);
				}  
			})
		}
	}

	inputChange(event) {
		if (event.target.name === 'HV_Department__c') {
			this.selectedDepartment = event.detail.value;
			let valuesSelected = (event.detail.value).join(",");
			this.contactObject.HV_Department__c = valuesSelected;
		}
		else if (event.target.name == 'Phone') //onchange specific for phone fields to add '-'
		{
			this.contactObject[event.target.name] = (event.target.value).replace(/-/g, '');
			this.contactObject[event.target.name] = this.validatePhone(this.contactObject[event.target.name]);
		}
		else {
			this.contactObject[event.target.name] = event.target.value;
		}
	}

	validatePhone(phoneNumber) { //phone validation add hypen automatically
		var len = phoneNumber.length;
		if ((len > 3) && (phoneNumber[3] != '-')) {
			phoneNumber = [phoneNumber.slice(0, 3), '-', phoneNumber.slice(3)].join('');
		}
		if ((len > 7) && (phoneNumber[7] != '-')) {
			phoneNumber = [phoneNumber.slice(0, 7), '-', phoneNumber.slice(7)].join('');
		}
		return phoneNumber;
	}

	//call this function on onblur of email address - to check if email entered by user is already in use or not
	validateEmailAddress(event) {
		this.template.querySelectorAll('lightning-input').forEach((ele) => {
			if (ele.name == 'Email') {
				ele.setCustomValidity("");
				ele.reportValidity();
			}
		})
		getSobjectRecords({ objectname: 'User', fieldnames: ' id,Name,Email', conditions: ' Where email = \'' + event.target.value + '\'' })
			.then(res => {
				if (res.length > 0) { //if email already in use show error to use another email address
					this.template.querySelectorAll('lightning-input').forEach((ele) => {
						if (ele.name == 'Email') {
							ele.setCustomValidity("Please use different Email as this is already used by other User");
							ele.reportValidity();
						}
					})
				}
			})
			.catch(error => {
				this.errormessage = error.body.message;
			})
	}

	/*facilityChange(event) {
		this.contactObject['AccountId'] = event.target.value;
		if (event.target.name = 'AccountId') {
			this.AccountId = event.target.value;
		}
	}*/

	toggleshowpassword(event) {
		let inputPass = this.template.querySelector('.inputPass');
		let inputConfirmPass = this.template.querySelector('.inputConfirmPass');
		let confirmPasswordErr = this.template.querySelector('.confirmPassowdError');
		let passwordErr = this.template.querySelector('.passowdError');
		if (inputPass.value) {
			if (event.target.checked) {
				inputPass.type = "text";
			} else {
				inputPass.type = "password";
			}
			passwordErr.innerText = '';
		} else {
			passwordErr.innerText = 'Please enter the passorwd';
		}
		if (inputConfirmPass.value) {
			if (event.target.checked) {
				inputConfirmPass.type = "text";
			} else {
				inputConfirmPass.type = "password";
			}
			confirmPasswordErr.innerText = '';
		} else {
			confirmPasswordErr.innerText = 'Please enter the confirm passorwd';
		}
	}

	confirmPasswordChange(event) {
		let ele = this.template.querySelector('.confirmPassowdError');
		if (event.target.value !== this.contactObject.HV_Password__c) {

			ele.innerText = 'Please enter correct passorwd';
		} else {
			ele.innerText = '';
		}
	}

	registerSubmit() {
		this.isLoading = true;
		if (this.validateFields()) {
			validateUser({
				accountRecord: null,
				contactRecord: this.contactObject
			})
				.then(result => {
					if (result.length > 0) {
						for (let i = 0; i < result.length; i++) {
							if (this.mapClinicAndExistingUser.has(result[i].Contact.AccountId)) {
								this.mapClinicAndExistingUser.get(result[i].Contact.AccountId).push(result[i]);
							}
							else {
								let listOfExistingUsers = [];
								listOfExistingUsers.push(result[i]);
								this.mapClinicAndExistingUser.set(result[i].Contact.AccountId, listOfExistingUsers);
							}
						}
						console.log('map', this.mapClinicAndExistingUser);
					}
					let existinguser = this.mapClinicAndExistingUser.has(this.contactObject.AccountId) ? this.mapClinicAndExistingUser.get(this.contactObject.AccountId) : result;
					if (existinguser.length > 0) {
						let i = 0;
						if (existinguser.length > 1) {
							i = (existinguser.length) - 1;
						}
						if (existinguser[i].Contact.AccountId == this.contactObject.AccountId && existinguser[i].IsActive == true) {
							this.isLoading = false;
							this.existinguserheader = this.roleValue + ' already active in this clinic';
							this.existinguserbody = HV_Same_Facility_Active_User;
							this.existingusercloselabel = '';
							this.existinguseroklabel = 'OK';
							this.showValidationModal = true;
						} else if (existinguser[i].Contact.AccountId == this.contactObject.AccountId && existinguser[i].IsActive == false) {
							this.isLoading = false;
							this.existingUserData = existinguser[i].Contact;
							this.existinguserheader = this.roleValue + ' already exists in this clinic and is inactive';
							this.existinguserbody = HV_Same_Facility_Inactive_User;
							this.existingusercloselabel = 'Cancel';
							this.existinguseroklabel = 'Continue';
							this.showValidationModal = true;
						} else if (existinguser[i].Contact.AccountId != this.contactObject.AccountId && existinguser[i].IsActive == true) {
							this.isLoading = false;
							this.existinguserheader = this.roleValue + ' already active at another clinic';
							this.existinguserbody = HV_Different_Facility_and_Active_User;
							this.existingusercloselabel = 'Reject';
							this.existinguseroklabel = 'Accept';
							this.showValidationModal = true;
						} else if (existinguser[i].Contact.AccountId != this.contactObject.AccountId && existinguser[i].IsActive == false) {
							this.isLoading = false;
							this.existinguserheader = this.roleValue + ' already exists at another clinic and is inactive';
							this.existinguserbody = HV_Different_Facility_Inactive_User;
							this.existingusercloselabel = 'Reject';
							this.existinguseroklabel = 'Accept';
							this.showValidationModal = true;
						}
					} else {
						this.currentTab++;
						this.handleProgress(this.currentTab);
						this.isLoading = false;
					}
				})
				.then(() => {
					this.availableFieldMap.forEach(fieldName => {
						let layoutEleText = this.template.querySelectorAll('.Text-' + fieldName);
						layoutEleText.forEach(ele => {
							ele.hidden = false;
						});
					})
				})
				.catch(e => {
					/*const evt = new ShowToastEvent({
						title: 'Error',
						message: e.body.message,
						variant: 'error',
					});
					this.dispatchEvent(evt);*/
					this.isLoading = false;
					this.errormessage = e.body.message;
					if (e.body.message == 'Licenses are not available for this clinic!') {
						this.licensevalidation = true;
						this.showValidationModal = true;
						this.existinguserheader = 'License exhausted for this facility';
						this.existinguserbody = HV_License_Validation_Error;
						this.existingusercloselabel = '';
						this.existinguseroklabel = 'OK';
					}
				})
		} else {
			this.isLoading = false;
		}
	}

	validateFields() {
		const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')] //validate all text input fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const isInputsTextAreaCorrect = [...this.template.querySelectorAll('lightning-textarea')] //validate all teaxt area fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const ismultipicklistCorrect = [...this.template.querySelectorAll('lightning-dual-listbox')] //validate all address fields
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        return (isInputsCorrect && isInputsTextAreaCorrect && ismultipicklistCorrect);
	}

	navigatetologinpage(event) {
		this[NavigationMixin.Navigate]({
			type: 'comm__namedPage',
			attributes: {
				name: 'Login',
			},
		});
	}

	createUser() {
		this.isLoading = true;
		this.contactObject.HV_Role__c = this.roleValue;
		const rtis = this.contactMetadata.data.recordTypeInfos;
		this.contactObject.RecordTypeId = Object.keys(rtis).find(rti => rtis[rti].name === 'Business'); //Object.keys(rtis).find(rti => rtis[rti].name === 'IndustriesBusiness');
		this.contactObject.attributes = { type : "Contact"}
		
		

			createSobjectRecord({
                createRec : this.contactObject
            })
			.then(record => {
				console.log(' con recon',record);
				console.log(' con recon',record.Id);
				if (record.Id) { //if account/contact created successfully create user record for the personaccount
					const notesTosend = this.notes;
					createUser({
						conOrAccRecordId: record.Id,
						notesForUser: null
					})
					.then(result => {
						const evt = new ShowToastEvent({ //show success toast
							title: 'Success',
							message: 'User Registered Successfully!!',
							variant: 'success',
						});
						this.dispatchEvent(evt);
						this.cancleClick();
						this.errormessage = '';
						this.currentTab++;
						this.handleProgress(this.currentTab);
						this.isLoading = false;
					})
					.catch(error => {
						console.log('error',error);
						const evt = new ShowToastEvent({
							title: 'Error',
							message: 'Error in User Registeration',
							variant: 'error',
						});
						this.dispatchEvent(evt);
						this.errormessage = error.body.message; //set error if any
						this.isLoading = false;
					})
				}else{
					this.isLoading = false;
				}

			})
			.catch(error => {
				console.log('error',error);
				const evt = new ShowToastEvent({
					title: 'Error',
					message: error.body.message,
					variant: 'error',
				});
				this.dispatchEvent(evt); 
				this.isLoading = false;
			});

	}

	cancleClick() { //cancel the registration process and go back
		this.dispatchEvent(new CustomEvent('backcall'));
	}

	printDocument(element) { //review all the input details and download it as pdf     
		// this.template.querySelector('[data-id="printblock"]').className = 'slds-col slds-size_12-of-12 printdiv';
		// window.print();
		// this.template.querySelector('[data-id="printblock"]').className = 'slds-col slds-size_12-of-12';

		this.template.querySelector('[data-id="printblock"]').className = 'slds-col slds-size_12-of-12 printdiv';
		this.template.querySelector('[data-name="printBlockContainer"]').className = 'customPrint customPrintActive';
		window.print();
		this.template.querySelector('[data-name="printBlockContainer"]').className = 'customPrint';
		this.template.querySelector('[data-id="printblock"]').className = 'slds-col slds-size_12-of-12';
	}

	handleexistingmodalclose(event) {
		if (event.detail == 'Reject' || 'Cancel') {
			this.showValidationModal = false;
			this.errormessage = '';
			this.cancleClick();
		}
	}

	handleexistingusersubmitmodal(event) {		
		if (event.detail == 'OK') {			
			if (this.licensevalidation) {				
				let conclinicname = this.facilityname;
			
				sendlicenserequestemail({ clinicname: conclinicname })
					.then(result => {
						const evt = new ShowToastEvent({ //show success toast
							title: 'Success',
							message: 'Request for additional license sent to Livmor Admin Team!!',
							variant: 'success',
						});
						this.dispatchEvent(evt);
					})
					.catch(error => {
						const evt = new ShowToastEvent({
							title: 'Error',
							message: error.body.message,
							variant: 'error',
						});
						this.dispatchEvent(evt);
						this.errormessage = error.body.message;
					})
				this.showValidationModal = false;
				this.errormessage = '';
				this.cancleClick();
			}
			else {
				this.showValidationModal = false;
				this.errormessage = '';
				this.cancleClick();
			}
		}
		if (event.detail == 'Accept') {
			this.showValidationModal = false;
			this.errormessage = '';
			this.currentTab++;
			this.handleProgress(this.currentTab);
		}
		if (event.detail == 'Continue') {
			this.errorMessage = 'A user matching this registration already exists in this clinic, but the user is inactive. Click on Register ' + this.roleValue + ' to activate the user.';
			this.showValidationModal = false;
			this.isexistinguserindiffclinic = true;
			//override existing patient data
			this.contactObject = this.existingUserData;
			if (this.contactObject.HV_Department__c) {
				this.selectedDepartment = this.contactObject.HV_Department__c.split(',');
			}
			this.currentTab++;
			this.handleProgress(this.currentTab);
		}
	}


	closeFacilityModal(event){
		if (event.detail == 'Reject' || 'Cancel' || 'NO') {
            this.showfacilityselect = false;
            this.dispatchEvent(new CustomEvent('backcall'));
        }
	}

	submitFacility(event){
		if (event.detail.eventname == 'Next') {
            this.contactObject['AccountId'] = event.detail.clinicid;
			this.facilityname = event.detail.clinicname;
			console.log('this.facilityname',this.facilityname)
            this.showfacilityselect = false;
			if(! this.showfacilityselect){
				this.showFields();
			}
			
        }
	}

	showFields(){
		let reqFieldMap = new Map();
		getSobjectRecord({
			objectname: 'HV_Roles_and_FieldsMapping__mdt',
			fieldnames: ' Id, HV_Avaliable_Fields__c, HV_Required_Fields__c, HV_Department_Values__c  ',
			conditions: ' Where MasterLabel = \'' + this.roleValue + '\''
		})
		.then(result => {
			if (result) {
				if (result.HV_Department_Values__c) {
					this.departmentOptions = [];
					result.HV_Department_Values__c.split(",").map(field => {
						field.trim();
						this.departmentOptions.push({ label: field.trim(), value: field.trim() });
					});
				}
				return result;
			}
		})
		.then(result => {
			if (result.HV_Avaliable_Fields__c) {
				if (result.HV_Required_Fields__c) {
					console.log('requiredfields', result.HV_Required_Fields__c);
					result.HV_Required_Fields__c.split(',').forEach(fieldName => {
						reqFieldMap.set(fieldName, fieldName);
					})
				}
				console.log('reqFieldMap',reqFieldMap);
				this.availableFieldMap = new Map();
				result.HV_Avaliable_Fields__c.split(',').forEach(fieldName => {
					this.availableFieldMap.set(fieldName, fieldName);
					let layoutEle = this.template.querySelectorAll('.' + fieldName);
					//console.log('layoutEle',layoutEle)
					layoutEle.forEach(ele => {
						//console.log('ele',ele)
						ele.hidden = false;
						this.nextChild(ele, reqFieldMap);
					})
				})
			}
			this.isLoading = false;
		})
		.catch(error => {
			this.isLoading = false;
		})
	}
}