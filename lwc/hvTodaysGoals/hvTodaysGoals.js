import { LightningElement, track, api } from 'lwc';
import HVChartJS from '@salesforce/resourceUrl/HVChartJS';
import { loadScript } from 'lightning/platformResourceLoader';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import patientId from '@salesforce/apex/HVPatientDashboardController.getUserId';

export default class HvTodaysGoals extends LightningElement {
@track dateToday;
@track counter = 0;
apiendpoint = 'https://salesforceapi.livmor.com/dashboard';
inputpatientId;
inputStartDate= Math.floor(new Date().getTime()/1000.0) - 86400 * 1;
inputenddate = Math.floor(new Date().getTime()/1000.0);
apimethod = 'POST';
devicetype = 'dashboardlatestdata';
apirequestbody = {};
weight;
heart_rate_data;
bp;
loadawsdata = false;
@track isChartJsInitialized;
chart;

renderedCallback() { 
    if (this.isChartJsInitialized) {
        return;
    }
    this.isChartJsInitialized = true;

    Promise.all([loadScript(this, HVChartJS)])
    .then(() => {
        console.log('load success');
    })
    .catch(error => {
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Error loading ChartJS',
                message: error.message,
                variant: 'error',
            }),
        );
    });
}

connectedCallback(){
    patientId()  
    .then(data => {
        if (data) {
            this.inputpatientId = data.Contact.HV_Patient_ID__c;
            console.log('this.inputpatientId ::::::: ',data.Contact.HV_Patient_ID__c);
            this.apirequestbody["patientid"] = this.inputpatientId;
            this.apirequestbody["startdate"] = this.inputStartDate;
            this.apirequestbody["enddate"] = this.inputenddate;
            this.loadawsdata = true;
        }
    })
    .catch(error => {
        this.error = error;  
    })
    //this.apirequestbody = { "patientid":"test00000001","startdate":"1624795200","enddate":"1624881600" };
    //this.loadawsdata = true;
    let date = new Date() 
    let options = {  
        weekday: "long", year: "numeric", month: "short",  
        day: "numeric", hour: "2-digit", minute: "2-digit"  
    };  

    console.log(date.toLocaleTimeString("en-us", options));
    this.dateToday = date;
}

handleresponse(event){
    let hasweight = 0;
    let hasbp = 0;
    let hasheartrate = 0;
    var actualData = event.detail;
    if(actualData && actualData.awsresponse.data){
        this.weight = actualData.awsresponse.data.weight;
        this.heart_rate_data = actualData.awsresponse.data.heart_rate_data;
        this.bp = actualData.awsresponse.data.systolic+ '/' + actualData.awsresponse.data.diastolic+' mm Hg';
        if(this.weight != undefined && this.weight != null && this.weight != ''){
            this.counter = this.counter + 1;
            hasweight = 1;
        }else{
            hasweight = 0;
        }
        if(this.heart_rate_data != undefined && this.heart_rate_data != null && this.heart_rate_data != ''){
            this.counter = this.counter + 1;
            hasheartrate = 1;
        }else{
            hasheartrate = 0;
        }
        if(this.bp != undefined && this.bp != null && this.bp != ''){
            this.counter = this.counter + 1;
            hasbp = 1;
        }else{
            hasbp = 0;
        }
        
        const ctx = this.template.querySelector('canvas.donut')
        .getContext('2d');
        //this.chart = new window.Chart(ctx, this.config);
        new window.Chart(ctx,
            {
                type : 'doughnut',
                data :{
                datasets :[
                {
                data: [
                    hasweight,hasbp,hasheartrate
                ],
                backgroundColor :[
                    'rgb(46, 132, 74)',
                    'rgb(255,205,86)',
                    'rgb(254, 147, 57)'
                ],
                    label:'Dataset 1'
                }
                ],
                labels:['Weight', 'Blood Pressure', ' Heart Rate']
                },
                options: {
                    responsive : true,
                legend : {
                    display: false,
                },
                tooltips: {
                    enabled: false
                 },
                animation:{
                    animateScale: true,
                    animateRotate : true
                }
                }
                }
            );
    }
    this.loadawsdata = false;
}

}