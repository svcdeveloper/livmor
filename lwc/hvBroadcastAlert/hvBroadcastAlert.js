import { LightningElement, wire, track } from 'lwc';
import bannerMessages from '@salesforce/apex/HVBroadcastAlertController.bannerMessages';

export default class HvBroadcastAlert extends LightningElement {
@track messages;
@track error;
@wire(bannerMessages) 
bannerMessages({ error, data }) {
        if (data) {
            this.messages = data[0]; 
        } else if (error) { 
            this.error = error;  
        }   
    }
}