import { LightningElement, track, api } from 'lwc';
import getContact from '@salesforce/apex/HVForgotPassword.getContact';

export default class HvForgotPassword extends LightningElement {
    @api role;
    step1 = true;
    step2 = false;
    step3 = false;
    step4 = false;
    email;
    dob;
    errorMessage;
    user;
    otp;
    selectedValue;

    emailchange(event) {
        this.email = event.target.value;
    }

    dobchange(event) {
        this.dob = event.target.value;
    }

    get ifPatientRole() {
        return this.role == "patient" ? true : false;
    }

    get ifOtherRole() {
        return this.role == "others" ? true : false;
    }

    haldleChild(event) {
        this.otp = event.detail.otp;
        this.selectedValue = event.detail.selectedValue;
        switch (event.detail.step) {
            case 1: this.step1 = true; this.step2 = false; this.step3 = false; this.step4 = false;
                break;
            case 2: this.step1 = false; this.step2 = true; this.step3 = false; this.step4 = false;
                break;
            case 3: this.step1 = false; this.step2 = false; this.step3 = true; this.step4 = false;
                break;
            case 4: this.step1 = false; this.step2 = false; this.step3 = false; this.step4 = true;
                break;
            default: this.step1 = true; this.step2 = false; this.step3 = false; this.step4 = false;
                break;
        }
    }

    continueNext() {
        if (this.validateFields()) {
            getContact({ email: this.email, dob: this.dob })
                .then(result => {
                    console.log(JSON.stringify(result));
                    this.user = result;
                    if (this.user.Contact.HV_isTwoFactorAuthentactionEnabled__c) {
                        console.log('Is2Factor ' +this.user.Contact.HV_isTwoFactorAuthentactionEnabled__c);
                        this.step1 = false; this.step2 = true; this.step3 = false; this.step4 = false;
                    } else {
                        this.errorMessage = 'Your User is not set as 2 Factory Authentication Please reach out Clinical System Admin ';
                    }
                })
                .catch(error => {
                    this.step1 = true; this.step2 = false; this.step3 = false; this.step4 = false;
                    console.log('error', error);
                    console.log(JSON.stringify(error));
                    if (error.body.message) {
                        this.errorMessage = error.body.message;
                    }
                });
        }
    }

    validateFields() {

        const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);
        return isInputsCorrect;
    }

    cancelForgetPassrd() {
        const event = new CustomEvent('child', {
            detail: { cancelforgotPassword: false }
        });
        this.dispatchEvent(event);
    }

}