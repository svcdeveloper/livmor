import LightningDatatable from 'lightning/datatable';
import editInventoryDatalist from './hvEditInventoryDataList.html';

export default class HvCustoomInventoryDatalist extends LightningDatatable {

    static customTypes = {
        isEdit : {
            template: editInventoryDatalist,
            standardCellLayout: true,
            typeAttributes: ['boxrecid','watchrecid','bprecid','qarecid','pulseoximeterrecid','spirometersrecid','wssrecid','glucometersrecid','tabletrecid','trackingrecid'],
        } 
    };
}