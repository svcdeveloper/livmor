import { LightningElement,api } from 'lwc';
import { updateRecord } from 'lightning/uiRecordApi';
export default class HvSelectColumns extends LightningElement { 

    @api optionsCheckbox;
    @api contactId;
    isLoading = false;
    slectedColumnCount = 0;
    selectJson = ''

    closeModal(event){
        this.dispatchEvent(new CustomEvent('close',{
                detail:{
                    closeModal: false
                }
            })
        )
    }

    submitDataColumns(event){
        this.isLoading = true;
        let columns = [];
        const elements = this.template.querySelectorAll('.Items');
        let colErrEle =  this.template.querySelector('.columnError');
        let optionColumns = [];

        elements.forEach((ele,index) => { 

            if (ele) {
                if(ele.checked){
                    columns.push(JSON.parse(ele.value));
                    if(this.slectedColumnCount >= 10){
                        colErrEle.innerHTML = 'You can not select more than 10 column';
                        this.isLoading = false;
                        this.slectedColumnCount++;
                        throw new Error("ERROR");
                    }
                }
                optionColumns.push(ele);
                
                if (this.selectJson == '') {
                    this.selectJson = JSON.stringify({checked:ele.checked,label:ele.label,disabled:ele.disabled,value:ele.value});
                } else {
                    this.selectJson = this.selectJson + ';' + JSON.stringify({checked:ele.checked,label:ele.label,disabled:ele.disabled,value:ele.value});
                }
            }
        })
        console.log('contactid',this.contactId);

        const fields = {};
        fields['Id'] = this.contactId; 
        fields['HV_Inventory_Data_Column__c'] = this.selectJson;
        const recordInput = {
            fields
        };

        updateRecord(recordInput)
            .then(() => {
                console.log('update')
                this.dispatchEvent(new CustomEvent('submit',{
                        detail:{
                            totalColumns: columns,
                            optionColumns : optionColumns
                        }
                    })
                )
                this.closeModal();
                this.isLoading = false;
            })
            .catch(error => {
                console.log('err', error)
                this.closeModal();
                this.isLoading = false;

            });
    }


    dragStart(event) {
        console.log('dropstart', event.target.value)
        console.log('dropstart', event)
        event.target.classList.add('drag')

    }

    dragOver(event) {
        event.preventDefault()
        console.log('dropover')
        return false
    }

    drop(event) {
        event.stopPropagation()
        const elements = this.template.querySelectorAll('.Items')
        const dragVal = this.template.querySelector('.drag');
        const dropVal = event.target;
       
        console.log('drag ',dragVal.checked);
        console.log('drop ',dropVal.checked);

        let curentArr = [];

        elements.forEach(ele => {
            if (ele.name == dragVal.name) {
                console.log('drop vl ',dropVal.checked);
                curentArr.push({checked:dropVal.checked,label:dropVal.label,disabled:dropVal.disabled,value:dropVal.value});
            } else if (ele.name  == dropVal.name) {
                console.log('dragVal vl ',dragVal.checked);
                curentArr.push({checked:dragVal.checked,label:dragVal.label,disabled:dragVal.disabled,value:dragVal.value});
            } else {
                console.log('else ',ele.checked);
                curentArr.push({checked:ele.checked,label:ele.label,disabled:ele.disabled,value:ele.value});
            } 
        })
        console.log('curentArr',curentArr)
        this.optionsCheckbox = []
        this.optionsCheckbox = curentArr;
        elements.forEach(element => {
            element.classList.remove('drag')
        })
        return this.optionsCheckbox;

    }
}