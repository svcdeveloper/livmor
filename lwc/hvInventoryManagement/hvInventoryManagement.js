import { LightningElement, track } from 'lwc';

export default class HvInventoryManagement extends LightningElement {

    @track isNewInventory = false;
    @track isDataList = true;
    @track isLoading = false;
    @track isEditInventory = false;
    @track boxrecid;
    @track watchrecid;
    @track bprecid;
    @track qarecid
    @track pulseoximeterrecid
    @track spirometersrecid
    @track wssrecid
    @track glucometersrecid
    @track tabletrecid;
    @track trackingrecid;

    
    onNewIventory(){
        this.isDataList = false;  
        this.isNewInventory = true;
        this.isEditInventory = false;
    }

    backToInventory(){
        this.isNewInventory = false;
        this.isDataList = true;
        this.isEditInventory = false;
    }

    spinnerLoading(event){
        this.isLoading = event.detail.isloading;
    }

    handleEditInventory(event){
        this.isEditInventory = event.detail.isedit;
        this.boxrecid = event.detail.boxrecid;
        this.watchrecid = event.detail.watchrecid;
        this.bprecid = event.detail.bprecid;
        this.qarecid = event.detail.qarecid;
        this.pulseoximeterrecid = event.detail.pulseoximeterrecid;
        this.spirometersrecid = event.detail.spirometersrecid;
        this.wssrecid = event.detail.wssrecid;
        this.glucometersrecid = event.detail.glucometersrecid;
        this.tabletrecid = event.detail.tabletrecid;
        this.trackingrecid = event.detail.trackingrecid;
        
        this.isDataList = false;  
        this.isNewInventory = false;
    }
}