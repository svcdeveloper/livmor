import { LightningElement, api, wire  } from 'lwc';
import updateSobjectRecord from '@salesforce/apex/HVDMLUtility.updateRecord';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSingleRecordImprative from '@salesforce/apex/HVQueryUtility.getSingleRecordImprative';
import sendEmail from '@salesforce/apex/hvSymptomStatusEmailing.sendSymptomChecker';

export default class HvSelectSymptomStatus extends LightningElement {

    isLoading = false;
    
    @api patientDetail;
    currentSymptomStatus;
    patientRecord;

    connectedCallback(){

        getSingleRecordImprative({ 
            objectname: 'Contact', 
            fieldnames: 'HV_Symptom_Status__c,FirstName,LastName,BirthDate',
            conditions: 'where Id = \'' + this.patientDetail + '\'' 
        }) //get selected user details to pass to user profile page
        .then(result => {
            console.log(result);
            this.patientRecord = result;
            this.currentSymptomStatus= result.HV_Symptom_Status__c;
            if(this.currentSymptomStatus){
                this.currentZoneBackground(this.currentSymptomStatus);
                this.currentZoneBorder(this.currentSymptomStatus);
            }
        })
        .catch(error => {
            console.error('error',error);
        })
            
        }
    
      

    renderedCallback(){
        console.log('patientDetail',this.patientDetail); 
        console.log('currentSymptomStatus',this.currentSymptomStatus); 
       /*if(this.currentSymptomStatus){
            this.currentZoneBackground(this.currentSymptomStatus);
            this.currentZoneBorder(this.currentSymptomStatus);
        }*/
    }

    closeModal(){
        this.dispatchEvent(new CustomEvent('closesymptomchecker',{
            detail:false
        }))
    }

    selectGreenSymptom(event){
        this.currentZoneBackground('Green')
    }

    selectYellowSymptom(event){  
        this.currentZoneBackground('Yellow')
    }

    selectRedSymptom(event){
        this.currentZoneBackground('Red')
    }


    highlightGreenZone(event){
       this.currentZoneBorder('Green');
    }

    highlightYellowZone(event){ 
        this.currentZoneBorder('Yellow');
    }

    highlightRedZone(event){
        this.currentZoneBorder('Red');
    }
 
    currentZoneBorder(zoneName){
        
        let eleGreen = this.template.querySelector('.greenSymptom');
        let eleYellow = this.template.querySelector('.yellowSymptom');
        let eleRed = this.template.querySelector('.redSymptom');

        if(eleGreen){
            eleGreen.style.boxShadow = "";
        }

        if(eleYellow){
            eleYellow.style.boxShadow = "";
        }

        if(eleRed){
            eleRed.style.boxShadow = "";
        }
        
        

        if(zoneName === 'Green'){
            if(eleGreen){
              eleGreen.style.boxShadow = "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)";
            }
            
        }

        if(zoneName === 'Yellow'){
            if(eleYellow){
                eleYellow.style.boxShadow = "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)";
            }
        }

        if(zoneName === 'Red'){
            if(eleRed){
                eleRed.style.boxShadow = "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)";
            }
        }
    }

    currentZoneBackground(zoneName){
        
        let eleGreen = this.template.querySelector('.greenZone');
        let eleYellow = this.template.querySelector('.yellowZone');
        let eleRed = this.template.querySelector('.redZone');

        if(eleGreen){
            eleGreen.style.backgroundColor = ""; 
        }

        if(eleYellow){
            eleYellow.style.backgroundColor = "";
        }

        if(eleRed){
            eleRed.style.backgroundColor = "";
            eleRed.style.color = "black"
        }
        
        if(zoneName === 'Green'){
            if(eleGreen){
                eleGreen.style.backgroundColor = "#6db156";
                this.currentSymptomStatus = 'Green';
            }
        }

        if(zoneName === 'Yellow'){ 
            
            if(eleYellow){
                eleYellow.style.backgroundColor = "#ffd966";
                this.currentSymptomStatus = 'Yellow';
            }
        }

        if(zoneName === 'Red'){
            if(eleRed){
                eleRed.style.backgroundColor = "#ae2535";
                eleRed.style.color = "white"
                this.currentSymptomStatus = 'Red';
            }
            
        }

    }

    submitSymptomStatus(){

        if(this.currentSymptomStatus){
            this.isLoading = true;
            let patientObject = {}
            patientObject.HV_Symptom_Status__c = this.currentSymptomStatus; 
            patientObject.Id = this.patientDetail;
            patientObject.attributes = { type : "Contact"}

            updateSobjectRecord({ 
                uapdateRec : patientObject
            })
            .then(record => {
                console.log(' con recon',record);  
                console.log(' con recon',record.Id);
                const evt = new ShowToastEvent({
                    title: 'Success',
                    message: 'Symptom status is changed',
                    variant: 'success',
                });
                this.dispatchEvent(evt); 
                
                return record;
            })
            .then(result => {
                if(this.currentSymptomStatus === 'Yellow' || this.currentSymptomStatus === 'Red'){
console.log('this.currentSymptomStatus now',this.currentSymptomStatus);
                    let current = new Date();
                    let cDate = current.getFullYear() + '-' + (current.getMonth() + 1) + '-' + current.getDate();
                    
                   
                    let hours = current.getHours() > 12 ? current.getHours() - 12 : current.getHours();
                    let am_pm = current.getHours() >= 12 ? "PM" : "AM";
                    hours = hours < 10 ? "0" + hours : hours;
                    let minutes = current.getMinutes() < 10 ? "0" + current.getMinutes() : current.getMinutes();
                    let dateTime = cDate + ' ' + hours +':'+minutes+' '+ am_pm; 
                    let mailBody = '<html><style>.Yellow{background-color:yellow;width:60px;height:20px}.Red{background-color:red;width:50px;height:20px}</style><body><p>Clinical team please be advises, The below patient(s) have logged as yellow zone in Heart View Poratl.</p><table border="1" style="width:100%;padding:10px;"><tr><td><b>Firts Name</b></td><td><b>Last Name</b></td><td><b>D.O.B</b></td><td><b>Reported Time (CST)</b></td><td><b>Symptom Status</b></td></tr><tr><td>'+this.patientRecord.FirstName+'</td><td>'+this.patientRecord.LastName+'</td><td>'+this.patientRecord.Birthdate+'</td><td>'+dateTime+'</td><td><h3 class=\''+this.currentSymptomStatus+'\''+'><span >'+this.currentSymptomStatus+'</span></h3></td></tr></table>	</body></html>'
                 
                    sendEmail({
                        mailBody: mailBody
                    })
                    .then(res => {
                        console.log('error',res)
                    })
                    .catch(error=>{
                        console.error('error',error);
                    })
                }
                this.closeModal();
                this.isLoading = false;
                
            })
            .catch(error => {
                console.log('error',error);
                const evt = new ShowToastEvent({
                    title: 'Error',
                    message: error.body.message,
                    variant: 'error',
                });
                this.dispatchEvent(evt); 
                this.isLoading = false;
            });
        }
        
    }

    
}