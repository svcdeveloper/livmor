import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import isguest from '@salesforce/user/isGuest';
import COMMUNITYURL from '@salesforce/label/c.HVCommunityBaseURL';//Import Custom Label
import HVLoginLabel from '@salesforce/label/c.HVLogin';//Import Custom Label
import login from '@salesforce/apex/HVLoginUser.login'; //Import Apex Class

export default class HvLogin extends LightningElement {
    // Expose the labels to use in the template.
    isguestuser = isguest;
    label = {
        HVLoginLabel,
    };
    errorMessage = '';
    showforgotpassword = false;
    username;
    upassword;
    starthomepageurl = COMMUNITYURL;

    connectedCallback() {
        if (!this.isguestuser) {
            location.href = this.starthomepageurl;
        }
    }

    emailchange(event) {
        this.username = event.target.value;
    }

    passwordchange(event) {
        this.upassword = event.target.value;
    }

    dologin(event) {
        let searchCmp = this.template.querySelector(".email");
        let PassWordCmp = this.template.querySelector(".inputTypesPass");
        let searchvalue = searchCmp.value;
        let dtValue = PassWordCmp.value;
        if (!searchvalue) {
            searchCmp.setCustomValidity("Please enter your Username or PatientID");
        } else {
            searchCmp.setCustomValidity(""); // clear previous value
        }
        searchCmp.reportValidity();

        if (!dtValue) {
            PassWordCmp.setCustomValidity("Please enter your password");
        } else {
            PassWordCmp.setCustomValidity(""); // clear previous value
        }
        PassWordCmp.reportValidity();
        if (searchvalue && dtValue) {

            if (this.username && this.upassword) {
                login({ username: this.username, password: this.upassword, startUrl: this.starthomepageurl })
                    .then(result => {
                        console.log('success', result);
                        location.href = result;
                    })
                    .catch(error => {
                        console.log('error', error);
                        if (error.body.message) {
                            this.errorMessage = error.body.message;
                        }
                    });
            }
        }

    }

    validateFields() {
        this.template.querySelectorAll('lightning-input').forEach(element => {
            element.reportValidity();
        });
    }

    handleNavigation() {
        this.showforgotpassword = true;
    }

    togglePaswrd() {
        this.template.querySelectorAll('.inputTypesPass').forEach(input => {
            if (input.type == "password") {
                input.type = "text";
            } else if (input.type == "text") {
                input.type = "password";
            }
        });
    }

    menuTriggerClick(event) {
        event.preventDefault();
        this.template.querySelector('lightning-button-menu').click();
    }

    handleOnselect(event) {
        event.preventDefault();
        this.selectedItemValue = event.detail.value;
        this.showforgotpassword = true;
    }

    backtoLogin(event) {
        this.showforgotpassword = event.detail.cancelforgotPassword;
    }
}