import { LightningElement, api, wire, track } from 'lwc';
import loginUserId from '@salesforce/user/Id';
import TIMEZONE from '@salesforce/i18n/timeZone';
import { getRecord } from 'lightning/uiRecordApi';
import CURRENT_USER_CONTACTID from '@salesforce/schema/User.Contact.Id';
import CURRENT_USER_ROLE from '@salesforce/schema/User.Contact.HV_Role__c';
import getDeviceTabsDetails from '@salesforce/apex/HVPatientMeasurementController.getDeviceTabsDetails';

const USERFIELDS = [CURRENT_USER_CONTACTID, CURRENT_USER_ROLE]; //fields to query from user object

export default class HvHeartRateMeasurment extends LightningElement {
    @api awsstartdate;
    @api awsenddate;
    @api awspatientid;
    @track toggleFlag = false;
    @track frameFlag = false;
    @track frameUrl;
    @track watchSessions = [];
    @track watchSessionsFinal = [];
    isPatient = false;
    afstatus2 = false;
    afstatus3 = false;
    /*endDate = Math.floor(Date.now() / 1000);
    startDate = this.endDate - (30 * 24 * 60 * 60);*/
    awsendpoint/* = 'https://salesforceapi.livmor.com/watchsessions'*/;
    awsmethod = 'POST';
    //awsbody = { "patientid": "test00000001", "startdate": "1623888000", "enddate": "1625082122" };
    @track awsbody;
    awscallidentifier = 'watchsessions';
    ppgViewerUrl/* = 'https://s3.us-east-2.amazonaws.com/ppg-viewer.livmor.com/ppg-viewer.html?session_id='*/;
    showspinner = true;


    get iswatchsessionavailable(){
        return this.watchSessionsFinal.length > 0 ? true :false;
    }
    @wire(getRecord, { recordId: loginUserId, fields: USERFIELDS })
    loggedInUserData({ error, data }) { //lds method to get current logged in user details using schema
        if (data) {
            console.log('WatchSessionAPIScreen role: ' + data.fields.Contact.value.fields.HV_Role__c.value);
            if(data.fields.Contact.value.fields.HV_Role__c.value === 'Patient'){
                this.isPatient = true;
            }
        } else if (error) {
            console.log('Error getting data from user: ' + error.body.message);
        }
    }

    connectedCallback() {
        this.awsbody = { "patientid": this.awspatientid, "startdate": this.awsstartdate, "enddate": this.awsenddate };

        getDeviceTabsDetails()
            .then(result => {
                let tabdata = [];
                result.forEach(device => {
                    tabdata.push({
                        Id: device.HV_DeviceUniqueIdentifier__c,
                        awsendpoint: device.HV_AWSEndpointAPI__c,
                        awsppgviewer: device.HV_PpgViewerURL__c
                    })
                });
                tabdata = tabdata.sort((a, b) => a.order - b.order);
                this.deviceData = tabdata;
                this.deviceData.forEach(device => {
                    if (device.Id == "Heart") {
                        this.awsendpoint = device.awsendpoint;
                        this.ppgViewerUrl = device.awsppgviewer;
                    }
                })

            })
            .catch(error => {
                this.errormessage = error.body.message;
            })
    }

    handelAwsResponse(event){
        console.log('WatchSessionAPIScreen loggedin user timezone: ' + TIMEZONE);
        console.log('WatchSessionAPIScreen startDate: ' + this.startDate);
        console.log('WatchSessionAPIScreen endDate: ' + this.endDate);
        console.log('WatchSessionAPIScreen body: ' + JSON.stringify(this.awsbody));
        console.log('WatchSessionAPIScreen handelAwsResponse triggered in HvHeartRateMeasurment: ' + JSON.stringify(event.detail));
        var temp2;
        var temp3;
        var temp4 = [];
        var temp5 = [];
        var dayDate;
        var durationFrom;
        var durationTo;
        var averageHR;
        var stepCount;
        var afStyle;
        var afstatus2 = false;
        var afstatus3 = false;
        var statusFlag;
        event.detail.awsresponse.data.forEach(data =>{
            const options = {
                day: 'numeric',
                year: 'numeric',
                month: 'long',
                weekday: 'short',
                hour: 'numeric',
                minute: 'numeric',
                timeZoneName: 'short',
                timeZone: TIMEZONE
            };
            temp2 = new Date((data.start_timestamp * 1000)).toLocaleString("en-US", options);
            temp3 = new Date((data.end_timestamp * 1000)).toLocaleString("en-US", options);
            temp4 = temp2.split(',');
            temp5 = temp3.split(',');
            durationFrom = temp4[3];
            durationTo = temp5[3];
            if(temp4[0] != temp5[0]){
                durationTo += ' (Next day)';
            }

            dayDate = temp4[0] + ', ' + temp4[1] + ', ' + temp4[2];
            averageHR = data.avg_hr;
            stepCount = data.step_count;
            if(data.ppg_count > 0 && data.af_count > 0){
                afStyle = 'width: ' + (data.af_count/data.ppg_count)*100 + '%';
            } else{
                afStyle = 0;
            }
            
            if(!this.isPatient){
                if(data.af_count > 0){
                    afstatus2 = false;
                    afstatus3 = true;
                    statusFlag = true;
                } else{
                    afstatus2 = true;
                    afstatus3 = false;
                }
            }
            this.watchSessions.push({ Id : data.session_id, dayDate : dayDate, 
                durationFrom : durationFrom, durationTo : durationTo, averageHR : averageHR, 
                stepCount : stepCount, afStyle : afStyle, afstatus2: afstatus2, afstatus3: afstatus3 });
        });
        this.watchSessionsFinal = [...this.watchSessions];
        //TODO: below 3 lines added for testing, to be removed later
        /*this.watchSessionsFinal[0].afStyle = 0;
        this.watchSessionsFinal[0].afstatus2 = true;
        this.watchSessionsFinal[0].afstatus3 = false;*/

        if(statusFlag){
            this.afstatus2 = false;
            this.afstatus3 = true;
        } else if(!statusFlag && !this.isPatient){
            this.afstatus2 = true;
            this.afstatus3 = false;
        }
        this.showspinner = false;
    }

    handelToggle(){
        if(this.toggleFlag){
            this.toggleFlag = false;
            this.watchSessionsFinal = [];
            this.watchSessionsFinal = [...this.watchSessions];
        } else{
            this.toggleFlag = true;
            this.watchSessionsFinal = [];
            this.watchSessions.forEach(data =>{
                if(data.afStyle != 0){
                    this.watchSessionsFinal.push(data);
                }
            });
        }
    }

    handleFrameClick(event){
        this.frameFlag = true;
        this.frameUrl = this.ppgViewerUrl + (event.currentTarget.id).split('-')[0];
        let iframe = this.template.querySelector('iframe');
        iframe.reload();
    }

    closeModal() {
        this.frameFlag = false;
    }
}