import { LightningElement, track,api,wire  } from 'lwc';
import { createRecord, updateRecord } from "lightning/uiRecordApi";
import { getRecord,getFieldValue } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
//2. Import the reference to Object and Fields
import BOX_OBJECT from "@salesforce/schema/HV_Box__c";
import BOX_STATUS_FIELD from "@salesforce/schema/HV_Box__c.HV_Status__c";
import BOX_CUSTOMER_FIELD from "@salesforce/schema/HV_Box__c.HV_Customer__c";
import BOX_NAME_FIELD from "@salesforce/schema/HV_Box__c.Name";
import deprovisionBox from  '@salesforce/apex/HVInventoryManagementController.deprovisionBox'
import getAllClinc from '@salesforce/apex/HVExistingUserValidationController.getAllClinc';
import {	getObjectInfo} from 'lightning/uiObjectInfoApi';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';

export default class HvNewInventoryDevice extends LightningElement {

    @api isAccessible;
    @api boxid ;
    @api watchRecordId;
    @api bpRecordId;
    @api qaRecordId;
    @api pulseoximeterRecordId;
    @api spirometersRecordId;
    @api wssRecordId; 
    @api glucometersRecordId;
    @api tabletRecordId;
    @api trackingRecordId;
    boxidNew;
    watchFields;
    bpFields;
    qaFields;
    pulseoximeterFields;
    spirometerFields;
    wssFields;
    glucometerFields;
    tabletFields;
    trackingFields;
    @track addWatchArray ;
    @track addBpArray 
    @track addQaArray 
    @track addPulseoximeterArray
    @track addSpirometerArray
    @track addWssArray 
    @track addGlucometerArray 
    @track addTabletArray 
    @track addTrakingArray 
    boxCustomerVal;
    boxStatusVal;
    boxName;
    boxCustomerOptions;
    boxStatusOptions;
    isDeprovision = 'no';
    boxRecordTypeId;
    activeTab = 'register';
    isBoxSaved = false 

    

    get deprovionsOptions(){
        return [{label: 'Yes', value: 'Deprovisioned'}];
    }

    @wire(getObjectInfo, {
		objectApiName: BOX_OBJECT
	})contactMetadata({data, error}){
        if(data){
            this.boxRecordTypeId = data.defaultRecordTypeId;
        }
        if(error){
            console.log('error',error);
        }
    };

    @wire(getPicklistValues,{ recordTypeId: '$boxRecordTypeId', fieldApiName: BOX_STATUS_FIELD })
    picklistData({data, error}){
        if(data){
            this.boxStatusOptions = [];
            this.boxStatusVal = data.defaultValue.value;
            console.log('data pic',data)
            data.values.forEach(pickVal => {
                this.boxStatusOptions.push({label: pickVal.label, value: pickVal.value });
            });
        }
        if(error){
            console.log(error);
        }
    }

    @wire(getRecord, { recordId: '$boxid', fields: [BOX_CUSTOMER_FIELD,BOX_NAME_FIELD,BOX_STATUS_FIELD]})
    boxRecord({data,err}){
        console.log('data box ',data)
        console.log('data box  err',err)
        if(data){
            console.log('data box ',data)
            this.boxName = data.fields.Name.value;
            this.boxStatusVal = data.fields.HV_Status__c.value;
            this.boxCustomerVal = data.fields.HV_Customer__c.value;
            this.isDeprovision = data.fields.HV_Status__c.value;
            if(data.fields.HV_Status__c.value === 'Deprovisioned'){
                this.isAlreadyDeprovisioned = true;
            }else{
                this.isAlreadyDeprovisioned = false;
            }
        }
        if(err){
            console.log('err b',err)
        }
    };

    connectedCallback(){
        getAllClinc()
        .then(result => {
            if (result) {
                this.boxCustomerOptions = [];
                result.forEach(record => {
                    this.boxCustomerOptions.push({
                        label: record.Label,
                        value: record.Label
                    });
                })
                console.log('this.boxCustomerOptions', this.boxCustomerOptions)
            }
        })
        .catch(e => {
            const evt = new ShowToastEvent({ //show success toast
                title: 'Error',
                message: e.body.message,
                variant: 'error',
            });
            this.dispatchEvent(evt);
        })
        
        if(this.watchRecordId){
            this.addWatchArray = [];
        }else{
            this.addWatchArray = [1];
        }
        
        if(this.bpRecordId){
            this.addBpArray = [];
        }else{
            this.addBpArray = [1];
        }

        if(this.qaRecordId){
            this.addQaArray = [];
        }else{
            this.addQaArray = [1];
        }

        if(this.pulseoximeterRecordId){
            this.addPulseoximeterArray = [];
        }else{
            this.addPulseoximeterArray = [1];
        }

        if(this.spirometersRecordId){
            this.addSpirometerArray = [];
        }else{
            this.addSpirometerArray = [1];
        }

        if(this.wssRecordId){
            this.addWssArray = [];
        }else{
            this.addWssArray = [1];
        }

        if(this.glucometersRecordId){
            this.addGlucometerArray = [];
        }else{
            this.addGlucometerArray = [1];
        }

        if(this.tabletRecordId){
            this.addTabletArray = [];
        }else{
            this.addTabletArray = [1];
        }

        if(this.trackingRecordId){
            this.addTrakingArray = [];
        }else{
            this.addTrakingArray = [1];
        }               
                        

        const watchAllfields = 'HV_Watch_Script_Output__c,HV_Watch_Model__c,HV_Watch_DUID__c,HV_Watch_Serial_Number__c,HV_Watch_MAC_ID__c,HV_Watch_ID__c,HV_Watch_MEID__c,HV_User_Device_Status__c,HV_Status__c';
        const watchRequiredFields = 'HV_Watch_Script_Output__c,HV_User_Device_Status__c,HV_Status__c';
        const watchDisabledFields = 'HV_Watch_Serial_Number__c,HV_Watch_Model__c,HV_Watch_ID__c,HV_Watch_MAC_ID__c,HV_Watch_DUID__c,HV_Watch_MEID__c';
        const watchReadOnlyFields = ''; 
        const watchMethodNameJson = {field:'HV_Watch_Script_Output__c',methodName:'watchScript'}
        this.watchFields = this.handleFieldJson(watchAllfields, watchRequiredFields, watchDisabledFields, watchReadOnlyFields, watchMethodNameJson);

        const bpAllfields = 'HV_BDA__c,HV_Model__c,HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c';
        const bpRequiredFields = 'HV_BDA__c,HV_Model__c,HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c';
        const bpDisabledFields = '';
        const bpReadOnlyFields = '';
        const bpMethodNameJson = ''
        this.bpFields = this.handleFieldJson(bpAllfields, bpRequiredFields, bpDisabledFields, bpReadOnlyFields, bpMethodNameJson);
    
        const qaAllfields = 'HV_QA_Date__c,QA_by__c,HV_Batch_Number__c,HV_Manufactured_Date__c,HV_Manufactured_By__c,HV_Status__c';
        const qaRequiredFields = 'HV_QA_Date__c,QA_by__c,HV_Batch_Number__c,HV_Manufactured_Date__c,HV_Manufactured_By__c,HV_Status__c';
        const qaDisabledFields = '';
        const qaReadOnlyFields = '';
        const qaMethodNameJson ='';
        this.qaFields = this.handleFieldJson(qaAllfields, qaRequiredFields, qaDisabledFields, qaReadOnlyFields, qaMethodNameJson);
  
        const pulseoximeterAllfields = 'HV_BDA__c,HV_Model__c,HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c';
        const pulseoximeterRequiredFields = 'HV_BDA__c,HV_Model__c,HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c';
        const pulseoximeterDisabledFields = '';
        const pulseoximeterReadOnlyFields = '';
        const pulseoximeterMethodNameJson = '';
        this.pulseoximeterFields = this.handleFieldJson(pulseoximeterAllfields, pulseoximeterRequiredFields, pulseoximeterDisabledFields, pulseoximeterReadOnlyFields, pulseoximeterMethodNameJson);
  
        const spirometerAllfields = 'HV_BDA__c,HV_Model__c,HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c';
        const spirometerRequiredFields = 'HV_BDA__c,HV_Manufacturer__c,HV_Model__c,HV_User_Device_Status__c,HV_Status__c';
        const spirometerDisabledFields = '';
        const spirometerReadOnlyFields = ''; 
        const spirometerMethodNameJson = '';
        this.spirometerFields = this.handleFieldJson(spirometerAllfields, spirometerRequiredFields, spirometerDisabledFields, spirometerReadOnlyFields, spirometerMethodNameJson);
  
        const glucometerAllfields = 'HV_BDA__c,HV_Model__c,HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c';
        const glucometerRequiredFields = 'HV_BDA__c,HV_Model__c,HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c';
        const glucometerDisabledFields = '';
        const glucometerReadOnlyFields = '';
        const glucometerMethodNameJson = '';
        this.glucometerFields = this.handleFieldJson(glucometerAllfields, glucometerRequiredFields, glucometerDisabledFields, glucometerReadOnlyFields, glucometerMethodNameJson);
  
        const wssAllfields = 'HV_BDA__c,HV_Model__c,HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c';
        const wssRequiredFields = 'HV_BDA__c,HV_Model__c,HV_Manufacturer__c,HV_User_Device_Status__c,HV_Status__c';
        const wssDisabledFields = '';
        const wssReadOnlyFields = '';
        const wssMethodNameJson = '';
        this.wssFields = this.handleFieldJson(wssAllfields, wssRequiredFields, wssDisabledFields, wssReadOnlyFields, wssMethodNameJson);
  
        const tabletAllfields = 'HV_Tablet_Script_Output__c,HV_Model__c,HV_IMEI__c,HV_Serial_Number__c,HV_Android_ID__c,HV_SSID__c,HV_Password__c,HV_User_Device_Status__c,HV_Status__c';
        const tabletRequiredFields = 'HV_Tablet_Script_Output__c,HV_User_Device_Status__c,HV_SSID__c,HV_Password__cHV_Status__c';
        const tabletDisabledFields = 'HV_Model__c,HV_IMEI__c,HV_Serial_Number__c,HV_Android_ID__c';
        const tabletReadOnlyFields = '';
        const tabletMethodNameJson =  {field:'HV_Tablet_Script_Output__c',methodName:'tabletScript'};
        this.tabletFields = this.handleFieldJson(tabletAllfields, tabletRequiredFields, tabletDisabledFields, tabletReadOnlyFields, tabletMethodNameJson);

        const trackingAllfields = 'HV_Shipped_Date__c,HV_Shipping_Address__c,HV_Tracking_number__c,HV_Description__c';
        const trackingRequiredFields = 'HV_Shipped_Date__c,HV_Shipping_Address__c,HV_Tracking_number__c,HV_Description__c';
        const trackingDisabledFields = 'HV_Model__c,HV_IMEI__c,HV_Serial_Number__c,HV_Android_ID__c';
        const trackingReadOnlyFields = ''; 
        const trackingMethodNameJson =  '';
        this.trackingFields = this.handleFieldJson(trackingAllfields, trackingRequiredFields, trackingDisabledFields, trackingReadOnlyFields, trackingMethodNameJson);

        
    }

    changeDeprovisionStatus(event){
        console.log('chnage event',event.target.value);
        this.isDeprovision = event.target.value;
        let recordsInput = [];
        if(event.target.value === 'Deprovisioned'){
            console.log('boxid',this.boxid);
            if(this.boxid){
                deprovisionBox({deprovisionVal:event.target.value,boxId:this.boxid})
                .then(res => {
                    console.log(res);
                    this.boxStatusVal = 'Deprovisioned';
                   
                    const evt = new ShowToastEvent({ //show success toast
                        title: 'Success',
                        message: 'Box deprovision successful',
                        variant: 'success',
                    });
                    this.dispatchEvent(evt);
                    this.isAlreadyDeprovisioned =true;
                })
                .catch(e => {
                    console.log(e);
                    const evt = new ShowToastEvent({ //show success toast
                        title: 'Error',
                        message: e.body.message,
                        variant: 'error',
                    });
                    this.dispatchEvent(evt);
                })
            }
            
        }
    }

    boxCustomerChange(event){
        this.boxCustomerVal = event.target.value;
    }

    boxStatusChange(event){
        this.boxStatusVal = event.target.value;
    }


    handleCreateBoxid(){

        console.log(' box exist else ')
        if(this.boxid){
            
            const fields = {};
            fields.Id = this.boxid;
            fields.HV_Status__c = this.boxStatusVal;
            fields.HV_Customer__c = this.boxCustomerVal;
            console.log('log');
       // fields[NAME_FIELD.fieldApiName] = '';   
            //4. Prepare config object with object and field API names 
            const recordInput = {
            fields: fields
            };  
                
                //5. Invoke createRecord by passing the config object
            updateRecord(recordInput)
            .then((record) => {
                console.log('record',record);
                console.log('record',record.id);
                this.boxid = record.id;
                this.isBoxSaved = true;
                
            })
            .then( () => {
                this.activeTab = 'watch';
            })
            .catch(err => {  
                console.log('err',err);
            });
        }else{
            const fields = {};
            fields.HV_Status__c = this.boxStatusVal;
            fields.HV_Customer__c = this.boxCustomerVal;
            console.log('log');
        // fields[NAME_FIELD.fieldApiName] = '';   
                //4. Prepare config object with object and field API names 
            const recordInput = {
            apiName: BOX_OBJECT.objectApiName,
            fields: fields
            };
                
                //5. Invoke createRecord by passing the config object
            createRecord(recordInput)
            .then((record) => {
                console.log('record',record);
                console.log('record',record.id);
                this.boxid = record.id;
                this.isBoxSaved = true;
                
            })
            .then( () => {
                this.activeTab = 'watch';
            })
            .catch(err => {  
                console.log('err',err);
            });
        }
        
            
    }

    handleFieldJson(allFields,requiredFields,disabledFields,readonlyFields,methodNameJson){
     
        let fieldsJsonArray = [];
        allFields.split(',').forEach(field => {
            let isRequired = false;
            let isDisabled = false;
            let isReadOnly = false; 
            let methodName = '';

            if(requiredFields){
                if(requiredFields.includes(field)){
                    isRequired = true;
                    console.log('is req',isRequired);
                }
            }
            
            if(disabledFields){
                if(disabledFields.includes(field)){
                    isDisabled = true;
                }
            }
            
            if(readonlyFields){
                if(readonlyFields.includes(field)){
                    isReadOnly = true;
                }
            }
            
            if(methodNameJson){
                if(methodNameJson.field.includes(field)){
                    methodName = methodNameJson.methodName;
                }
            }
            
            fieldsJsonArray.push({fieldApiName :field, isRequired: isRequired, isDisabled: isDisabled, isReadOnly: isReadOnly , name:field, onchange:methodName});
        })
        return fieldsJsonArray;
    }

    addWatch(event){
        console.log('event',event);
        console.log('this.addWatchArray',this.addWatchArray);
        this.addWatchArray.unshift(1);
    }
    addBp(event){
        this.addBpArray.push(1);
    }

    addQa(event){
        this.addQaArray.push(1);
    }

    addPulseoximeter(event){
        this.addPulseoximeterArray.push(1);
    }

    addSpirometer(event){
        this.addSpirometerArray.push(1);
    }

    addWss(event){
        this.addWssArray.push(1);
    }

    addGlucometer(event){
        this.addGlucometerArray.push(1);
    }

    addTablet(event){
        this.addTabletArray.push(1);
    }

    addTracking(event){
        this.addTrakingArray.push(1);
    }
    spinnerLoading(event){
        const evt = new CustomEvent('spinner',{
            detail:{
                isloading :event.detail.isloading
            }
        })
        this.dispatchEvent(evt);
    }
}