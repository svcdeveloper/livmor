import { LightningElement, api, track } from 'lwc';

const columns = [
    { label: 'Report Name', fieldName: 'name' },
    { label: 'Report Date', fieldName: 'date' }
];

export default class HvViewReportHistory extends LightningElement {
    @api patientid;
    columns = columns;
    @track tabledata = [];
    totalNumberOfRows = 5;
    rowOffSet = 0;
    numberofrecordtoshow = 5;
    @track displaydata = [];
    isLoading = false;
    apiendpoint = 'https://salesforceapi.livmor.com/reportdetails';
    apirequestbody = {};/*{ "PatientID": "lesliechow" };*/
    apimethod = 'POST';
    devicetype = 'reportsessions';
    error;
    datafromaws;
    
    handleresponse(event){
        var actualData = event.detail['awsresponse']['data']['Reports'];
        this.datafromaws = actualData;
        for (let x in actualData) {
            let eachRec = {};
            eachRec['id'] = x;
            eachRec['name'] = x;
            eachRec['date'] = actualData[x];
            this.displaydata.push(eachRec);
        }
        this.tabledata = this.displaydata.slice(0, this.totalNumberOfRows);
        console.log('tabledata :::: ',this.tabledata);
        this.error = undefined;
        this.isLoading = false;
    }

    connectedCallback() {
        this.apirequestbody['PatientID'] = this.patientid;
	}

    loadMoreData(event) {
        console.log('in loadmore');
        this.isLoading = true;
        if (event.target.isLoading) {
            return;
        }
        let isload = event.target.isLoading;
        isload = true;
        this.rowOffSet = this.rowOffSet + this.numberofrecordtoshow;
        this.totalNumberOfRows = this.totalNumberOfRows + this.numberofrecordtoshow;

        if (this.displaydata.length > 0 && this.totalNumberOfRows < (this.displaydata.length + this.numberofrecordtoshow)) {
            this.tabledata = this.displaydata.slice(0, this.totalNumberOfRows);
            isload = false;
        }
        else if (this.displaydata.length > 0 && this.totalNumberOfRows > this.displaydata.length) {
            isload = false;
        }
        else {
            this.tabledata = [];
            isload = false;
        }
        this.isLoading = false;
    }
}