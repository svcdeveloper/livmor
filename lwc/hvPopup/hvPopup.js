import { LightningElement, api } from 'lwc';
import getSobjectRecord from '@salesforce/apex/HVQueryUtility.getSingleRecordImprative'

export default class HvPopup extends LightningElement {

   
    currentTab = 1;
    prevDisable = true;
    nextDisable = false;
    popDisabled = true;
    footerCondition = false;
    nextLabel = "Next"
    CancelLabel = "Reject"
    stepHeading = "Consent Form"
    suggestedPatientIds = [];
    suggestedPatientIdsOptions = [];
    dummyData = [];
    @api isConsentOpen/* = true*/;
    activeSections =['A'];
    activeSectionMessage = '';
    

    
    connectedCallback(){
        var maxBirthdayDate = new Date();
        maxBirthdayDate.setFullYear( maxBirthdayDate.getFullYear() - 18);       
        this.maxdate = this.formatDate(maxBirthdayDate);
    } 
    
    toggleDropdown(event){ 
        if(event.currentTarget.offsetParent.classList.contains('slds-is-open')){
            event.currentTarget.offsetParent.classList.remove('slds-is-open');
            event.currentTarget.iconName = "utility:chevronright";
        }else{
            event.currentTarget.offsetParent.classList.add('slds-is-open');
            event.currentTarget.iconName = "utility:chevrondown";
        }        
    }
    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
        return [year, month, day].join('-');
    }

    // Nex and previous and progress bar code start
    handleToggleSection(event) {
        this.activeSectionMessage =
            'Open section name:  ' + event.detail.openSections;
    }
    get stepCondition1() {
        return this.currentTab == 1 ? true : false;
    }
    get stepCondition2() {
        return this.currentTab == 2 ? true : false;
    }
    get stepCondition3() {
        return this.currentTab == 3 ? true : false;
    }
    handleProgress(step){
        switch (step) {
            case 1:
                this.currentStep ="1";
                this.prevDisable = true;
                this.nextLabel = "next";
                this.stepHeading = "Consent Form";
                popDisabled = true;
                break;
            case 2:
                this.currentStep = "2";
                this.prevDisable = false;
                this.nextDisable = false;
                this.nextLabel = "next";
                this.stepHeading = "";
                popDisabled = true;
                break;
            case 3:
                this.currentStep = "3";
                this.nextDisable = true;
                this.nextLabel = "next";
                this.stepHeading = "Security Questions";
                popDisabled = false;
                break;
            default:
                this.currentStep = "1";
                this.prevDisable = true;
                this.nextLabel = "next";
                this.stepHeading = "Consent Form";
                popDisabled = true;
                break;
        }
    }
    previous(){
        if(this.currentTab > 1){
            this.currentTab--;
            this.handleProgress(this.currentTab);
        }
    }
    next(){
        // if(this.validateFields()){
            if(this.currentTab < 3){
                this.currentTab ++;
                this.handleProgress(this.currentTab);
            }
        // }
    }
    /*rejectForm(){
        this.isConsentOpen = false;
    }*/
    acceptForm(event){
        this.currentTab = 2;
        // this.currentTab = event.target.value;
        this.handleProgress(this.currentTab);
    }

    haldleChild(event){
        if(event.detail.changePassword){
            this.currentTab = 3;
                // this.currentTab = event.target.value;
                this.handleProgress(this.currentTab);
        }else if(event.detail.setSecurity){
            this.isConsentOpen = false;
            const evnt = new CustomEvent('consentgiven', {
                detail: { refreshview: true }
            });
            this.dispatchEvent(evnt);
        }
        // switch(event.detail.step){
        //     case 1: this.step1 = true; this.step2 = false; this.step3 = false; this.step4 = false; 
        //         break;
        //     case 2: this.step1 = false; this.step2 = true; this.step3 = false; this.step4 = false;  
        //         break;  
        //     case 3: this.step1 = false; this.step2 = false; this.step3 = true; this.step4 = false;  
        //         break;  
        //     default: this.step1 = true; this.step2 = false; this.step3 = false; this.step4 = false; 
        //         break;
        // }
    }
    /*closePopUP(){
        this.isConsentOpen = false;
    }*/

    // changeTab(event){
    //     event.preventDefault();
    //     this.currentTab = event.target.value;
    //     this.currentStep = '"'+this.currentTab+'"';
    //     if(this.currentTab == 1){
    //         this.prevDisable = true;
    //         this.nextDisable = false;
    //         this.nextLabel = "Create Account";
    //         this.stepHeading = "Consent Form";
    //     }else if(this.currentTab == 2){
    //         this.prevDisable = false;
    //         this.nextDisable = false;
    //         this.nextLabel = "Verify";
    //         this.stepHeading = "";
    //     }else if(this.currentTab == 3){
    //         this.nextDisable = true;
    //         this.prevDisable = false;
    //         this.stepHeading = "Security Questions";
    //     }else{

    //     }
    // } 
    // Next and previous and progress bar code end

    
 
    getEmailRecord(emailtemplatename){
        
        return getSobjectRecord({
             objectname: 'EmailTemplate',
             fieldnames: 'Id, DeveloperName , HtmlValue',
             conditions: 'where developername = \''+emailtemplatename+'\''
         }) 
         .then( result=> {
             return result.HtmlValue;            
         })
         .catch(error => {
             console.log('error',error);
         })
     }

     renderedCallback(){
        if(this.isConsentOpen){ 
            this.getEmailRecord('HV_Terms_and_Conditions')
            .then( result => {
               
                let deom = this.template.querySelector('.d3');
             
                let li = document.createElement('div');
                li.innerHTML =result; 
                deom.appendChild(li);
            });            
        }
        
    }
}