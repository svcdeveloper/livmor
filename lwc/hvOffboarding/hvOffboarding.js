import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import deactivateUser from '@salesforce/apex/HVPhysicianDashboard.deactivateUser';
import isOnboradingOrAdminUser from '@salesforce/apex/HVPhysicianDashboard.isOnboradingOrAdminUser';

export default class HvOffboarding extends LightningElement {
    @api userid;
    @api selectedRows;
    @track showButton;
    @track error;

    connectedCallback() {
        isOnboradingOrAdminUser()
        .then(result => {
            this.showButton = result;
        })
        .catch(error => {
            this.error = error;
        });
    }

    offboardUser(){
        if (this.selectedRows.length > 0) {
            deactivateUser({contactId : this.userid})
            .then(result => {
                if(result.includes('Updated')){                
                    this.showNotification('', result.split(':')[1]+' is offboarded.', 'success');
                }else if(result.includes('Inactive')){
                    this.showNotification('', result.split(':')[1]+' is already inactive.', 'info');
                }else if(result == 'Failed to update'){
                    this.showNotification('', 'Failed to update.', 'error');
                }
            })
            .catch(error => {
                this.error = error;
            });
        }else {
            this.showNotification('Error', 'Please select one row in the Table.', 'error');
        }
    }

    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }
}