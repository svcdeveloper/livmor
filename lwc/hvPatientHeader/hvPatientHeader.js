import { LightningElement, track, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import {getRecord} from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id';
import NAME_FIELD from '@salesforce/schema/User.Name';
import CREATED_DATE_FIELD from '@salesforce/schema/User.CreatedDate';
import getLoginCount from '@salesforce/apex/HVPatientDashboardController.getLoginCount';

export default class HvPatientHeader extends NavigationMixin(LightningElement) {
    @track error;
    name;
    joiningDate;
    loginCount;
    error;

    connectedCallback(){
        if(!this.loginCount){
            getLoginCount()  
                .then(data => {
                    if (data) {
                        this.loginCount = data;
                    }
                })
                .catch(error => {
                    this.error = error;  
                })
            }
    }

    @wire(getRecord, {
        recordId: USER_ID,
        fields: [NAME_FIELD,CREATED_DATE_FIELD]
    }) wireuser({
        error,
        data
    }) {
        if (error) {
           this.error = error ; 
        } else if (data) {
            this.name = data.fields.Name.value;
            var joindate = (data.fields.CreatedDate.value).split('T')[0];
            var options = {
                  month: "long", //to display the full name of the month
                  year: "numeric"
            }
                  this.joiningDate = (new Date(joindate)).toLocaleDateString("en-US", options);

        }
    }

    dologout(){
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__webPage',
            attributes: {
                url: ''
            }
        }).then(url => {
            window.open("/HeartViewStandard/secur/logout.jsp", "_self");
        });
    }

    showHeartView(){
        this.dispatchEvent(new CustomEvent('showhv'));
    }

    handleMessages(){
        console.log('hello ::::::');
        this.dispatchEvent(new CustomEvent('messages'));
    }

    handleProfile(){
        this.dispatchEvent(new CustomEvent('editprofile'));
    }
}