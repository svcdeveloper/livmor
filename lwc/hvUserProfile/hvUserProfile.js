import { LightningElement, wire, api, track } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import currentUserId from '@salesforce/user/Id';
import CURRENT_USER_USERNAME from '@salesforce/schema/User.Username';
import CURRENT_USER_ROLE from '@salesforce/schema/User.Contact.HV_Role__c';

const USERFIELDS = [CURRENT_USER_ROLE, CURRENT_USER_USERNAME]; //fields to query form user object

export default class HvUserProfile extends LightningElement {

    @api currentuserrecord; //show user profile of specific user record passed from other cmp
    @track loggedinuserroleName; //logged in user role name
    @api patientid;
    @api frompatientdashboard = false;
    roleName; //logged in user name
    isPatient; //is logged in user patient
    currentUserName; //store logged in user's username
    errormessage; // store error message if any
    isAdminUser = false; //is logged in user admin user like - livmor admin    
    showChangePassword = false; //show change password section or not
    showChangePasswordTab = false; //show change password tab or not
    showsecurityquestionsTab = false; //show security questions tab or not
    showUserProfileTab = true; //show profile tab or not
    showNotesTab = false; //show notes tab or not
    showOffboardButton = false;
    showSetGoal = false; // Show set goals for Physician
    showNotifcation = false; // Show patient notifications for Physician

    renderedCallback() { //code for responsive UI
        var width = window.innerWidth;
        if (width <= 685) {
            console.log();
            this.template.querySelector('lightning-tabset').variant = "";
        }
    }

    @wire(getRecord, { recordId: currentUserId, fields: USERFIELDS })
    loggedInUserData({ error, data }) { //lds method to get current logged in user details using schema
        if (data) {
            console.log('currentuserrecord :::::: ',JSON.stringify(this.currentuserrecord));
            this.loggedinuserroleName = data.fields.Contact.value.fields.HV_Role__c.value;
            if (this.currentuserrecord) { //if user record is passed from other cmp store respective values in attributes 
                this.loggedinuserroleName = data.fields.Contact.value.fields.HV_Role__c.value;
                //show patient's notes only to physician/clinical user/call center admin/call center user/livmor admin
                if(this.loggedinuserroleName == 'Physician' || this.loggedinuserroleName == 'Clinical User' || this.loggedinuserroleName == 'Call Center Admin' || this.loggedinuserroleName == 'Call Center User' || this.loggedinuserroleName == 'Livmor Admin'){
                    this.showNotesTab = true;
                }
                if(this.loggedinuserroleName == 'Physician'){
                    this.showSetGoal = true;
                    this.showNotifcation = true;
                }
                //show patient's change password tab to clinical user, call center admin, call center user, livmor admin
                if(this.loggedinuserroleName == 'Clinical User' || this.loggedinuserroleName == 'Call Center Admin' || this.loggedinuserroleName == 'Call Center User' || this.loggedinuserroleName == 'Livmor Admin'){
                    this.showChangePasswordTab = true;
                }
                //show patient's security questions tab to call center admin, call center user
                if(this.loggedinuserroleName == 'Call Center Admin' || this.loggedinuserroleName == 'Call Center User'){
                    this.showsecurityquestionsTab = true;
                }
                if(this.loggedinuserroleName == 'Clinical User'){
                    this.showOffboardButton = true;
                }
                let returndata = JSON.parse(JSON.stringify(this.currentuserrecord));
                this.roleName = returndata.Contact.HV_Role__c;
                this.currentUserName = returndata.Username;
                if(this.roleName != 'Patient'){
                    this.showsecurityquestionsTab = false;
                    this.showNotesTab = false;
                }
            }
            else { //if not - store logged in user data
                this.loggedinuserroleName = '';
                this.roleName = data.fields.Contact.value.fields.HV_Role__c.value;
                this.currentUserName = data.fields.Username.value;
                this.showChangePasswordTab = true;
            }
            if (this.currentUserName) {
                this.showChangePassword = true;
            }
            else {
                this.showChangePassword = false;
            }
            if (!this.loggedinuserroleName && this.roleName === 'Patient') { //show Notes and Security tabs only for Patient
                this.isPatient = true;
                this.showNotesTab = true;
                this.showsecurityquestionsTab = true;
            }
            if (!this.loggedinuserroleName && (this.roleName === 'Livmor Admin' || this.roleName === 'Call Center Admin' || this.roleName === 'Call Center User' || this.roleName === 'Asset Admin' || this.roleName === 'Asset User')) { //do not show change password tab for livmor admin/call center user
                this.isAdminUser = true;
                this.showChangePasswordTab = false;
            }
        }
        if (error) {
            this.errormessage = error.body.message; //set error if any
        }
    }

    backToProfileLandingPage(event) { //cancel button event handler
        this.showChangePassword = false;
        this.template.querySelector('lightning-tabset').activeTabValue = 'landingpage';
    }

    handleClick(event) { //show hide tabs based on tab click
        this.showChangePassword = true;
    }

    goBackToPatientDashboard() {
        const event = new CustomEvent('backtopd');
        this.dispatchEvent(event);
    }
}