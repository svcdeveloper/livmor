import { LightningElement } from 'lwc';

export default class HvCancelButtonToLogin extends LightningElement {
    gotoLogin(){
        location.reload();
    }
}