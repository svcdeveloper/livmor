<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Clinic One</label>
    <protected>false</protected>
    <values>
        <field>HV_Clinic_ID__c</field>
        <value xsi:type="xsd:string">0012300000XfOt6AAF</value>
    </values>
    <values>
        <field>HV_Number_of_License__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
</CustomMetadata>
