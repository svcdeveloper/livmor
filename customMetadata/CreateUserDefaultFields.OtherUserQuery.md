<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OtherUserQuery</label>
    <protected>false</protected>
    <values>
        <field>HV_Admin_User_ID__c</field>
        <value xsi:type="xsd:string">00523000001pywoAAA</value>
    </values>
    <values>
        <field>HV_EmailEncodingKey__c</field>
        <value xsi:type="xsd:string">UTF-8</value>
    </values>
    <values>
        <field>HV_FieldsToQueryFromObject__c</field>
        <value xsi:type="xsd:string">id,FirstName,LastName,Phone,Email,isActive,username,Contact.AccountId,Contact.Account.Name,Contact.FirstName,Contact.LastName,Contact.Email,Contact.Phone,Contact.HV_Password__c,Contact.HV_Department__c,Contact.HV_License_Number__c,Contact.HV_Qualification__c,Contact.HV_Role__c,Contact.HV_Specialization__c</value>
    </values>
    <values>
        <field>HV_LanguageLocaleKey__c</field>
        <value xsi:type="xsd:string">en_US</value>
    </values>
    <values>
        <field>HV_LocaleSidKey__c</field>
        <value xsi:type="xsd:string">en_US</value>
    </values>
    <values>
        <field>HV_ProfileId__c</field>
        <value xsi:type="xsd:string">00e23000000HqKz</value>
    </values>
    <values>
        <field>HV_Profile_Name__c</field>
        <value xsi:type="xsd:string">HeartView Portal Other Users</value>
    </values>
    <values>
        <field>HV_QueryObjectName__c</field>
        <value xsi:type="xsd:string">user</value>
    </values>
    <values>
        <field>HV_TimeZoneSidKey__c</field>
        <value xsi:type="xsd:string">GMT</value>
    </values>
    <values>
        <field>HV_User_License_Name__c</field>
        <value xsi:type="xsd:string">Customer Community Plus</value>
    </values>
</CustomMetadata>
