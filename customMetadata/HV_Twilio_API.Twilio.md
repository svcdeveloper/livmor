<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Twilio</label>
    <protected>false</protected>
    <values>
        <field>HV_Account_SID__c</field>
        <value xsi:type="xsd:string">AC54ed9ad1f92cc9bf16b11ea1157d5b06</value>
    </values>
    <values>
        <field>HV_FromPhoneNumber__c</field>
        <value xsi:type="xsd:string">+18334771658</value>
    </values>
    <values>
        <field>HV_SendOTPEndpoint__c</field>
        <value xsi:type="xsd:string">https://verify.twilio.com/v2/Services/VA1caff4072be7b091bcbfb21cf9a24903/Verifications</value>
    </values>
    <values>
        <field>HV_ServiceSid__c</field>
        <value xsi:type="xsd:string">VA1caff4072be7b091bcbfb21cf9a24903</value>
    </values>
    <values>
        <field>HV_ToNumberCountryCode__c</field>
        <value xsi:type="xsd:string">+91</value>
    </values>
    <values>
        <field>HV_Token__c</field>
        <value xsi:type="xsd:string">132eb02d5b1a2cb2166de0a1cb80d6c8</value>
    </values>
    <values>
        <field>HV_VerifyOTPEndpoint__c</field>
        <value xsi:type="xsd:string">https://verify.twilio.com/v2/Services/VA1caff4072be7b091bcbfb21cf9a24903/VerificationCheck</value>
    </values>
</CustomMetadata>
