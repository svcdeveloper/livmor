<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AdminNotifications</label>
    <protected>false</protected>
    <values>
        <field>HV_Email_Address__c</field>
        <value xsi:type="xsd:string">kirti.dayma@mindtree.com,rachana.desai@mindtree.com,pratima.shabnavees@mindtree.com,rajkumar.rengaswamy@livmor.com</value>
    </values>
</CustomMetadata>
