<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Onboarding Admin</label>
    <protected>false</protected>
    <values>
        <field>HV_Avaliable_Fields__c</field>
        <value xsi:type="xsd:string">FirstName,LastName,Email,HV_Password__c,HV_Qualification__c,AccountId,Phone,HV_Department__c</value>
    </values>
    <values>
        <field>HV_Department_Values__c</field>
        <value xsi:type="xsd:string">Heart Failure,Electrophysiology,General Medicine,Primary Care,Physical Therapy,Rehabilitative Medicine,Other</value>
    </values>
    <values>
        <field>HV_Required_Fields__c</field>
        <value xsi:type="xsd:string">FirstName,LastName,Phone,Email,HV_Department__c</value>
    </values>
</CustomMetadata>
