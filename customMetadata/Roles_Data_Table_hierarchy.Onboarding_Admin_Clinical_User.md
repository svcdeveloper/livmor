<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Onboarding Admin</label>
    <protected>false</protected>
    <values>
        <field>HV_Column_Api_Name__c</field>
        <value xsi:type="xsd:string">HV_Clinical_User_Data_Columns__c</value>
    </values>
    <values>
        <field>HV_Create_User_Access__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>HV_Role__c</field>
        <value xsi:type="xsd:string">Clinical User</value>
    </values>
    <values>
        <field>HV_Tab_Access__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
