<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PatientQuery</label>
    <protected>false</protected>
    <values>
        <field>HV_Admin_User_ID__c</field>
        <value xsi:type="xsd:string">00523000001pywoAAA</value>
    </values>
    <values>
        <field>HV_EmailEncodingKey__c</field>
        <value xsi:type="xsd:string">UTF-8</value>
    </values>
    <values>
        <field>HV_FieldsToQueryFromObject__c</field>
        <value xsi:type="xsd:string">id,FirstName,LastName,Phone,Email,isActive,username,Contact.AccountId,Contact.Account.HV_Patient_Clinic__c,Contact.Account.PersonBirthdate,Contact.Account.HV_Patient_ID__pc,Contact.Account.FirstName,Contact.Account.LastName,contact.account.PersonEmail,contact.account.phone,contact.account.PersonHomePhone,contact.account.HV_Password__pc,contact.account.HealthCloudGA__Gender__pc,contact.account.HV_Height__pc,contact.account.HV_Prescribed_By__pc,contact.account.Preferred_Contact_Method__pc,contact.account.HV_isTwoFactorAuthentactionEnabled__pc,contact.account.HV_Department__pc,contact.account.ShippingStreet,contact.account.ShippingCity,contact.account.ShippingCountry,contact.account.ShippingState,contact.account.ShippingPostalCode,contact.account.HV_Box_ID__pc,Contact.Account.HV_Patient_Clinic__r.Name</value>
    </values>
    <values>
        <field>HV_LanguageLocaleKey__c</field>
        <value xsi:type="xsd:string">en_US</value>
    </values>
    <values>
        <field>HV_LocaleSidKey__c</field>
        <value xsi:type="xsd:string">en_US</value>
    </values>
    <values>
        <field>HV_ProfileId__c</field>
        <value xsi:type="xsd:string">00e23000000HqL0</value>
    </values>
    <values>
        <field>HV_Profile_Name__c</field>
        <value xsi:type="xsd:string">HeartView Portal User</value>
    </values>
    <values>
        <field>HV_QueryObjectName__c</field>
        <value xsi:type="xsd:string">user</value>
    </values>
    <values>
        <field>HV_TimeZoneSidKey__c</field>
        <value xsi:type="xsd:string">GMT</value>
    </values>
    <values>
        <field>HV_User_License_Name__c</field>
        <value xsi:type="xsd:string">Customer Community Plus Login</value>
    </values>
</CustomMetadata>
