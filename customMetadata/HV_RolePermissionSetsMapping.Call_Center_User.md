<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Call Center User</label>
    <protected>false</protected>
    <values>
        <field>HV_PermissionSetIds__c</field>
        <value xsi:type="xsd:string">0PS23000000D8LI,0PS23000000D8MD</value>
    </values>
    <values>
        <field>HV_PermissionSetNames__c</field>
        <value xsi:type="xsd:string">HVCallCenterUser,HV_Broadcast_Access</value>
    </values>
</CustomMetadata>
