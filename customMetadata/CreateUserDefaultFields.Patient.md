<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Patient</label>
    <protected>false</protected>
    <values>
        <field>HV_Admin_User_ID__c</field>
        <value xsi:type="xsd:string">00523000001pywoAAA</value>
    </values>
    <values>
        <field>HV_EmailEncodingKey__c</field>
        <value xsi:type="xsd:string">UTF-8</value>
    </values>
    <values>
        <field>HV_FieldsToQueryFromObject__c</field>
        <value xsi:type="xsd:string">Id,FirstName,LastName,PersonEmail,Phone,PersonContactId,HV_Password__pc,HV_Role__pc,OwnerId</value>
    </values>
    <values>
        <field>HV_LanguageLocaleKey__c</field>
        <value xsi:type="xsd:string">en_US</value>
    </values>
    <values>
        <field>HV_LocaleSidKey__c</field>
        <value xsi:type="xsd:string">en_US</value>
    </values>
    <values>
        <field>HV_ProfileId__c</field>
        <value xsi:type="xsd:string">00e23000000HqL0</value>
    </values>
    <values>
        <field>HV_Profile_Name__c</field>
        <value xsi:type="xsd:string">HeartView Portal User</value>
    </values>
    <values>
        <field>HV_QueryObjectName__c</field>
        <value xsi:type="xsd:string">Account</value>
    </values>
    <values>
        <field>HV_TimeZoneSidKey__c</field>
        <value xsi:type="xsd:string">GMT</value>
    </values>
    <values>
        <field>HV_User_License_Name__c</field>
        <value xsi:type="xsd:string">Customer Community Plus Login</value>
    </values>
</CustomMetadata>
