<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Department</label>
    <protected>false</protected>
    <values>
        <field>HV_Checked__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>HV_Disabled__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>HV_Editable__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>HV_Field_Name__c</field>
        <value xsi:type="xsd:string">HV_Department__c</value>
    </values>
    <values>
        <field>HV_Label__c</field>
        <value xsi:type="xsd:string">Department</value>
    </values>
    <values>
        <field>HV_Parent_Role__c</field>
        <value xsi:type="xsd:string">Livmor Admin,Onboarding Admin,Clinical User,Physician,Call Center Admin,Call Center User</value>
    </values>
    <values>
        <field>HV_Roles__c</field>
        <value xsi:type="xsd:string">Physician,Clinical User,Call Center Admin,Call Center User,Livmor Admin,Onboarding Admin,Patient</value>
    </values>
    <values>
        <field>HV_Sortable__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>HV_Type_Attributes__c</field>
        <value xsi:type="xsd:string">{&quot;rowId&quot;: {&quot;fieldName&quot;: &quot;Id&quot;}}</value>
    </values>
    <values>
        <field>HV_Type__c</field>
        <value xsi:type="xsd:string">pickListColumn</value>
    </values>
</CustomMetadata>
