<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Clinic One</label>
    <protected>false</protected>
    <values>
        <field>HV_AccountInformationSectionFields__c</field>
        <value xsi:type="xsd:string">HV_Patient_ID__pc,HV_Password__pc</value>
    </values>
    <values>
        <field>HV_Clinic_Id__c</field>
        <value xsi:type="xsd:string">0012300000XfOt6AAF</value>
    </values>
    <values>
        <field>HV_ContactInformationSectionFields__c</field>
        <value xsi:type="xsd:string">Preferred_Contact_Method__pc,PersonEmail,Phone,PersonHomePhone</value>
    </values>
    <values>
        <field>HV_Department_Values__c</field>
        <value xsi:type="xsd:string">Heart Failure,Electrophysiology,General Medicine,Primary Care,Physical Therapy,Rehabilitative Medicine,Other</value>
    </values>
    <values>
        <field>HV_GeneralSectionFields__c</field>
        <value xsi:type="xsd:string">PersonBirthdate,HV_Prescribed_By__pc,HealthCloudGA__Gender__pc,notes,Preferred_Contact_Method__pc,PersonEmail,Phone,PersonHomePhone,HV_isTwoFactorAuthentactionEnabled__pc,HV_Department__pc,HV_Patient_Clinic__c,HV_Height__pc,ShippingAddress</value>
    </values>
    <values>
        <field>HV_RequiredFields__c</field>
        <value xsi:type="xsd:string">PersonBirthdate,HealthCloudGA__Gender__pc,Preferred_Contact_Method__pc,HV_Height__pc,HV_isTwoFactorAuthentactionEnabled__pc,HV_Department__pc,ShippingAddress</value>
    </values>
</CustomMetadata>
