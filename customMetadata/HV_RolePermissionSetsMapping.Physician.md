<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Physician</label>
    <protected>false</protected>
    <values>
        <field>HV_PermissionSetIds__c</field>
        <value xsi:type="xsd:string">0PS23000000D8Ko,0PS23000000D8MD,0PS23000000DDGx</value>
    </values>
    <values>
        <field>HV_PermissionSetNames__c</field>
        <value xsi:type="xsd:string">HVPhysician,HV_Broadcast_Access,HV_Set_Goal_Permission</value>
    </values>
</CustomMetadata>
