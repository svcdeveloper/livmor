<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ContactUs</label>
    <protected>false</protected>
    <values>
        <field>HV_Contact_Us_Email__c</field>
        <value xsi:type="xsd:string">kirti.dayma@mindtree.com</value>
    </values>
    <values>
        <field>HV_Contact_Us_Header__c</field>
        <value xsi:type="xsd:string">Have any questions? Contact us anytime.</value>
    </values>
    <values>
        <field>HV_Contact_Us_Phone_Number__c</field>
        <value xsi:type="xsd:string">1-800-647-1681</value>
    </values>
    <values>
        <field>HV_Contact_Us_SubHeader__c</field>
        <value xsi:type="xsd:string">IF THIS IS A MEDICAL EMERGENCY, CALL 911 IMMEDIATELY.</value>
    </values>
</CustomMetadata>
