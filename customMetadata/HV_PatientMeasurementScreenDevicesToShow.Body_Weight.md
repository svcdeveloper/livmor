<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Body Weight</label>
    <protected>false</protected>
    <values>
        <field>HV_AWSEndpointAPI__c</field>
        <value xsi:type="xsd:string">https://salesforceapi.livmor.com/peripheral</value>
    </values>
    <values>
        <field>HV_DefaultData__c</field>
        <value xsi:type="xsd:string">---</value>
    </values>
    <values>
        <field>HV_DeviceHelpText__c</field>
        <value xsi:type="xsd:string">Latest body weight measurement. Click on the box to see measurement history.</value>
    </values>
    <values>
        <field>HV_DeviceSubText__c</field>
        <value xsi:type="xsd:string">What&apos;s This?</value>
    </values>
    <values>
        <field>HV_DeviceUniqueIdentifier__c</field>
        <value xsi:type="xsd:string">weightscale</value>
    </values>
    <values>
        <field>HV_Oder__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>HV_PpgViewerURL__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>IsDefaultShow__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
