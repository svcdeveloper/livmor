public with sharing class HVModalController {
    public HVModalController() {

    }

    @AuraEnabled(cacheable=true)
    public static List<HV_ClinicFieldsMapping__mdt> getAllFacilities(){
        List<HV_ClinicFieldsMapping__mdt> facilityList = new List<HV_ClinicFieldsMapping__mdt>();
        try {
            facilityList = HV_ClinicFieldsMapping__mdt.getAll().values(); 
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,'HV_ClinicFieldsMapping__mdt',null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
        return facilityList;
    }

    @AuraEnabled(cacheable=true)
    public static ContactUsDetails__mdt getcontactusdetails(){
        ContactUsDetails__mdt contactusdetails = new ContactUsDetails__mdt();
        try {            
            contactusdetails = ContactUsDetails__mdt.getInstance('ContactUs');             
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,'HV_ClinicFieldsMapping__mdt',null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
        return contactusdetails;
    }

    @AuraEnabled
    public static void sendEmailtoLivmorSupport(List<String> toaddress, String contactusrecorddata ){
        try {
            Object contactusobj = (Object)JSON.deserializeUntyped(contactusrecorddata);
            Map<String, Object> contactusrecord = (Map<String, Object>)contactusobj;
            Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
            String[] sendingTo = toaddress;
            semail.setToAddresses(sendingTo);
            semail.setSubject('Heartview Poratl: Contact Us Support Case');
            semail.setPlainTextBody('Hi Team,\nFind below details of the case raised from HeartView Portal: \n'+'Name: '+contactusrecord.get('FirstName')+' '+contactusrecord.get('LastName')+'\nUsername: '+contactusrecord.get('HVUsername')+'\nEmail: '+contactusrecord.get('Email')+'\nCell Phone: '+contactusrecord.get('Phone')+'\nHome Phone: '+contactusrecord.get('HomePhone')+'\nMessage: '+contactusrecord.get('Message'));
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,'sendEmailtoLivmorSupport',null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
    }
}