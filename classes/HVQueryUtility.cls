/**
 * @description       : 
 * @author            : Vipin Kumar
 * @group             : 
 * @last modified on  : 07-06-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   06-07-2021   Vipin Kumar                          Initial Version
**/
public without sharing class HVQueryUtility {
    
    /* Description :
     * This method will retrun schema of All object
     */
    private static Map<String, Schema.SObjectType> schemaMap(){
        return  Schema.getGlobalDescribe();
    }
     /* Description :
      * We need to pass object name in this method
      * and it will return object schema 
      */ 
    private static Map<String, Schema.SObjectField> schemaObjectMap(String objectName){
        return schemaMap().get(objectName).getDescribe().fields.getMap();
    }
    
     /* Description :
      * we need to pass object name 
      * and it will return of all field of that object in comma seprated string
      */ 
    public static string getSobjectFields(String objectName){
       
        return String.join(new List<String>(schemaObjectMap(objectName).keySet()), ',');
    }
    
	 /* Description :
	  * we need to pass object name 
	  * and it will return of updateable field of that objcect in comma seprated string
	  */ 
	private static string getSobjectUpdateableFields(String objectName){
       String fields;
        for(String fieldName : schemaObjectMap(objectName).keySet()) {
            if(schemaObjectMap(objectName).get(fieldName).getDescribe().isUpdateable()) {
                if(String.isBlank(fields)){
                    fields = fieldName;
                }else{
                    fields = fields+','+ fieldName;
                }
            }
            
        }
        return fields;
    }
    
     /* Description :
     * In lighting if you are using LDS wire and want to fetch Multiple record
     * it will return multiple records based on the parameter
     */
     
    @AuraEnabled( cacheable = true)
    public static List<sObject> getMultipleRecordWire(String objectname, String fieldnames, String conditions){
        return getSobjectRecords(objectname,fieldnames,conditions);
    }

     /* Description : 
      * In lighting if you are using imrative(mean you are calling apex in js method) and want to fetch Multiple record
      * it will return multiple records based on the parameter
      */ 
    @AuraEnabled
    public static List<sObject> getMultipleRecordImprative(String objectname, String fieldnames, String conditions){
        return getSobjectRecords(objectname,fieldnames,conditions);
    }
    
    /* Description : 
     * In lighting if you are using LDS wire and want to fetch single record
     * it will return single records based on the parameter
     */
    @AuraEnabled( cacheable = true)
    public static sObject getSingleRecordWire(String objectname, String fieldnames, String conditions){
        return getSobjectRecord(objectname,fieldnames,conditions);
    }

    /* Description : 
     * In lighting if you are using imrative(mean you are calling apex in js method) and want to fetch Multiple record
     * it will return multiple records based on the parameter
     */
    @AuraEnabled
    public static sObject getSingleRecordImprative(String objectname, String fieldnames, String conditions){
        return getSobjectRecord(objectname,fieldnames,conditions);
    }

    /* Description : 
     * This method will return sobject multiple records
     * you need to pass (objectname, fieldnames, conditions)
     * we are using database.query method to fetch the data 
     */ 
    public static List<sObject> getSobjectRecords(String objectname, String fieldnames, String conditions){
        
        try{
            system.debug(' User Query @@ '+ 'Select '+fieldnames+' from '+objectname+' '+conditions);
            return Database.query('Select '+fieldnames+' from '+objectname+' '+conditions);
        } 
        catch(Exception ex){
            system.debug('ex: '+ex.getMessage());
            HVLogExceptionHandler.createLog('Error',ex,objectname,null,null,null);
            return null;
        }
    }
    
    /* Description : 
     * This method will return sobject multiple records
     * you need to pass (objectname, conditions)
     * we are using database.query method to fetch the data    
     */ 
    @AuraEnabled
    public static List<sObject> getSobjectRecords(String objectname, String conditions){
        
        try{
            return Database.query('Select '+getSobjectFields(objectname)+' from '+objectname+' '+conditions);
        }
        catch(Exception ex){
            system.debug('ex: '+ex.getMessage());
            HVLogExceptionHandler.createLog('Error',ex,objectname,null,null,null);
            return null;
        }
        
    }
    
    /* Description : 
     * This method will return sobject single record
     * you need to pass (objectname, fieldnames, conditions)
     * we are using database.query method to fetch the data 
     */ 
    public static sObject getSobjectRecord(String objectname, String fieldnames, String conditions){
        try{
            system.debug('User Query '+'Select '+fieldnames+' from '+objectname+' '+conditions);
            return Database.query('Select '+fieldnames+' from '+objectname+' '+conditions);
        }
        catch(Exception ex){
            system.debug('ex: '+ex.getMessage());
            HVLogExceptionHandler.createLog('Error',ex,objectname,null,null,null);
            return null;
        }
    }
    
   /* Description : 
    * This method will return sobject single record
    * you need to pass (objectname, conditions)
    * we are using database.query method to fetch the data 
    */ 
    public static sObject getSobjectRecord(String objectname, String conditions){
        try{
            return Database.query('Select '+getSobjectFields(objectname)+' from '+objectname+' '+conditions);
        }
        catch(Exception ex){
            system.debug('ex: '+ex.getMessage());
            HVLogExceptionHandler.createLog('Error',ex,objectname,null,null,null);
            return null;
        }
    }

    public static void manualShareRead(Id recordId,String fieldName,String fieldAccess, Id userOrGroupId,String objectName,String  accessRight){
        // Create new sharing object for the custom object Job.
        Schema.SObjectType convertType = Schema.getGlobalDescribe().get(objectName);
         
        Sobject genericObject = convertType.newSObject(); 
   
    
        // Set the ID of record being shared.
        genericObject.put(fieldName, recordId);
        
        genericObject.put('OpportunityAccessLevel', accessRight);
        // Set the ID of user or group being granted access.
        genericObject.put('UserOrGroupId', userOrGroupId);
        
          
        // Set the access level.
        genericObject.put(fieldAccess, accessRight);
        try{
            Database.SaveResult sr = Database.insert(genericObject,false);
            system.debug('**** Error **** '+sr.getErrors());
        }
        catch(Exception e){
            HVLogExceptionHandler.createLog('Error',e,String.valueOf(convertType),null,null,null);
        }
        
     }
    
}