/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 09-13-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
public without sharing class hvSymptomStatusEmailing {
    @AuraEnabled
    public static void sendSymptomChecker(String  mailBody){
        try {
            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            // Set recipients to two contact IDs.
            // Replace IDs with valid record IDs in your org.
            message.toAddresses = new String[] { System.Label.HV_Symptom_Checker_Email };
            message.optOutPolicy = 'FILTER';
            message.subject = '[External] Symptom Checker - Email-Report';
            message.htmlBody = mailBody;
            Messaging.SingleEmailMessage[] messages = 
            new List<Messaging.SingleEmailMessage> {message};
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: '
                    + results[0].errors[0].message);
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}