/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-06-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-06-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public without sharing class HVLogExceptionHandler {
    
    public static void createLog(String type, Exception ex, String objectname, String recordId, Integer httpstatuscode, String httpresponse){
        try{
            
            Logs__c addLog = new Logs__c();
            addLog.HV_Type__c = type;
            addLog.HV_Class__c = ex.getStackTraceString().substringAfter('.').substringBefore('.');
            addLog.HV_Method__c = ex.getStackTraceString().substringBefore(':').substringAfter(addLog.HV_Class__c).substringAfter('.');
            addLog.HV_Description__c = 'Message: '+ex.getMessage() + '\n StackTrace: '+ex.getStackTraceString()+ '\n Cause: '+ex.getCause();
            addLog.HV_Line_Number__c = ex.getLineNumber();
            addLog.HV_Object__c = objectname;
            addLog.HV_Record_Id__c = recordId;
            addLog.HV_Running_User__c = userinfo.getUserId();
            addLog.HV_HTTP_Status_Code__c = httpstatuscode;
            addLog.HV_HTTP_Response__c = httpresponse;
            
            insert addLog;
        }
        catch(Exception e){
            System.debug('Exception: '+e.getMessage());
        }
        
    }
}