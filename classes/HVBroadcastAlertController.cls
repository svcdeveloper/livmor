/* 	Description			: HVBroadcastAlertController class is using for Registering Physician,Patient etc.
    * CalssName 			: HVRegisterUser 
    * Created by			: Mindtree Dev Team
    * Created Date 			: 12 July 2021
    * Last Modified By 		: Kunal.Rana
    * Last Modified Date 	: 
*/
public without sharing class HVBroadcastAlertController {
    /**
     * This method to return the broadcast message
     * @param  
     * @return  List<string>
     */
    @AuraEnabled(cacheable=true)
    public static List<String> bannerMessages(){
        List<String> warningMsg = new List<String>();
        DateTime todaysDate = System.today();
        for(Warning_Message__c eachRec : [Select Id, Is_Active__c, HV_Alert_Message__c from Warning_Message__c where Start_Date__c <= :todaysDate AND End_Date__c >= :todaysDate]){
            if(eachRec.Is_Active__c){
                warningMsg.add(eachRec.HV_Alert_Message__c);
            }
        }
        return warningMsg;
    }
    
	@AuraEnabled
    public static List<HV_Notifications__c> getLogedInUserNotifications(String contactId){
        User currUser = new User();
        DateTime thresholdDate = System.today().addDays(-30);
        if(!String.isEmpty(contactId)){
            currUser = [Select Id, ContactId, UserName, Email, Contact.HV_Patient_Id__c from User where ContactId =: contactId];
        }else{
            currUser = [Select Id, ContactId, UserName, Email, Contact.HV_Patient_Id__c from User where Id =: UserInfo.getUserId()];
        }
        return [Select Id, UserId__c, Message__c, Sent_By__c, Time_Stamp__c from HV_Notifications__c where (UserId__c =:currUser.Email OR
                                                                                              UserId__c =:currUser.UserName OR UserId__c =:currUser.Contact.HV_Patient_Id__c) and createdDate >=:thresholdDate];
    }  
}