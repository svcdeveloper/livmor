@RestResource(urlMapping='/api/tablet/*')
global with sharing class HVPatientBoxService {
	@HttpGet
    global static HV_Box__c doGet() {
        RestRequest request = RestContext.request;
        RestResponse res = RestContext.response;
        String tabletId = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1);
        
        HV_Tablet__c tabletRec = [Select Id, HV_Box__c,HV_Android_ID__c from HV_Tablet__c where HV_Android_ID__c = :tabletId];
        
        List<HV_Box__c> lstbox = new List<HV_Box__c>([ SELECT Id, Name, (Select Id, HV_Watch_ID__c from Watches__r), (Select Id,HV_BDA__c from BPs__r),
                                                      (Select Id, HV_BDA__c from Pulse_Oximeters__r),
                                                      (Select Id, HV_BDA__c from Spirometers__r),(Select Id, HV_BDA__c from Glucometers__r),
                                                      (Select Id, HV_Serial_Number__c from Tablets__r),(Select Id, HV_BDA__c from WSS__r),
                                                      (Select Id, HV_Batch_Number__c from QAs__r), (Select Id, HV_Tracking_number__c from 
                                                       Tracking_and_Shipping__r) FROM HV_Box__c where Id =:tabletRec.HV_Box__c]);
        return lstbox[0];
    }
}