/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-05-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
        
/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-05-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-09-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public without sharing class HVInventoryManagementController {
    public HVInventoryManagementController() {

    }

    @AuraEnabled
    public static void deprovisionBox(String deprovisionVal, String boxId){
        try {
            list<Sobject> lstSobjCon = new List<sObject>();
            HV_Box__c hvBoxData = (HV_Box__c)HVQueryUtility.getSobjectRecord( 'HV_Box__c',
            ' Id, HV_Customer__c,'+
            '(SELECT Id,HV_Box_ID__c, Account.HV_Patient_Clinic__r.Name FROM Contacts__r), '+
            '(SELECT Id FROM BPs__r), '+
            '(SELECT Id FROM Pulse_Oximeters__r), '+
            '(SELECT Id FROM Spirometers__r), '+ 
            '(SELECT Id FROM Tablets__r), '+
            '(SELECT Id FROM WSS__r), '+ 
            '(SELECT Id FROM Watches__r), '+
            '(SELECT Id FROM Glucometers__r), '+
            '(SELECT Id FROM QAs__r)',  
            ' where Id = \''+boxId+'\'');
            
            ID conBoxId = null;
            String clinicName = '';
            if(!hvBoxData.Contacts__r.isEmpty()){
                for(contact objConData : hvBoxData.Contacts__r){
                   
                       
                    contact objcon = new contact();
                    if(deprovisionVal == 'Deprovisioned'){
                        objcon.HV_Box_ID__c = conBoxId;
                    }
                    objcon.Id = objConData.Id;
                    
                    clinicName = objConData.Account.HV_Patient_Clinic__r.Name;
                    system.debug('objConData'+objConData);
                    lstSobjCon.add(objcon);

                }
            } 

            system.debug('clinicName'+clinicName);
            HV_Box__c hvBoxObj= new HV_Box__c();
            hvBoxObj.Id = hvBoxData.Id;
            hvBoxObj.HV_Status__c = deprovisionVal;
            hvBoxObj.HV_Customer__c = clinicName;
            update hvBoxObj;

            if(!lstSobjCon.isEmpty()){
                update lstSobjCon;
            } 
            getListOfSobject(deprovisionVal,'BPs__r',hvBoxData);
            getListOfSobject(deprovisionVal,'Pulse_Oximeters__r',hvBoxData);
            getListOfSobject(deprovisionVal,'Spirometers__r',hvBoxData);
            getListOfSobject(deprovisionVal,'Tablets__r',hvBoxData);
            getListOfSobject(deprovisionVal,'WSS__r',hvBoxData);
            getListOfSobject(deprovisionVal,'Watches__r',hvBoxData);
            getListOfSobject(deprovisionVal,'Glucometers__r',hvBoxData);
            getListOfSobject(deprovisionVal,'QAs__r',hvBoxData);

        } catch (Exception e) {
            system.debug('ex: '+e.getMessage());
            HVLogExceptionHandler.createLog('Error',e,null,null,null,null);
            
            throw new AuraHandledException(e.getMessage()); 
        }
    }

    private static void getListOfSobject(String deprovisionVal,String objectName,sobject sobjectData){
        List<sobject> sobList = new List<sobject>();
        if(objectName == 'QAs__r'){
            for(sobject sobjData : sobjectData.getSobjects(objectName)){
                sobList.add(setFieldValueQa(sobjData,deprovisionVal));
            }
        }else{
            for(sobject sobjData : sobjectData.getSobjects(objectName)){
                sobList.add(setFieldValue(sobjData,deprovisionVal));
            }
        }
        

        update sobList;
    } 
    private static sObject setFieldValue(sObject sobj, String deprovisionVal){
        sobj.put('HV_Status__c',deprovisionVal);
        sobj.put('HV_User_Device_Status__c','Inactive');
        return sobj;
    }

    private static sObject setFieldValueQa(sObject sobj, String deprovisionVal){
        sobj.put('HV_Status__c',deprovisionVal);
        return sobj;
    }

   

    @AuraEnabled
    public static void boxDeassociation(String boxId){
        try {
            list<Sobject> lstSobjCon = new List<sObject>();
            HV_Box__c hvBoxData = (HV_Box__c)HVQueryUtility.getSobjectRecord( 'HV_Box__c',
            ' Id, HV_Customer__c,'+
            '(SELECT Id,HV_Box_ID__c, Account.HV_Patient_Clinic__r.Name FROM Contacts__r)',
            ' where Id = \''+boxId+'\'');
            
            ID conBoxId = null;
            if(!hvBoxData.Contacts__r.isEmpty()){
                for(contact objConData : hvBoxData.Contacts__r){
                   
                       
                    contact objcon = new contact();
            
                    objcon.HV_Box_ID__c = conBoxId;
                    objcon.Id = objConData.Id;
                    lstSobjCon.add(objcon);

                }
            } 
            HV_Box__c hvBoxObj= new HV_Box__c();
            hvBoxObj.Id = hvBoxData.Id;
            hvBoxObj.HV_Status__c = 'Manufacturing In-Progress';
            update hvBoxObj;
            if(!lstSobjCon.isEmpty()){
                update lstSobjCon;
            }

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}