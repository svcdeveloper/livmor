public without sharing class HVUserProfileController {
    public HVUserProfileController() {

    }

    /*Description : getting custom metadata records 
     *which has the role based field to show on UI.
     */
    @AuraEnabled
    public static HV_Roles_and_FieldsMapping__mdt getRoleFieldsMapping(String roleFieldsName){
        String objectname = 'HV_Roles_and_FieldsMapping__mdt';
        String fieldstoQuery = 'MasterLabel,DeveloperName,HV_Avaliable_Fields__c,HV_Required_Fields__c';            
        String conditions = 'where MasterLabel = \''+roleFieldsName+'\'';
        try {
            return (HV_Roles_and_FieldsMapping__mdt) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
    }

    /* Description: 
     * Method to get all clinics and 
     * there fields mapping for patient registration
     * Patient Registration dynamic form as per Clinic
     */
    @AuraEnabled
    public static HV_ClinicFieldsMapping__mdt getPatientClinicFieldsMapping(String clinicId){
        String objectname = 'HV_ClinicFieldsMapping__mdt';
        String fieldstoQuery = 'MasterLabel,DeveloperName,HV_Clinic_Id__c,HV_RequiredFields__c,HV_GeneralSectionFields__c,HV_ContactInformationSectionFields__c,HV_AccountInformationSectionFields__c,HV_Department_Values__c';            
        String conditions = 'where HV_Clinic_Id__c = \''+clinicId+'\'';
        try {
            return (HV_ClinicFieldsMapping__mdt) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
    }

    /* Description: 
     * Method to get existing values of the 
     * requested fields for user 
     */
    @AuraEnabled
    public static Contact getOtherUserData(String userContactRecordId, List<String> fields){
        String objectname;
            String fieldstoQuery;
            String conditions;
            Contact contactToReturn;
        try {
            objectname = 'Contact';
            fieldstoQuery = String.join(fields,',');
            conditions = 'where id = \''+ userContactRecordId +'\'';
            contactToReturn = (Contact) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
        return contactToReturn;
    }

    /* Description: 
     * Method to get existing values of the 
     * requested fields for patient 
     */
    @AuraEnabled
    public static Account getPatientUserData(String userPersonAcountId, List<String> fields){
        String objectname;
            String fieldstoQuery;
            String conditions;
            Account accountToReturn;
        try {
            if(!String.isBlank(userPersonAcountId)){
                objectname = 'Account';
                fieldstoQuery = String.join(fields,',');
                conditions = 'where id = \''+ userPersonAcountId +'\'';
                accountToReturn = (Account) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
            }
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
        return accountToReturn;
    }

    /* Description: 
     * Method to save the modified 
     * data from user profile page
     */
    @AuraEnabled
    public static sObject saveUserData(sObject objectToSave, String roleName){
        String objectname = 'User';
        String fieldsToQuery = 'Id,FirstName,LastName,Email,Phone,Username,Alias,CommunityNickname';
        String conditions;
        User userToUpdate = new User();
        try {
            update objectToSave;
            if(roleName == 'Patient'){
                Account patientUser = (Account)objectToSave;
                conditions = 'where contactid = \''+patientUser.PersonContactId+'\'';
                User userrecord = (User) HVQueryUtility.getSobjectRecord(objectname,fieldsToQuery,conditions);
                if(userrecord.Email != patientUser.PersonEmail || userToUpdate.Phone != patientUser.Phone){
                    userToUpdate = userrecord;
                    userToUpdate.Email = patientUser.PersonEmail;
                    userToUpdate.Phone = patientUser.Phone;
                }
            }
            else{
                Contact otherUsers = (Contact)objectToSave;
                conditions = 'where contactid = \''+otherUsers.Id+'\'';
                User userrecord = (User) HVQueryUtility.getSobjectRecord(objectname,fieldsToQuery,conditions);
                if(userToUpdate.Phone != otherUsers.Phone){
                    userToUpdate = userrecord;
                    userToUpdate.Phone = otherUsers.Phone;
                }
            }

            if(!String.isblank(userToUpdate.Id)){
                update userToUpdate;
            }
            
            return objectToSave;
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
    }

    /* Description: 
     * Method to get all the notes
     * related to patient
     */
    @AuraEnabled
    public static List<HV_Notes__c> getPatientNotes(String patientId){
        String objectname = 'HV_Notes__c';
        String fieldsToQuery = 'id,HV_Comment__c,HV_Contact__c,Name,createdby.FirstName,createdby.LastName,createddate,createdby.SmallPhotoUrl';
        String conditions = 'where HV_Contact__c= \''+patientId+'\'';
        List<HV_Notes__c> listofNotes = new List<HV_Notes__c>();            
        try {
            listofNotes = (List<HV_Notes__c>) HVQueryUtility.getSobjectRecords(objectname,fieldstoQuery,conditions);
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,'SaveContact/PA',null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
        return listofNotes;
    }

    /*Description
    *Method to create notes for patients
    *by physician/clinical users
    */
    @AuraEnabled
    public static void createnotes(HV_Notes__c noteToCreate){
        try {
            insert noteToCreate;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static Boolean patientOffboard(Id patientContactId){
        Boolean dmlSuccess = false;
        String objectname = 'Contact';
        String fieldsToQuery = 'Id,HV_Box_ID__c ';
        String conditions = 'where Id = \''+patientContactId+'\'';
        
        Contact patientContactRec = (Contact) HVQueryUtility.getSobjectRecord(objectname,fieldsToQuery,conditions);
        if(patientContactRec != null && patientContactRec.HV_Box_ID__c != null){
            String fieldsToQuery1 = 'Id,HV_Status__c ';
            String fieldsToQuery2 = 'Id,HV_Status__c,HV_User_Device_Status__c ';
            String conditions1 = 'where Id = \''+patientContactRec.HV_Box_ID__c+'\'';
            String conditions2 = 'where HV_Box__c = \''+patientContactRec.HV_Box_ID__c+'\'';
            
            changeStatus('HV_Box__c',fieldsToQuery1,conditions1);
            changeStatus('HV_Watch__c',fieldsToQuery2,conditions2);
            changeStatus('HV_BP__c',fieldsToQuery2,conditions2);
            changeStatus('HV_Pulse_Oximeter__c',fieldsToQuery2,conditions2);
            changeStatus('HV_Spirometer__c',fieldsToQuery2,conditions2);
            changeStatus('HV_Glucometer__c',fieldsToQuery2,conditions2);
            changeStatus('HV_Tablet__c',fieldsToQuery2,conditions2);
            changeStatus('HV_WS__c',fieldsToQuery2,conditions2);
            changeStatus('HV_QA__c',fieldsToQuery1,conditions2);
            
            patientContactRec.HV_Box_ID__c = null;
            try{
                Database.update(patientContactRec);
                makePatientUserInactive(patientContactId);
                dmlSuccess = true;
            } catch (Exception e) {
                HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
                throw new AuraHandledException(e.getMessage());
            }
        }
        
        return dmlSuccess;
    }
    
    public static void changeStatus(String objectname, String fieldsToQuery, String conditions){
        List<SObject> recL = HVQueryUtility.getSobjectRecords(objectname,fieldstoQuery,conditions);
        if(recL.size() > 0){
            for(SObject rec : recL){
                rec.put('HV_Status__c', 'Deprovisioned');
                if(objectname != 'HV_QA__c' && objectname != 'HV_Box__c'){
                    rec.put('HV_User_Device_Status__c', 'Inactive');
                }
            }
            try{
                Database.update(recL);
            } catch (Exception e) {
                HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
                throw new AuraHandledException(e.getMessage());
            }
        }
    }
    
    @future
    public static void makePatientUserInactive(Id patientContactId){
        String objectnameUser = 'User';
        String fieldsToQueryUser = 'Id,IsActive ';
        String conditionsUser = 'where ContactId = \''+patientContactId+'\'';
        User patientUserRec = (User) HVQueryUtility.getSobjectRecord(objectnameUser,fieldsToQueryUser,conditionsUser);
        patientUserRec.IsActive = false;
        
        try{
            Database.update(patientUserRec);
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,objectnameUser,null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
    }
}