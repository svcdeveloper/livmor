public with sharing class HVSecurityQuestionsController {
    public HVSecurityQuestionsController() {

    }

    /* Description: 
     * Method to get all the saved security questions
     * and answers related to user
     */
    @AuraEnabled
    public static HV_UserPreference__c getUserSelectedQuestions(String userId){
        String objectname = 'HV_UserPreference__c';
        String fieldsToQuery = 'Id,Name,HV_Security_Question_One__c,HV_Security_Question_Two__c,HV_Security_Question_Three__c,HV_Security_Answer_One__c,HV_Security_Answer_Two__c,HV_Security_Answer_Three__c,HV_User__c,IsFirstTimeLoginDone__c';
        String conditions = 'where HV_User__c = \''+userId+'\'';
        HV_UserPreference__c userPreference;
        try {
            userPreference = (HV_UserPreference__c) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
        return userPreference;
    }
    
    @AuraEnabled
    public static Boolean setUserSelectedQuestions(String userId, Map<String, String> userpreferencedetails){
        Boolean dmlSuccess = false;
        String objectname = 'User';
        String objectname2 = 'HV_UserPreference__c';
        String fieldsToQuery = 'Name';
        String conditions = 'where Id = \''+userId+'\'';
        User loginUser = new User();
        HV_UserPreference__c userPref = new HV_UserPreference__c();
        
        loginUser = (User) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
        if(loginUser != null){
            userPref.HV_User__c = userId;
            userPref.Name = loginUser.Name;
            userPref.HV_Security_Question_One__c = userpreferencedetails.get('HV_Security_Question_One__c');
            userPref.HV_Security_Question_Two__c = userpreferencedetails.get('HV_Security_Question_Two__c');
            userPref.HV_Security_Question_Three__c = userpreferencedetails.get('HV_Security_Question_Three__c');
            userPref.HV_Security_Answer_One__c = userpreferencedetails.get('HV_Security_Answer_One__c');
            userPref.HV_Security_Answer_Two__c = userpreferencedetails.get('HV_Security_Answer_Two__c');
            userPref.HV_Security_Answer_Three__c = userpreferencedetails.get('HV_Security_Answer_Three__c');
            userPref.HV_LoginDateTimeStamp__c = System.now();
            userPref.IsFirstTimeLoginDone__c = true;
            
            try{
            	Database.insert(userPref);   
                dmlSuccess = true;
            } catch (Exception e) {
                HVLogExceptionHandler.createLog('Error',e,objectname2,null,null,null);
                throw new AuraHandledException(e.getMessage());
            }
        }
        
        return dmlSuccess;
    }
}