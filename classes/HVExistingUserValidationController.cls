public with sharing class HVExistingUserValidationController {
    
    /* Description: 
    * Method to check if user(patient/other) already
    * exists in system on the basis of 
    * user personal details (email,phone,name,dob)
    */
    @AuraEnabled
    public static List<User> validateUser(Account accountRecord,Contact contactRecord){  
        System.debug('conrecord'+contactRecord);
        List<User> existingUserList = new List<User>(); //list of existing user if any to return
        CreateUserDefaultFields__mdt UserFields = new CreateUserDefaultFields__mdt(); //metadata to fetch fields of user        
        String conditions=''; //conditions to call query
        
        try{
                        
            //If person account - check for existing patient validations
            //HV should validate patients account on three combination of checks to see if the patients exist or not.
			//a.	First Name + Last Name + D.O.B
			//b.	Email Address
			//c.	Phone Number
            if(accountRecord != null){
                //get fields,objectname and other static details from metadata which are used to get existing user details
                UserFields = (CreateUserDefaultFields__mdt) HVQueryUtility.getSobjectRecord('CreateUserDefaultFields__mdt','Id,MasterLabel,DeveloperName,HV_FieldsToQueryFromObject__c,HV_QueryObjectName__c','where developername = \'PatientQuery\'');
                
                if(String.isNotBlank(accountRecord.FirstName) && String.isNotBlank(accountRecord.LastName) && accountRecord.PersonBirthdate != null){
                    Date createDob = date.ValueOf(accountRecord.PersonBirthdate);    
                    DateTime dobInDateTime = DateTime.newInstance(createDob.year(), createDob.month(),createDob.day());
                    String formatedDob = dobInDateTime.format('yyyy-MM-dd');     
                    conditions = 'where lastname=\''+accountRecord.LastName+'\''+'and firstname=\''+accountRecord.FirstName+'\''+'and Contact.Account.PersonBirthdate= '+formatedDob;
                    for(User userRec : (List<User>) HVQueryUtility.getSobjectRecords(UserFields.HV_QueryObjectName__c,UserFields.HV_FieldsToQueryFromObject__c,conditions)){
                        if(((String.isBlank(accountRecord.PersonEmail) || String.isBlank(userRec.email) || isdefaultemail(userRec)) && (removeSpecialCharsFromPhone(userRec.phone) == removeSpecialCharsFromPhone(accountRecord.Phone)))||
                           ((String.isBlank(accountRecord.Phone) || String.isBlank(userRec.Phone)) && (((!isdefaultemail(userRec)) && (userRec.email == accountRecord.PersonEmail)) || ((isdefaultemail(userRec)) && (userRec.email == accountRecord.PersonEmail)))) ||
                           (((!isdefaultemail(userRec)) && (userRec.email == accountRecord.PersonEmail)) && (removeSpecialCharsFromPhone(userRec.Phone) == removeSpecialCharsFromPhone(accountRecord.Phone))))                   
                        {                  
                            existingUserList.add(userRec);                         
                        }
                    }
                }
            }
            //If contact - check for existing other users validations(physician/clinical user)
            //HV Should validate users based on three combination of checks to see if the user exist or not.
            //a. First Name + Last Name 
            //b. Email Address
            //c. Phone Number
            else if(contactRecord != null){
                //get fields,objectname and other static details from metadata which are used to get existing user details
                UserFields = (CreateUserDefaultFields__mdt) HVQueryUtility.getSobjectRecord('CreateUserDefaultFields__mdt','Id,MasterLabel,DeveloperName,HV_FieldsToQueryFromObject__c,HV_QueryObjectName__c','where developername = \'OtherUserQuery\'');

                //for users other than Patient, check if licenses are available as per clinic
                List<HV_Clinic_and_License_Mapping__mdt> hvClinic = HV_Clinic_and_License_Mapping__mdt.getAll().values(); //metadata to get clinic and license mapping
                Map<String,HV_Clinic_and_License_Mapping__mdt> clinicIdLicenseNumMap = new Map<String,HV_Clinic_and_License_Mapping__mdt>();
                for(HV_Clinic_and_License_Mapping__mdt val :hvClinic){
                    clinicIdLicenseNumMap.put(val.HV_Clinic_ID__c, val);
                }
                conditions = 'where contact.accountId=\''+contactRecord.accountId+'\''+' and isActive=true and contact.HV_Role__c != \''+'Patient'+'\'';
                Integer numberOfSameClinicUsers = (HVQueryUtility.getSobjectRecords(UserFields.HV_QueryObjectName__c,UserFields.HV_FieldsToQueryFromObject__c,conditions)).size();
                if((clinicIdLicenseNumMap.get(contactRecord.accountId)).HV_Number_of_License__c > numberOfSameClinicUsers){
                    
                }
                if((clinicIdLicenseNumMap.get(contactRecord.accountId)).HV_Number_of_License__c > numberOfSameClinicUsers){
                    if(String.isNotBlank(contactRecord.FirstName) && String.isNotBlank(contactRecord.LastName)){ //if license availabe for clinic, allow to create user
                        conditions = 'where lastname=\''+contactRecord.LastName+'\''+'and firstname=\''+contactRecord.FirstName+'\'';
                        for(User userRec : (List<User>) HVQueryUtility.getSobjectRecords(UserFields.HV_QueryObjectName__c,UserFields.HV_FieldsToQueryFromObject__c,conditions)){
                            if(((String.isBlank(userRec.email) || String.isBlank(contactRecord.Email)) && (removeSpecialCharsFromPhone(userRec.Phone) == removeSpecialCharsFromPhone(contactRecord.Phone)))||
                               ((String.isBlank(userRec.phone) || String.isBlank(contactRecord.Phone)) && (userRec.Email == contactRecord.Email)) ||
                               ((userRec.email == contactRecord.Email) && (removeSpecialCharsFromPhone(userRec.Phone) == removeSpecialCharsFromPhone(contactRecord.Phone))))
                            {
                                existingUserList.add(userRec);    
                            }
                        }
                    }
                }
                else{ //if license not available for the clinic do not allow to create user and throw error                    
                    AuraHandledException licenseException=  new AuraHandledException('Licenses are not available for this clinic!');
                    licenseException.setMessage('Licenses are not available for this clinic!');
                    throw licenseException;
                }
            }
        }
        catch(Exception e){
            HVLogExceptionHandler.createLog('Error',e,'validateUser',null,null,null);
            throw new AuraHandledException(e.getMessage());
        }        
        return existingUserList;
    }
    
    /**
    * This method to remove the non-numeric characters from the Phone number 
    * @param  Phone Number in String 
    * @return  string
    */ 
    public static String removeSpecialCharsFromPhone(String phone){
        if (phone != null ) {
            phone = phone.replaceAll('\\D','');
        }
        return phone;
    }
    
   /* Description: 
    * Method to check if user's email is
    * default email generated by system
    * or valid user's email
    */
    public static boolean isdefaultemail(User userDetails){
        boolean isdefaultemail = false;
        String clinicname = userDetails.Contact.Account.HV_Patient_Clinic__r.Name;
        String defaultemail = userDetails.Contact.Account.HV_Patient_ID__pc + '@' + String.join(clinicname.split(' '),'') + '.com';	
        if((userDetails.email).equalsIgnoreCase(defaultemail)){
            isdefaultemail = true;
        }
        return isdefaultemail;
    }

    /* Description: 
    * Method to send email to livmor
    * admin team for additional email 
    * request
    */
    @AuraEnabled
    public static void sendlicenserequestemail(String clinicname){
        try {
            String objectname = 'HV_SystemAdminsEmailAddress__mdt';
            String fieldstoQuery = 'MasterLabel,DeveloperName,HV_Email_Address__c';            
            String conditions = 'where MasterLabel = \'AdminNotifications\'';
            HV_SystemAdminsEmailAddress__mdt adminemails = (HV_SystemAdminsEmailAddress__mdt) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
            List<String> toEmailAddress = adminemails.HV_Email_Address__c.split(',');
            EmailTemplate templateEmail = (EmailTemplate)HVQueryUtility.getSobjectRecord('Emailtemplate','id,developername,body,htmlvalue,subject','where developername = \''+'HVNoLicenseAvailable'+'\'');
            String htmlbody= templateEmail.htmlvalue;
            htmlbody= htmlbody.replace('{!Account.Name}', clinicname);
            sendEmailNotification(toEmailAddress,templateEmail.subject,htmlbody);
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,'sendlicenserequestemail',null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
    }
    

    /* Description: 
     * Generic Method to send email notification
     * pass send to email address and subject to this method
     * method will send emails respectively
     */
    public static void sendEmailNotification(List<String> toaddress, String subject,String emailBody){
        try{
            Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
            semail.setToAddresses(toaddress);
            semail.setSubject(subject);               
            semail.setHtmlBody(emailBody);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
        }catch(Exception e){
            HVLogExceptionHandler.createLog('Error',e,'sendEmailNotification',null,null,null);
            throw new AuraHandledException(e.getMessage());
        }        
    }

    /*Description : getting custom metadata records which has the role based field to show on UI.
    */
    @AuraEnabled(cacheable=true)
    public static HV_Roles_and_FieldsMapping__mdt getRoleFieldsMapping(String roleFieldsName){
        try {
            Map<String,HV_Roles_and_FieldsMapping__mdt> maptCustomMetadata = HV_Roles_and_FieldsMapping__mdt.getAll();
            return maptCustomMetadata.get(roleFieldsName);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    /*Description : getting custom metadata records which is returing all clinc.
    */
    @AuraEnabled(cacheable=true)
    public static List<HV_ClinicFieldsMapping__mdt> getAllClinc(){
        try {
            return HV_ClinicFieldsMapping__mdt.getAll().values();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}