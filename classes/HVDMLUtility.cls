/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 08-26-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
public without sharing class HVDMLUtility {
    public HVDMLUtility() {

    }
    /* Description: 
     * Generic method used for insert DML for any object type
     * if user does not have access to insert the record call
     * this method as without sharing 
     */
    

   
    @AuraEnabled
    public static sObject updateRecord(object uapdateRec){
         
        try{
            system.debug('uapdateRec'+uapdateRec);
            object obj = JSON.deserialize(JSON.serialize(uapdateRec),sobject.class);
            update (sobject)obj;  
            return (sobject)obj;           
        }
        catch(Exception e){
            HVLogExceptionHandler.createLog('Error',e,'sObject',null,null,null);
            return null;
        }
        
    }

    @AuraEnabled
    public static sObject createRecord(object createRec){
         
        try{ 
            system.debug('uapdateRec'+createRec);
            object obj = JSON.deserialize(JSON.serialize(createRec),sobject.class);
            insert (sobject)obj;  
            return (sobject)obj;           
        }
        catch(Exception e){
            HVLogExceptionHandler.createLog('Error',e,'sObject',null,null,null);
            return null;
        }
        
    }
}