public class HVAFReportView {
    public static String getToken(){
        String USER_NAME = Label.HV_AF_Report_Username;
        String PASSWORD = Label.HV_AF_Report_Password;
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(Label.HV_AF_Report_Endpoint_Login_URL+'?accountname=groupo' + '&username='+USER_NAME + '&pwd='+PASSWORD);
        Http http = new Http();
        HTTPResponse response = http.send(req);
        if(response.getStatusCode() == 200){
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            return results.get('token').toString();
        }
        return '';
    }
    
    @auraEnabled
    public static String navigateToAFReport(String server, String userid, String s3bucket, Integer startdate, Integer enddate){
        String token = HVAFReportView.getToken();
        String watch = 'yes';
        String weight = 'no';
        String bp = 'no';
        return Label.HV_AF_Report_URL+'?startdate='+startdate+'&enddate='+enddate+'&server='+server+'&userid='+userid+'&s3bucket='+s3bucket+'&watch='+watch+'&weight='+weight+'&bp=no&token='+token;
    }
}