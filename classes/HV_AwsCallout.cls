public class HV_AwsCallout implements Queueable, Database.AllowsCallouts{
    public String awsEndPoint = '';
    public String awsMethod = '';
    public String awsBody = '';
    
    public HV_AwsCallout(String calloutEndPoint, String calloutMethod, String calloutBody){
        awsEndPoint = calloutEndPoint;
        awsMethod = calloutMethod;
        awsBody = calloutBody;
    }
    
    public void execute(QueueableContext context){
        HttpRequest req = new HttpRequest();
        req.setEndpoint(awsEndPoint);
        req.setMethod(awsMethod);
        req.setBody(awsBody);
        Http http = new Http();
        HttpResponse resp = http.send(req);     
        System.debug('AWS callout response: ' + resp);
    }
}