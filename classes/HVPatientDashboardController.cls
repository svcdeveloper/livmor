public without sharing class HVPatientDashboardController {
    @AuraEnabled(cacheable=true)
    public static HV_Goal__c getLongTermGoal(){
        Id contactId = [Select ContactId from User where Id =:Userinfo.getUserId()].ContactId;
        return [Select Id, Heart_Rate__c, Weight__c, Blood_Pressure__c, Related_Patient__c, Target_Date__c from HV_Goal__c where Related_Patient__c =:contactId LIMIT 1];
    }
    
    @AuraEnabled
    public static HV_Goal__c getSetGoal(Id patientId){
        return [Select Id, Heart_Rate__c, Weight__c, Blood_Pressure__c, Related_Patient__c, Target_Date__c from HV_Goal__c where Related_Patient__c =:patientId LIMIT 1];
    }
    
    @AuraEnabled
    public static boolean updateGoal(HV_Goal__c goal_rec){//2021-09-13T08:16:57.000Z
        if(!(goal_rec.Weight__c).contains('lb')){
            goal_rec.Weight__c = goal_rec.Weight__c+' lb';
        }
        if(!(goal_rec.Blood_Pressure__c).contains('mm Hg')){
            goal_rec.Blood_Pressure__c = goal_rec.Blood_Pressure__c+' mm Hg';
        }
        if(!(goal_rec.Heart_Rate__c).contains('bpm')){
            goal_rec.Heart_Rate__c = goal_rec.Heart_Rate__c+' bpm';
        }
        try{
           if(goal_rec.Id != null){
            	update goal_rec;
            }else{
                insert goal_rec;
            } 
            return true;
        }catch(Exception exp){
            return false;
        }        
    }
    
    @AuraEnabled
    public static Contact getPatientId(Id contactId){
        return [Select Id, HV_Patient_Id__c, Account.HV_Patient_Clinic__c from Contact where Id =:contactId LIMIT 1];
    }
    
    @AuraEnabled
    public static User getUserId(){
        return [Select Id, ContactId, Contact.HV_Patient_Id__c, Contact.Account.HV_Patient_Clinic__c from User where Id =:UserInfo.getUserId() LIMIT 1];
    }
    
    @AuraEnabled
    public static Integer getLoginCount(){
        return [SELECT Id,LoginTime,LoginType,LoginUrl,NetworkId,Status,UserId FROM LoginHistory WHERE NetworkId =:System.Label.HV_Community_NetworkId and LoginTime = TODAY].size();
    }
}