/* Description				: HVForgotPassword class is using for reseting password
     * CalssName 			: HVForgotPassword 
     * Created by			: Mindtree Dev Team
     * Created Date 		: 6th June 2021
     * Last Modified By 	:
     * Last Modified Date 	:
     */
public without sharing  class HVForgotPassword {
        	
 		 static String objectname = 'user';
         static  String fieldstoQuesry = 'id,lastname,phone,email,username,contact.account.PersonHomePhone,contact.Preferred_Contact_Method__c,contact.HV_Patient_ID__c,contact.HV_isTwoFactorAuthentactionEnabled__c,contact.birthdate';
   
    /* Description: 
     * Method is using for verify the user available in the system
     */    
    @AuraEnabled
    public static user getContact(string email, string dob){        
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; 
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
        DateTime dobInDateTime;
        date createDob ;
        String formatedDob;
        if(dob !=null){
            createDob = date.ValueOf(dob);            
            dobInDateTime = DateTime.newInstance(createDob.year(), createDob.month(),createDob.day());
			formatedDob = dobInDateTime.format('yyyy-MM-dd');
        }         
          String conditions = ' where email=\''+email+'\' and contact.Birthdate= '+ formatedDob;
          String patientCondtions=' where contact.HV_Patient_ID__c=\''+email+'\' and contact.Birthdate= '+ formatedDob;
          String emailConditions = ' where email=\''+email+'\'';  
        list<user> userData =new list<user>();
        
        try{
            if(MyMatcher.matches()){
                if(email != null && dob !=null){
                    userData = (list<user>) HVQueryUtility.getSobjectRecords(objectname,fieldstoQuesry,conditions);
                    system.debug('Userdata @@'+userData);
                        if(userData.size()==0){
                            system.debug('userData '+userData);
                            AuraHandledException e=  new AuraHandledException('Email or Dob Incorrect ');
                            e.setMessage('Email or Dob Incorrect !');
                            throw e;
                        }                                        
                }else{
                    userData = (list<user>) HVQueryUtility.getSobjectRecords(objectname,fieldstoQuesry,emailConditions);
                    if(userData.size()==0){
                        AuraHandledException e1=   new AuraHandledException('Email is Incorrect !');
                        e1.setMessage('Email is Incorrect !'); 
                        throw e1;
                    }
                }                
            }else{
               userData = (list<user>) HVQueryUtility.getSobjectRecords(objectname,fieldstoQuesry,patientCondtions);               
                if(userData.size()==0){
                    AuraHandledException e2= new AuraHandledException('PatientId or Dob Incorrect !'); 
                    e2.setMessage('PatientId or Dob Incorrect !');
                    throw e2;
                }
            }            
        }catch(Exception ex) {
            system.debug('Email or Dob Incorrect !'+ex.getMessage());     
            throw new AuraHandledException(ex.getMessage());             
        }
        if(userData.size()>0){
            return userData[0];
        }
        else{
            return null;
        }        
    }
    
      /* Description: 
     * Method is using for verifing phone OTP to User
     */
    @AuraEnabled
    public static string verifyPhoneOTP(string otp,string phone){
        	system.debug('phone '+phone+ 'otp '+otp);
               // Verify OTP BY TWILIO Verify app
                HV_Twilio_API__mdt twilioAPI = HV_Twilio_API__mdt.getInstance('Twilio');
                String verifyAccountSid = twilioAPI.HV_Account_SID__c;
                String verifyToken = twilioAPI.HV_Token__c;
                String verifyFromPhNumber = twilioAPI.HV_FromPhoneNumber__c;
                String tonumbercountrycode = twilioAPI.HV_ToNumberCountryCode__c;
                String VerifyPhoneNumber = tonumbercountrycode+phone;
                String VerifyServiceSid = twilioAPI.HV_ServiceSid__c;
                
                HttpRequest verifyReq = new HttpRequest();
                verifyReq.setEndpoint(twilioAPI.HV_VerifyOTPEndpoint__c);
                verifyReq.setMethod('POST');
                String verifyVERSION  = '3.2.0';
                verifyReq.setHeader('X-Twilio-Client', 'salesforce-' + verifyVERSION);
                verifyReq.setHeader('User-Agent', 'twilio-salesforce/' + verifyVERSION);
                verifyReq.setHeader('Accept', 'application/json');
                verifyReq.setHeader('Accept-Charset', 'utf-8');
                verifyReq.setHeader('Authorization','Basic '+EncodingUtil.base64Encode(Blob.valueOf(verifyAccountSid+':' +verifyToken)));
                verifyReq.setBody('To='+EncodingUtil.urlEncode(VerifyPhoneNumber,'UTF-8')+'&Code='+otp);
                
                Http VerifyHttp = new Http();
                HTTPResponse verifyRes = VerifyHttp.send(verifyReq);
                System.debug(verifyRes.getBody());
                if(verifyRes.getStatusCode()==201)
                {
                    system.debug('SMS Verified Successfully');
                }
                else{
                    system.debug('error: '+verifyRes.getBody());
                }
        return verifyRes.getBody();
    }
    
      /* Description: 
     * Method is using for Sending OTP to User
     */
    
    @AuraEnabled
    public static string sendOTPtoUser(string email,string phone, string channel){ 
          String usernameConditions = 'where email=\''+email+'\'';  	
       	  String VerificationCode;        
        try {
            system.debug('username '+email+ 'Phone '+phone);
            // Send OTP BY TWILIO Verify app
            if( phone != null){
                HV_Twilio_API__mdt twilioAPI = HV_Twilio_API__mdt.getInstance('Twilio');
                String accountSid = twilioAPI.HV_Account_SID__c;
                String token = twilioAPI.HV_Token__c;
                String fromPhNumber = twilioAPI.HV_FromPhoneNumber__c;                       
                String ServiceSid = twilioAPI.HV_ServiceSid__c;
                String tonumbercountrycode = twilioAPI.HV_ToNumberCountryCode__c;
                String phNumber = tonumbercountrycode+phone; // number to whom to send otp     
                
                HttpRequest req = new HttpRequest();
                req.setEndpoint(twilioAPI.HV_SendOTPEndpoint__c);
                req.setMethod('POST');
                String VERSION  = '3.2.0';
                req.setHeader('X-Twilio-Client', 'salesforce-' + VERSION);
                req.setHeader('User-Agent', 'twilio-salesforce/' + VERSION);
                req.setHeader('Accept', 'application/json');
                req.setHeader('Accept-Charset', 'utf-8');
                req.setHeader('Authorization','Basic '+EncodingUtil.base64Encode(Blob.valueOf(accountSid+':' +token)));
                req.setBody('To='+EncodingUtil.urlEncode(phNumber,'UTF-8')+'&Channel='+channel);
                
                Http http = new Http();
                HTTPResponse res = http.send(req);
                System.debug(res.getBody());
                if(res.getStatusCode()==201)
                {
                    system.debug('SMS Sent Successfully');
                }
                else{
                    system.debug('error: '+res.getBody());
                } 
                return null;
            }
            // Send OTP Via Email
            
            if(email !=null){
				user userEmail = (user) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuesry,usernameConditions);                
                Integer rand = Math.round(Math.random()*100000);
                VerificationCode = string.valueOf(rand);
                Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
                String[] sendingTo = new String[]{userEmail.email};
                semail.setToAddresses(sendingTo);
                semail.setSubject('Password Reset OTP Verification');
                semail.setPlainTextBody('The OTP for password reset is: '+ VerificationCode);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
                system.debug('VerificationCode'+VerificationCode);
                return VerificationCode;
            }
            return VerificationCode;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    /* Description: 
     * Method is using for change the password to the User
     */
    @AuraEnabled
    public static string changePassword(string username,string password){       
        String patientConditions = 'where username=\''+username+'\'';    
        try{ 
            string startUrl= Label.HVCommunityBaseURL;
            user userPassword = (user) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuesry,patientConditions);
            system.setPassword(userPassword.id,password);
            if(userPassword.id !=null){
                string result = HVLoginUser.login(userPassword.username, password, startUrl);
                return result;
            }else{
                return  null;
            }
           }catch(exception e){
               HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
               throw new AuraHandledException(e.getMessage());
           }
    }

    /* Description: 
     * Method is used to Change Password by standard change password method
     */
    @AuraEnabled
    public static string changePasswordStandard(String newPassword, String verifyNewPassword, String oldpassword){
        try {
            return (Site.changePassword(newPassword, verifyNewPassword, oldpassword)).getUrl(); 
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void sendpasswordchangeconfirmation(String username){
        try {
            String objectname = 'Contact';
            String fieldstoQuery = 'Id,name,email,hv_role__c,(select id,email from users)';            
            String conditions = 'where id in (select contactid from user where username = \''+username+'\''+')';
            Contact contacttosendemail = (Contact) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
            if(contacttosendemail != null){
                List<String> toEmailAddress = new List<String>();
                toEmailAddress.add(contacttosendemail.users[0].email);
                EmailTemplate templateEmail = (EmailTemplate)HVQueryUtility.getSobjectRecord('Emailtemplate','id,developername,body,htmlvalue,subject','where developername = \''+'HVPasswordChange'+'\'');
                Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
                semail.setToAddresses(toEmailAddress);
                semail.setTemplateId(templateEmail.Id);
                semail.setTargetObjectId(contacttosendemail.Id);                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
            }
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,'sendpasswordchangeconfirmation',null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
    }
}