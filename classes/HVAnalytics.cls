public without sharing class HVAnalytics {

    public class LicenseWrap{
        @AuraEnabled
        public String clinicId;
        @AuraEnabled
        public String clinicName;
        @AuraEnabled
        public String license;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<LicenseWrap> getClinicLicense(){
        List<LicenseWrap> licenseWrapL = new List<LicenseWrap>();
        LicenseWrap licenseWrap = new LicenseWrap();
        
        try {
            List<HV_Clinic_and_License_Mapping__mdt> hvClinicLicenseL = HV_Clinic_and_License_Mapping__mdt.getAll().values();
            for(HV_Clinic_and_License_Mapping__mdt hvClinicLicense : hvClinicLicenseL){
                licenseWrap.clinicId = hvClinicLicense.HV_Clinic_ID__c;
                licenseWrap.clinicName = hvClinicLicense.MasterLabel;
                licenseWrap.license = String.valueOf(hvClinicLicense.HV_Number_of_License__c);
                licenseWrapL.add(licenseWrap);
                licenseWrap = new LicenseWrap();
            }
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,null,null,null,null);
        }
        return licenseWrapL;
    }
}