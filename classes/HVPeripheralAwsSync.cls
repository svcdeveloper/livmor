public virtual class HVPeripheralAwsSync {
    
    public class PeripheralDetails {
        @InvocableVariable
        public String peripheralId;
        
        @InvocableVariable
        public String boxId;
        
        @InvocableVariable
        public String boxName;
    }
    
	@InvocableMethod
    public static void callAwsForSyncBp(List<PeripheralDetails> peripheralDetailsL){
        List<HVPatientRegistrationController.PatientDetails> patientDetailsL = new List<HVPatientRegistrationController.PatientDetails>();
        HVPatientRegistrationController.PatientDetails patientDetails = new HVPatientRegistrationController.PatientDetails();
		List<Contact> patientRecL = new List<Contact>();
        
        if(peripheralDetailsL.size() > 0 && peripheralDetailsL[0].boxId != null){
            patientRecL = [SELECT Id,HV_Patient_ID__c,Account.HV_Patient_Clinic__r.Name,HV_Prescribed_By_Email__c 
                           FROM Contact WHERE HV_Box_ID__c = :peripheralDetailsL[0].boxId];
            
            if(patientRecL.size() > 0){
                for(PeripheralDetails peripheralDetails : peripheralDetailsL){
                    patientDetails.patientBoxId = peripheralDetails.boxId;
                    patientDetails.patientBoxName = peripheralDetails.boxName;
                    patientDetails.patientId = patientRecL[0].HV_Patient_ID__c;
                    patientDetails.patientClinic = patientRecL[0].Account.HV_Patient_Clinic__r.Name;
                    patientDetails.prescribingPhysician = patientRecL[0].HV_Prescribed_By_Email__c;
                    patientDetailsL.add(patientDetails);
                }
                HVPatientRegistrationController.callAwsForSync(patientDetailsL);
            }
        }
    }
}