/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 08-05-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
public without sharing class HVPhysicianDashboard {
    public class PatientDataWrap{
        @AuraEnabled public Contact patientRec = new Contact();
        @AuraEnabled public Boolean isFavorite;
    }
    
    @AuraEnabled
    public static List<PatientDataWrap> getPatientData(Id currentUserClinic, String roleName, String fieldnames){
        List<PatientDataWrap> pWrapL = new List<PatientDataWrap>();
        PatientDataWrap pWrap = new PatientDataWrap();
        String objectname = 'Contact';
        String conditions = '';
        String loggedinuserId = userinfo.getUserId();
        User loggedinuser = (User) HVQueryUtility.getSobjectRecord('user', 'id,name,contactid,contact.hv_role__c', 'where id = \''+loggedinuserId+'\'');
        String loggedinuserrole = loggedinuser.contact.hv_role__c;
        
        if(loggedinuserrole != 'Livmor Admin' && loggedinuserrole != 'Call Center Admin' && loggedinuserrole != 'Call Center User'){
            if(roleName == 'Patient'){
                conditions = ' WHERE Contact.Account.RecordType.Name = \'' + 'Person Account' + '\'' + 'AND Contact.Account.HV_Patient_Clinic__c = \'' + currentUserClinic + '\''+' And id in (select contactid from user)';
            }else{
                conditions = ' WHERE HV_Role__c = \'' + roleName + '\'' + ' AND Contact.AccountId = \'' + currentUserClinic + '\''+' And id in (select contactid from user)';
            } 
        }
        else{
           if(roleName == 'Patient'){
                conditions = ' WHERE Contact.Account.RecordType.Name = \'' + 'Person Account'+'\''+' And id in (select contactid from user)';
            }else{
                conditions = ' WHERE HV_Role__c = \'' + roleName + '\''+' And id in (select contactid from user)';
            }  
        }
        
        List<Contact> patientL = (List<Contact>) HVQueryUtility.getMultipleRecordImprative(objectname, fieldnames, conditions);
        if(patientL.size() > 0){
            String objectname2 = 'HV_Contact_Contact_Relation__c';
            String fieldnames2 = 'Contact__c,Related_Contact__c,Id ';
            //String conditions2 = ' WHERE Related_Contact__r.HV_Clinic__c = \'' + currentUserClinic + '\'';
            String conditions2 = ' WHERE Contact__c IN :patientL';

            //List<HV_Contact_Contact_Relation__c> contactContactRelationL = (List<HV_Contact_Contact_Relation__c>) HVQueryUtility.getMultipleRecordImprative(objectname2, fieldnames2, conditions2);
            List<HV_Contact_Contact_Relation__c> contactContactRelationL = (List<HV_Contact_Contact_Relation__c>) Database.query('Select '+fieldnames2+' from '+objectname2+' '+conditions2);
            Set<Id> favPatientS = new Set<Id>();
            if(contactContactRelationL != null && contactContactRelationL.size() > 0){
                for(HV_Contact_Contact_Relation__c ccr : contactContactRelationL){
                    favPatientS.add(ccr.Contact__c);
                }
            }
            
            for(Contact patient : patientL){
                pWrap.patientRec = patient;
                pWrap.isFavorite = favPatientS.contains(patient.Id) ? true : false;
                pWrapL.add(pWrap);
                pWrap = new PatientDataWrap();
            }
        }
        return pWrapL;
    }
    
    @AuraEnabled
    public static Boolean setUnsetFavourite(Id patientId, Id loginUser, String action){
        Boolean dmlSuccess = false;
        HV_Contact_Contact_Relation__c contactContactRelation;
        String objectname = 'HV_Contact_Contact_Relation__c';
        String objectname2 = 'User';
        String fieldnames2 = 'ContactId,Id ';
        String conditions2 = 'where Id = \''+loginUser+'\'';
        User loginUserRecord = (User) HVQueryUtility.getSobjectRecord(objectname2,fieldnames2,conditions2);
        
        if(action == 'set' && patientId != null){
            contactContactRelation = new HV_Contact_Contact_Relation__c();
            contactContactRelation.Contact__c = patientId;
            contactContactRelation.Related_Contact__c = loginUserRecord.ContactId;
            
            try{
                Database.insert(contactContactRelation);
                dmlSuccess = true;
            } catch(Exception e) {
                HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
                throw new AuraHandledException(e.getMessage());
            }
        } else if(action == 'unset' && patientId != null){
            String fieldnames = 'Contact__c,Related_Contact__c,Id ';
        	String conditions = ' WHERE Contact__c = \'' + patientId + '\''+'AND Related_Contact__c = \''+loginUserRecord.ContactId+'\'';
            
            List<HV_Contact_Contact_Relation__c> contactContactRelationL = (List<HV_Contact_Contact_Relation__c>) HVQueryUtility.getMultipleRecordImprative(objectname,fieldnames,conditions);
            
            try{
                Database.delete(contactContactRelationL);
                dmlSuccess = true;
            } catch(Exception e) {
                HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
                throw new AuraHandledException(e.getMessage());
            }
        }
        
        return dmlSuccess;
    }
    
    @AuraEnabled
    public static Boolean insertReviewData(Id selectedRowId, String selectedRowLastName, Id loginUser){
        Boolean dmlSuccess = false;
        String objectname = 'User';
        String fieldnames = 'ContactId,Id,Contact.Name,Contact.HV_Clinic__r.Name ';
        String conditions = 'where Id = \''+loginUser+'\'';
        User loginUserRecord = (User) HVQueryUtility.getSobjectRecord(objectname,fieldnames,conditions);
        String objectname2 = 'Contact';
        String fieldnames2 = 'Id,HV_Reviewed__c ';
        String conditions2 = 'where Id = \''+selectedRowId+'\'';
        Contact patientRec = (Contact) HVQueryUtility.getSobjectRecord(objectname2,fieldnames2,conditions2);
        
        patientRec.HV_Reviewed_By__c = loginUserRecord.ContactId;
        patientRec.HV_Reviewed_Date__c = System.now();
        patientRec.HV_Reviewed__c = true;
        
        try{
            Database.update(patientRec);
            dmlSuccess = true;
        } catch(Exception e) {
            HVLogExceptionHandler.createLog('Error',e,objectname2,null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
        
        return dmlSuccess;
    }
    
    @AuraEnabled
    public static String deactivateUser(Id contactId){
        try{
            User selectedUser = [Select Id, IsActive, Contact.Name from User where ContactId =:contactId];
            if(selectedUser.IsActive){
                selectedUser.IsActive = false;
                update selectedUser;
                return 'Updated:'+selectedUser.Contact.Name;
             }else{
                 return 'Inactive:'+selectedUser.Contact.Name;       
             }
            
        }catch(Exception exp){
            return 'Failed to update';
        }
    }
    
    @AuraEnabled
    public static boolean isOnboradingOrAdminUser(){
        User currUser = [Select Id, Contact.HV_Role__c from User where Id =:UserInfo.getUserId() LIMIT 1];
        if(currUser.Contact.HV_Role__c != null && (currUser.Contact.HV_Role__c == 'Onboarding Admin' || currUser.Contact.HV_Role__c == 'Livmor Admin')){
            return true;
        }else{
            return false;
        }
    }
}