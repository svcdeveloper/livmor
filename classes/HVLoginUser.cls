/*   Description			: HVLoginUser class is using for verify the user has entered proper username and password
* CalssName 			: HVLoginUser 
* Created by			: Mindtree Dev Team
* Created Date 		: 6th June 2021
* Last Modified By 	:
* Last Modified Date 	:
*/

global with sharing class HVLoginUser {
    
    public HVLoginUser(){
        
    }
    
    /* Description: 
* Method will validate the username and password. If username and password match then it will allow the user to community page
*/
    @AuraEnabled
    public static String login(String username, String password, String startUrl) {        
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$'; 
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(username);
        String objectname = 'user';
        String fieldstoQuesry = 'id, username,contact.HV_Patient_ID__c,contact.hv_password__c';
        String conditions = 'where contact.HV_Patient_ID__c=\''+username+'\'';      
        if(MyMatcher.matches()){                      
            try{
                system.debug('username'+username);
                system.debug('username password'+password);  
                ApexPages.PageReference lgn = Site.login(username, password, startUrl);
                system.debug('lgn  '+lgn);
                return lgn.getUrl();
            }
            catch (Exception ex) {                   
                throw new AuraHandledException(ex.getMessage());    
            }
        } else{
            try{
                user uName = (user) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuesry,conditions);        
                ApexPages.PageReference lgn = Site.login(uName.username, password, startUrl);
                return lgn.getUrl();
            }
            catch (Exception ex) {
                throw new AuraHandledException('Your login attempt has failed. Make sure the username and password are correct');              
            }
        }
    }
    
    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getUsernamePasswordEnabled();
    }
    
    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getSelfRegistrationEnabled();
    }
    
    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        if (authConfig.getSelfRegistrationEnabled()) {
            return authConfig.getSelfRegistrationUrl();
        }
        return null;
    }
    
    @AuraEnabled
    public static String getForgotPasswordUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getForgotPasswordUrl();
    }
    
    @TestVisible
    private static Auth.AuthConfiguration getAuthConfig(){
        Id networkId = Network.getNetworkId();
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'');
        return authConfig;
    }
    
    @AuraEnabled(cacheable=true)
    public static Boolean getUserTypeAndLoginInfo(Id userId){
        Boolean isPatientAndFirstLogin = false;
        String objectname = 'User';
        String fieldsToQuery = 'Contact.HV_Role__c';
        String conditions = 'where Id = \''+userId+'\'';
        String objectname2 = 'HV_UserPreference__c';
        String fieldsToQuery2 = 'IsFirstTimeLoginDone__c';
        String conditions2 = 'where HV_User__c = \''+userId+'\'';
        
        User loginUser = (User) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
        if(loginUser != null && loginUser.Contact.HV_Role__c == 'Patient'){
            isPatientAndFirstLogin = true;
        }
        
        List<HV_UserPreference__c> userPrefL = (List<HV_UserPreference__c>) HVQueryUtility.getSobjectRecords(objectname2,fieldstoQuery2,conditions2);
        if(userPrefL.size() > 0 && userPrefL[0].IsFirstTimeLoginDone__c){
            isPatientAndFirstLogin = false;
        }
        
        return isPatientAndFirstLogin;
    }
}