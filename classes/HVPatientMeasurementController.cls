public without sharing class HVPatientMeasurementController {
    public HVPatientMeasurementController() {

    }

    @AuraEnabled
    public static List<HV_PatientMeasurementScreenDevicesToShow__mdt> getDeviceTabsDetails(){
        try {
            return HV_PatientMeasurementScreenDevicesToShow__mdt.getAll().values();
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static HV_AWSIntegrationDetails__mdt getDeviceAWSIntegrationDetails(){
        try {
            return HV_AWSIntegrationDetails__mdt.getInstance('AWS');
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}