/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-13-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-02-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public without sharing class HVPatientRegistrationController { 
	public static Set<String> boxIdS = new Set<String>();
    public static final Integer maxAliasSize = 8;
    public static final Integer maxNickNameSize = 40;

    //default constructor
    public HVPatientRegistrationController() {}

    /* Description: 
     * Method to get all unique options for patient Ids
     * at the time of patient registration and 
     * as per entered firstname and lastname
     */
    @AuraEnabled
    public static List<String> generatePatientIds(String firstname, String lastname){

        String option1 = '';
        String option2 = '';
        String option3 = '';
        String objectname = '';
        String fieldstoQuery = '';            
        String conditions = '';            
        Map<String,List<Contact>> patientIdsOptions = new Map<String,List<Contact>>();  
        List<String> patientIds = new List<String>();
        List<Contact> existingContactWithPatientId = new List<Contact>();

        try {
            if(!(String.ISBLANK(firstname)) && !(String.ISBLANK(lastname))){ //set default 3 options as per firat name and last name
                option1 = '%'+firstname.substring(0,1)+lastname+'%';
                option2 = '%'+lastname.substring(0,1)+firstname+'%';
                option3 = '%'+firstname + lastname+'%';

                patientIdsOptions.put(firstname.substring(0,1)+lastname,new List<Contact>());
                patientIdsOptions.put(lastname.substring(0,1)+firstname,new List<Contact>());
                patientIdsOptions.put(firstname + lastname,new List<Contact>());

                objectname = 'Contact';
                fieldstoQuery = 'id,name,HV_Patient_ID__c';            
                conditions = 'where HV_Patient_ID__c IN (\''+String.join((Iterable<String>)patientIdsOptions.keyset(),'\',\'')+'\')'+'or HV_Patient_ID__c Like \''+ option1+ '\' or HV_Patient_ID__c Like \''+ option2+ '\' or HV_Patient_ID__c Like \''+ option3+ '\'';             
            
                existingContactWithPatientId = HVQueryUtility.getSobjectRecords(objectname,fieldstoQuery,conditions); 
            }
			
			if(existingContactWithPatientId != null && existingContactWithPatientId.size() > 0){	//if defalut options are already used as id for other user, use the increment logic to add to the option
                
                for(String pIdOption : patientIdsOptions.keyset()){
                    patientIdsOptions.get(pIdOption).addall(existingContactWithPatientId);
                }
                
                for(String pIdOption : patientIdsOptions.keyset()){   
                    boolean checkPidAdded = false;
                    Integer counter = 0;
                    for(Contact existingPatient : patientIdsOptions.get(pIdOption)){
                        if(existingPatient.HV_Patient_ID__c.containsIgnoreCase(pIdOption)){                           
                            String countString = existingPatient.HV_Patient_ID__c.substring(pIdOption.length());
                            if(String.ISBLANK(countString)){ 
                                if(counter == 0){
                                	counter = 1;
                                    checkPidAdded = true;
                                }
                            }
                            else{
                                if(countString.isNumeric()){
                                	Integer count = Integer.valueof(countString);
                                	counter = count+1;  
                                    checkPidAdded = true;
                                }
                            }
                        }
                    }
                    
                    if(!checkPidAdded){
                        patientIds.add(pIdOption);
                    }
                    else{
                        patientIds.add(pIdOption+'0'+counter);
                    }
                }
            }
            else{
                patientIds.addall(patientIdsOptions.keyset());
            }
            return patientIds;
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
    }

    /* Description: 
     * Method to get all clinics and 
     * there fields mapping for patient registration
     * Patient Registration dynamic form as per Clinic
     */
    @AuraEnabled
    public static HV_ClinicFieldsMapping__mdt getClinicFieldsMapping(String clinicId){
        String objectname = 'HV_ClinicFieldsMapping__mdt';
        String fieldstoQuery = 'MasterLabel,DeveloperName,HV_Clinic_Id__c,HV_RequiredFields__c,HV_GeneralSectionFields__c,HV_ContactInformationSectionFields__c,HV_AccountInformationSectionFields__c,HV_Department_Values__c';            
        String conditions = 'where HV_Clinic_Id__c = \''+clinicId+'\'';
        try {
            return (HV_ClinicFieldsMapping__mdt) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,objectname,null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
    }

    /* Description: 
     * Generic Method to create user 
     * it may be Patient/Physician/Others
     * after user creation add permission set to user as per role
     */
    @AuraEnabled
    public static void createUser(String conOrAccRecordId, String notesForUser) {        
        Schema.SObjectType sObjectAccount = Schema.Account.getSObjectType(); //Account sobject type
        Schema.SObjectType sObjectContact = Schema.Contact.getSObjectType(); //Contact sobject typr
        Contact contactRecord;
        Account accountRecord;
        CreateUserDefaultFields__mdt defaultUserFields = new CreateUserDefaultFields__mdt(); //custom metadata that stores user default fields like profile/license,timezone details
        List<String> toEmailAddress = new List<String>(); //list of to address to send email notification
        String alias = '';
        String nickname = '';
        String passwordUser = '';
        String userRole = '';
        String objectname = '';
        String fieldstoQuery = '';            
        String conditions = ''; 
        String patientContactId = '';     
        String accOwnerId =''; //store account owner Id to share it with apex sharing  
        String conOwnerId = '';
        User userToCreate = new User(); //new user to create   
        Id whatId;
        String templateDevName = '';
        try{   
            if(!String.ISBLANK(conOrAccRecordId)){ //if incoming parameter is not empty
                if(((Id)conOrAccRecordId).getSObjectType() == sObjectContact){ //check if coming sobject is of type contact
                    defaultUserFields = CreateUserDefaultFields__mdt.getInstance('Others'); 
                    conditions = 'where id = \''+conOrAccRecordId+'\'';
                    contactRecord = (Contact) HVQueryUtility.getSobjectRecord(defaultUserFields.HV_QueryObjectName__c,defaultUserFields.HV_FieldsToQueryFromObject__c,conditions);
                    contactRecord.ownerId = defaultUserFields.HV_Admin_User_ID__c;
                    conOwnerId = contactRecord.ownerId;
                    update contactRecord;
                }
                if(((Id)conOrAccRecordId).getSObjectType() == sObjectAccount){ //check if coming sobject is of type account (personaccount)
                    defaultUserFields = CreateUserDefaultFields__mdt.getInstance('Patient');
                    conditions = 'where id = \''+conOrAccRecordId+'\'';
                    accountRecord = (Account) HVQueryUtility.getSobjectRecord(defaultUserFields.HV_QueryObjectName__c,defaultUserFields.HV_FieldsToQueryFromObject__c,conditions);
                    accOwnerId = accountRecord.ownerId;
                    accountRecord.ownerId = defaultUserFields.HV_Admin_User_ID__c;
                    update accountRecord;
                }

                //If person account record - create Patient User
                if(accountRecord != null){
                    objectname = 'UserLicense';
                    fieldstoQuery = 'Id,MasterLabel,Name,Status,TotalLicenses,UsedLicenses';            
                    conditions = 'where Name = \''+ defaultUserFields.HV_User_License_Name__c+'\'';
                    UserLicense patientLicense = (UserLicense) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
                    //check if Patient uSer Licenses are availabe in system
                    if((patientLicense.TotalLicenses - patientLicense.UsedLicenses) <=0){ //if not send error message to system admin
                        //send email to system admins
                        objectname = 'HV_SystemAdminsEmailAddress__mdt';
                        fieldstoQuery = 'MasterLabel,DeveloperName,HV_Email_Address__c';            
                        conditions = 'where MasterLabel = \'AdminNotifications\'';
                        HV_SystemAdminsEmailAddress__mdt adminemails = (HV_SystemAdminsEmailAddress__mdt) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
                        toEmailAddress = adminemails.HV_Email_Address__c.split(',');
                        sendEmailNotification(toEmailAddress,null,null,null,'LivMor Organization - Patient License Exhausted','Hi Team, <br/><br/> Livmor Organization total Patients User License Exhausted. Cannot create new Patient for any of the clinic');
                        AuraHandledException e=  new AuraHandledException('Cannot create Patient User. Contact System Admin');
                        e.setMessage('Cannot create Patient User. Contact System Admin!');
                        throw e;
                    }
                    else{ //if license available create user using account records fields
                        alias = accountRecord.firstname + accountRecord.lastname;
                        if(alias.length() > maxAliasSize){
                            alias = alias.substring(0, maxAliasSize);
                        }
                        nickname = alias + System.now();
                        if(nickname.length() > maxNickNameSize){
                            nickname = nickname.substring(0, maxNickNameSize);
                        }
                        userToCreate.lastname = accountRecord.lastname;
                        userToCreate.firstname = accountRecord.firstname;
                        userToCreate.Email = accountRecord.PersonEmail;
                        userToCreate.Phone = accountRecord.Phone;
                        userToCreate.Username = accountRecord.PersonEmail;
                        userToCreate.Alias = alias;
                        userToCreate.CommunityNickname = nickname; 
                        userToCreate.LocaleSidKey = defaultUserFields.HV_LocaleSidKey__c;                    
                        userToCreate.ContactId = accountRecord.PersonContactId;
                        userToCreate.ProfileId = defaultUserFields.HV_ProfileId__c;
                        userToCreate.IsActive = true; 
                        userToCreate.TimeZoneSidKey = defaultUserFields.HV_TimeZoneSidKey__c;
                        userToCreate.LanguageLocaleKey = defaultUserFields.HV_LanguageLocaleKey__c;
                        userToCreate.EmailEncodingKey = defaultUserFields.HV_EmailEncodingKey__c;                 
                        passwordUser = accountRecord.HV_Password__pc;
                        userRole = accountRecord.HV_Role__pc;
                    }
                    whatId = accountRecord.Id;
                    templateDevName = 'HVRegistrationSuccessPatient';
                } // If contact record - create Physician/Other User
                else if(contactRecord != null){
                    objectname = 'UserLicense';
                    fieldstoQuery = 'Id,MasterLabel,Name,Status,TotalLicenses,UsedLicenses';            
                    conditions = 'where Name = \''+ defaultUserFields.HV_User_License_Name__c+'\'';
                    UserLicense otherUserLicense = (UserLicense)HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
                    //check if Physician/Other uSer Licenses are availabe in system
                    if((otherUserLicense.TotalLicenses - otherUserLicense.UsedLicenses) <=0){ //if not send error message to system admin
                        //send email to system admins
                        objectname = 'HV_SystemAdminsEmailAddress__mdt';
                        fieldstoQuery = 'MasterLabel,DeveloperName,HV_Email_Address__c';            
                        conditions = 'where MasterLabel = \'AdminNotifications\'';
                        HV_SystemAdminsEmailAddress__mdt adminemails = (HV_SystemAdminsEmailAddress__mdt) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
                        toEmailAddress = adminemails.HV_Email_Address__c.split(',');
                        sendEmailNotification(toEmailAddress,null,null,null,'LivMor Organization -Other License Exhausted','Hi Team, <br/><br/> Livmor Organization total Other Users License Exhausted. Cannot create new User for any of the clinic');
                        AuraHandledException e=  new AuraHandledException('Cannot create User. Contact System Admin');
                        e.setMessage('Cannot create User. Contact System Admin!');
                        throw e;
                    }
                    else{ //if license available create user using contact records fields
                        alias = contactRecord.firstname + contactRecord.lastname;
                        if(alias.length() > maxAliasSize){
                            alias = alias.substring(0, maxAliasSize);
                        }
                        nickname = alias + System.now();
                        if(nickname.length() > maxNickNameSize){
                            nickname = nickname.substring(0, maxNickNameSize);
                        }
                        userToCreate.lastname = contactRecord.lastname;
                        userToCreate.firstname = contactRecord.firstname;
                        userToCreate.Email = contactRecord.Email;
                        userToCreate.Phone = contactRecord.Phone;
                        userToCreate.Username = contactRecord.Email;
                        userToCreate.Alias = alias;
                        userToCreate.CommunityNickname = nickname; 
                        userToCreate.LocaleSidKey = defaultUserFields.HV_LocaleSidKey__c;                    
                        userToCreate.ContactId = contactRecord.Id;
                        userToCreate.ProfileId = defaultUserFields.HV_ProfileId__c;
                        userToCreate.IsActive = true; 
                        userToCreate.TimeZoneSidKey = defaultUserFields.HV_TimeZoneSidKey__c;
                        userToCreate.LanguageLocaleKey = defaultUserFields.HV_LanguageLocaleKey__c;
                        userToCreate.EmailEncodingKey = defaultUserFields.HV_EmailEncodingKey__c;                 
                        passwordUser = contactRecord.HV_Password__c;
                        userRole = contactRecord.HV_Role__c;
                    }
                    whatId = contactRecord.AccountId;
                    templateDevName = 'HVRegistrationSuccessOtherUsers';
                } 
            }
            if(userToCreate != null){
                insert userToCreate;              
                assigenUserPermissionSet(userToCreate.id,userRole,passwordUser); //assign respective permission set to user as per role
                if(accountRecord != null){
                    HVQueryUtility.manualShareRead(accountRecord.Id,'AccountId','AccountAccessLevel',accOwnerId,'AccountShare','Edit');
                }
                toEmailAddress.add(userToCreate.Email); //send email notification to user - user account created successfully
                sendEmailNotification(toEmailAddress,templateDevName,whatId,userToCreate.ContactId,null,null);
                if(accountRecord != null){ //add notes for patient if any added by physician/other user
                    if(notesForUser != null){
                        addNotesToUserForPatient(accountRecord,notesForUser);
                    }
                }
            }
        }catch(Exception e){
            if(accountRecord != null){
                if(!String.ISBLANK(accountRecord.Id)){ //if any error occurred while creating user, delete the respective contact/personaccount so that we have contact and user in sync
                    deleteRecord(accountRecord.Id, 'Account');
                }
            }
            
            if(contactRecord != null){
                if(!String.ISBLANK(contactRecord.Id)){
                    deleteRecord(contactRecord.Id, 'Contact');
                }
            }
            
            HVLogExceptionHandler.createLog('Error',e,'User',null,null,null);
            if(!test.isRunningTest()){
            	throw new AuraHandledException(e.getMessage());
            }
        }
    }

    @future
    public static void deleteRecord(String recordId, String objectName){
        try{
            delete HVQueryUtility.getSobjectRecord(objectName,' Id ',' Where Id =\''+recordId+'\'');
        }catch(Exception e){
            HVLogExceptionHandler.createLog('Error',e,'User',null,null,null);
        }
        
    }
    /* Description: 
     * Generic Method to assign permission sets to user
     * as per user role 
     * call this method in future to avoid mixed DML error
     */
    @future
    public static void assigenUserPermissionSet(Id userId, String userRole,String passwordToSet){
        if(userRole == 'Patient'){
            system.setPassword(userId, passwordToSet);
        }
        List<PermissionSetAssignment> permissionSetAssignList = new List<PermissionSetAssignment>();        
        Map<String,List<String>> mapRoleAndPermissionSetIds = new Map<String,List<String>>();
        List<HV_RolePermissionSetsMapping__mdt> listOfRolePermission = HV_RolePermissionSetsMapping__mdt.getAll().values(); //custom metadata that stores roles and respective permission set mapping details

        try{
            for(HV_RolePermissionSetsMapping__mdt metadataRec : listOfRolePermission){ //create map of role as key and permission sets to be assigned as list of values 
                if(!mapRoleAndPermissionSetIds.containsKey(metadataRec.MasterLabel)){
                    List<String> permissionsList = metadataRec.HV_PermissionSetIds__c.split(',');
                    mapRoleAndPermissionSetIds.put(metadataRec.MasterLabel,permissionsList);
                }
            }
            
            for(String permissionSetId : mapRoleAndPermissionSetIds.get(userRole)){ //create permissionset assignmnet for the user     
                PermissionSetAssignment permissionSetAssign = new PermissionSetAssignment(PermissionSetId = permissionSetId, AssigneeId = userId);
                permissionSetAssignList.add(permissionSetAssign);
            } 
            
            if(!permissionSetAssignList.isEmpty()){
                insert permissionSetAssignList; //assign permission set to user
            }
        }catch(Exception e){
            HVLogExceptionHandler.createLog('Error',e,'PermissionSetAssignment',null,null,null);
            throw new AuraHandledException(e.getMessage());
        }
    }

    /* Description: 
     * Generic Method to send email notification
     * pass send to email address and subject to this method
     * method will send emails respectively
     */
    @AuraEnabled
    public static void sendEmailNotification(List<String> toaddress, String template, Id whatId, Id targetObjectId, String emailsubject, String emailbody){
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
        Id templateId;
        try{
            semail.setToAddresses(toaddress);
            if(!String.isBlank(template)){
                String objects = 'EmailTemplate';
                String fields = 'Id,DeveloperName';
                String condition = 'where DeveloperName = \'' + template + '\'';
                templateId = ((EmailTemplate)HVQueryUtility.getSobjectRecord(objects,fields,condition)).Id;
                semail.setTemplateId(templateId);
                semail.setWhatId(whatId);
                semail.setTargetObjectId(targetObjectId);
            }
            else{
                semail.setSubject(emailsubject);
                semail.setHtmlBody(emailbody);
            }
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {semail});
        }catch(Exception e){
            HVLogExceptionHandler.createLog('Error',e,'sendEmailNotification',null,null,null);
            throw new AuraHandledException(e.getMessage());
        }        
    }    

    /* Description: 
    * Method to add notes for the patient
    * added by physician/other users at the time of registration
    */
    public static void addNotesToUserForPatient(Account patientAcc, String notes){
        if(patientAcc != null && !String.ISBLANK(notes)){
            HV_Notes__c newnote = new HV_Notes__c();
            newnote.HV_Comment__c = notes;
            newnote.HV_Contact__c = patientAcc.PersonContactId;                
            newnote.Name = patientAcc.firstname + ' '+ patientAcc.lastname + ' Note';
            try{
                insert newnote;
            }catch(Exception e){
                HVLogExceptionHandler.createLog('Error',e,'HV_Notes__c',null,null,null);
            }
        }
    }

    /* Description: 
     * If patient does not have box id/halo system
     * update the address in system and
     * send an email to Group O team with
     * patient name, Id, Shipping address
     */
    @AuraEnabled
    public static void bovIdNotAvailableNotifyTeam(Account patientRec){
        try{
            //update patientRec;
            String objectname = 'HV_SystemAdminsEmailAddress__mdt';
            String fieldstoQuery = 'MasterLabel,DeveloperName,HV_Email_Address__c';            
            String conditions = 'where MasterLabel = \'AdminNotifications\'';
            HV_SystemAdminsEmailAddress__mdt adminemails = (HV_SystemAdminsEmailAddress__mdt) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
            List<String> toEmailAddress = adminemails.HV_Email_Address__c.split(',');            
            String shipAddress = 'Street: ' + patientRec.ShippingStreet + '\nCity: ' + patientRec.ShippingCity + '\nCountry:' + patientRec.ShippingCountry + '\nPostalCode: ' + patientRec.ShippingPostalCode;
            String emailbodysend = 'Hi Team, \nPatient having Patient Id: '+patientRec.HV_Patient_ID__pc+' does not have halo system.\nFind below Shipping address details of Patient: \n'+shipAddress;
            //this method is no more used - as box id is always mandatory for patient registration
            //sendEmailNotification(toEmailAddress,'User Does Not Have Halo System',emailbodysend);
        }catch(Exception e){
            HVLogExceptionHandler.createLog('Error',e,'Account',null,null,null);
        }

    }

    /* Description: 
     * Use this method to update patient record
     * in system.
     * Update existing patient details with new 
     * Box Id and shipping address
     */
    @AuraEnabled
    public static void updateExistingPatient(Account accountToUpdate){        
        try {            
            update accountToUpdate;            
            updateUser(accountToUpdate.id);
            
        } catch (Exception e) {
            HVLogExceptionHandler.createLog('Error',e,'Account',null,null,null);
            if(!test.isRunningTest()){
            	throw new AuraHandledException(e.getMessage());
            }
        }
    }

    @future
    public static void updateUser(id idsAcc){
          list<user> userls = new list<user>(); 
          try{         
            list<user> userList = (List<User>) HVQueryUtility.getSobjectRecords('User','id,contactid,isactive','where contact.accountid=\''+idsAcc+'\'');
            for(user u:userList){
                u.isactive=true;
                userls.add(u);
            }
            update userls;  
        }
        catch(Exception e) {
            HVLogExceptionHandler.createLog('Error',e,'Account',null,null,null);
        }
    }
    /* Description: 
     * Use this method to send notification to livmor admin
     * when, an existing patient is newly created for 
     * different clinic.
     * Notigy admin user to transfer existing user record measumerment 
     * details to newly created record
     */
    @AuraEnabled
    public static void sendExistingUserNotiftn(Account patientRec, String oldclinic, String newclinic){
        try{
            String objectname = 'HV_SystemAdminsEmailAddress__mdt';
            String fieldstoQuery = 'MasterLabel,DeveloperName,HV_Email_Address__c';            
            String conditions = 'where MasterLabel = \'AdminNotifications\'';
            HV_SystemAdminsEmailAddress__mdt adminemails = (HV_SystemAdminsEmailAddress__mdt) HVQueryUtility.getSobjectRecord(objectname,fieldstoQuery,conditions);
            List<String> toEmailAddress = adminemails.HV_Email_Address__c.split(',');            
            String template = 'HVPatientTransfer';
            String objects = 'EmailTemplate';
            String fields = 'id,developername,body,htmlvalue,subject';
            String condition = 'where developername = \'' + template + '\'';
            EmailTemplate templ = (EmailTemplate)HVQueryUtility.getSobjectRecord(objects,fields,condition);
            String htmlbody= templ.htmlvalue;
            htmlbody= htmlbody.replace('{!Contact.HV_Prescribed_By__c}', patientRec.HV_Prescribed_By__pc);
            htmlbody= htmlbody.replace('{!Contact.HV_Patient_ID__c}', patientRec.HV_Patient_ID__pc);
            htmlbody= htmlbody.replace(' {!Contact.HV_Clinic__c}', oldclinic);
            htmlbody= htmlbody.replace('{!Account.HV_Patient_Clinic__c}', newclinic);          
            sendEmailNotification(toEmailAddress,null,null,null,templ.subject,htmlbody);
        }catch(Exception e){
            HVLogExceptionHandler.createLog('Error',e,'Account',null,null,null);
        }
    }

    /*Description: 
     *Method to activate the existing user 
     *if same user already inactive in 
     *same clinic
     */
    @AuraEnabled
    public static void updateOtherUsers(String contactRecId){
        try {
            List<User> usertoactivate = new List<User>();
            User existinguser = (User) HVQueryUtility.getSobjectRecord('User','id,contactid,isactive','where contactid = \''+contactRecId+'\'');
            if(existinguser != null){
                existinguser.isActive = true;
                usertoactivate.add(existinguser);
            }
            if(usertoactivate.size() > 0){
                update usertoactivate;
                system.resetPassword(existinguser.Id, true);
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    public class PatientDetails {
        @InvocableVariable
        public String patientId;
        
        @InvocableVariable
        public String patientBoxId;
        
        @InvocableVariable
        public String patientBoxName;
        
        @InvocableVariable
        public String patientClinic;
        
        @InvocableVariable
        public String prescribingPhysician;
    }

    @InvocableMethod
    public static void callAwsForSync(List<PatientDetails> patientDetailsL){
        Map<String,String> dataM = new Map<String,String>();
        String boxIdString = '(';
        for(PatientDetails patientDetails : patientDetailsL){
            boxIdS.add((String)patientDetails.patientBoxId);
        }
        String fieldsToQuery1 = 'Id,HV_Box__c,HV_User_Device_Status__c';
        String fieldsToQuery2 = 'Id,HV_Box__c,HV_User_Device_Status__c,HV_BDA__c ';
        String conditions = 'where HV_Box__c IN :boxIdS';
        
        Map<Id,List<HV_BP__c>> bpM = (Map<Id,List<HV_BP__c>>) queryObject('HV_BP__c',fieldsToQuery2,conditions);
        Map<Id,List<HV_WS__c>> wsM = (Map<Id,List<HV_WS__c>>) queryObject('HV_WS__c',fieldsToQuery2,conditions);
        Map<Id,List<HV_Watch__c>> watchM = (Map<Id,List<HV_Watch__c>>) queryObject('HV_Watch__c',fieldsToQuery1 + ',HV_Watch_ID__c ',conditions);
        Map<Id,List<HV_Tablet__c>> tabM = (Map<Id,List<HV_Tablet__c>>) queryObject('HV_Tablet__c',fieldsToQuery1 + ',HV_Android_ID__c ',conditions);
        Map<Id,List<HV_Pulse_Oximeter__c>> pulseM = (Map<Id,List<HV_Pulse_Oximeter__c>>) queryObject('HV_Pulse_Oximeter__c',fieldsToQuery2,conditions);
        Map<Id,List<HV_Glucometer__c>> glucoM = (Map<Id,List<HV_Glucometer__c>>) queryObject('HV_Glucometer__c',fieldsToQuery2,conditions);
        
        String body = '';
        String param1, param2;
        for(PatientDetails patientDetails : patientDetailsL){
            dataM.put('patientid', patientDetails.patientId);
            dataM.put('boxid', patientDetails.patientBoxName);
            dataM.put('customer', patientDetails.patientClinic);
            dataM.put('prescribed_physician', patientDetails.prescribingPhysician);
            
            param1 = ''; param2 = '';
            if(bpM.size() > 0){
                for(HV_BP__c peripheralRec : bpM.get(patientDetails.patientBoxId)){
                    param1 += peripheralRec.HV_BDA__c + ',';
                    param2 += peripheralRec.HV_User_Device_Status__c + ',';
                }
            }
            dataM.put('bp_id', param1.removeEnd(','));
            dataM.put('bp_status', param2.removeEnd(','));
            
            param1 = ''; param2 = '';
            if(wsM.size() > 0){
                for(HV_WS__c peripheralRec : wsM.get(patientDetails.patientBoxId)){
                    param1 += peripheralRec.HV_BDA__c + ',';
                    param2 += peripheralRec.HV_User_Device_Status__c + ',';
                }
            }
            dataM.put('ws_id', param1.removeEnd(','));
            dataM.put('ws_status', param2.removeEnd(','));
            
            param1 = ''; param2 = '';
            if(watchM.size() > 0){
                for(HV_Watch__c peripheralRec : watchM.get(patientDetails.patientBoxId)){
                    param1 += peripheralRec.HV_Watch_ID__c + ',';
                    param2 += peripheralRec.HV_User_Device_Status__c + ',';
                }
            }
            dataM.put('watch_id', param1.removeEnd(','));
            dataM.put('watch_status', param2.removeEnd(','));
            
            param1 = ''; param2 = '';
            if(tabM.size() > 0){
                for(HV_Tablet__c peripheralRec : tabM.get(patientDetails.patientBoxId)){
                    param1 += peripheralRec.HV_Android_ID__c + ',';
                    param2 += peripheralRec.HV_User_Device_Status__c + ',';
                }
            }
            dataM.put('tablet_id', param1.removeEnd(','));
            dataM.put('tablet_status', param2.removeEnd(','));
            
            param1 = ''; param2 = '';
            if(pulseM.size() > 0){
                for(HV_Pulse_Oximeter__c peripheralRec : pulseM.get(patientDetails.patientBoxId)){
                    param1 += peripheralRec.HV_BDA__c + ',';
                    param2 += peripheralRec.HV_User_Device_Status__c + ',';
                }
            }
            dataM.put('pulse_id', param1.removeEnd(','));
            dataM.put('pulse_status', param2.removeEnd(','));
            
            param1 = ''; param2 = '';
            if(glucoM.size() > 0){
                for(HV_Glucometer__c peripheralRec : glucoM.get(patientDetails.patientBoxId)){
                    param1 += peripheralRec.HV_BDA__c + ',';
                    param2 += peripheralRec.HV_User_Device_Status__c + ',';
                }
            }
            dataM.put('glucometer_id', param1.removeEnd(','));
            dataM.put('glucometer_status', param2.removeEnd(','));
            
            body = JSON.serializePretty(dataM);
            System.debug('Patient registration AWS callout body: ' + body);
            HV_AwsCallout awsCallout = new HV_AwsCallout('callout:LivmorWatchSessions/userdetails','POST',body);
            System.enqueueJob(awsCallout);
        }
    }
    
    public static Map<Id, List<sObject>> queryObject(String objectname, String fieldsToQuery, String conditions){
        Map<Id, List<sObject>> recM = new Map<Id, List<sObject>>();
		List<sObject> recL = new List<sObject>();
        //recL = HVQueryUtility.getSobjectRecords(objectname,fieldstoQuery,conditions);
        recL = Database.query('Select '+fieldsToQuery+' from '+objectname+' '+conditions);
        
        if(recL.size() > 0){
            recM.put((Id)recL[0].get('HV_Box__c'), recL);
        }
        return recM;
    }
}